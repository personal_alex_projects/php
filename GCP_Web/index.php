<!DOCTYPE html>
<html>
<head>
	<title>PRIMAL</title>
	<link rel="icon" href="http://rust.glaucus.net/img/gcn_icon.ico" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<?php include('_resources/_nav.php'); ?>
	<br>
	<center>
		<iframe width="560" height="315" src="https://www.youtube.com/embed/puaZr0a0rDg" frameborder="0"></iframe>
		<div class="division-bottom">
			<hr>
			<p class="copyrights">Copyright &copy 2016 GlaucusPrimal, all rights reserved. <small>Powered by Glaucus Network</small></p>
		</div>
	</center>
</body>
</html>