<!DOCTYPE html>
<html>
<head>
	<title>PRIMAL</title>
	<link rel="icon" href="img/gcn_icon.ico" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="../css/main.css">
</head>
<body>
	<?php include('../_resources/_nav.php'); ?>
	<br>
	<center>
		<h2 class="maintenance-text">Page in maintenance!</h2>
		<div class="division-bottom">
			<hr>
			<p class="copyrights">Copyright &copy 2016 GlaucusPrimal, all rights reserved. <small>Powered by Glaucus Network</small></p>
		</div>
	</center>
</body>
</html>