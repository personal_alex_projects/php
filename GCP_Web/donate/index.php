<!DOCTYPE html>
<html>
<head>
	<title>PRIMAL</title>
	<link rel="icon" href="http://rust.glaucus.net/img/gcn_icon.ico" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="../css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	<?php include('../_resources/_nav.php'); ?>
	<br>
	<center>
		<div class="wrapper">
			<table width="100%" border="0">
				<tr>
					<th id="donator">DONATOR</th>
					<td></td>
					<th id="premium">PREMIUM</th>
				</tr>
				<tr>
					<td id="vip-font">Soon...</td>
					<td id="medium"></td>
					<td id="vip-font">Soon...</td>
				</tr>
			</table>
		</div>
		<div class="division-bottom">
			<hr>
			<p class="copyrights">Copyright &copy 2016 GlaucusPrimal, all rights reserved. <small>Powered by Glaucus Network</small></p>
		</div>
	</center>
</body>
</html>