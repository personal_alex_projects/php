<!DOCTYPE html>
<html lang="en">
<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once ('/var/www/html/dynamic/recaptchalib.php');
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
 $webmetatitle = "Minegrech Login";
 $webmetadesc  = "Minegrech Inicio, descubre el mundo de diversion que ofrecemos para nuestros jugadores de todo el mundo!";
 $webtitle     = "Minegrech - Login";
 $extrahead    = '<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/index.register.css">';
 $extrahead   .= '<script type="text/javascript"> $(document).ready(function(){ $.c0okieBar({ }); }); </script>';
 $extrahead   .= '<script type="text/javascript" src="https://cdn.glaucus.net/cookies/jq.c0okie.js"></script>';
 $extrahead   .= '<link rel="stylesheet" type="text/css" href="https://cdn.glaucus.net/cookies/jq.c0okie.css"/>';

include  '/var/www/MineGrech/dynamic/content/extrahead.php';

include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

if($gnsession->isLoggedIn()){
	header("Location: https://www.glaucus.net/profile");
	return;
}

include('/var/www/html/dynamic/language/setupLanguage.php');

?>
<body>
	<style>
		.margin{
			margin-top: 2%;
			margin-bottom: 2%;
		}
		.border{
			border: 1px solid #F5F5F5;
		}
	</style>
	<div class="navbar-position">
		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
	</div>
	<div class="container">
		<div class="row">
			<div class="body-site" style="margin-top: 2em;">
				<div class="col-md-8" style="margin-top: 0.6em; margin-bottom: 0.5em;">
					<center>
						<div class="panel-principal">
							<div class="panel-login">
								<div class="mensaje-error">
									<?php
									if (isset($_COOKIE['LoginError'])) {
										switch ($_COOKIE['LoginError']) {
											case 'validation_fields':
											echo '<div class="error">'.$lang['validation_fields'].'</div>';
											break;
											case 'validation_email':
											echo '<div class="error">'.$lang['validation_email'].'</div>';
											break;
											case 'validation_password':
											echo '<div class="error">'.$lang['validation_password'].'</div>';
											break;
											case 'validation_minpassword':
											echo '<div class="error">'.$lang['validation_minpassword'].'</div>';
											break;
											case 'validation_cpassword':
											echo '<div class="error">'.$lang['validation_cpassword'].'</div>';
											break;
											case 'validation_noemail':
											echo '<div class="error">'.$lang['validation_noemail'].'</div>';
											break;
											case 'validation_alreadyemail':
											echo '<div class="error">'.$lang['validation_alreadyemail'].'</div>';
											break;
											case 'validation_captchaFailed':
											echo '<div class="error">'.$lang['validation_captchaFailed'].'</div>';
											break;
											default:
											echo '<div class="error">'.$lang['validation_default'].'</div>';
											break;
										}
									}
									?>
								</div>
								<form action="https://www.minegrech.com/login/validate" method="post">
									<div class="form-style-6">
										<h1 style="margin-left: -12em;margin-right: -12em;"><?php echo $lang['LOGIN_TITLE']; ?></h1>
										<div class="texto-email">
											<span><?php echo $lang['LOGIN_EMAIL']; ?></span><br>
										</div>
										<input type="text" name="email" placeholder="<?php echo $lang['LOGIN_EMAILPLACEHOLDER']; ?>">
										<div class="texto-pass">
											<span><?php echo $lang['LOGIN_PASSWORD']; ?></span><br>
										</div>
										<input type="password" name="password" placeholder="<?php echo $lang['LOGIN_PASSPLACEHOLDER']; ?>">
										<div class="re-captcha">
											<div class="g-recaptcha" data-sitekey="6LfDKgwUAAAAAFGPdoBNz3LF-nhTYGbE7NDK0bDR"></div>
										</div>
										<br>
										<input type="submit" name="submit" value="<?php echo $lang['LOGIN_STARTSESSION']; ?>">
										<!-- <input type="submit" name="register" value="%LOGIN_LOGIN%"> !-->
										<?php echo "<p style='font-size: 10px;'>".$lang['PAGE_DEVVER_TITLE']."</p>"; ?>
									</div>
								</form>
								
								<script src='https://www.google.com/recaptcha/api.js'></script>
							</div>
						</div>
					</div>
				</center>
				<div class="col-md-4 border" style="margin-bottom: 5em;">
					<?php include '/var/www/MineGrech/dynamic/content/right_nav.php'; ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>