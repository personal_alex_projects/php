<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once ('/var/www/html/dynamic/recaptchalib.php');
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

if($gnsession->isLoggedIn()){
	header("Location: https://www.glaucus.net/profile");
	return;
}

include('/var/www/html/dynamic/language/setupLanguage.php');

/* Cookie Name */
$cookie_name = "ErrorLogin";


if ($gnsession->isLoggedIn() != false) {
	header("Refresh: 0, url=https://www.glaucus.net/profile/");
	return;
}

/* CAPTCHA */
// your secret key
$secret = "6LfDKgwUAAAAAHt_5jn_h9LGn58z-K3N4VHf5IZh";

// empty response
$response = null;

// check secret key
$reCaptcha = new ReCaptcha($secret);
if ($_POST["g-recaptcha-response"]) {
	$response = $reCaptcha->verifyResponse(
		$_SERVER["REMOTE_ADDR"],
		$_POST["g-recaptcha-response"]
		);
}
/*****/

if (!isset($_POST['password']) && !isset($_POST['email'])) {
	$cookie_error = "validation_fields";
	setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
	header("Refresh: 1, url=https://www.minegrech.com/login");
}else{
	if ($response != null && $response->success) {
		$llamado = $gnsession->login($_POST['email'], $_POST['password']);
		if ($llamado == 400 || $llamado == 401) {
			$cookie_error = "validation_account";
			setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
			header("Refresh: 1, url=https://www.minegrech.com/login");
		}elseif ($llamado == 402) {
			$cookie_error = "validation_password";
			setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
			header("Refresh: 1, url=https://www.minegrech.com/login");
		}elseif ($llamado == 200) {
			header("Refresh: 1, url=https://www.minegrech.com/edit/profile");
		}
	}else{
		$cookie_error = "validation_captchaFailed";
		setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
		header("Refresh: 1, url=https://www.minegrech.com/login");
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - Joining...</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<link rel="stylesheet" href="https://glaucus.net/assets/styles/index.reg.css">
</head>
<body>
<center>
	<div class="mensaje-error">
		<img src="https://cdn.glaucus.net/img/logo2.png" style="width: 400px; margin-right:15px;">	
	</div>
	<img src="https://cdn.glaucus.net/img/Loading1.gif" style="height: 200px;">
</center>
<style>
	center {
		top: calc(50% - 250px);
		position: absolute;
		left: calc(50% - 207px);
	}
	body{
		background-color: #F5F5F5;
	}
	.ajust{
		margin-top:10em;
	}
</style>
</body>
</html>