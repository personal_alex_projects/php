<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once ('/var/www/html/dynamic/recaptchalib.php');

////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

if($gnsession->isLoggedIn()){
	header("Location: https://www.minegrech.com/profile");
	return;
}

include('/var/www/html/dynamic/language/setupLanguage.php');

/* COOKIE NAME */
$cookie_name = "LoginError";

// your secret key
$secret = "6LfDKgwUAAAAAHt_5jn_h9LGn58z-K3N4VHf5IZh";
 
// empty response
$response = null;
 
// check secret key
$reCaptcha = new ReCaptcha($secret);
if ($_POST["g-recaptcha-response"]) {
    $response = $reCaptcha->verifyResponse(
        $_SERVER["REMOTE_ADDR"],
        $_POST["g-recaptcha-response"]
    );
}


	
if ($_POST['email'] && $_POST['password'] && $_POST['cpassword'] && $_POST['username']) {
	$email = $_POST['email'];
	$password = $_POST['password'];
	$cpassword = $_POST['cpassword'];
	$username = $_POST['username'];
	if ($response != null && $response->success) {
		if(!$mgclass->isNameAvailable($username)){
			$cookie_error = "validation_username";
			setcookie($cookie_name, $cookie_error, time()+7, '/' , '.minegrech.com', 1);
			header("Refresh: 1, url=../");
		}else{
			$llamado = $gnsession->registerAccount($email, $password, $cpassword);
			if ($llamado == 1) {
				$cookie_error = "validation_fields";
				setcookie($cookie_name, $cookie_error, time()+7, '/' , '.minegrech.com', 1);
				header("Refresh: 1, url=../");
			}elseif ($llamado == 2) {
				$cookie_error = "validation_minpassword";
				setcookie($cookie_name, $cookie_error, time()+7, '/' , '.minegrech.com', 1);
				header("Refresh: 1, url=../");
			}elseif ($llamado == 3) {
				$cookie_error = "validation_cpassword";
				setcookie($cookie_name, $cookie_error, time()+7, '/' , '.minegrech.com', 1);
				header("Refresh: 1, url=../");
			}elseif ($llamado == 4) {
				$cookie_error = "validation_alreadyemail";
				setcookie($cookie_name, $cookie_error, time()+7, '/' , '.minegrech.com', 1);
				header("Refresh: 1, url=../");
			}elseif ($llamado == 5) {
				header("Refresh: 1, url=https://www.minegrech.com/profile/");
			}
		}
		
	}else{
		$cookie_error = "validation_captchaFailed";
		setcookie($cookie_name, $cookie_error, time()+7, '/' , '.minegrech.com', 1);
		header("Refresh: 1, url=../");
	}
}else{
	$cookie_error = "validation_fields";
	setcookie($cookie_name, $cookie_error, time()+7, '/' , '.minegrech.com', 1);
	header("Refresh: 1, url=../");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - <?php echo $lang['PAGE_REGTITLE']; ?></title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<link rel="stylesheet" href="../../assets/styles/index.reg.css">
	<?php include_once('/var/www/html/sessions/glaucusstatics.php'); ?>
</head>
<body>
<center>
	<div class="mensaje-error">
		<img src="https://cdn.glaucus.net/img/logo2.png" style="width: 400px; margin-right:15px;">	
	</div>
	<img src="https://cdn.glaucus.net/img/Loading1.gif" style="height: 200px;">	
</center>
<style>
	center {
		top: calc(50% - 250px);
		position: absolute;
		left: calc(50% - 207px);
	}
	body{
		background-color: #F5F5F5;
	}
	.ajust{
		margin-top:10em;
	}
</style>
</body>
</html> 