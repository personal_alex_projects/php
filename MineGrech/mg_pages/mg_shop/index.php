<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
global $gnsession;
global $mgsession;
global $mgclass;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');

$mgclass = new MineGrech();

if ($gnsession->isLoggedIn()) {
	$mymgsession = new MGSession(null);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - Shop</title>
	<link rel="icon" href="https://cdn.glaucus.net/img/favicon.png" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdn.glaucus.net/cookies/jq.c0okie.js"></script> 
	<link rel="stylesheet" type="text/css" href="https://cdn.glaucus.net/cookies/jq.c0okie.css"/>
	<script type="text/javascript">
		$(document).ready(function(){
			$.c0okieBar({
			});
		});
	</script>
</head>
<body>
	<div class="navbar-position">
		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
	</div>
	<div class="container" style="margin-top: 2.5em;">
		<div class="wrapper">
			<div class="better-items">
				<div class="row">
					<div class="col-md-4">
						<div class="item0">
							<div class="thumbnail" style="border-radius: 12px;">
								<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=340%C3%97200&w=340&h=200" alt="...">
								<div class="caption">
									<h3>#item_0</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores earum dolorum amet nobis. Commodi corrupti consectetur aliquid alias hic, expedita explicabo sed quaerat sunt deserunt, totam obcaecati saepe perspiciatis voluptates.</p>
									<div class="text" style="display: ruby-text-container;">
										<p>
											<a href="#" class="btn btn-primary" role="button">BUY</a>
										</p>
										<p class="pull-right">
											$0000.0000
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="item1">
							<div class="thumbnail" style="border-radius: 12px;">
								<img src="https://placeholdit.imgix.net/~text?txtsize=23&amp;txt=310%C3%97260&amp;w=310&amp;h=260" alt="..." style="box-shadow: 9px 9px 5px gray;border-radius: 9px;border: 1px solid coral;" width="94%">
								<div class="caption">
									<h3>#item_1</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, eveniet quasi atque. Exercitationem similique eum molestias doloremque provident aut sunt sit tempora unde error, quibusdam, beatae repellendus, dolore quod animi.</p>
									<div class="text" style="display: ruby-text-container;">
										<p>
											<a href="#" class="btn btn-primary" role="button">BUY</a>
										</p>
										<p class="pull-right">
											$0000.0000
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="item2">
							<div class="thumbnail" style="border-radius: 12px;">
								<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=340%C3%97200&w=340&h=200" alt="...">
								<div class="caption">
									<h3>#item_2</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
									<div class="text" style="display: ruby-text-container;">
										<p>
											<a href="#" class="btn btn-primary" role="button">BUY</a>
										</p>
										<p class="pull-right">
											$0000.0000
										</p>
									</div>
								</div>
							</div> 							
						</div>
					</div>
				</div>
			</div>
			<hr>
			<span>60% Complete Goal</span>
			<div class="progress">
				<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
				</div>
			</div>
			<hr>
			<div class="general-shop-by-category">
				<div class="row">
					<div class="col-md-3" style="margin-bottom: 5em;">
						<h3>Category Items</h3>
						<ul class="nav nav-pills nav-stacked">
							<li class="active"><a data-toggle="pill" href="#general">general</a></li>
							<li><a data-toggle="pill" href="#test">test</a></li>
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
									Menu 1 <span class="caret"></span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#">Submenu 1-1</a></li>
									<li><a href="#">Submenu 1-2</a></li>
									<li><a href="#">Submenu 1-3</a></li>
								</ul>
							</li>
							<li><a href="#">Button</a></li>
							<li><a href="#">Button</a></li>
							<li><a href="#">Button</a></li>
							<li><a href="#">Button</a></li>
							<li><a href="#">Button</a></li>
							<li><a data-toggle="pill" href="#gcoins">Buy gCoins</a></li>
						</ul>
					</div>
					<div class="col-md-8" style="margin-left: 6em;">
						<div class="tab-content">
							<div id="gcoins" class="tab-pane fade">
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://i.gyazo.com/fa3b6e2f803b23d99c558bbbf0d33efc.png" alt="gCoins_pack1">
										<div class="caption">
											<h3>gCoins Pack #1</h3>
											<p>Este paquete contiene una bolsa de gCoins de: 5 G$</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
												</p>
												<p class="pull-right">
													$2 US
												</p>
											</div>
										</div>
									</div> 
								</div>
							</div>
							<div id="general" class="tab-pane fade in active">
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=330%C3%97140&w=330&h=140" alt="...">
										<div class="caption">
											<h3>#item</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
													<a href="#" class="btn btn-default" role="button">INFO</a>
												</p>
												<p class="pull-right">
													$000.000
												</p>
											</div>
										</div>
									</div> 
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=330%C3%97140&w=330&h=140" alt="...">
										<div class="caption">
											<h3>#item</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
													<a href="#" class="btn btn-default" role="button">INFO</a>
												</p>
												<p class="pull-right">
													$000.000
												</p>
											</div>
										</div>
									</div> 
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=330%C3%97140&w=330&h=140" alt="...">
										<div class="caption">
											<h3>#item</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
													<a href="#" class="btn btn-default" role="button">INFO</a>
												</p>
												<p class="pull-right">
													$000.000
												</p>
											</div>
										</div>
									</div> 
								</div>
							</div>
							<div id="test" class="tab-pane fade">
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=330%C3%97140&w=330&h=140" alt="...">
										<div class="caption">
											<h3>#item_test</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
													<a href="#" class="btn btn-default" role="button">INFO</a>
												</p>
												<p class="pull-right">
													$000.000
												</p>
											</div>
										</div>
									</div> 
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=330%C3%97140&w=330&h=140" alt="...">
										<div class="caption">
											<h3>#item_test</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
													<a href="#" class="btn btn-default" role="button">INFO</a>
												</p>
												<p class="pull-right">
													$000.000
												</p>
											</div>
										</div>
									</div> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>