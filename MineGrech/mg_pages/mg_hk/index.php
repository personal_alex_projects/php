<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
$mymgsession = new MGSession(null);

include('/var/www/html/dynamic/classes/MineGrech/mg.administration.class.php');
$hkclass = new HKClass();

include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');
$mgclass = new MineGrech();

if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/");
	return;
}

if (!($mymgsession->getRank() > "500")) {
	header("Location: https://minegrech.com/");
	return;
}





?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - HouseKeeping</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/global.hk.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<script src="https://use.fontawesome.com/6dbaa26fe2.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="margin-right: 0.16em;">
	<?php include '/var/www/MineGrech/mg_pages/mg_hk/_includes/navbar.php'; ?>
	<div class="container" style="margin-left: 23em; margin-top: 2em; width: 75%;">
		<div class="wrapper">
			<div class="dashboard-title">
				<h3>Dashboard</h3>
				<hr>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-user-o fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge"><?php echo $hkclass->getLastRegisteredUsers(2); ?></div>
								<div>New Users Today!</div>
							</div>
						</div>
					</div>
					<a data-toggle="modal" data-target="#newuser">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-green">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-tasks fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">12</div>
								<div>New GAC Bans!</div>
							</div>
						</div>
					</div>
					<a href="#">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-yellow">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-shopping-cart fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">113</div>
								<div>New Orders!</div>
							</div>
						</div>
					</div>
					<a href="#">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-red">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-support fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge"><?php echo $hkclass->getLastTickets(2); ?></div>
								<div>New Support Tickets!</div>
							</div>
						</div>
					</div>
					<a data-toggle="modal" data-target="#newticket">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<br>
		<div class="col-lg-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-bell fa-fw"></i> Notifications Panel
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
					<div class="list-group">
						<a href="#" class="list-group-item">
							<i class="fa fa-comment fa-fw"></i> New Comment
							<span class="pull-right text-muted small"><em>4 minutes ago</em>
							</span>
						</a>
						<a href="#" class="list-group-item">
							<i class="fa fa-twitter fa-fw"></i> 3 New Followers
							<span class="pull-right text-muted small"><em>12 minutes ago</em>
							</span>
						</a>
						<a href="#" class="list-group-item">
							<i class="fa fa-envelope fa-fw"></i> Message Sent
							<span class="pull-right text-muted small"><em>27 minutes ago</em>
							</span>
						</a>
						<a href="#" class="list-group-item">
							<i class="fa fa-tasks fa-fw"></i> New Task
							<span class="pull-right text-muted small"><em>43 minutes ago</em>
							</span>
						</a>
						<a href="#" class="list-group-item">
							<i class="fa fa-upload fa-fw"></i> Server Rebooted
							<span class="pull-right text-muted small"><em>11:32 AM</em>
							</span>
						</a>
						<a href="#" class="list-group-item">
							<i class="fa fa-bolt fa-fw"></i> Server Crashed!
							<span class="pull-right text-muted small"><em>11:13 AM</em>
							</span>
						</a>
						<a href="#" class="list-group-item">
							<i class="fa fa-warning fa-fw"></i> Server Not Responding
							<span class="pull-right text-muted small"><em>10:57 AM</em>
							</span>
						</a>
						<a href="#" class="list-group-item">
							<i class="fa fa-shopping-cart fa-fw"></i> New Order Placed
							<span class="pull-right text-muted small"><em>9:49 AM</em>
							</span>
						</a>
						<a href="#" class="list-group-item">
							<i class="fa fa-money fa-fw"></i> Payment Received
							<span class="pull-right text-muted small"><em>Yesterday</em>
							</span>
						</a>
					</div>
					<!-- /.list-group -->
					<a href="#" class="btn btn-default btn-block">View All Alerts</a>
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
	</div>
</body>
</html>

<div id="newuser" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">New Users (today)</h4>
			</div>
			<div class="modal-body">
				<table class="table table-stripped">
					<thead>
						<tr>
							<th>GUID</th>
							<th>Username</th>
							<th>Ago</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$detail = $hkclass->getLastRegisteredUsers(1);
						if ($hkclass->getLastRegisteredUsers(2) != 0) {
							foreach ($detail as $key) {
							$guid = $key['GUID'];
							$user = $key['Username'];
							$ago  = $key['AddedMills'];

							$agotime = $mgclass->timeToAGO($ago);
							echo "<tr><td>$guid</td><td>$user</td><td>$agotime</td></tr>";
						}
						}else{
							echo "<tr><td>No records for the last 24 hours!</td></tr>";
						}
						
						?>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="newticket" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">New Ticket (last 30 days)</h4>
			</div>
			<div class="modal-body">
				<table class="table table-stripped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Author</th>
							<th>Ago</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$detail = $hkclass->getLastTickets(1);
						if ($hkclass->getLastTickets(2) != 0) {
							foreach ($detail as $key) {
							$id = $key['ticketID'];
							$user = $key['ticketAuthorName'];
							$ago  = $key['ticketMills'];
							$guid = $key['ticketAuthorGUID'];

							$agotime = $mgclass->timeToAGO($ago);
							echo "<tr><td>$id</td><td>$user</td><td>$agotime</td><td><a href='https://minegrech.com/housekeeping/ticketmanager/view/?tid=$id&guid=$guid' class='btn btn-default'>VIEW</a></td></tr>";
						}
						}else{
							echo "<tr><td>No records for the last 24 hours!</td></tr>";
						}
						
						?>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

