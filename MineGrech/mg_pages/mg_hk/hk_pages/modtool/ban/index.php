<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
$mymgsession = new MGSession(null);
if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/");
	return;
}

if (!($mymgsession->getRank() > "500")) {
	header("Location: https://minegrech.com/");
	return;
}

?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Minegrech - HouseKeeping</title>
 	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/global.hk.css">
 	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
 	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
 	<script src="https://use.fontawesome.com/6dbaa26fe2.js"></script>
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 	<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 	<script>
 		$(document).ready(function(){ 
 			$('#characterLeft').text('500 characters left');
 			$('#detail_reason').keyup(function () {
 				var max = 500;
 				var len = $(this).val().length;
 				if (len >= max) {
 					$('#characterLeft').text('You have reached the limit');
 					$('#characterLeft').addClass('red');
 					$('#btnSubmit').addClass('disabled');            
 				} 
 				else {
 					var ch = max - len;
 					$('#characterLeft').text(ch + ' characters left');
 					$('#btnSubmit').removeClass('disabled');
 					$('#characterLeft').removeClass('red');            
 				}
 			});    
 		});
 	</script>
 </head>
 <body style="margin-right: 0.16em;">
 	<?php include '/var/www/MineGrech/mg_pages/mg_hk/_includes/navbar.php'; ?>
 	<div class="container" style="margin-left: 23em; margin-top: 2em; width: 78%;">
 		<div class="wrapper">
 			<div class="dashboard-title">
 				<h4>Ban Manager</h4>
 				<hr>
 			</div>
 			<div class="form-request">
 				<form action="">
 					<div class="form-group">
 						<span><b>Player GUID</b></span>
 						<?php if (isset($_GET['guid'])): ?>
 							<input type="text" placeholder="GUID" value="<?php echo $_GET['guid']; ?>" class="form-control">
 						<?php else: ?>
 							<input type="text" placeholder="GUID" class="form-control">
 						<?php endif; ?>
 						<br>
 						<span><b>Ban Type</b></span>
 						<select class="form-control" name="ban_type" id="status_log">
 							<option value="1" selected>Permanent Ban</option>
 							<option value="2">Temporal Ban</option>
 						</select>
 						<br>
 						<br>
 						<span><b>This problem occurred in?</b></span>
 						<input type="text" placeholder="Example: Lobby" class="form-control">
 						<br>
 						<span><b>Reasons:</b></span>
 						<select class="form-control" name="reasons" id="reasons">
 							<option value="null" disabled selected>Select Reason</option>
 							<option value="1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. (1 Day Ban)</option>
 							<option value="2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. (2 Hour Ban)</option>
 							<option value="3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. (4 Days Ban)</option>
 						</select>
 						<br>
 						<span><b>Insert Full Detail:</b></span>
 						<textarea name="detail_reason" id="detail_reason" placeholder="Details about this players, reports, GAC, etc." maxlength="500" cols="30" rows="5" style="resize: none;" class="form-control input-sm"></textarea>
 						<span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>
 						<button class="form-control input-sm btn btn-success disabled" id="btnSubmit" name="btnSubmit" type="button" style="height:35px"> Send</button>
 					</form>
 				</div>
 			</div>
 		</div>
 		<br>
 	</div>
 </body>
 </html>  