<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
global $gnsession;
global $mgsession;
global $mgclass;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');

$mgclass = new MineGrech();

if ($gnsession->isLoggedIn()) {
	$mymgsession = new MGSession(null);
}else{
	header("Location: https://www.minegrech.com/login");
}

if (!($mymgsession->getRank() > "500")) {
	header("Location: https://minegrech.com/");
	return;
}

if (isset($_POST['taken_submit'])) {
	$ticket = $_GET['ticket'];
	$guid = $_GET['guid'];
	$name = $_GET['myname'];
	$mgclass->setTakenByGUID($ticket, $guid, $name);
	header("Location: https://minegrech.com/housekeeping/ticketmanager/view/?tid=$ticket&guid=$guid");
}

if (isset($_POST['close_submit'])) {
	$ticket = $_GET['ticket'];
	$guid = $_GET['guid'];
	$name = $_GET['myname'];
	$mgclass->setClosedByGUID($ticket, $name);
	header("Location: https://minegrech.com/housekeeping/ticketmanager/view/?tid=$ticket&guid=$guid");
}
 ?> 