<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
global $gnsession;
global $mgsession;
global $mgclass;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');

$mgclass = new MineGrech();

if ($gnsession->isLoggedIn()) {
	$mymgsession = new MGSession(null);
}else{
	header("Location: https://www.minegrech.com/login");
}

if (!($mymgsession->getRank() > "500")) {
	header("Location: https://minegrech.com/");
	return;
}

/* PAGINATION */
$limit = 2;
$p=$_GET['p']=="" ? 1:$_GET['p'];
$start=($p-1)*$limit;

if (isset($_GET['tid']) && isset($_GET['guid'])) {
		$ticket = $mgclass->getMyTickedID($_GET['tid'], $_GET['guid']);
		if ($ticket == 100) {
			header("Location: https://minegrech.com/housekeeping/ticketmanager/list");
		}else{
			foreach ($ticket as $key) {
				$ticketID 	= $key['ticketID'];
				$tAuthor 	= $key['ticketAuthorName'];
				$tAuthorG 	= $key['ticketAuthorGUID'];
				$problem 	= $key['ticketProblem'];
				$message	= $key['ticketMessage'];
				$subject	= $key['ticketSubject'];
				$takenby	= $key['ticketTakenBy'];
				$created 	= $key['ticketCreation'];
				$lastupdate	= $key['ticketLastUpdate'];
				$priority	= $key['ticketPriority'];
				$superior	= $key['ticketSuperior'];
				$personinfo	= $key['ticketPersonalInfo'];
				$download	= $key['ticketDownloadble'];
				$tests      = $key['ticketTest'];
				$status 	= $key['ticketStatus'];
			}
			if (!$ticketID == $_GET['tid'] && !$tAuthorG == $_GET['guid']) {
				header("Location: https://minegrech.com/housekeeping/ticketmanager/list");
			}
		}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - HouseKeeping</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/global.hk.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<script src="https://use.fontawesome.com/6dbaa26fe2.js"></script>
	<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="margin-right: 0.16em;">
	<?php include '/var/www/MineGrech/mg_pages/mg_hk/_includes/navbar.php'; ?>
	<div class="container" style="margin-left: 23em; margin-top: 2em; width: 78%;">
		<div class="wrapper">
			<div class="dashboard-title">
				<h4>All Tickets</h4>
				<hr>
			</div>
			<div class="table-tickets">
				<table class="table table-stripped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Status</th>
							<th>Problem</th>
							<th>Open By</th>
							<th>Case taken by</th>
							<th>Priority</th>
							<th>Staff Required</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php $ticket = $mgclass->getAllTickets(); ?>
						<?php 
						if ($ticket == 100) {
							echo "<tr><td>You dont have any ticket!</td></tr>";
						}else{
							foreach ($ticket as $key) {
								$ticketID 	= $key['ticketID'];
								$status 	= $key['ticketStatus'];
								$problem    = $key['ticketProblem'];
								$takenby    = $key['ticketTakenBy'];
								$open 		= $key['ticketAuthorName'];
								$trequire  	= $key['ticketSuperior'];
								$priority   = $key['ticketPriority'];
								$aguid   	= $key['ticketAuthorGUID'];

								switch ($status) {
									case '0':
									$status = "<span class='label label-default'>Sended</span>";
									break;
									case '1':
									$status = "<span class='label label-success'>Accepted</span>";
									break;
									case '2':
									$status = "<span class='label label-warning'>Pending</span>";
									break;
									case '3':
									$status = "<span class='label label-danger'>Closed</span>";
									break;	
									default:
									$status = "<span class='label label-danger'>Closed</span>";
									break;
								}

								switch ($problem) {
									case '1':
									$problem = "Website Problem.";
									break;
									case '2':
									$problem = "Account Problem";
									break;
									case '3':
									$problem = "Bug / glitch.";
									break;
									case '4':
									$problem = "Premium Purchase.";
									break;
									case '5':
									$problem = "Deliberate punishment.";
									break;
									case '6':
									$problem = "Other Reason.";
									break;
									default:
									$problem = "unknown problem";
									break;
								}

								switch ($priority) {
									case '0':
									$priority = "Normal";
									break;
									case '1':
									$priority = "High";
									break;
									default:
									$priority = "Normal";
									break;
								}

								switch ($trequire) {
									case 0:
									$trequire = "Moderator";
									break;
									case 1:
									$trequire = "Admin";
									break;
									default:
									$trequire = "Moderator";
									break;
								}
								if ($takenby == "No one") {
									echo "<tr><td><a href='https://minegrech.com/housekeeping/ticketmanager/view?tid=$ticketID&guid=$aguid'>$ticketID</a></td><td>$status</td><td>$problem</td><td>$open</td><td>$takenby</td><td>$priority</td><td>$trequire</td><td><a href='https://minegrech.com/housekeeping/ticketmanager/view?tid=$ticketID&guid=$aguid' class='btn btn-default'>View</a> <a href='https://www.minegrech.com/edit/tickets/view?tid=$ticketID&guid=$aguid&taken=".$mymgsession->getName()."' class='btn btn-primary'>Taken</a></td></tr>";
								}else{
									echo "<tr><td><a href='https://minegrech.com/housekeeping/ticketmanager/view?tid=$ticketID&guid=$aguid'>$ticketID</a></td><td>$status</td><td>$problem</td><td>$open</td><td>$takenby</td><td>$priority</td><td>$trequire</td><td><a href=https://minegrech.com/housekeeping/ticketmanager/?tid=$ticketID&guid=$aguid' class='btn btn-default'>View</a></td></tr>";

								}
							}
						}
						?>
						
					</tbody>
				</table>
			</div>
		</div>
		<br>
	</div>
</body>
</html>  