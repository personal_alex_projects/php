<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
$mymgsession = new MGSession(null);

include('/var/www/html/dynamic/classes/MineGrech/mg.administration.class.php');
$hkclass = new HKClass();

include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');
$mgclass = new MineGrech();

if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/");
	return;
}

if (!($mymgsession->getRank() > "800")) {
	header("Location: https://minegrech.com/housekeeping/dashboard/");
	return;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - HouseKeeping</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/global.hk.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<script src="https://use.fontawesome.com/6dbaa26fe2.js"></script>
	<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function(){ 
			$('#characterLeft').text('100 characters left');
			$('#subject').keyup(function () {
				var max = 100;
				var len = $(this).val().length;
				if (len >= max) {
					$('#characterLeft').text('You have reached the limit');
					$('#characterLeft').addClass('red');
					$('#btnSubmit').addClass('disabled');            
				} 
				else {
					var ch = max - len;
					$('#characterLeft').text(ch + ' characters left');
					$('#btnSubmit').removeClass('disabled');
					$('#characterLeft').removeClass('red');            
				}
			});    
		});
	</script>
</head>
<body style="margin-right: 0.16em;">
	<?php include '/var/www/MineGrech/mg_pages/mg_hk/_includes/navbar.php'; ?>
	<div class="container" style="margin-left: 23em; margin-top: 2em; width: 78%;">
		<div class="wrapper">
			<div class="dashboard-title">
				<h4>Add new Post (in main Blog)</h4>
				<hr>
			</div>
			<div class="form-request">
				<form action="https://minegrech.com/housekeeping/blogmanager/add/update" method="POST">
					<div class="form-group">
						<span><b>Author</b></span>
						<input type="text" name="author_nickname" value="<?php echo $mymgsession->getName(); ?>"  disabled class="form-control">
						<br>
						<span><b>Subject</b></span>
						<input type="text" id="subject" name="subject" placeholder="Ex: New Point System." maxlength="100" class="form-control">
						<span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>
						<br>
						<span><b>Header</b></span>
						<textarea name="header" id="header" name="header" cols="102" rows="50" style="resize: none;"></textarea>
						<script>
							CKEDITOR.replace( 'header' );
						</script>
						<br>
						<span><b>Description</b></span>
						<textarea name="message" id="message" name="message" cols="102" rows="50" style="resize: none;"></textarea>
						<script>
							CKEDITOR.replace( 'message' );
						</script>
						<br>
						<input class="form-control input-sm btn btn-success disabled" id="btnSubmit" name="submit" type="submit" style="height:35px" value="Send">
					</form>
				</div>
			</div>
		</div>
		<br>
	</div>
</body>
</html>  