 <?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
$mymgsession = new MGSession(null);

include('/var/www/html/dynamic/classes/MineGrech/mg.administration.class.php');
$hkclass = new HKClass();

include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');
$mgclass = new MineGrech();

if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/");
	return;
}

if (!($mymgsession->getRank() > "500")) {
	header("Location: https://minegrech.com/");
	return;
}

if ($_POST['submit']) {
	if ($_POST['subject'] && $_POST['header'] && $_POST['message']) {
		$subject 	= $_POST['subject'];
		$header 	= $_POST['header'];
		$message 	= $_POST['message'];
		$myguid 	= $mymgsession->getGUID();
		$myname 	= $mymgsession->getName();

		$hkclass->addNewBlog($myguid, $myname, $subject, $header, $message);
		header("Location: https://minegrech.com/housekeeping/blogmanager/add?st=sended");
	}else{
		header("Location: https://minegrech.com/housekeeping/blogmanager/add?st=missinginput");
	}
}else{
	header("Location: https://minegrech.com/housekeeping/blogmanager/add?st=notsubmit");
}

  ?>