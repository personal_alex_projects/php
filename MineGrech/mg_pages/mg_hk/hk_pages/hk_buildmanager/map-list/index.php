<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - Admin [Map List]</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/global.hk.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<script src="https://use.fontawesome.com/6dbaa26fe2.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function () {

			$('.star').on('click', function () {
				$(this).toggleClass('star-checked');
			});

			$('.ckbox label').on('click', function () {
				$(this).parents('tr').toggleClass('selected');
			});

			$('.btn-filter').on('click', function () {
				var $target = $(this).data('target');
				if ($target != 'all') {
					$('.table tr').css('display', 'none');
					$('.table tr[data-status="' + $target + '"]').fadeIn('slow');
				} else {
					$('.table tr').css('display', 'none').fadeIn('slow');
				}
			});

		});
		$(document).ready(function() {
    var activeSystemClass = $('.list-group-item.active');

    //something is entered in search form
    $('#system-search').keyup( function() {
       var that = this;
        // affect all table rows on in systems table
        var tableBody = $('.table-list-search tbody');
        var tableRowsClass = $('.table-list-search tbody tr');
        $('.search-sf').remove();
        tableRowsClass.each( function(i, val) {
        
            //Lower text for case insensitive
            var rowText = $(val).text().toLowerCase();
            var inputText = $(that).val().toLowerCase();
            if(inputText != '')
            {
                $('.search-query-sf').remove();
                tableBody.prepend('<tr class="search-query-sf"><td colspan="6"><strong>Searching for: "'
                    + $(that).val()
                    + '"</strong></td></tr>');
            }
            else
            {
                $('.search-query-sf').remove();
            }

            if( rowText.indexOf( inputText ) == -1 )
            {
                //hide rows
                tableRowsClass.eq(i).hide();
                
            }
            else
            {
                $('.search-sf').remove();
                tableRowsClass.eq(i).show();
            }
        });
        //all tr elements are hidden
        if(tableRowsClass.children(':visible').length == 0)
        {
            tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="6">No entries found.</td></tr>');
        }
    });
});
	</script>
</head>
<body style="margin-right: 0.16em;">
	<?php include '/var/www/MineGrech/mg_pages/mg_hk/_includes/navbar.php'; ?>
	<div class="container" style="margin-left: 23em; margin-top: 2em; width: 78%;">
		<div class="wrapper">
			<div class="dashboard-title" style="display: inline-flex;">
				<h4>Build Logs -> List</h4>
				<div class="filter-btns" style="margin-left: 40em;">
					<div class="pull-right">
						<div class="btn-group">
							<button type="button" class="btn btn-success btn-filter" data-target="added">Added</button>
							<button type="button" class="btn btn-warning btn-filter" data-target="fixed">Fixed</button>
							<button type="button" class="btn btn-danger btn-filter" data-target="removed">Removed</button>
							<button type="button" class="btn btn-default btn-filter" data-target="all">All</button>
						</div>
					</div>
					<div>
						<form action="#" method="get">
							<div class="input-group">
								<!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
								<input class="form-control" id="system-search" name="q" placeholder="Search for" required>
								<span class="input-group-btn">
								<button type="submit" class="btn btn-default"><i class="fa fa-neuter fa-lg"></i></button>
								</span>
							</div>
						</form>
					</div>
				</div>
				<hr>
			</div>
			<div class="table-logs">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-striped table-list-search">
							<thead>
								<tr>
									<th>#</th>
									<th>Status</th>
									<th>Server</th>
									<th>Map Name</th>
									<th>Subject</th>
									<th>By</th>
									<th>Date</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<tr data-status="added">
									<td>bl01d5k</td>
									<td><span class="label label-success">Added</span></td>
									<td>Lobby</td>
									<td>LobbyV1</td>
									<td>Bloque faltante en camino.</td>
									<td>AlexBanPer</td>
									<td>21/11/2016 0:24 CL</td>
									<td><button class="btn btn-default">Action</button></td>
								</tr>
								<tr data-status="removed">
									<td>bl01d5k</td>
									<td><span class="label label-danger">Removed</span></td>
									<td>Lobby</td>
									<td>LobbyV1</td>
									<td>Bloque faltante en camino.</td>
									<td>AlexBanPer</td>
									<td>21/11/2016 0:24 CL</td>
									<td><button class="btn btn-default">Action</button></td>
								</tr>
								<tr data-status="fixed">
									<td>bl01d5k</td>
									<td><span class="label label-warning">Warning</span></td>
									<td>Lobby</td>
									<td>LobbyV1</td>
									<td>Bloque faltante en camino.</td>
									<td>AlexBanPer</td>
									<td>21/11/2016 0:24 CL</td>
									<td><button class="btn btn-default">Action</button></td>
								</tr>
								<tr data-status="added">
									<td>bl01d5k</td>
									<td><span class="label label-success">Added</span></td>
									<td>Lobby</td>
									<td>LobbyV1</td>
									<td>Bloque faltante en camino.</td>
									<td>AlexBanPer</td>
									<td>21/11/2016 0:24 CL</td>
									<td><button class="btn btn-default">Action</button></td>
								</tr>
								<tr data-status="removed">
									<td>bl01d5k</td>
									<td><span class="label label-danger">Removed</span></td>
									<td>Lobby</td>
									<td>LobbyV1</td>
									<td>Bloque faltante en camino.</td>
									<td>AlexBanPer</td>
									<td>21/11/2016 0:24 CL</td>
									<td><button class="btn btn-default">Action</button></td>
								</tr>
								<tr data-status="fixed">
									<td>bl01d5k</td>
									<td><span class="label label-warning">Warning</span></td>
									<td>Lobby</td>
									<td>LobbyV1</td>
									<td>Bloque faltante en camino.</td>
									<td>AlexBanPer</td>
									<td>21/11/2016 0:24 CL</td>
									<td><button class="btn btn-default">Action</button></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>