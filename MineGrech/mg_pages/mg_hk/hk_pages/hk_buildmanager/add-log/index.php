<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
$mymgsession = new MGSession(null);

if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/login");
	return;
}

if (!($mymgsession->getRank() > "500")) {
	header("Location: https://minegrech.com/");
	return;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - HouseKeeping</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/global.hk.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<script src="https://use.fontawesome.com/6dbaa26fe2.js"></script>
	<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function(){ 
			$('#characterLeft').text('500 characters left');
			$('#detail_reason').keyup(function () {
				var max = 500;
				var len = $(this).val().length;
				if (len >= max) {
					$('#characterLeft').text('You have reached the limit');
					$('#characterLeft').addClass('red');
					$('#btnSubmit').addClass('disabled');            
				} 
				else {
					var ch = max - len;
					$('#characterLeft').text(ch + ' characters left');
					$('#btnSubmit').removeClass('disabled');
					$('#characterLeft').removeClass('red');            
				}
			});    
		});
	</script>
</head>
<body style="margin-right: 0.16em;">
	<?php include '/var/www/MineGrech/mg_pages/mg_hk/_includes/navbar.php'; ?>
	<div class="container" style="margin-left: 23em; margin-top: 2em; width: 78%;">
		<div class="wrapper">
			<div class="dashboard-title">
				<h4>Request Map Change</h4>
				<hr>
			</div>
			<div class="form-request">
				<form method="POST" action="https://www.minegrech.com/housekeeping/building/update/?guid=<?php echo $mymgsession->getGUID()."&type=3&url=addlog"; ?>">
					<div class="form-group">
						<span><b>You GUID</b></span>
						<input type="text"  value="<?php echo $mymgsession->getGUID(); ?>" disabled name="GUID" class="form-control">
						<br>
						<span><b>Insert Server</b></span>
						<input type="text" placeholder="Example: Lobby" class="form-control" name="servername">
						<br>
						<span><b>Reason. <i>this will be shown as data at the beginning of the site.</i></b></span>
						<div class="input-group">
							<span class="input-group-addon" id="reason_id">Added/Fixed/Remove: </span>
							<input type="text" class="form-control" aria-describedby="reason_id" placeholder="Example: A new floor color.." id="reason_id" class="form-control" name="reason">
						</div>
						<br>
						<span><b>Insert Builders</b></span>
						<input type="text" placeholder="Example: GUID or Username." class="form-control" name="who">
						<br>
						<span><b>Insert Log Status</b></span>
						<select class="form-control" name="status_log" id="status_log">
							<option value="null" disabled selected>Select Status</option>
							<option value="1">Added</option>
							<option value="2">Fixed</option>
							<option value="3">Removed</option>
						</select>
						<br>
						<span><b>Map Making Details:</b></span>
						<textarea name="detail_reason" id="detail_reason" placeholder="It details the last one that was realized for the creation of this Log." maxlength="500" cols="30" rows="5" style="resize: none;" class="form-control input-sm"></textarea>
						<span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>
						<input type="submit" class="form-control input-sm btn btn-success disabled" id="btnSubmit" name="submit" style="height:35px">
					</form>
				</div>
			</div>
		</div>
		<br>
	</div>
</body>
</html> 