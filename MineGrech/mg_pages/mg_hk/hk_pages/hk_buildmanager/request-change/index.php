<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
$mymgsession = new MGSession(null);

if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/login");
	return;
}

if (!($mymgsession->getRank() > "500")) {
	header("Location: https://minegrech.com/");
	return;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - HouseKeeping</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/global.hk.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<script src="https://use.fontawesome.com/6dbaa26fe2.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function(){ 
			$('#characterLeft').text('500 characters left');
			$('#detail_reason').keyup(function () {
				var max = 500;
				var len = $(this).val().length;
				if (len >= max) {
					$('#characterLeft').text('You have reached the limit');
					$('#characterLeft').addClass('red');
					$('#btnSubmit').addClass('disabled');            
				} 
				else {
					var ch = max - len;
					$('#characterLeft').text(ch + ' characters left');
					$('#btnSubmit').removeClass('disabled');
					$('#characterLeft').removeClass('red');            
				}
			});    
		});
	</script>
</head>
<body style="margin-right: 0.16em;">
	<?php include '/var/www/MineGrech/mg_pages/mg_hk/_includes/navbar.php'; ?>
	<div class="container" style="margin-left: 23em; margin-top: 2em; width: 78%;">
		<div class="wrapper">
			<div class="dashboard-title">
				<h4>Request Map Change</h4>
				<hr>
			</div>
			<div class="form-request">
					<form method="POST" action="https://www.minegrech.com/housekeeping/building/update/?guid=<?php echo $mymgsession->getGUID()."&type=1&url=request/map-change"; ?>">
					<div class="form-group">
						<span><b>You GUID</b></span>
						<input type="text"  value="<?php echo $mymgsession->getGUID(); ?>" disabled name="GUID" class="form-control">
						<br>
						<span><b>On which server do you want to make the change?</b></span>
						<input type="text" placeholder="Example: Lobby" class="form-control" name="servername">
						<br>
						<span><b>The reason for this change</b></span>
						<input type="text" placeholder="Example: Fix empty block in wall." class="form-control" name="reason">
						<br>
						<span><b>Insert Builders</b></span>
						<input type="text" placeholder="Example: GUID or Username." class="form-control" name="who">
						<br>
						<span><b>It details what will be done during this change, periods of construction, etc.</b></span>
						<textarea name="detail_reason" id="detail_reason" placeholder="Full Details about this change." maxlength="500" cols="30" rows="5" style="resize: none;" class="form-control input-sm"></textarea>
						<span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>
						<input type="submit" class="form-control input-sm btn btn-success disabled" id="btnSubmit" name="submit" style="height:35px">
					</form>
				</div>
			</div>
		</div>
		<br>
	</div>
</body>
</html>