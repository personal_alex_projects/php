<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
global $gnsession;
global $mgsession;
global $mgclass;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');
include('/var/www/html/dynamic/classes/MineGrech/mg.administration.class.php');
include('/var/www/MineGrech/dynamic/functions/setupRanks.php');

$hkclass = new HKClass();
$mgclass = new MineGrech();

if ($gnsession->isLoggedIn()) {
	$mymgsession = new MGSession(null);
}else{
	header("Location: https://www.minegrech.com/login");
}

if (!($mymgsession->getRank() > "500")) {
	header("Location: https://minegrech.com/");
	return;
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - HouseKeeping</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/global.hk.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<script src="https://use.fontawesome.com/6dbaa26fe2.js"></script>
	<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
		$(document).ready(function () {

			$('.star').on('click', function () {
				$(this).toggleClass('star-checked');
			});

			$('.ckbox label').on('click', function () {
				$(this).parents('tr').toggleClass('selected');
			});

			$('.btn-filter').on('click', function () {
				var $target = $(this).data('target');
				if ($target != 'all') {
					$('.table tr').css('display', 'none');
					$('.table tr[data-status="' + $target + '"]').fadeIn('slow');
				} else {
					$('.table tr').css('display', 'none').fadeIn('slow');
				}
			});

		});
		$(document).ready(function() {
			var activeSystemClass = $('.list-group-item.active');

    //something is entered in search form
    $('#system-search').keyup( function() {
    	var that = this;
        // affect all table rows on in systems table
        var tableBody = $('.table-list-search tbody');
        var tableRowsClass = $('.table-list-search tbody tr');
        $('.search-sf').remove();
        tableRowsClass.each( function(i, val) {

            //Lower text for case insensitive
            var rowText = $(val).text().toLowerCase();
            var inputText = $(that).val().toLowerCase();
            if(inputText != '')
            {
            	$('.search-query-sf').remove();
            	tableBody.prepend('<tr class="search-query-sf"><td colspan="6"><strong>Searching for: "'
            		+ $(that).val()
            		+ '"</strong></td></tr>');
            }
            else
            {
            	$('.search-query-sf').remove();
            }

            if( rowText.indexOf( inputText ) == -1 )
            {
                //hide rows
                tableRowsClass.eq(i).hide();
                
            }
            else
            {
            	$('.search-sf').remove();
            	tableRowsClass.eq(i).show();
            }
        });
        //all tr elements are hidden
        if(tableRowsClass.children(':visible').length == 0)
        {
        	tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="6">No entries found.</td></tr>');
        }
    });
});
</script>
</head>
<body style="margin-right: 0.16em;">
	<?php include '/var/www/MineGrech/mg_pages/mg_hk/_includes/navbar.php'; ?>
	<div class="container" style="margin-left: 23em; margin-top: 2em; width: 78%;">
		<div class="wrapper">
			<div class="dashboard-title">
				<h4>All Tickets</h4>
				<hr>
				<div class="filter-btns" style="margin-left: 40em;">
					<div class="pull-right">
						<div class="btn-group">
							<button type="button" class="btn btn-danger btn-filter" data-target="staff">STAFF</button>
							<button type="button" class="btn btn-default btn-filter" data-target="all">All</button>
						</div>
					</div>
					<div>
						<form action="#" method="get">
							<div class="input-group">
								<!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
								<input class="form-control" id="system-search" name="q" placeholder="Search for" required>
								<span class="input-group-btn">
									<button type="submit" class="btn btn-default"><i class="fa fa-neuter fa-lg"></i></button>
								</span>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="table-tickets">
				<table class="table table-stripped table-list-search">
					<thead>
						<tr>
							<th>GUID</th>
							<th>Username</th>
							<th>UUID</th>
							<th>Rank</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<?php $user = $hkclass->getAllUsers(); ?>
						<?php 
						
						foreach ($user as $key) {
							$username 	= $key['Username'];
							$guid 		= $key['GUID'];
							$uuid 		= $key['UUID'];
							$ranknum 		= $key['UserRank'];
							$rank = getRankName($ranknum);

							if ($ranknum >= "500") {
								echo "<tr data-status='staff'>";
								echo "<td><a href='#'>$guid</a></td>";
								echo "<td>$username</td>";
								echo "<td><a href='#'>$uuid</a></td>";
								echo "<td>$rank</td>";
								echo "<td>%btns_hk%</td>";
								echo "</tr>";
							}else{
								echo "<tr>";
								echo "<td><a href='#'>$guid</a></td>";
								echo "<td>$username</td>";
								echo "<td><a href='#'>$uuid</a></td>";
								echo "<td>$rank</td>";
								echo "<td>%btns_hk%</td>";
								echo "</tr>";
							}
							
						}
						?>

					</tbody>
				</table>
			</div>
		</div>
		<br>
	</div>
</body>
</html> 