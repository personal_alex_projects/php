<div class="navbar-position">
	<div class="nav-wrapper">
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; margin-right: -2.5px;">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.html">HouseKeeping</a>
			</div>
			<ul class="nav navbar-top-links navbar-right" style="margin-right: 40px; margin-top: 0.7em;">
				<li class="dropdown ">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">
						<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="http://minegrech.com/profile/<?php echo $mymgsession->getName(); ?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
						</li>
						<li><a href="http://minegrech.com/edit/profile"><i class="fa fa-gear fa-fw"></i> Settings</a>
						</li>
						<li class="divider"></li>
						<li><a href="https://www.glaucus.net/profile/leave.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
						</li>
					</ul>
					<!-- /.dropdown-user -->
				</li>
			</ul>
		</nav>
	</div>
</div>
<div class="nav-side-menu">
	<div class="brand">MinegrechLogo</div>
	<i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

	<div class="menu-list">
		<style>
			.aPadding{
				padding-right: 13em;
				padding-top: 0.5em;
				padding-bottom: 0.5em;
			}
		</style>
		<ul id="menu-content" class="menu-content collapse out">
			<li>
				<a class="aPadding" href="https://www.minegrech.com/housekeeping/dashboard">
					<i class="fa fa-dashboard fa-lg"></i> Dashboard
				</a>
			</li>
			<li data-toggle="collapse" data-target="#build" class="collapsed">
				<a href="#"><i class="fa fa-gavel fa-lg"></i> Build Manager <span class="arrow"></span></a>
			</li>  
			<ul class="sub-menu collapse" id="build">
				<li><b>ACTIONS</b></li>
				<li><a href="https://www.minegrech.com/housekeeping/building/request/map-change">Request Map Change</a></li>
				<li><a href="https://www.minegrech.com/housekeeping/building/request/server">Request Build Server</a></li>
				<li><a href="https://www.minegrech.com/housekeeping/building/addlog">Add Build Log</a></li>
				<li><b>TOOLS</b></li>
				<li><a href="https://www.minegrech.com/housekeeping/building/list">View Build Logs</a></li>
			</ul>
			<li  data-toggle="collapse" data-target="#moderation" class="collapsed">
				<a href="#"><i class="fa fa-address-card"></i> Moderation <span class="arrow"></span></a>
			</li>
			<ul class="sub-menu collapse" id="moderation">
				<li><b>PUNISH</b></li>
				<li class="active"><a href="https://www.minegrech.com/housekeeping/modtool/ban">Ban Player</a></li>
				<li><a href="https://www.minegrech.com/housekeeping/modtool/mute">Mute Player</a></li>
				<li><a href="#">Warn Player</a></li>
				<li><b>DETAILS</b></li>
				<li><a href="#">View Punishments</a></li>
				<li><a href="#">View GAC Infractions</a></li>
				<li><a href="#">View Chat Logs</a></li>
				<li><a href="#">View Commands Logs</a></li>
				<li><a href="#">View Players</a></li>
				<li><a href="#">View in-game Reports</a></li>
				<li><b>TOOLS</b></li>
				<li><a href="#">Find Player</a></li>
				<li><a href="#">Full Player List</a></li>
			</ul>
			<?php if ($mymgsession->getRank() > "800"): ?>
				<li data-toggle="collapse" data-target="#smod" class="collapsed">
					<a href="#"><i class="fa fa-sitemap fa-lg"></i> Staff Manager <span class="arrow"></span></a>
				</li>  
				<ul class="sub-menu collapse" id="smod">
					<li><b>ACTIONS</b></li>
					<li>View Staff List</li>
					<li>Ascend Player</li>
					<li>Descend Player</li>
				</ul>
				<li data-toggle="collapse" data-target="#administration" class="collapsed">
					<a href="#"><i class="fa fa-eye fa-lg"></i> Administration <span class="arrow"></span></a>
				</li>  
				<ul class="sub-menu collapse" id="administration">
					<li><b>TOOLS</b></li>
					<li>Discord IP</li>
				</ul>
				<li data-toggle="collapse" data-target="#man" class="collapsed">
					<a href="#"><i class="fa fa-street-view fa-lg"></i> Manager <span class="arrow"></span></a>
				</li>  
				<ul class="sub-menu collapse" id="man">
					<li><b>ACTIONS</b></li>
					<li><a href="#">Request Build Fix</a></li>
					<li><a href="#">Request Plugin Change</a></li>
					<li><b>TOOLS</b></li>
					<li><a href="#">Server Actions</a></li>
					<li><a href="#">Server Statistics</a></li>
					<li><a href="#">Request Server List</a></li>
					<li><a href="#">Request Build List</a></li>
				</ul>
			<?php endif ?>
			<li data-toggle="collapse" data-target="#support" class="collapsed">
				<a href="#"><i class="fa fa-support fa-lg"></i> Support Tickets <span class="arrow"></span></a>
			</li>  
			<ul class="sub-menu collapse" id="support">
				<li><b>ACTIONS</b></li>
				<li><a href="https://minegrech.com/housekeeping/ticketmanager/list">View Last Tickets</a></li>
			</ul>
			<li data-toggle="collapse" data-target="#marketing" class="collapsed">
				<a href="#"><i class="fa fa-cubes fa-lg"></i>  Marketing <span class="arrow"></span></a>
			</li>
			<ul class="sub-menu collapse" id="marketing">
				<li><b>ACTIONS</b></li>
				<li>Add Marketing Logs</li>
				<li><b>TOOLS</b></li>
				<li>View Marketing Logs</li>
			</ul>
			<?php if ($mymgsession->getRank() > "800"): ?>
			<li data-toggle="collapse" data-target="#webmaster" class="collapsed">
				<a href="#"><i class="fa fa-puzzle-piece fa-lg"></i>  Web Master <span class="arrow"></span></a>
			</li>
			<ul class="sub-menu collapse" id="webmaster">
				<li><b>ACTIONS</b></li>
				<li>Add LOG</li>
				<li><b>TOOLS</b></li>
				<li>View Logs</li>
			</ul>
			<?php endif ?>
			<li data-toggle="collapse" data-target="#blog" class="collapsed">
				<a href="#"><i class="fa fa-file-text-o fa-lg"></i> Blog Manager <span class="arrow"></span></a>
			</li>  
			<ul class="sub-menu collapse" id="blog">
				<li><b>ACTIONS</b></li>
				<li><a href="https://minegrech.com/housekeeping/blogmanager/add">New Blog</a></li>
				<li><b>TOOLS</b></li>
				<li><a href="https://www.minegrech.com/housekeeping/building/list">View Products</a></li>
			</ul>
			<li data-toggle="collapse" data-target="#other" class="collapsed">
				<a href="#"><i class="fa fa-gear fa-lg"></i> Other Actions <span class="arrow"></span></a>
			</li>  
			<ul class="sub-menu collapse" id="other">
				<li><b>ACTIONS</b></li>
				<li><a href="#">Request Server Manager</a></li>
			</ul>
			<li data-toggle="collapse" data-target="#shop" class="collapsed">
				<a href="#"><i class="fa fa-shopping-cart fa-lg"></i> Shop Manager <span class="arrow"></span></a>
			</li>  
			<ul class="sub-menu collapse" id="shop">
				<li><b>ACTIONS</b></li>
				<li><a href="">Add product</a></li>
				<li><b>TOOLS</b></li>
				<li><a href="https://www.minegrech.com/housekeeping/building/list">View Products</a></li>
			</ul>
			<li>
				<a href="#">
					<i class="fa fa-bar-chart fa-lg"></i> Statistics
				</a>
			</li>
			<li>
				<a href="https://www.minegrech.com/housekeeping/users">
					<i class="fa fa-users fa-lg"></i> Users
				</a>
			</li>
			<li>
				<a href="https://www.minegrech.com/housekeeping/logs">
					<i class="fa fa-keyboard-o fa-lg"></i> HK Logs
				</a>
			</li>
		</ul>
	</div>
</div>
</div>
