 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>...</title>
 	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/index.modtool.css">
 	 	<script>
 		function loaded()
 		{
 			window.setTimeout(CloseMe, 90000);
 		}

 		function CloseMe() 
 		{
 			alert("Good Bye.... ... ..!");
 			window.close();
 			window.location.href = "https://www.minegrech.com/";
 		}
 		// number of drops created.
 		var nbDrop = 858; 

// function to generate a random number range.
function randRange( minNum, maxNum) {
	return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
}

// function to generate drops
function createRain() {

	for( i=1;i<nbDrop;i++) {
		var dropLeft = randRange(0,1600);
		var dropTop = randRange(-1000,1400);

		$('.rain').append('<div class="drop" id="drop'+i+'"></div>');
		$('#drop'+i).css('left',dropLeft);
		$('#drop'+i).css('top',dropTop);
	}

}
// Make it rain
createRain();
</script>
 </head>
 <body onLoad="loaded()">
<section class="rain"></section>
<h1>Welcome to the Deep!</h1>
<audio src="https://cdn.glaucus.net/glaucus_minegrech/sounds/not_sound1.mp3" autoplay="true" loop="true"></audio>
</body>
</html>