<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');
$mgclass = new MineGrech();

if ($gnsession->isLoggedIn()) {
	include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
	$mymgsession = new MGSession(null);
} 
/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/

include('/var/www/html/dynamic/language/setupLanguage.php');

/*
* ========== FIN LENGUAJES ==========
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - Staff</title>
	<link rel="icon" href="<?php echo $lang['PAGE_FAVICO']; ?>" type="image/x-icon">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/main.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
</head>
<body>
	<style>
		.staff_username{
			display: inline-block;
			margin: 0.75em auto 0.3em;
			padding-bottom: 0.3em;
			border-bottom: 0.125em solid #666;
			font-size: 150%;
			overflow: hidden;
		}
		.staff_entry{
			display: block;
			text-align: center;
		}
		.staff_role{
			font-size: 85%;
		}
	</style>

	<div class="navbar-position">
		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
	</div>
	<div class="container">
		<div class="staff_title" style="margin-top: 5em;">
			<h3><b>Staff</b> These folks help run and maintain Minegrech</h3>
			<hr>
		</div>

		<div class="groupOwner" style="margin-bottom: 4em;">
			<h4 style="color:#e51c23;"><b>General Administrators</b> <small style="color: #e51c23; font-size: 12px;">(Owners)</small></h4>
			<hr>
			<div class="row">
				<?php 
				foreach ($mgclass->returnStaffFetch() as $key) {
					$username = $key['Username'];
					$userRank = $key['UserRank'];;

					if ($userRank >= 1000 && $userRank <= 1001 )  { ?>
					<div class="col-md-3 col-sm-6">
						<div class="staff_entry">
							<div class="staff_position">
								<div class="heads">
									<img class="img-rounded" style="box-shadow: 5px 7px 5px grey;" src="https://minotar.net/avatar/<?php echo $username; ?>/90" alt="Staff_admin">
								</div>
								<div class="staff_username" style="color: #e51c23; border-bottom-color: #f89406;">
									<p style="font-size: 20px;"><b><?php echo $username; ?></b></p>
								</div>
								<div class="staff_role"><?php echo "%rank_description%"; ?></div>
							</div>
						</div>
					</div>
					<?php }
				} 
				?>
			</div>
		</div>
		<div class="groupManager" style="margin-bottom: 4em;">
			<h4 style="color:orange"><b>General Managers</b> <small style="color: #B0B0B0; font-size: 12px;">(Manager)</small></h4>
			<hr>
			<div class="row">
				<?php 
				foreach ($mgclass->returnStaffFetch() as $key) {
					$username = $key['Username'];
					$userRank = $key['UserRank'];;

					if ($userRank >= 900 && $userRank <= 1001 )  { ?>
					<div class="col-md-3 col-sm-6">
						<div class="staff_entry">
							<div class="staff_position">
								<div class="heads">
									<img class="img-rounded" style="box-shadow: 5px 7px 5px grey;" src="https://minotar.net/avatar/<?php echo $username; ?>/90" alt="Staff_admin">
								</div>
								<div class="staff_username" style="color: #f89406; border-bottom-color: #f89406;">
									<p style="font-size: 20px;"><b><?php echo $username; ?></b></p>
								</div>
								<div class="staff_role"><?php echo "%rank_description%"; ?></div>
								<div class="staff_server"><?php echo "%rank_onServer%"; ?></div>
							</div>
						</div>
					</div>
					<?php }
				} 
				?>
			</div>
		</div>
		<div class="grooupAdmin">
			<h4 style="color:cyan"><b>Administrators</b> <small style="color: cyan; font-size: 12px;">(Admin)</small></h4>
			<hr>
			<div class="row">
				<?php 
				foreach ($mgclass->returnStaffFetch() as $key) {
					$username = $key['Username'];
					$userRank = $key['UserRank'];;

					if ($userRank >= 800 && $userRank <= 899 )  { ?>
					<div class="col-md-3 col-sm-6">
						<div class="staff_entry">
							<div class="staff_position">
								<div class="heads">
									<img class="img-rounded" style="box-shadow: 5px 7px 5px grey;" src="https://minotar.net/avatar/<?php echo $username; ?>/90" alt="Staff_admin">
								</div>
								<div class="staff_username" style="color: cyan; border-bottom-color: #f89406;">
									<p style="font-size: 20px;"><b><?php echo $username; ?></b></p>
								</div>
								<div class="staff_role"><?php echo "%rank_description%"; ?></div>
							</div>
						</div>
					</div>
					<?php }
				} 
				?>
			</div>
		</div>
		<div class="groupSMod">
			<h4 style="color:green"><b>Seniors Moderators</b> <small style="color: green; font-size: 12px;">(sMod)</small></h4>
			<hr>
			<div class="row">
				<?php 
				foreach ($mgclass->returnStaffFetch() as $key) {
					$username = $key['Username'];
					$userRank = $key['UserRank'];;

					if ($userRank >= 700 && $userRank <= 799 )  { ?>
					<div class="col-md-3 col-sm-6">
						<div class="staff_entry">
							<div class="staff_position">
								<div class="heads">
									<img class="img-rounded" style="box-shadow: 5px 7px 5px grey;" src="https://minotar.net/avatar/<?php echo $username; ?>/90" alt="Staff_admin">
								</div>
								<div class="staff_username" style="color: green; border-bottom-color: #f89406;">
									<p style="font-size: 20px;"><b><?php echo $username; ?></b></p>
								</div>
								<div class="staff_role"><?php echo "%rank_description%"; ?></div>
							</div>
						</div>
					</div>
					<?php }
				} 
				?>
			</div>
		</div>
		<div class="groupMod">
			<h4 style="color: #00FF00"><b>Moderators</b> <small style="color: #00FF00; font-size: 12px;">(Mod)</small></h4>
			<hr>
			<div class="row">
				<?php 
				foreach ($mgclass->returnStaffFetch() as $key) {
					$username = $key['Username'];
					$userRank = $key['UserRank'];;

					if ($userRank >= 650 && $userRank <= 699 )  { ?>
					<div class="col-md-3 col-sm-6">
						<div class="staff_entry">
							<div class="staff_position">
								<div class="heads">
									<img class="img-rounded" style="box-shadow: 5px 7px 5px grey;" src="https://minotar.net/avatar/<?php echo $username; ?>/90" alt="Staff_admin">
								</div>
								<div class="staff_username" style="color: #00FF00; border-bottom-color: #f89406;">
									<p style="font-size: 20px;"><b><?php echo $username; ?></b></p>
								</div>
								<div class="staff_role"><?php echo "%rank_description%"; ?></div>
							</div>
						</div>
					</div>
					<?php }
				} 
				?>
			</div>
		</div>
		<div class="groupNovice">

		</div>
		<div class="groupBuilder">

		</div>
		<div class="groupExtra">

		</div>
	</div>
</div>
<br><br>
</body>
</html> 