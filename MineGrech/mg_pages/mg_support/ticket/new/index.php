<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');


if ($gnsession->isLoggedIn()) {
	$mymgsession = new MGSession(null);
}else{
	header("Location: https://minegrech.com/login");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - Support</title>
	<link rel="icon" href="<?php echo $lang['PAGE_FAVICO']; ?>" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/index.profile.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
	$(document).ready(function(){ 
			$('#characterLeft').text('16 characters left');
			$('#comment').keyup(function () {
				var max = 500;
				var len = $(this).val().length;

				if (len >= max) {
					$('#characterLeft').text('You have reached the limit');
					$('#characterLeft').addClass('red');
					if (len > max) { 
						var elem = document.getElementById('btnSubmit');
						elem.parentNode.removeChild(elem);
						$('#characterLeft').text('You trying to put more letters of the allowed thing, reloads the page.');
					}
				} 
				else {
					var ch = max - len;
					var min = len >= 10;
					$('#characterLeft').text(ch + ' characters left');
					if (min) {$('#btnSubmit').prop("disabled", false);}else{$('#btnSubmit').prop("disabled", true);}
					if (min) {$('#btnSubmit').removeClass('disabled');}else{$('#btnSubmit').addClass('disabled');}
					$('#characterLeft').removeClass('red');            
				}
			});    
		});
</script>
</head>
<body>
	<div class="navbar-position">
		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
	</div>
	<div class="container" style="margin-top: 5em;">
		<div class="ticket_support">
			<div class="row">
				<form action="https://www.minegrech.com/support/ticket/update/" method="POST">
					<h3><b>OPEN A NEW TICKET</b></h3>
					<br>
					<div class="col-md-6">
						<div class="form-group">
						<label for="sel1">My problem is:</label>
							<select class="form-control" name="problem" id="sel1">
								<option value="1">I have a problem with my website account.</option>
								<option value="2">I have a problem with my account on the server.</option>
								<option value="3">I want to report a bug / glitch.</option>
								<option value="4">I want information about the premium purchase.</option>
								<option value="5">I want to deliberate my last punishment.</option>
								<option value="6">Other Reason</option>
							</select>
						</div>
						<div class="form-group">
							<label for="usr">Subject</label>
							<input required type="text" class="form-control" name="subject" id="subject">
						</div>
						<div class="form-group">
							<label for="comment">Comment:</label>
							<textarea required style="resize: none;" class="form-control" rows="5" name="comment" id="comment"></textarea>
							<span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>
						</div>
					</div>	
					<div class="col-md-6">
						<div class="checkbox">
							<h4>Ticket Options</h4>
							<div class="checkbox">
								<label><input type="checkbox" name="priority" value="1"> My ticket is priority</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" name="superior" value="1"> My ticket will be sent to a superior</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" name="personinfo" value="1"> My ticket contain personal information</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" name="downloadble" value="1"> My ticket contain downloadble material</label>
							</div>
							<div class="form-group">
								<label for="usr">Tests:</label>
								<input type="text" class="form-control" placeholder="Links, videos." name="tests" id="test">
							</div>
							<input type="submit" name="submit-btn" id="btnSubmit" disabled value="CREATE TICKET" class="btn btn-block btn-primary">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>