

 <?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/");
	return;
}else{
	include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
	$mymgsession = new MGSession(null);
}

include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');
$mgclass = new MineGrech();

$problemtype 	= $_POST['problem'];
$subject 		= $_POST['subject'];
$comment 		= $_POST['comment'];
$priority 		= $_POST['priority'];
$superior 		= $_POST['superior'];
$personinfo		= $_POST['personinfo'];
$downloadble 	= $_POST['downloadble'];
$test 			= $_POST['tests'];

if (isset($_POST['submit-btn'])) {
	if ($problemtype>=1 && $problemtype<=6) {
		if (is_null($priority)) {
			$priority = 0;
		}
		if (is_null($superior)) {
			$superior = 0;
		}
		if (is_null($personinfo)) {
			$personinfo = 0;
		}
		if (is_null($downloadble)) {
			$downloadble = 0;
		}
			$guid = $mymgsession->getGUID();
			$user = $mymgsession->getName();
			$mgclass->createNewTicket($problemtype, $subject, $comment, $priority, $superior, $personinfo, $downloadble, $test, $guid, $user);
			header("Location: https://www.minegrech.com/edit/tickets");
	}else{
		header("Location: https://www.minegrech.com/support/ticket/");
	}
}else{
	header("Location: https://www.minegrech.com/support/ticket/");
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - <?php echo $lang['PAGE_REGTITLE']; ?></title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<link rel="stylesheet" href="../../assets/styles/index.reg.css">
</head>
<body>
	<center>
		<div class="mensaje-error">
			<img src="https://cdn.glaucus.net/img/logo2.png" style="width: 400px; margin-right:15px;">	
		</div>
		<img src="https://cdn.glaucus.net/img/Loading1.gif" style="height: 200px;">	
	</center>
	<style>
		center {
			top: calc(50% - 250px);
			position: absolute;
			left: calc(50% - 207px);
		}
		body{
			background-color: #F5F5F5;
		}
		.ajust{
			margin-top:10em;
		}
	</style>
</body>
</html>  