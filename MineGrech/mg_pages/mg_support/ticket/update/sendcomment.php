 <?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');

$mgclass = new MineGrech();


if ($gnsession->isLoggedIn()) {
	$mymgsession = new MGSession(null);
}else{
	header("Location: https://minegrech.com/login");
}


$tid = $_GET['tid'];
$usr = $mymgsession->getName();
$uguid = $_GET['guid'];
$type = 0;

if (isset($_POST['comment-submit'])) {
	if (isset($_GET['tid']) && isset($_GET['guid'])) {
		$comment = $_POST['comment'];
		$mgclass->sendComment($type, $tid, $uguid, $usr, $comment);
		header("Location: https://www.minegrech.com/edit/tickets/view?tid=$tid&myguid=$uguid");
	}else{
		header("Location: https://www.minegrech.com/edit/tickets/view?tid=$tid&myguid=$uguid");
	}
}else{
	header("Location: https://www.minegrech.com/edit/tickets/view?tid=$tid&myguid=$uguid");
}
?>

