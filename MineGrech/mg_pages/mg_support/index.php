<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

if ($gnsession->isLoggedIn()) {
	include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
	$mymgsession = new MGSession(null);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - Support</title>
	<link rel="icon" href="https://cdn.glaucus.net/img/favicon.png" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/index.profile.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$.c0okieBar({
			});
		});
	</script>
</head>
<body>
	<div class="navbar-position">
		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
	</div>
	<div class="container" style="margin-top: 4em;">
		<div class="support-wrapepr">
			<div class="select-type">
				<center>
					<div class="row">
						<div class="col-md-6">
							<div class="title">
								<h3>Appeal Your Ban</h3>
							</div>
							<div class="text">
								<p>Appeals exist in order to catch mistakes, not to argue about the rules, or your personal virtues. <br> If you feel that your punishment was wrong and contains evidence, open an appeal case.</p>
							</div>
							<div class="button-2">
								<?php if ($gnsession->isLoggedIn()) {echo '<a class="btn btn-primary btn-block" href="https://www.minegrech.com/appeal">APPEAL  BAN</a>';}
								else{echo '<a class="btn btn-primary btn-block" href="https://www.minegrech.com/login?appealnew">LOGIN</a>';} ?>

							</div>
						</div>
						<div class="col-md-6">
							<div class="title">
							<h3>Business</h3>
							</div>
							<div class="text">
								<p>If you have questions, problems, you want to report an error, etc. You can do this through a support ticket. You should not appeal your punishment by this method.</p>
							</div>
							<div class="button-2">
								<?php if ($gnsession->isLoggedIn()) {echo '<a class="btn btn-primary btn-block" href="https://www.minegrech.com/support/ticket/new">OPEN TICKET</a>';}
								else{echo '<a class="btn btn-primary btn-block" href="https://www.minegrech.com/login?newticket">LOGIN</a>';} ?>
							</div>
						</div>
						<div class="col-md-12">
							<div class="title">
								<h3>Support Ticket</h3>
							</div>
							<div class="text">
								<p>If you have questions, problems, you want to report an error, etc. You can do this through a support ticket. You should not appeal your punishment by this method.</p>
							</div>
							<div class="button-2">
								<?php if ($gnsession->isLoggedIn()) {echo '<a class="btn btn-primary btn-block" href="https://www.minegrech.com/support/ticket/new">OPEN TICKET</a> <a class="btn btn-primary btn-block" href="https://www.minegrech.com/edit/tickets">My Ticket List</a>';}
								else{echo '<a class="btn btn-primary btn-block" href="https://www.minegrech.com/login">LOGIN</a>';} ?>
								<br>
								<?php if (!$gnsession->isLoggedIn()) {echo '<span class="label label-danger">Please login to open tickets, get bussines email and anothers actions.</span>';} ?>
							</div>
						</div>
					</div>
				</center>
			</div>
		</div>
	</div>	
</body>
</html>