<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
global $gnsession;
global $mgsession;
global $mgclass;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/");
	return;
}else{
	include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
	$mymgsession = new MGSession(null);
}

include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');
$mgclass = new MineGrech();

$cookie_name = "mg_profile";

if (isset($_POST['checksubmit'])) {
	$user = $_GET['updatenewusername'];
	$replaced = array("&", "$", "§", "'", "`", "´", '"', "%", "-", ";", "^", "nurio" ,"banper", "owner", "staff", "ceo", "Nurio", "Banper", "NURIO", "BanPer", "á", "é", "í", "ó" ,"ú", "à", "è", "ì", "ò" , "ù", "ç", "·", "Á", "É", "Í", "Ó" ,"Ú", "À", "È", "Ì", "Ò" , "Ù",  "и", "т");
	$allow = array("(", ")", "_", "[", "]", ".", ":");
	$ustring = str_replace($replaced, '', preg_replace('/\s+/', '_', $user));


	if (ctype_alnum(str_replace($allow, '', $ustring))) {
		$llamado = $mgclass->changeName($mymgsession->getGUID(), $ustring);
		if ($llamado == 400) {
			$cookie_error = "validation_profileuser";
			setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
			header("Location: https://minegrech.com/edit/profile?#userchange");
		}elseif ($llamado == 100) {
			$cookie_error = "validation_profileerror";
			setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
			header("Location: https://minegrech.com/edit/profile?#userchange");
		}elseif ($llamado == 500) {
			$cookie_error = "validation_profilesuccess";
			setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
			header("Location: https://minegrech.com/edit/profile?#userchange");
		}elseif ($llamado == 600) {
			$cookie_error = "validation_alreadyuse";
			setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
			header("Location: https://minegrech.com/edit/profile?#userchange");
		}
	}else{
		$cookie_error = "validation_profilestring";
		setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
		header("Location: https://minegrech.com/edit/profile?#userchange");
	}
}else{
	header("Location: https://minegrech.com/edit/profile");
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - <?php echo $lang['PAGE_REGTITLE']; ?></title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<link rel="stylesheet" href="../../assets/styles/index.reg.css">
</head>
<body>
	<center>
		<div class="mensaje-error">
			<img src="https://cdn.glaucus.net/img/logo2.png" style="width: 400px; margin-right:15px;">	
		</div>
		<img src="https://cdn.glaucus.net/img/Loading1.gif" style="height: 200px;">	
	</center>
	<style>
		center {
			top: calc(50% - 250px);
			position: absolute;
			left: calc(50% - 207px);
		}
		body{
			background-color: #F5F5F5;
		}
		.ajust{
			margin-top:10em;
		}
	</style>
</body>
</html> 