<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
$mymgsession = new MGSession(null);

include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');

$mgclass = new MineGrech();


if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/");
	return;
}

$cookie_name = "mg_profilepass";
if (isset($_POST['passSubmit'])) {
	$newpassword = $_POST['newpassword'];
	$repeatpass  = $_POST['rnewpassword'];
	$actualpass  = $_POST['cpassword'];

	if ($newpassword == $repeatpass) {
		if (strlen($newpassword) >= 3) {
			$llamado = $gnsession->changePassword($actualpass, $newpassword);
			if ($llamado == 200) {
				$cookie_error = "validation_success";
				setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
				header("Location: https://minegrech.com/edit/profile");
			}else if ($llamado == 402) {
				$cookie_error = "validation_notequalsreal";
				setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
				header("Location: https://minegrech.com/edit/profile");
			}else{
				$cookie_error = "validation_default";
				setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
				header("Location: https://minegrech.com/edit/profile");
			}
		}else{
			$cookie_error = "validation_min";
			setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
			header("Location: https://minegrech.com/edit/profile");
		}
	}else{
		$cookie_error = "validation_notequals";
		setcookie($cookie_name, $cookie_error, time()+6, '/', '.minegrech.com', 1);
		header("Location: https://minegrech.com/edit/profile");
	}
}else{
	header("Location: https://www.minegrech.com/edit/profile");
}



?> 
