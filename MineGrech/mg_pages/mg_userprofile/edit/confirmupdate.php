<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
global $gnsession;
global $mgsession;
global $mgclass;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');

$mgclass = new MineGrech();

if ($gnsession->isLoggedIn()) {
	$mymgsession = new MGSession(null);
}else{
	header("Location: https://www.minegrech.com/login");
}


if(!isset($_REQUEST['newusername'])){
	header("Location: https://www.minegrech.com/edit/profile");
}


if (!isset($_POST['checksubmitconfirm'])) {
	header("Location: https://minegrech.com/edit/profile");
}

$thename = $_POST['newusername'];


?>
<?php $filters = array("&", "$", "§", "'", "`", "´", '"', "%", "-", ";", "^"); ?>
<?php $allow = array("(", ")"); ?>
<?php $disallow = array("&", "$", "§", "'", "`", "´", '"', "%", "-", ";", "^", "nurio" ,"banper", "owner", "staff", "ceo", "Nurio", "Banper", "NURIO", "BanPer", "á", "é", "í", "ó" ,"ú", "à", "è", "ì", "ò" , "ù", "ç", "·", "Á", "É", "Í", "Ó" ,"Ú", "À", "È", "Ì", "Ò" , "Ù", "и", "т"); ?>
<?php $ustring = str_replace($disallow, '', preg_replace('/\s+/', '_', $thename));  ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - Confirm Update</title>
	<link rel="icon" href="<?php echo $lang['PAGE_FAVICO']; ?>" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/index.profile.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="navbar-position">
		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
	</div>
	<div class="container" style="margin-top: 5em;">
		<div class="row">
			<div class="col-md-4">
				<h3>Update Confirmation</h3>
				<p>Please follow the protocol of the usernames. You must follow all the rules, otherwise you can make a punishment or prohibition that you can make name changes in a future or restricting other uses of our website.</p>
				<p><b>Some Rules</b> <br>You can not exceed 16 characters.<br>
					You can not contain signs indicated in "Filters".<br>
					You can not contain a name similar to any staff.<br>
					You should not imitate celebrity names. In the case of this it is necessary a verification by the administration. <br> <b>Technical specifications</b> <br>In case of an error in setting your new username <i>(I have been wrong in uppercase / lowercase or a sign, letter, character, number in wrong position)</i> is under your commitment, you had the possibility to correct your new username . Only exceptional cases occur when the SYSTEM fails. <i>(All system error logs are saved). (We may consider system errors: UTF-8 signs or other character encoding that are not in our filters and need to be changed or added to our rules.)</i></p>
				</div>
				<div class="col-md-4">
					<h4>Your new username.</h4>
					<p>Remember that by containing special characters not allowed according to our filters, these are replaced by "_".</p>
					<p>Confirm that this <b><?php echo $thename; ?></b> is your new username?</p>
					<p>New Username: <?php echo " <b>".$thename."</b> with <b>".strlen($thename)."</b> characters"; ?></p>
					<p>New Username filtered: <?php echo " <b>".$ustring."</b> with <b>".strlen($ustring)."</b> characters"; ?></p>
					<form action="https://www.minegrech.com/edit/update/?updatenewusername=<?php echo "$ustring"; ?>" method="POST">
						<div class="form-group">
							<label for="updatenewusername">Username:</label>
							<input type="text" name="updatenewusername" class="form-control" id="updatenewusername" disabled value="<?php echo $ustring; ?>">
						</div>
						<input type="submit" class="btn btn-primary btn-block" name="checksubmit" value="CONFIRM, New username will be <?php echo $ustring; ?>">
					</form>
					<button data-toggle="modal" data-target="#changename" class="btn btn-block btn-warning">CHANGE</button>
				</div>
				<div class="col-md-4">
					<h4>Filters</h4>
					<p><b>Any unusual character leads to a sanction.</b></p>
					<p style="color: red;">Verify that your username does <b>NOT</b> contain any digits like these: </p>
					<p style="font-size: 20px;">
						<?php if (ctype_alnum(str_replace($allow, '', $ustring))): ?>
							<p>This is a valid username, but is filtered.</p>
						<?php else: ?>
							<p>This name is not valid.</p>
						<?php endif; ?>
						<?php foreach ($filters as $key => $value) {
							echo " (".$value.") ";
						} ?>
					</p>
					<p style="font-size: 15px;">Your name with the filters looks like this: <b><?php echo $ustring; ?></b></p>
					<p>If it has any character like the one already mentioned, it will be replaced by a underscore ( _ )</p>
				</div>
			</div>
		</div>
		<!-- Modal -->
		<div id="changename" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Change your username</h4>
					</div>
					<div class="modal-body">
						<form action="https://minegrech.com/edit/update/confirm/" method="POST">
							<div class="form-group">
								<label for="usr">Username:</label>
								<input type="text" name="newusername" maxlength="16" minlength="3" class="form-control" id="newusername">
							</div>
							<input type="submit" class="btn btn-success btn-block" name="checksubmitconfirm" value="CHANGE">
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
	</body>
	</html>