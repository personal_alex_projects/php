<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/");
	return;
}else{
	include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
	$mymgsession = new MGSession(null);
}

include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');
$mgclass = new MineGrech();

if (isset($_POST['personalsubmit'])) {
 if (is_numeric($_POST['age']) && is_numeric($_POST['gender'])) {
 		$mgclass->updatePersonalInfo($mymgsession->getGUID(), $_POST['age'],  $_POST['gender']);
	header("Location: https://minegrech.com/edit/profile");
	$cookie_error = "validation_profilesuccess";
	setcookie("mg_profilepersonal", $cookie_error, time()+6, '/', '.minegrech.com', 1);
 }
}else{
	header("Location: https://minegrech.com/edit/profile");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - <?php echo $lang['PAGE_REGTITLE']; ?></title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<link rel="stylesheet" href="../../assets/styles/index.reg.css">
</head>
<body>
	<center>
		<div class="mensaje-error">
			<img src="https://cdn.glaucus.net/img/logo2.png" style="width: 400px; margin-right:15px;">	
		</div>
		<img src="https://cdn.glaucus.net/img/Loading1.gif" style="height: 200px;">	
	</center>
	<style>
		center {
			top: calc(50% - 250px);
			position: absolute;
			left: calc(50% - 207px);
		}
		body{
			background-color: #F5F5F5;
		}
		.ajust{
			margin-top:10em;
		}
	</style>
</body>
</html>   