<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/");
	return;
}else{
	include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
	$mymgsession = new MGSession(null);
}

include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');
$mgclass = new MineGrech();

$cookie_name = "mg_profile";

if (isset($_POST['socialsubmit'])) {

	$twitter 	= $_POST['utwitter'];
	$skype 	 	= $_POST['uskype'];
	$steam 		= $_POST['usteam'];
	$facebook	= $_POST['uface'];
	$youtube	= $_POST['uyoutube'];
	$reddit		= $_POST['ureddit'];

	$regtwitter = preg_replace('/\s+/', '', $twitter);
	$regskype = preg_replace('/\s+/', '', $skype);
	$regsteam = preg_replace('/\s+/', '', $steam);
	$regfacebook = preg_replace('/\s+/', '', $facebook);
	$regyoutube = preg_replace('/\s+/', '', $youtube);
	$regreddit = preg_replace('/\s+/', '', $reddit);

	$mgclass->updateSocialNetworks($mymgsession->getGUID(), $regtwitter, $regskype, $regsteam, $regfacebook, $regyoutube, $regreddit);
	header("Location: https://minegrech.com/edit/profile");
	$cookie_error = "validation_profilesuccess";
	setcookie("mg_profile", $cookie_error, time()+6, '/', '.minegrech.com', 1);

	
}else{
	header("Location: https://minegrech.com/edit/profile");
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - <?php echo $lang['PAGE_REGTITLE']; ?></title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<link rel="stylesheet" href="../../assets/styles/index.reg.css">
</head>
<body>
	<center>
		<div class="mensaje-error">
			<img src="https://cdn.glaucus.net/img/logo2.png" style="width: 400px; margin-right:15px;">	
		</div>
		<img src="https://cdn.glaucus.net/img/Loading1.gif" style="height: 200px;">	
	</center>
	<style>
		center {
			top: calc(50% - 250px);
			position: absolute;
			left: calc(50% - 207px);
		}
		body{
			background-color: #F5F5F5;
		}
		.ajust{
			margin-top:10em;
		}
	</style>
</body>
</html>  