
<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
$mymgsession = new MGSession(null);

include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');

$mgclass = new MineGrech();


if (!$gnsession->isLoggedIn()) {
	header("Location: https://minegrech.com/login");
	return;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - Edit Profile</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/global.hk.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/image-picker.css">
	<script src="https://use.fontawesome.com/6dbaa26fe2.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdn.glaucus.net/glaucus_minegrech/js/image-picker.js"></script>
	<script>
		$(document).ready(function(){ 
			$('#characterLeft').text('16 characters left');
			$('#newusername').keyup(function () {
				var max = 16;
				var len = $(this).val().length;

				if (len >= max) {
					$('#characterLeft').text('You have reached the limit');
					$('#characterLeft').addClass('red');
					if (len > max) { 
						var elem = document.getElementById('btnSubmit');
						elem.parentNode.removeChild(elem);
						$('#characterLeft').text('You trying to put more letters of the allowed thing, reloads the page.');
					}
				} 
				else {
					var ch = max - len;
					var min = len >= 3;
					$('#characterLeft').text(ch + ' characters left');
					if (min) {$('#btnSubmit').prop("disabled", false);}else{$('#btnSubmit').prop("disabled", true);}
					if (min) {$('#btnSubmit').removeClass('disabled');}else{$('#btnSubmit').addClass('disabled');}
					$('#characterLeft').removeClass('red');            
				}
			});    
		});

		$(document).ready(function () {
			$("#selectImage").imagepicker({
				hide_select: true
			});

			var $container = $('.image_picker_selector');
    // initialize
    $container.imagesLoaded(function () {
    	$container.masonry({
    		columnWidth: 30,
    		itemSelector: '.thumbnail'
    	});
    });
});
</script>
</head>
<body>
	<div class="navbar-position">
		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
	</div>
	<div class="container" style="margin-top: 6em;">
		<div class="profile-panel">
			<div class="row">
				<div class="col-md-4">
					<div class="title-config">
						<h3><b>Account</b></h3>
						<?php
						if (isset($_COOKIE['mg_profilepass'])) {
							switch ($_COOKIE['mg_profilepass']) {
								case 'validation_cpass':
								echo '<div class="error"><span class="label label-danger">Enter your currect password!</span></div>';
								break;
								case 'validation_pass':
								echo '<div class="error"><span class="label label-danger">Enter your new password an repeat!</span></div>';
								break;
								case 'validation_min':
								echo '<div class="error"><span class="label label-danger">The minimum password is 3 characters</span></div>';
								break;
								case 'validation_notequals':
								echo '<div class="error"><span class="label label-danger">The password dont not match!</span></div>';
								break;
								case 'validation_success':
								echo '<div class="error"><span class="label label-success">Password changed!</span></div>';
								break;
								case 'validation_notequalsreal':
								echo '<div class="error"><span class="label label-danger">Actual Password Invalid!</span></div>';
								break;
								default:
								echo '<div class="error"><span class="label label-danger">Database error</span></div>';
								break;
							}
						}
						?>	
						<hr>
					</div>
					<script>
						$(document).ready(function(){
							$("#newpass").focus(function(){
								this.type = "text";
							}).blur(function(){
								this.type = "password";
							})
						});
					</script>
					<div class="body-config">
						<form action="https://minegrech.com/edit/update/password" method="POST">
							<div class="form-group">
								<label for="newpass"><b>New Password:</b></label>
								<input type="password" name="newpassword" class="form-control" id="newpass">
							</div>
							<div class="form-group">
								<label for="rnewpass"><b>Repeat New Password:</b></label>
								<input type="password" name="rnewpassword" class="form-control" id="rnewpass">
							</div>
							<div class="form-group">
								<label for="newpass"><b>Actual Password</b></label>
								<input type="password" name="cpassword" class="form-control" id="newpass">
							</div>
							<input type="submit" name="passSubmit" class="btn btn-block btn-primary" value="Update">
						</form>
					</div>
				</div>
				<div class="col-md-4">
					<div class="title-config">
						<h3><b>Personal</b></h3>
						<?php
						if (isset($_COOKIE['mg_profilepersonal'])) {
							switch ($_COOKIE['mg_profilepersonal']) {
								case 'validation_profilesuccess':
								echo '<div class="error"><span class="label label-success">Values changed!</span></div>';
								break;
								default:
								echo '<div class="error"><span class="label label-danger">Database error</span></div>';
								break;
							}
						}
						?>	
						<hr>
					</div>
					<div class="body-config">
						<form action="https://www.minegrech.com/edit/update/personal" method="POST">
							<div class="form-group">
								<label for="gender"><b>Gender:</b></label>
								<select class="form-control" name="gender" id="gender">
									<?php 
									if ($mymgsession->getUserGender(1) == "0") {
										echo '<option value="0" selected>Male</option>';
										echo '<option value="1">Female</option>';
										echo '<option value="2">Not assigned</option>';
									}elseif ($mymgsession->getUserGender(1) == "1") {
										echo '<option value="0" >Male</option>';
										echo '<option value="1" selected>Female</option>';
										echo '<option value="2">Not assigned</option>';
									}elseif ($mymgsession->getUserGender(1) == "2") {
										echo '<option value="0" >Male</option>';
										echo '<option value="1" >Female</option>';
										echo '<option value="2" selected>Not assigned</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="age"><b>Age:</b></label>
								<select class="form-control" name="age" id="age">
									<option value="nulled2" disabled><b>Your privacy is our duty</b></option>
									<?php 
									switch ($mymgsession->getUserAge("Update")) {
										case -3:
										echo '<option value="-3" selected>+10 Years</option>';
										echo '<option value="-2">+15 Years</option>';
										echo '<option value="-1">+18 Years</option>';
										echo '<option value="0">+20 Years</option>';
										break;
										case -2:
										echo '<option value="-3">+10 Years</option>';
										echo '<option value="-2" selected>+15 Years</option>';
										echo '<option value="-1">+18 Years</option>';
										echo '<option value="0">+20 Years</option>';
										break;
										case -1:
										echo '<option value="-3">+10 Years</option>';
										echo '<option value="-2">+15 Years</option>';
										echo '<option value="-1" selected>+18 Years</option>';
										echo '<option value="0">+20 Years</option>';
										break;
										case 0:
										echo '<option value="-3">+10 Years</option>';
										echo '<option value="-2">+15 Years</option>';
										echo '<option value="-1">+18 Years</option>';
										echo '<option value="0"  selected>+20 Years</option>';
										break;
										default:
										echo '<option value="-3">+10 Years</option>';
										echo '<option value="-2">+15 Years</option>';
										echo '<option value="-1">+18 Years</option>';
										echo '<option value="0">+20 Years</option>';
										break;
									}
									?>
									<option value="nulled" disabled></option>
									<option value="nulled2" disabled><b>Normal Years</b></option>
									<?php $x = 5;
									while ($x <= 50) {
										if ($mymgsession->getUserAge("Update") == $x) {
											echo '<option value="'.$x.'" selected>'.$x.' Years</option>';
										}else{
											echo '<option value="'.$x.'">'.$x.' Years</option>';
										}

										$x++;
									} ?>
								</select>
							</div>
							<div class="form-group">
								<label for="gender"><b>Occupation:</b></label>
								<select disabled class="form-control" name="gender" id="gender">
									<option value="null" selected disabled>Select a Occupation</option>
								</select>
							</div>
							<input name="personalsubmit" type="submit" class="btn btn-block btn-primary" value="Update">
						</form>
					</div>
				</div>
				<div class="col-md-4">
					<div class="title-config">
						<h3><b>Parameters</b></h3>
						<hr>
					</div>
					<div class="body-config">
						<form action="">
							<div class="form-group">
								<b> Regular Parameters</b> <br>
								<label style="margin-left: 2.7%;" class="checkbox-inline"><input type="checkbox" name="news[]" value=""> Receive Newsletter</label>
								<label class="checkbox-inline"><input type="checkbox" name="privacy[]" value=""> Notify me in server the new blog posted</label>
							</div>
							<div class="form-group">
								<b> Privacy Parameters</b> <br>
								<label style="margin-left: 2.7%;" class="checkbox-inline"><input type="checkbox" name="privacy[]" value=""> Show the server I'm playing on in my profile</label>
								<label disabled class="checkbox-inline"><input type="checkbox" name="privacy[]" value=""> Show 'My Last Connection' on in my profile</label>
								<label class="checkbox-inline"><input type="checkbox" name="privacy[]" value=""> Show 'My Inventory Rewards' on in my profile</label>
								<label class="checkbox-inline"><input type="checkbox" name="privacy[]" value=""> Show 'My Punishments' on in my profile</label>
							</div>
							<div class="form-group">
							</div>
							<input id="userchange" style="margin-top: 2.5em;" type="submit" disabled class="btn btn-block btn-primary" value="Update">
						</form>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="profile-change-username">
			<div class="row">
				<div class="col-md-6">
					<h3><b>Change username</b></h3>
					<h5>You can change: <b><?php echo $mymgsession->getTotalChangeNameLeft(); ?></b> times</h5>
					<?php if (!$mymgsession->getTotalChangeNameLeft() == "0"): ?>
						<p>You have <b><?php echo $mymgsession->getFreeNameChangeLeft(); ?></b> free changes and <b><?php echo $mymgsession->getNameChangeLeft(); ?></b> normal changes.</p>
					<?php else: ?>	
						<p>You don't have any change available. <a href="#">Buy More Changes</a></p>
					<?php endif; ?>

					<p>See you <a data-toggle="modal" data-target="#history">history</a> here <br> Your GUID: <?php echo $mymgsession->getGUID(); ?></p>
				</div>
				<div class="col-md-6">
					<form action="https://www.minegrech.com/edit/update/confirm/" method="POST" style="margin-top: 3em;">
						<div class="form-group">
							<?php
							if (isset($_COOKIE['mg_profile'])) {
								switch ($_COOKIE['mg_profile']) {
									case 'validation_profileuser':
									echo '<div class="error"><span class="label label-danger">The username can´t be the same.</span></div>';
									break;
									case 'validation_profileerror':
									echo '<div class="error"><span class="label label-danger">Username must be different from current.</span></div>';			
									break;
									case 'validation_profilesuccess':
									echo '<div class="error"><span class="label label-success">The username has been changed!</span></div>';
									break;
									case 'validation_profilespace':
									echo '<div class="error"><span class="label label-danger">The username can´t contains spaces</span></div>';
									break;
									case 'validation_alreadyuse':
									echo '<div class="error"><span class="label label-danger">Username already use. Try Again</span></div>';
									break;
									case 'validation_profilestring':
									echo '<div class="error"><span class="label label-danger">The username is invalid!</span></div>';
									break;
									default:
									echo '<div class="error"><span class="label label-danger">Database error</span></div>';
									break;
								}
							}
							?>	
							<label for="newusername"><b>New Username:</b></label>
							<?php if ($mymgsession->getTotalChangeNameLeft() == 0): ?>
								<input type="text" maxlength="16" name="newusername" disabled placeholder="You Actual username is <?php echo $mymgsession->getName(); ?>" class="form-control" id="newusername">
							<?php else: ?>
								<input type="text" maxlength="16" name="newusername" placeholder="You Actual username is <?php echo $mymgsession->getName(); ?>" class="form-control" id="newusername">
							<?php endif; ?>

							<span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>
						</div>
						<input type="submit" name="checksubmitconfirm" id="btnSubmit" onclick="userInfo()" class="btn btn-primary btn-block" disabled value="update">
					</form>
				</div>
			</div>
		</div>
		<hr>
		<?php include '/var/www/MineGrech/dynamic/content/mg_profile/edit.socials.php'; ?>
		<hr>
		<?php if ($mymgsession->getRank() >= 1000): ?>
			<div class="background-selecter">
				<div class="row">
					<div class="col-md-4">
						<h3>Background Selector</h3>
						<p>You can select a background for your profile.</p>
					</div>
					<div class="col-md-8">
						<style>
							.image_picker_image {
								max-width: 130px;
								max-height: 130px;
								width: 200px;
								height: 200px;
								background-color: #FF0000;
							}
						</style>
						<form action="" method="post">
							<select id="selectImage" class="image-picker">
								<?php if ($mymgsession->getUserTheme() == 0): ?>
									<option data-img-src="https://cdn.glaucus.net/glaucus_minegrech/images/backgrounds/default.png" value="0" selected></option>
									<option data-img-src="https://cdn.glaucus.net/glaucus_minegrech/images/backgrounds/castle_black.jpg" value="1"></option>
									<option data-img-src="https://0.s3.envato.com/files/149997647/bacgrounds.jpg" data-img-alt="Page 2" value="2"></option>
									<option data-img-src="https://www.walldevil.com/wallpapers/a82/gothic-wallpaper-image-girl-industrial-cyber-best-desktop-wallpapers.jpg" data-img-alt="Page 3" value="3"></option>
								<?php elseif($mymgsession->getUserTheme() == 1): ?>
									<option data-img-src="https://cdn.glaucus.net/glaucus_minegrech/images/backgrounds/default.png" value="0"></option>
									<option data-img-src="https://cdn.glaucus.net/glaucus_minegrech/images/backgrounds/castle_black.jpg" selected value="1"></option>
									<option data-img-src="https://0.s3.envato.com/files/149997647/bacgrounds.jpg" data-img-alt="Page 2" value="2"></option>
									<option data-img-src="https://www.walldevil.com/wallpapers/a82/gothic-wallpaper-image-girl-industrial-cyber-best-desktop-wallpapers.jpg" data-img-alt="Page 3" value="3"></option>
								<?php endif; ?>
							</select>
							<input type="submit" class="btn btn-primary btn-block" value="UPDATE">
						</form>
					</div>
				</div>
			</div>
		<?php endif ?>
		<br> <br>
	</div>
</body>
</html> 

<!-- Modal -->
<div id="history" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">All username change.</h4>
			</div>
			<div class="modal-body">
				<table class="table table-stripped">
					<thead>
						<tr>
							<th>Detail</th>
							<th>Ago</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$count = $mgclass->getMyLogCount($mymgsession->getGUID(), 2);
						if ($count != 0) {
							$detail = $mgclass->getMyLog($mymgsession->getGUID());
							foreach ($detail as $key) {
								$textr = $key['text'];
								$micror = $key['AddedMills'];
								$typer = $key['type'];

								
									echo "<tr>";
									echo "<td>";
									echo $textr;
									echo "</td>";
									echo "<td>";
									echo $mgclass->timeToAGO($micror);
									echo "</td>";
									echo "</tr>";
								
							}
						} else{
							echo "<tr><td>You dont have any record!</td></tr>";
						}
						?>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>