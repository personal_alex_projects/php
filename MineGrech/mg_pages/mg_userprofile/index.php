<?php 
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

if ($gnsession->isLoggedIn()) {
	include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
	$mymgsession = new MGSession(null);
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - Find User</title>
	<link rel="icon" href="<?php echo $lang['PAGE_FAVICO']; ?>" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="navbar-position">
		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
	</div>
	<div class="container" style="margin-top: 4em;">
		<center>
			<div class="find-user">
				<form action="">
					<div class="form-group">
						<label for="finduser"><h3>Find User By GUID or USERNAME</h3></label>
						<input type="text" name="linked" class="form-control" id="finduser" placeholder="GUID or USERNAME">
						<br>
						<input type="submit" name="submit" class="btn btn-block btn-default" value="Find User">
					</div>
				</form>
				<?php 
				$link = $_GET['linked'];
				if (isset($_GET['submit'])) {
					header("Location: https://minegrech.com/profile/$link");
				}
				

				?>
			</div>
		</center>
	</div>
</body>
</html>

