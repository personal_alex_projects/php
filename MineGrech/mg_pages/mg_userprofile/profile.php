<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
global $gnsession;
global $mgsession;
global $mgclass;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');

$mgclass = new MineGrech();


if (isset($_GET['username'])) {
	if (strpos($_GET['username'], 'GUID-') !== false) {
		$mgsession = new MGSession($_GET['username']);
	}else{
		$userGUID = $mgclass->getGUIDbyName($_GET['username']);
		$mgsession = new MGSession($userGUID);
	}
}else{
	if ($gnsession->isLoggedIn()) {
		$mgsession = new MGSession(null);
	}

}

if ($gnsession->isLoggedIn()) {
	$mymgsession = new MGSession(null);
}

include('/var/www/html/dynamic/classes/MineGrech/fullpvp.class.php');
$pvpclass = new FullPvP();

if(!isset($_GET['username'])){
	header("Location: https://www.minegrech.com");
}

if($mgsession->checkWorking()){
	header("Location: https://minegrech.com/profile/");
}

include('/var/www/MineGrech/dynamic/functions/setupRanks.php');
include('/var/www/MineGrech/dynamic/functions/setupThemes.php');

$rank = getRankName($mgsession->getRank());
$theme = getTheme($mgsession->getUserTheme());
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - <?php echo $mgsession->getName(); ?></title>
	<link rel="icon" href="<?php echo $lang['PAGE_FAVICO']; ?>" type="image/x-icon">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/index.profile.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(window).load(function(){
			$('#changnick').modal('show');
		});
	</script>
	<?php echo $theme; ?>
</head>
<body>
	<div class="navbar-position">
		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
	</div>
	<div class="container" style="margin-top: 4em;">
		<div class="row">
			<div class="col-md-2">
				<div class="personal_detail">
					<div class="img-container">
						<img class="img-thumbnail" src="https://www.minotar.net/avatar/<?php echo $mgsession->getName(); ?>/160" alt="_avatar">
					</div>
					<div class="align-center">
						<div class="mg-username">
							<h4 class="theme"><?php echo $mgsession->getName(); ?></h4>
						</div>
						<div class="mg-user-role">
							<?php echo $rank; ?>
						</div>
						<hr>
						<div class="mg-actions-btn">
							<div class="mg-actions-title">
								<h5 class="theme">Actions</h5>
							</div>
							<div class="mg-actions-buttons">
								<button class="btn btn-default btn-block disabled">Add Friend</button>
								<button id="themeLink" class="btn btn-default btn-block">Report Player</button>
								<button id="themeLink" class="btn btn-default btn-block">Report Issue</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-10">
				<div class="separation">
					<hr>
				</div>
				<div class="col-md-10">
					<div class="mg-title">
						<h4><b><?php echo $mgsession->getName(); ?></b>'s Account</h4>
					</div>
					<div id="well" class="well"  style="padding-bottom: 20em;">
						<div class="socialnetworks" style="margin-bottom: 13em;">
							<div class="col-md-4 col-sm-6">
								<div class="mg-profile">
									<div class="mg-stat">
										<div class="mg-profile-title">
											<p><b>Twitter</b></p>
										</div>
										
										<div class="mg-profile-value">
											<?php if (is_null($mgsession->getUserTwitter()) or empty($mgsession->getUserTwitter())): ?>
												<blockquote>
													<p>
														No data provided
													</p>
												</blockquote>
											<?php else: ?>
												<blockquote>
													<p>
														<a href="https://twitter.com/<?php echo $mgsession->getUserTwitter() ?>" target="_blank"><?php echo $mgsession->getUserTwitter() ?></a>
													</p>
												</blockquote>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6">
								<div class="mg-profile">
									<div class="mg-stat">
										<div class="mg-profile-title">
											<p><b>Facebook</b></p>
										</div>
										<div class="mg-profile-value">
											<?php if (is_null($mgsession->getUserFacebook()) or empty($mgsession->getUserFacebook())): ?>
												<blockquote>
													<p>
														No data provided
													</p>
												</blockquote>
											<?php else: ?>
												<blockquote>
													<p>
														<a href="https://facebook.com/<?php echo $mgsession->getUserFacebook(); ?>" target="_blank"><?php echo $mgsession->getUserFacebook(); ?></a>
													</p>
												</blockquote>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6">
								<div class="mg-profile">
									<div class="mg-stat">
										<div class="mg-profile-title">
											<p><b>Skype</b></p>
										</div>
										<div class="mg-profile-value">
											<?php if (is_null($mgsession->getUserSkype()) or empty($mgsession->getUserSkype())): ?>
												<blockquote>
													<p>
														No data provided
													</p>
												</blockquote>
											<?php else: ?>
												<blockquote>
													<p>
														<a href="skype:<?php echo $mgsession->getUserSkype(); ?>" target="_blank"><?php echo $mgsession->getUserSkype(); ?></a>
													</p>
												</blockquote>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
							

							<?php if (!is_null($mgsession->getUserYoutube())): ?>
								<div class="col-md-4 col-sm-6">
									<div class="mg-profile">
										<div class="mg-stat">
											<div class="mg-profile-title">
												<p><b>Youtube</b></p>
											</div>
											<div class="mg-profile-value">
												<blockquote>
													<p>
														<a href="https://youtube.com/<?php echo $mgsession->getUserYoutube(); ?>" target="_blank"><?php echo $mgsession->getUserYoutube(); ?></a>
													</p>
												</blockquote>
											</div>
										</div>
									</div>
								</div>
							<?php endif; ?>

							<?php if (!is_null($mgsession->getUserSteam())): ?>
								<div class="col-md-4 col-sm-6">
									<div class="mg-profile">
										<div class="mg-stat">
											<div class="mg-profile-title">
												<p><b>Steam</b></p>
											</div>
											<div class="mg-profile-value">
												<blockquote>
													<p>
														<a href="http://steamcommunity.com/id/<?php echo $mgsession->getUserSteam(); ?>" target="_blank"><?php echo $mgsession->getUserSteam(); ?></a>
													</p>
												</blockquote>
											</div>
										</div>
									</div>
								</div>
							<?php endif; ?>

							<?php if (!is_null($mgsession->getUserReddit())): ?>
								<div class="col-md-4 col-sm-6">
									<div class="mg-profile">
										<div class="mg-stat">
											<div class="mg-profile-title">
												<p><b>Reddit</b></p>
											</div>
											<div class="mg-profile-value">
												<blockquote>
													<p>
														<a href="https://www.reddit.com/user/<?php echo $mgsession->getUserReddit(); ?>" target="_blank"><?php echo $mgsession->getUserReddit(); ?></a>
													</p>
												</blockquote>
											</div>
										</div>
									</div>
								</div>
							<?php endif; ?>
						</div>
						<br>
						<hr>
						<div class="col-sm-6">
							<h5><b>Gender</b></h5>
							<pre><?php echo $mgsession->getUserGender(0); ?></pre>
						</div>
						<div class="col-sm-6">
							<h5><b>Age</b></h5>
							<pre><?php echo $mgsession->getUserAge(0); ?></pre>
						</div>
						<div class="col-sm-6">
							<h5><b>Country</b></h5>
							<pre>Working it</pre>
						</div>
						<div class="col-sm-6">
							<h5><b>Occupation</b></h5>
							<pre>Working it</pre>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="align-center">
						<div class="some-extra">
							<div class="some-title">
								<h5 class="header"><b>Quick Links</b></h5>
							</div>
							<div class="some-buttons">
								<?php if ($gnsession->isLoggedIn()): ?>
									<a id="themeLink" href="https://minegrech.com/edit/profile" class=" btn btn-default btn-block ">Edit My Profile</a>
								<?php else: ?>
									<a id="themeLink" href="https://minegrech.com/login" class="btn btn-primary btn-block  ">Login</a>
									<a id="themeLink" href="https://minegrech.com/register" class="btn btn-primary btn-block  ">Register</a>
								<?php endif; ?>
								<button id="themeLink" class=" btn btn-default btn-block">Forums</button>
								<button id="themeLink" class=" btn btn-default btn-block">Support</button>
							</div>
						</div>
						<?php
						if ($gnsession->isLoggedIn()) {
							if ($mymgsession->isOnline() && $mymgsession->getRank() > "500") {
								?>
								<hr>
								<div class="mod-actions">
									<div class="mod-actions-title">
										<h5 class="modtool-title"><b>Mod Tools</b></h5>
									</div>
									<div class="modtool-buttons">
										<a href="https://minegrech.com/housekeeping/view/player/<?php echo $mgsession->getName(); ?>" class="btn btn-warning btn-block">Deep Profile</a>
										<a href="" class="btn btn-warning btn-block">Punishment</a>
										<a href="" class="btn btn-warning btn-block">Player Record</a>
										<a href="https://www.minegrech.com/housekeeping/modtool/ban?guid=<?php echo $mgsession->getGUID(); ?>" class="btn btn-danger btn-block">Ban Player</a>
									</div>
								</div>
								<?php 
							} 
						}
						?>
					</div>
				</div>
			</div>
			<div class="col-md-10">
				<hr>
				<div id="well" class="well2">
					<h4>Stats</h4>
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#glob">Global</a></li>
						<li><a data-toggle="tab" href="#fullpvp">FullPvP</a></li>
						<li><a data-toggle="tab" href="#skywars">SkyWars (Casual)</a></li>
						<li><a data-toggle="tab" href="#swranked">SkyWars (Ranked)</a></li>
					</ul>

					<div class="tab-content">
						<div id="glob" class="tab-pane fade in active">
							<div class="row">
								<div class="col-md-6">
									<div class="title-row">
										<p style="margin-top:1em;"><b>General Stats</b></p>
									</div>
									<div class="stats firstdays">
										<h4>28 <small>days since first join</small></h4>
									</div>
									<div class="stats hourplayed">
										<h4>28 <small>hours played</small></h4>
									</div>
									<div class="stats serverchange">
										<h4>1547 <small>change server</small></h4>
									</div>
									<div class="stats serverchange">
										<h4>1547 <small>change server</small></h4>
									</div>
									<div class="stats serverchange">
										<h4>1547 <small>change server</small></h4>
									</div>
								</div>
								<div class="col-md-6">
									<div class="title-row">
										<p style="margin-top:1em;"><b>Server Stats</b></p>
									</div>
									<div class="stats #">
										<h4>547 <small>Global Kills</small></h4>
									</div>
									<div class="stats #">
										<h4>157 <small>Global Deaths</small></h4>
									</div>
									<div class="stats #">
										<h4>0.41 <small>Global KD</small></h4>
									</div>
									<div class="stats #">
										<h4>0.41 <small>Global KD</small></h4>
									</div>
									<div class="stats #">
										<h4>0.41 <small>Global KD</small></h4>
									</div>
								</div>
							</div>
						</div>
						<div id="fullpvp" class="tab-pane fade">
							<div class="row">
								<div class="col-md-6">
									<?php if (!is_null($pvpclass->getFullPvPKills())): ?>
										<div class="stats kills">
											<h4><?php echo $pvpclass->getFullPvPKills(); ?><small> Kills</small></h4>
										</div>
									<?php else: ?>
										<div class="stats kills">
											<h4>Kills <small>No data provided</small></h4>
										</div>
									<?php endif; ?>
									<?php if (!is_null($pvpclass->getFullPvPDeaths())): ?>
										<div class="stats deaths">
											<h4><?php echo $pvpclass->getFullPvPDeaths(); ?><small> Deaths</small></h4>
										</div>
									<?php else: ?>
										<div class="stats deaths">
											<h4>Deaths <small>No data provided</small></h4>
										</div>
									<?php endif; ?>

									<div class="stats damage">
										<h4>Damage PS <small>No data available</small></h4>
									</div>
									<div class="stats damager">
										<h4>Damager PS <small>No data available</small></h4>
									</div>
								</div>
								<div class="col-md-6">
									<?php if (!is_null($pvpclass->getFullPvPElo())): ?>
										<div class="stats elo">
											<h4><?php echo $pvpclass->getFullPvPElo(); ?><small> ELO Rating</small></h4>
										</div>
									<?php else: ?>
										<div class="stats elo">
											<h4>ELO Rating <small>No data provided</small></h4>
										</div>
									<?php endif; ?>
									<div class="stats elorank">
										<h4> 
										<?php 
											if ($pvpclass->getFullPvPElo() <= 999) {
												echo "Bronce I";
											}elseif($pvpclass->getFullPvPElo() >= 1000){
												echo "Bronce II";
											}elseif($pvpclass->getFullPvPElo() >= 3000){
												echo "Bronce III";
											}elseif($pvpclass->getFullPvPElo() >= 5000){
												echo "Bronce IV";
											}elseif($pvpclass->getFullPvPElo() >= 8000){
												echo "Bronce V";
											}elseif($pvpclass->getFullPvPElo() >= 13000){
												echo "Plata I";
											}elseif($pvpclass->getFullPvPElo() >= 15000){
												echo "Plata II";
											}elseif($pvpclass->getFullPvPElo() >= 18000){
												echo "Plata III";
											}elseif($pvpclass->getFullPvPElo() >= 20000){
												echo "Plata IV";
											}elseif($pvpclass->getFullPvPElo() >= 25000){
												echo "Plata V";
											}elseif($pvpclass->getFullPvPElo() >= 30000){
												echo "ORO I";
											}elseif($pvpclass->getFullPvPElo() >= 39000){
												echo "ORO II";
											}else{
												echo "Without clasification!";
											}
										 ?>
										<small>ELO Rank</small></h4>
									</div>
									<div class="stats elorankposition">
										<h4>ELO Rank Position <small>No data available</small></h4>
									</div>
								</div>
							</div>
						</div>
						<div id="skywars" class="tab-pane fade">
							<div class="row">
								<div class="col-md-6">
									<div class="stats firstdays">
										<h4>28 <small>days since first join</small></h4>
									</div>
									<div class="stats hourplayed">
										<h4>28 <small>hours asdplayed</small></h4>
									</div>
									<div class="stats serverchange">
										<h4>1547 <small>change server</small></h4>
									</div>
								</div>
								<div class="col-md-6">
									<div class="stats #">
										<h4>547 <small>lorem 1</small></h4>
									</div>
									<div class="stats #">
										<h4>157 <small>lorem 2</small></h4>
									</div>
									<div class="stats #">
										<h4>147 <small>lorem 3</small></h4>
									</div>
								</div>
							</div>
						</div>
						<div id="swranked" class="tab-pane fade">
							<div class="row">
								<div class="col-md-6">
									<div class="stats firstdays">
										<h4>28 <small>days since firsadt join</small></h4>
									</div>
									<div class="stats hourplayed">
										<h4>28 <small>hours played</small></h4>
									</div>
									<div class="stats serverchange">
										<h4>1547 <small>change server</small></h4>
									</div>
								</div>
								<div class="col-md-6">
									<div class="stats #">
										<h4>547 <small>lorem 1</small></h4>
									</div>
									<div class="stats #">
										<h4>157 <small>lorem 2</small></h4>
									</div>
									<div class="stats #">
										<h4>147 <small>lorem 3</small></h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-10">
				<hr>
				<div id="well" class="well2">
					<h4>Inventory & Rewards</h4>
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#inventory">Inventory</a></li>
						<li><a data-toggle="tab" href="#rewards">Rewards</a></li>
					</ul>

					<div class="tab-content">
						<div id="inventory" class="tab-pane fade in active">
							<h3>Inventory System</h3>
							<p>We are working on it ...</p>
						</div>
						<div id="rewards" class="tab-pane fade">
							<h3>Rewards System</h3>
							<p>We are working on it ...</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- MODAL CAMBIO NICK -->
	<div id="changnick" class="modal in" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
</body>
</html>

