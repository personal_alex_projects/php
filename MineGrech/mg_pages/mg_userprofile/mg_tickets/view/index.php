<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');

$mgclass = new MineGrech();


if ($gnsession->isLoggedIn()) {
	$mymgsession = new MGSession(null);
}else{
	header("Location: https://minegrech.com/login");
}

if (isset($_GET['tid']) or isset($_GET['myguid'])) {
	if ($_GET['myguid'] === $gnsession->getGUID()) {
		$ticket = $mgclass->getMyTickedID($_GET['tid'], $gnsession->getGUID());
		if ($ticket == 100) {
			header("Location: https://minegrech.com/edit/tickets/");
		}else{
			foreach ($ticket as $key) {
				$ticketID 	= $key['ticketID'];
				$tAuthor 	= $key['ticketAuthorName'];
				$tAuthorG	= $key['ticketAuthorGUID'];
				$problem 	= $key['ticketProblem'];
				$message	= $key['ticketMessage'];
				$subject	= $key['ticketSubject'];
				$takenby	= $key['ticketTakenBy'];
				$created 	= $key['ticketCreation'];
				$lastupdate	= $key['ticketLastUpdate'];
				$priority	= $key['ticketPriority'];
				$superior	= $key['ticketSuperior'];
				$personinfo	= $key['ticketPersonalInfo'];
				$download	= $key['ticketDownloadble'];
				$tests      = $key['ticketTest'];
				$status 	= $key['ticketStatus'];
			}
		}
	}else{
		header("Location: https://minegrech.com/edit/tickets/");
	}
}else{
	header("Location: https://minegrech.com/edit/tickets/");
}

$comment = $mgclass->getComments($ticketID);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - Edit Ticket (<?php echo $ticketID; ?>)</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style type="text/css" media="all">
		@import "https://cdn.glaucus.net/glaucus_main/editor/widgEditor.css";
	</style>
	<script type="text/javascript" src="https://cdn.glaucus.net/glaucus_main/editor/widgEditor.js"></script>
</head>
<body>
	<div class="navbar-position">
		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
	</div>
	<div class="container" style="margin-top: 5em;">
		<div class="well">
			<div class="row">
				<a href="https://www.minegrech.com/edit/tickets/"><< BACK</a>
				<h2>Ticket: <?php echo $ticketID; ?></h2>
				<p>You <?php echo $tAuthor; ?> have created this ticket.</p>
				<div class="col-md-6">
					<div class="form-group">
						<label for="reason">Problem Type:</label>
						<?php 
						switch ($problem) {
							case '1':
							$problem = "I have a problem with my website account.";
							break;
							case '2':
							$problem = "I have a problem with my account on the server.";
							break;
							case '3':
							$problem = "I want to report a bug / glitch.";
							break;
							case '4':
							$problem = "I want information about the premium purchase.";
							break;
							case '5':
							$problem = "I want to deliberate my last punishment.";
							break;
							case '6':
							$problem = "Other Reason.";
							break;
							default:
							$problem = "unknown problem";
							break;
						}
						switch ($status) {
							case '0':
							$status = "<span class='label label-default'>Sended</span>";
							break;
							case '1':
							$status = "<span class='label label-success'>Accepted</span>";
							break;
							case '2':
							$status = "<span class='label label-warning'>Pending</span>";
							break;
							case '3':
							$status = "<span class='label label-danger'>Closed</span>";
							break;	
							default:
							$status = "<span class='label label-danger'>Closed</span>";
							break;
						}
						?>
						<input type="text" class="form-control" value="<?php echo $problem; ?>" disabled name="reason">
					</div>
					<div class="form-group">
						<label for="subj">Subject:</label>
						<input type="text" class="form-control" disabled value="<?php echo $subject; ?>" id="subj">
					</div>
					<div class="form-group">
						<label for="test">Some Tests:</label>
						<input type="text" class="form-control" disabled value="<?php echo $tests; ?>" id="test">
					</div>
					<div class="form-group">
						<label for="commentr">Full Detail:</label>
						<textarea class="form-control" disabled rows="5" style="resize: none;" id="commentr"><?php echo $message; ?></textarea>
					</div>
				</div>
				<div class="col-md-6">
					<div class="options">
						<h4>Ticket Options</h4>
						<div class="col-md-6">
							<div class="form-group">
								<label for="status"><b>Ticket Status:</b></label>
								<?php echo $status; ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="status"><b>Taken By:</b></label>
								<?php echo $takenby; ?>
							</div>
						</div>
						<br>
						<div class="col-md-6">
							<div class="form-group">
								<label for="status"><b>Ticket Created:</b></label>
								<?php echo $created; ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="status"><b>Last Update:</b></label>
								<?php echo $lastupdate; ?>
							</div>
						</div>
						<br>
						<div class="col-md-6">
							<div class="form-group">
								<label for="status"><b>Ticket Send to:</b></label>
								<?php if ($superior == 1): ?>
									<p>My ticket was sent to a Superior</p>
								<?php else: ?>
									<p>My ticket was sent to a Moderator</p>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="status"><b>Ticket Priority:</b></label>
								<?php if ($priority == 1): ?>
									<p>My ticket was sent with high priority</p>
								<?php else: ?>
									<p>My ticket was sent with normal priority</p>
								<?php endif; ?>
							</div>
						</div>
						<br>
						<div class="col-md-6">
							<div class="form-group">
								<label for="status"><b>Ticket Personal Info:</b></label>
								<?php if ($personinfo == 1): ?>
									<p>My ticket contain personal information</p>
								<?php else: ?>
									<p>My ticket not contain personal information</p>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="status"><b>Ticket Downloadble content:</b></label>
								<?php if ($download == 1): ?>
									<p>My ticket contain downloadable content</p>
								<?php else: ?>
									<p>My ticket not contain downloadable content</p>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<hr>
			<div class="comments-body">
				<div class="send-comment">
					<h3>Conversation</h3>
					<form action="https://minegrech.com/support/ticket/update/comment/?<?php echo "tid=$ticketID&guid=$tAuthorG"; ?>" method="POST">
						<?php if ($status === "<span class='label label-danger'>Closed</span>"): ?>
							<div class="form-group">
								<label for="comment"><b>Send Comment:</b></label>
								<textarea disabled class="widgEditor nothing form-control" maxlength="300" rows="5" style="resize: none;" id="comment"></textarea>
							</div>
							<p class="btn btn-primary" disabled>TICKET CLOSED</p>
							
						<?php else: ?>
							<input name="comment-submit" type="submit" class="btn btn-primary" value="SEND">
							<input name="comment-submit" type="submit" class="btn btn-primary" value="SEND & CLOSE">
							<div class="form-group">
								<label for="comment"><b>Send Comment:</b></label>
								<textarea class="widgEditor nothing form-control" minlength="4" required name="comment" maxlength="300" rows="5" style="resize: none;" id="comment"></textarea>
							</div>
						<?php endif; ?>
					</form>
				</div>
				<hr>
				<div class="comments">
					<?php if ($comment == 100): ?>
						<p>No comments in this ticket!</p>
					<?php else: ?>
						<?php foreach ($comment as $key): ?>
							<?php  

							$username = $key['message_author'];
							$usertype = $key['message_usertype'];
							$message  = $key['message'];
							$at 	  = $key['message_created'];
							?>
							<?php if ($usertype == 1): ?>
									<blockquote class="blockquote-reverse">
										<p><?php echo $message; ?></p>
										<footer><span class="label label-danger"><?php echo $username; ?></span> at <?php echo $at; ?></footer>
									</blockquote>
								<?php elseif($usertype == 2): ?>
									<center>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h3 class="panel-title"><?php echo $username; ?></h3>
											</div>
											<div class="panel-body">
												<span class="label label-success"><?php echo $message; ?></span> at  <?php echo $at; ?>
											</div>
										</div>
									</center>
								<?php elseif($usertype == 3): ?>
									<center>
										<div class="panel panel-default">
											<div class="panel-heading">
												<h3 class="panel-title"><?php echo $username; ?></h3>
											</div>
											<div class="panel-body">
												<span class="label label-danger"><?php echo $message; ?></span> at  <?php echo $at; ?>
											</div>
										</div>
									</center>
								<?php else: ?>
									<blockquote>
										<p><?php echo $message; ?></p>
										<footer><?php echo $username; ?> at <?php echo $at; ?></footer>
									</blockquote>
								<?php endif; ?>
						<?php endforeach ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>