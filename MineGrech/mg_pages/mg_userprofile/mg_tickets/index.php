<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
global $gnmysql;
global $gnsession;
global $mgsession;
global $mgclass;
include('/var/www/html/dynamic/classes/Global/mysql.class.php');
$gnmysql = new GNMySQL();
////////////////////////////////////////////////////
include('/var/www/html/dynamic/classes/Global/session.class.php');
$gnsession = new GNSession();

include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');

$mgclass = new MineGrech();

if ($gnsession->isLoggedIn()) {
	$mymgsession = new MGSession(null);
}else{
	header("Location: https://www.minegrech.com/login");
}

?> 
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Minegrech - My Tickets</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="navbar-position">
		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
	</div>
	<div class="container" style="margin-top: 5em;">
		<div class="text">
			<h2>My Tickets</h2>
			<p>List all your tickets sent to the staff.</p> 
		</div>
		<div class="table-tickets">
			<table class="table table-stripped">
				<thead>
					<tr>
						<th>ID</th>
						<th>Status</th>
						<th>Problem</th>
						<th>Case taken by</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php $ticket = $mgclass->getMyTickets($mymgsession->getGUID()); ?>
					<?php 
					if ($ticket == 100) {
						echo "<tr><td>You dont have any ticket!</td></tr>";
					}else{
						foreach ($ticket as $key) {
							$ticketID 	= $key['ticketID'];
							$status 	= $key['ticketStatus'];
							$problem    = $key['ticketProblem'];
							$takenby    = $key['ticketTakenBy'];

							switch ($status) {
								case '0':
								$status = "<span class='label label-default'>Sended</span>";
								break;
								case '1':
								$status = "<span class='label label-success'>Accepted</span>";
								break;
								case '2':
								$status = "<span class='label label-warning'>Pending</span>";
								break;
								case '3':
								$status = "<span class='label label-danger'>Closed</span>";
								break;	
								default:
								$status = "<span class='label label-danger'>Closed</span>";
								break;
							}

							switch ($problem) {
								case '1':
								$problem = "I have a problem with my website account.";
								break;
								case '2':
								$problem = "I have a problem with my account on the server.";
								break;
								case '3':
								$problem = "I want to report a bug / glitch.";
								break;
								case '4':
								$problem = "I want information about the premium purchase.";
								break;
								case '5':
								$problem = "I want to deliberate my last punishment.";
								break;
								case '6':
								$problem = "Other Reason.";
								break;
								default:
								$problem = "unknown problem";
								break;
							}
								echo "<tr><td>$ticketID</td><td>$status</td><td>$problem</td><td>$takenby</td><td><a href='https://www.minegrech.com/edit/tickets/view?tid=$ticketID&myguid=".$mymgsession->getGUID()."' class='btn btn-default'>View</a></td></tr>";
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>
