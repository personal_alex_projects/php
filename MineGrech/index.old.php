<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="Title" content="Minegrech"/>
	<meta name="Description" content="Minegrech es un servidor dedicado a la comunidad de jugadores en Minecraft, contamos con grandes cantidades de serivicios para el entretenimiento de nuestros jugadores."/>
	<meta name="Keywords" content="minegrech, glaucus network, servidores minecraft, 2016, 2017, alexbanper, xxnurioxx, plugins, kohi, servidores, network, minegrech network, minegrech twitter, glaucus.net, minegrech.com, minijuegos, staff, lobby."/>
	<meta name="Language" content="es"/>
	<meta name="Distribution" content="Global"/>
	<meta name="robots" content="index,follow,all"/>

	<title>Minegrech Network - Home</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<link rel="stylesheet" href="https://minegrech.com/index_data/style.css">
	<link rel="icon" href="https://cdn.glaucus.net/img/favicon.png" type="image/x-icon">
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-88958119-1', 'auto');
		ga('send', 'pageview');

	</script>
	<style>
		center {
			top: calc(50% - 250px);
			position: absolute;
			left: calc(50% - 207px);
		}
		body{
			background-color: #F5F5F5;
		}
		.ajust{
			margin-top:10em;
		}
	</style>
</head>
<body style="background-color: #D1CDCD;">
	<div id="snow"></div>
	<div style="padding-top:541px; align-items: center;" class="fond">

		<div class="contener_home_one">
			<div class="fireplace">&nbsp;</div>
			<div class="fireplace_top">&nbsp;</div>
			<div class="triangle">&nbsp;</div>
			<div class="parallelogram">&nbsp;</div>
			<div class="door">&nbsp;</div>
			<div class="window_one">&nbsp;</div>
			<div class="window_two">&nbsp;</div>
			<div class="home_base">&nbsp;</div>
			<div class="christmas_tree"></div>
			<div class="christmas_tree" style="left:-140px;"></div>
			<div class="mountain_one"><div class="sub_mountain_one">&nbsp;</div></div>
			<div class="mountain_two"><div class="sub_mountain_two">&nbsp;</div></div>
			<div class="lutz">
				<div class="lutin_pom">&nbsp;</div>
				<div class="lutin_top">&nbsp;</div>
				<div class="lutin_head">&nbsp;</div>
				<div class="lutin_arm1">&nbsp;</div>
				<div class="lutin_arm2">&nbsp;</div>
				<div class="lutin_body">&nbsp;</div>
				<div class="lutin_jb1">&nbsp;</div>
				<div class="lutin_jb2">&nbsp;</div>
			</div>
		</div>
		<div class="contener_snow">
			<div class="snowflakes">
				<div class="snowflake">&nbsp;</div>
				<div class="snowflake">&nbsp;</div>
				<div class="snowflake">&nbsp;</div>
				<div class="snowflake">&nbsp;</div>
				<div class="snowflake">&nbsp;</div>
				<div class="snowflake">&nbsp;</div>
				<div class="snowflake">&nbsp;</div>
				<div class="snowflake">&nbsp;</div>
			</div>
		</div>

		<div style=" width:500px;height:9px; background-color:#ffffff; border-radius:5px;">&nbsp;</div>


   <!--<div style=" padding:5px; color:#639da8; font-weight:300; font-size:55px; font-family:'Roboto';padding-top:20px;">Merry <font style="font-weight:400;">Christmas</font></div>
   <a href="http://texxsmith.com" style="text-decoration:none;" target="_blank"><div style="  color:#639da8; font-weight:300; font-size:20px; font-family:'Roboto';">From the Smith Family</div></a>-->
</div>
<div style="text-align: center;" class="center">
	<h1>Mantenimiento Programado</h1>
	<div id="form" style="position: relative; width: 400px; top: 3px;margin-left: 35em;">

		<table style="margin-top: -2em;margin-left: -3px;">

			<tr>

				<td colspan="8"><div class="numbers" id="count2" style="text-align: center;"></div></td>

			</tr>

			<tr>

				<td ><div class="numbers" id="dday"></div></td>       

				<td ><div class="title" id="days">Days</div></td>

				<td><div class="numbers" id="dhour"></div></td>

				<td ><div class="title" id="hours">Hours</div></td>

				<td ><div class="numbers" id="dmin"></div></td>

				<td ><div class="title" id="minutes">Minutes</div></td>

				<td ><div class="numbers" id="dsec"></div></td>

				<td ><div class="title" id="seconds">Seconds</div></td>

			</tr>
		</table>
	</div>
	<p style="font-weight: bold;">The site is under maintenance, stay informed!</p>
</div>
<script type="text/javascript">


/*

Count down until any date script-

By JavaScript Kit (www.javascriptkit.com)

Over 200+ free scripts here!

Modified by Robert M. Kuhnhenn, D.O.

on 5/30/2006 to count down to a specific date AND time,

and on 1/10/2010 to include time zone offset.

*/


/*  Change the items below to create your countdown target date and announcement once the target date and time are reached.  */

var current="Minegrech in cooming";       //—>enter what you want the script to display when the target date and time are reached, limit to 20 characters

var year=2016;        //—>Enter the count down target date YEAR

var month=12;          //—>Enter the count down target date MONTH

var day=16;           //—>Enter the count down target date DAY

var hour=18;          //—>Enter the count down target date HOUR (24 hour clock)

var minute=0;        //—>Enter the count down target date MINUTE

var tz=-5;            //—>Offset for your timezone in hours from UTC (see http://wwp.greenwichmeantime.com/index.htm to find the timezone offset for your location)


//—>    DO NOT CHANGE THE CODE BELOW!    <—

var montharray=new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");


function countdown(yr,m,d,hr,min){

	theyear=yr;themonth=m;theday=d;thehour=hr;theminute=min;

	var today=new Date();

	var todayy=today.getYear();

	if (todayy < 1000) {

		todayy+=1900; }

		var todaym=today.getMonth();

		var todayd=today.getDate();

		var todayh=today.getHours();

		var todaymin=today.getMinutes();

		var todaysec=today.getSeconds();

		var todaystring1=montharray[todaym]+" "+todayd+", "+todayy+" "+todayh+":"+todaymin+":"+todaysec;

		var todaystring=Date.parse(todaystring1)+(tz*1000*60*60);

		var futurestring1=(montharray[m-1]+" "+d+", "+yr+" "+hr+":"+min);

		var futurestring=Date.parse(futurestring1)-(today.getTimezoneOffset()*(1000*60));

		var dd=futurestring-todaystring;

		var dday=Math.floor(dd/(60*60*1000*24)*1);

		var dhour=Math.floor((dd%(60*60*1000*24))/(60*60*1000)*1);

		var dmin=Math.floor(((dd%(60*60*1000*24))%(60*60*1000))/(60*1000)*1);

		var dsec=Math.floor((((dd%(60*60*1000*24))%(60*60*1000))%(60*1000))/1000*1);

		if(dday<=0&&dhour<=0&&dmin<=0&&dsec<=0){

			document.getElementById('count2').innerHTML=current;

			document.getElementById('count2').style.display="block";

			document.getElementById('count2').style.width="390px";

			document.getElementById('dday').style.display="none";

			document.getElementById('dhour').style.display="none";

			document.getElementById('dmin').style.display="none";

			document.getElementById('dsec').style.display="none";

			document.getElementById('days').style.display="none";

			document.getElementById('hours').style.display="none";

			document.getElementById('minutes').style.display="none";

			document.getElementById('seconds').style.display="none";

			return;

		}

		else {

			document.getElementById('count2').style.display="none";

			document.getElementById('dday').innerHTML=dday;

			document.getElementById('dhour').innerHTML=dhour;

			document.getElementById('dmin').innerHTML=dmin;

			document.getElementById('dsec').innerHTML=dsec;

			setTimeout("countdown(theyear,themonth,theday,thehour,theminute)",1000);

		}

	}


	countdown(year,month,day,hour,minute);


</script>
<br>
<br>
<div class="all">
	<div style="margin-left: 33em;" class="mensaje-error">
		<img src="https://cdn.glaucus.net/img/logo2.png" style="width: 400px; margin-right:15px;">	
	</div>
	<img src="https://cdn.glaucus.net/img/Loading1.gif" style="height: 200px; margin-left: 42em;">	
</div>
</body>
</html>