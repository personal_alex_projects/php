<?php

class GNLanguage {
	
	private $lang = array();

	public function __construct()
	{
		//Lang import "es" replace
		include("lang/es.lang.php");
		$this->lang = $lang;
	}

	public function translateVar($var){
		echo $this->getVar($var);
	}

	public function getVar($var)
	{
		if (isset($this->lang[$var])) return $this->lang[$var];
		return $var;
	}

}

?>