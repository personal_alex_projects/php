<?php

class MGHead {
	
	private $extra = array();
	private $default = array();
	private $sessionsMode = true;

	public function __construct()
	{
		$this->default["script_example"] = "<script>\nalert('NOT DISABLED!');\n</script>";
		$this->default["title"] = "<title>NOTITLE | MineGrech</title>";
	}

	public function setTitle($var){
		$this->default["title"] = "<title>$var | MineGrech</title>";
	}

	public function disableSessions(){
		$this->$sessionsMode = false;
	}

	public function setDisabled($var){
		unset($this->default["script_example"]);
	}

	public function addExtraLine($var){
		array_push($this->extra, $var);
	}

	public function printHead()
	{
		echo "<head>";
		foreach ($this->extra as $val) {
    		echo $val;
		}
		foreach ($this->default as $val) {
    		echo $val;
		}
		echo "</head>";
	}

}

?>