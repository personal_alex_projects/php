	$(document).ready(function(){ 
			$('#characterLeft').text('16 characters left');
			$('#comment').keyup(function () {
				var max = 500;
				var len = $(this).val().length;

				if (len >= max) {
					$('#characterLeft').text('You have reached the limit');
					$('#characterLeft').addClass('red');
					if (len > max) { 
						var elem = document.getElementById('btnSubmit');
						elem.parentNode.removeChild(elem);
						$('#characterLeft').text('You trying to put more letters of the allowed thing, reloads the page.');
					}
				} 
				else {
					var ch = max - len;
					var min = len >= 10;
					$('#characterLeft').text(ch + ' characters left');
					if (min) {$('#btnSubmit').prop("disabled", false);}else{$('#btnSubmit').prop("disabled", true);}
					if (min) {$('#btnSubmit').removeClass('disabled');}else{$('#btnSubmit').addClass('disabled');}
					$('#characterLeft').removeClass('red');            
				}
			});    
		});