$(document).ready(function(){
 			$("#add-test").click(function(e){
 				e.preventDefault();
 				var numberOfAddresses = $("#form1").find("input[name^='data[test]']").length;
 				var label = '<label for="data[test][' + numberOfAddresses + ']"><b>Test File ' + (numberOfAddresses + 1) + '</b></label> ';
 				var input = '<input class="form-control" type="text" name="data[test][' + numberOfAddresses + ']" id="data[test][' + numberOfAddresses + ']" />';
 				var removeButton = '<button class="remove-address btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i></button>';
 				var html = "<div class='tests'>" + label + input + removeButton +"</div>";
 				$("#form1").find("#add-test").before(html);
 			});
 		});

 		$(document).on("click", ".remove-address",function(e){
 			e.preventDefault();
 			$(this).parents(".tests").remove();
    //update labels
    $("#form1").find("label[for^='data[test]']").each(function(){
    	$(this).html("Test File " + ($(this).parents('.tests').index() + 1));
    });
});