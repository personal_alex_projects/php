<div class="general-shop-by-category">
				<div class="row">
					<div class="col-md-3" style="margin-bottom: 5em;">
						<h3>Category Items</h3>
						<ul class="nav nav-pills nav-stacked">
							<li class="active"><a data-toggle="pill" href="#general">general</a></li>
							<li><a data-toggle="pill" href="#test">test</a></li>
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
									Menu 1 <span class="caret"></span>
								</a>
								<ul class="dropdown-menu">
									<li><a href="#">Submenu 1-1</a></li>
									<li><a href="#">Submenu 1-2</a></li>
									<li><a href="#">Submenu 1-3</a></li>
								</ul>
							</li>
							<li><a href="#">Button</a></li>
							<li><a href="#">Button</a></li>
							<li><a href="#">Button</a></li>
							<li><a href="#">Button</a></li>
							<li><a href="#">Button</a></li>
							<li><a data-toggle="pill" href="#gcoins">Buy gCoins</a></li>
						</ul>
					</div>
					<div class="col-md-8" style="margin-left: 6em;">
						<div class="tab-content">
							<div id="gcoins" class="tab-pane fade">
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://i.gyazo.com/fa3b6e2f803b23d99c558bbbf0d33efc.png" alt="gCoins_pack1">
										<div class="caption">
											<h3>gCoins Pack #1</h3>
											<p>Este paquete contiene una bolsa de gCoins de: 5 G$</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
												</p>
												<p class="pull-right">
													$2 US
												</p>
											</div>
										</div>
									</div> 
								</div>
							</div>
							<div id="general" class="tab-pane fade in active">
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=330%C3%97140&w=330&h=140" alt="...">
										<div class="caption">
											<h3>#item</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
													<a href="#" class="btn btn-default" role="button">INFO</a>
												</p>
												<p class="pull-right">
													$000.000
												</p>
											</div>
										</div>
									</div> 
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=330%C3%97140&w=330&h=140" alt="...">
										<div class="caption">
											<h3>#item</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
													<a href="#" class="btn btn-default" role="button">INFO</a>
												</p>
												<p class="pull-right">
													$000.000
												</p>
											</div>
										</div>
									</div> 
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=330%C3%97140&w=330&h=140" alt="...">
										<div class="caption">
											<h3>#item</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
													<a href="#" class="btn btn-default" role="button">INFO</a>
												</p>
												<p class="pull-right">
													$000.000
												</p>
											</div>
										</div>
									</div> 
								</div>
							</div>
							<div id="test" class="tab-pane fade">
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=330%C3%97140&w=330&h=140" alt="...">
										<div class="caption">
											<h3>#item_test</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
													<a href="#" class="btn btn-default" role="button">INFO</a>
												</p>
												<p class="pull-right">
													$000.000
												</p>
											</div>
										</div>
									</div> 
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="thumbnail" style="border-radius: 12px;">
										<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=330%C3%97140&w=330&h=140" alt="...">
										<div class="caption">
											<h3>#item_test</h3>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
											<br>
											<div class="text" style="display: ruby-text-container;">
												<p>
													<a href="#" class="btn btn-primary" role="button">BUY</a>
													<a href="#" class="btn btn-default" role="button">INFO</a>
												</p>
												<p class="pull-right">
													$000.000
												</p>
											</div>
										</div>
									</div> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>