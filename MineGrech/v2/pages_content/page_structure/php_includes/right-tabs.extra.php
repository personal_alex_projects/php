<div class="right-navar">
	<div class="account_quick-links">
		<div class="account_title">
			<h4>ACCOUNT</h4>
		</div>
		<div class="account_quick_buttons">
			<!-- SI ESTA ONLINE -->
			<?php 		
			echo '<a href="https://www.minegrech.com/profile" class="btn btn-primary btn-block">My Profile</a>';
			echo '<a href="https://www.glaucus.net/profile" class="btn btn-primary btn-block">My Glaucus Account</a>';
			echo '<a href="https://www.glaucus.net/profile/leave.php" class="btn btn-danger btn-block">Log out</a>';
			 ?>
			<!-- SI ESTA OFFLINE -->
			<?php 
			echo '<a href="https://www.minegrech.com/login" class="btn btn-danger btn-block">RECOVERY PASSWORD</a>';
			echo '<a href="https://www.minegrech.com/login" class="btn btn-primary btn-block">LOGIN</a>';
			echo '<a href="https://www.minegrech.com/register" class="btn btn-primary btn-block">REGISTER</a>';
			 ?>
		</div>
		<hr>
		<div class="quick-links">
			<a href="https://www.glaucus.net/terms/" class="btn btn-default btn-block">TERMS</a>
			<a href="https://www.minegrech.com/staff" class="btn btn-default btn-block">STAFF</a>
			<button class="btn btn-default btn-block">RULES</button>
		</div>
		<hr>
		<div class="online-staff">
			<div class="online-staff-title">
				<h4>Last Forum Post</h4>
			</div>
			<div class="online-staff-body">
				<p>Not feature available.</p>
			</div>
		</div>
	</div>
</div>