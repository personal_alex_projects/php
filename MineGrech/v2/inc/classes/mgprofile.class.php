<?php 
/**
* 
*/
class Profile{

	private $userTwitter;
	private $userFacebook;
	private $userSkype;

	private $userYoutube;
	private $userSteam;
	private $userReddit;

	private $userGender;
	private $userAge;

	private $userTheme; 
	
	function __construct(argument){
		$this->load();
	}

		public function getUserTwitter(){
		return $this->userTwitter;
	}

	public function getUserFacebook(){
		return $this->userFacebook;
	}

	public function getUserSkype(){
		return $this->userSkype;
	}

	public function getUserYoutube(){
		return $this->userYoutube;
	}

	public function getUserSteam(){
		return $this->userSteam;
	}

	public function getUserReddit(){
		return $this->userReddit;
	}

	public function getNameChangeLeft(){
		return $this->nameChangeLeft;
	}

	public function getFreeNameChangeLeft(){
		return $this->freeNameLeft;
	}

	public function getTotalChangeNameLeft(){
		return ($this->nameChangeLeft + $this->freeNameLeft);
	}

	public function getUserGender($type){
		// $type = 0; Entrega los datos para ser mostrados en perfil. (Male, Female, Unassigned)
		// $type = 1; Entrega los datos para ser mostrados en edicion del perfil. (0, 1, 2)
		if ($type == 0) {
			if ($this->userGender == "0") {
				return "Male";
			}elseif ($this->userGender == "1") {
				return "Female";
			}elseif ($this->userGender == "2") {
				return "Not assigned";
			}
		}
		if ($type == 1) {
			return $this->userGender;
		}


	}

	public function getUserAge($type){
		// $type = "Profile"; Entrega los datos para ser mostrados en perfil. (+10, +15, etc)
		// $type = "Update"; Entrega los datos para ser mostrados en edicion del perfil. (-3 - 0, 0 - 50)
		if ($type == "Profile") {
			switch ($this->userAge) {
				case '-3':
				return "+10 years";
				break;
				case '-2':
				return "+15 years";
				break;
				case '-1':
				return "+18 years";
				break;
				case '0':
				return "+20 years";
				break;
				default:
				return $this->userAge." years";
				break;
			}
		}
		if ($type == "Update") {
			return $this->userAge;
		}
		
	}

	public function getUserTheme(){
		return $this->userTheme;
	}

	private function load(){
		global $gnmysql; //Define GNMySQL

		$guid = $this->getGUID();

		$sql = $gnmysql->getConn()->prepare("SELECT * FROM mg_accounts WHERE GUID=:guid LIMIT 1");
		$sql->bindParam(":guid", $guid, PDO::PARAM_STR, 21);
		$sql->execute();

		if($sql->rowCount() == 0){
			$this->error = true;
			return;
		}

		$row = $sql->fetchAll();

		foreach ($row as $key) {
			$this->username 		= $key['Username'];
			$this->uuid 			= $key['UUID'];
			$this->userrank 		= $key['UserRank'];
			$this->currentServer 	= $key['currentServer'];
			$this->SecondsPlayed 	= $key['secondsPlayed'];
			$this->nameChangeLeft 	= $key['nameChangeLeft'];
			$this->freeNameLeft 	= $key['freeNameChangeLeft'];

			// Social Networks
			$this->userTwitter		= $key['userTwitter'];
			$this->userFacebook		= $key['userFacebook'];
			$this->userSkype		= $key['userSkype'];

			$this->userYoutube		= $key['userYoutube'];
			$this->userSteam		= $key['userSteam'];
			$this->userReddit		= $key['userReddit'];

			$this->userGender       = $key['userGender'];
			$this->userAge 			= $key['userAge'];

			$this->userTheme 		= $key['userTheme'];
		}
		$this->error = false;
	}
}

 ?> 