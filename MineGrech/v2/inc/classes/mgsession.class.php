<?php 

/*
* 
*   MineGrech Session Class per Player
*
*/
class MGSession
{

	private $currentmills;
	private $guid;

	private $error;
	
	private $username;
	private $uuid;
	private $userrank;

	private $currentServer;
	private $SecondsPlayed;
	private $nameChangeLeft;
	private $freeNameLeft;

	function __construct($guid)
	{
		if(!isset($guid) || $guid == null){
			global $gnsession;
			if(!isset($gnsession)){
				echo '<script type="text/javascript">alert("Glaucus Error Reporter (E:1)\n\nLa session MG fue cargada antes de GN!\nPorfavor envia este error a un desarollador de Glaucus Network.");</script>';
				return;
			}
			if(!$gnsession->isLoggedIn()){
				echo '<script type="text/javascript">alert("Glaucus Error Reporter (E:2)\n\nLa session MG fue cargada sin ninguna sesion activa de GN!\nPorfavor envia este error a un desarollador de Glaucus Network.");</script>';
				return;
			}
			$this->guid = $gnsession->getGUID();
		}else{
			$this->guid = $guid;
		}
		$this->load();
	}

	public function registerAccount($email, $password, $cpassword, $username){
		global $gnsession;
		if($gnsession->isLoggedIn()) return -1;

		if(!filter_var($email, FILTER_VALIDATE_EMAIL))return 1;
		if((strlen($password) < 6) && (strlen($password) > 32)) return 2; //Password short/long
		if(!($password === $cpassword)) return 3; //Password don't match
		if((strlen($email) >= 42)) return 4;
		
		global $gnmysql; //Define GNMySQL

		$sql = $gnmysql->getConn()->prepare("SELECT Email FROM gn_accounts WHERE Email=:email");
		$sql->bindParam(":email", $email, PDO::PARAM_STR, 42);
		$sql->execute();

		$count = $sql->rowCount();
		if($count != 0)return 4;
		
		$salt = $gnsession->generateSALT();
		$currenttime = $gnsession->currentmills;
		$guid = $gnsession->generateGUID();
		$encpass = hash('sha256', hash('sha256', $password).$salt);
		$sql = $gnmysql->getConn()->prepare("INSERT INTO gn_accounts (Email, Password, Salt, GUID,AddedMills) VALUES(:email, :password, :salt, :guid, :currenttime)");
		$sql->bindParam(":email", $email, PDO::PARAM_STR, 42);
		$sql->bindParam(":password", $encpass, PDO::PARAM_STR, 256);
		$sql->bindParam(":salt", $salt, PDO::PARAM_STR, 16);
		$sql->bindParam(":guid", $guid, PDO::PARAM_STR, 21);
		$sql->bindParam(":currenttime", $currenttime, PDO::PARAM_STR, 64);
		$sql->execute();

		// HACER FILTRADO DE USERNAME

		$sql2 = $gnmysql->getConn()->prepare("UPDATE gn_accounts SET linkMG=1, Username=:user WHERE GUID=guid");
		$sql2->bindParam(":guid", $guid, PDO::PARAM_STR, 21);
		$sql2->bindParam(":user", $ustring, PDO::PARAM_STR, 21);
		$sql2->execute();

		$gnsession->login($email, $password);
		
		return 5;
	}

	public function isOwn(){
		global $gnsession;
		if(!$gnsession->isLoggedIn())return false;
		if ($gnsession->getGUID() != $this->getGUID()) return false;
		return true;
	}

	public function checkWorking(){
		return $this->error;
	}

	public function isOnline(){
		return ($this->currentServer != "Offline");
	}

	public function getOnlineServer(){
		return $this->currentServer;
	}

	public function getPlayedHours(){
		return ($this->SecondsPlayed / 60 / 60);
	}

	public function getRank(){
		return $this->userrank;
	}

	public function getUUID(){
		return $this->uuid;
	}

	public function getName(){
		return $this->username;
	}

	public function getGUID(){
		return $this->guid;
	}

	public function getAllStaffOnline(){
		return $this->onlineStaffName;
	}



}
?> 