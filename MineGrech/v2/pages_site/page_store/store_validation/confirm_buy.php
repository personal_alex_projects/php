 <!DOCTYPE html>
 <html lang="en">
 <!--
-
- ESTA PAGINA REQUIERE DE: 
- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
- <link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/global.css">
- <link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/image-picker.css">
- <script src="https://cdn.glaucus.net/glaucus_minegrech/js/image-picker.js"></script>
- FontAwesome (Importad a CDN Glaucus) <script src="https://use.fontawesome.com/eab7cd6eb3.js"></script>
-
-->

 <body>
 	<style>
 		.margin{
 			margin-top: 2%;
 			margin-bottom: 2%;
 		}
 		.border{
 			border: 1px solid #F5F5F5;
 		}
 		td{
 			vertical-align: inherit;

 		}
 	</style>
 	<script>
 		$(document).ready(function () {
 			$("#selectImage").imagepicker({
 				hide_select: true
 			});

 			var $container = $(".image_picker_selector");
    // initialize
    $container.imagesLoaded(function () {
    	$container.masonry({
    		columnWidth: 30,
    		itemSelector: ".thumbnail"
    	});
    });
});
</script>
	<div class="navbar-position">
		<!-- include navbar -->
	</div>
<div class="container" style="margin-top: 5em;">
	<div class="checkout">
		<div class="products">
			<div class="row">
				<div class="col-md-8">
				<div class="errors">
					<span class="label label-danger" style="font-size: 25px;">The buy was failed! Try again</span>
				</div>
					<table class="table table-stripped">
						<thead>
							<tr>
								<th>Packshot</th>
								<th>Product Name</th>
								<th>Content</th>
								<th>Unit Price</th>
								<th>Amount</th>
								<th style="padding-right: 5em;">Price</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><img src="https://placeholdit.imgix.net/~text?txtsize=9&txt=100%C3%97100&w=100&h=100" alt="100x100"></td>
								<td>Testing gCoins Buy.</td>
								<td>Lorem ipsum dolor sit amet: 20 pvpCoins.</td>
								<td>US $2.4</td>
								<td>
									<select name="" id="" style="padding: 16%;padding-left: 0.5em;padding-right: 0.7em;">
										<?php $i = 1; while($i <= 10){echo "<option value='$i'>$i</option>";  $i++;  } ?>
									</select>
								</td>
								<td>x4(<b>9.6 G$</b>)</td>
								<td><a href="" class="fa fa-times" aria-hidden="true"></a></td>
							</tr>
							<tr>
								<td><img src="https://placeholdit.imgix.net/~text?txtsize=9&txt=100%C3%97100&w=100&h=100" alt="100x100"></td>
								<td>Testing gCoins Buy.</td>
								<td>Lorem ipsum dolor sit amet: 20 pvpCoins.</td>
								<td>US $2.4</td>
								<td>
									<select name="" id="" style="padding: 16%;padding-left: 0.5em;padding-right: 0.7em;">
										<?php $i = 1; while($i <= 10){echo "<option value='$i'>$i</option>";  $i++;  } ?>
									</select>
								</td>
								<td>x4(<b>US $9.6</b>)</td>
								<td><a href="" class="fa fa-times" aria-hidden="true"></a></td>
							</tr>
							<tr>
								<td><img src="https://placeholdit.imgix.net/~text?txtsize=9&txt=100%C3%97100&w=100&h=100" alt="100x100"></td>
								<td>Testing gCoins Buy.</td>
								<td>Lorem ipsum dolor sit amet: 20 pvpCoins.</td>
								<td>US $2.4</td>
								<td>
									<select name="" id="" style="padding: 16%;padding-left: 0.5em;padding-right: 0.7em;">
										<?php $i = 1; while($i <= 10){echo "<option value='$i'>$i</option>";  $i++;  } ?>
									</select>
								</td>
								<td>x4(<b>9.6 G$</b>)</td>
								<td><a href="" class="fa fa-times" aria-hidden="true"></a></td>
							</tr>
							<tr>
								<td><img src="https://placeholdit.imgix.net/~text?txtsize=9&txt=100%C3%97100&w=100&h=100" alt="100x100"></td>
								<td>Testing gCoins Buy.</td>
								<td>Lorem ipsum dolor sit amet: 20 pvpCoins.</td>
								<td>2.4 G$</td>
								<td>
									<select name="" id="" style="padding: 16%;padding-left: 0.5em;padding-right: 0.7em;">
										<?php $i = 1; while($i <= 10){echo "<option value='$i'>$i</option>";  $i++;  } ?>
									</select>
								</td>
								<td>x4(<b>9.6 G$</b>)</td>
								<td><a href="" class="fa fa-times" aria-hidden="true"></a></td>
							</tr>
							<tr>
								<td><img src="https://placeholdit.imgix.net/~text?txtsize=9&txt=100%C3%97100&w=100&h=100" alt="100x100"></td>
								<td>Testing gCoins Buy.</td>
								<td>Lorem ipsum dolor sit amet: 20 pvpCoins.</td>
								<td>2.4 G$</td>
								<td>
									<select name="" id="" style="padding: 16%;padding-left: 0.5em;padding-right: 0.7em;">
										<?php $i = 1; while($i <= 10){echo "<option value='$i'>$i</option>";  $i++;  } ?>
									</select>
								</td>
								<td>x4(<b>9.6 G$</b>)</td>
								<td><a href="" class="fa fa-times" aria-hidden="true"></a></td>
							</tr>
							<tr>
								<td><img src="https://placeholdit.imgix.net/~text?txtsize=9&txt=100%C3%97100&w=100&h=100" alt="100x100"></td>
								<td>Testing gCoins Buy.</td>
								<td>Lorem ipsum dolor sit amet: 20 pvpCoins.</td>
								<td>2.4 G$</td>
								<td>
									<select name="" id="" style="padding: 16%;padding-left: 0.5em;padding-right: 0.7em;">
										<?php $i = 1; while($i <= 10){echo "<option value='$i'>$i</option>";  $i++;  } ?>
									</select>
								</td>
								<td>x4(<b>9.6 G$</b>)</td>
								<td><a href="" class="fa fa-times" aria-hidden="true"></a></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-4">
					<div class="well" style="position: fixed;padding-left: 5em;padding-right: 5em;margin-left: 5em;margin-top: -2em;padding-top: 2em;">
						<div class="total">
							<h2>Total</h2>
						</div>
						<div class="yourgcoins">
							<p>Your Balance: <b>300.45 G$</b></p>
						</div>
						<div class="remainingwithvat">
							<p>GAC: <b>1%</b> <br> Price GAC: + <b>0.2 G$</b></p>
						</div>
						<div class="lessgcoins">
							<p>Remaining gCoins: <b>290.65 G$</b> </p>
						</div>
						<div class="">

						</div>
						<div class="total_num">
							<p><b>Total Price:</b></p>
							<p>Discount: <b>0%</b> - <b>0 G$</b></p>
							<center><u><h3>9<small>.6</small> G$</h3></u></center>
						</div>
						<div class="confirm">
							<form action="">
								<input type="submit" class="btn btn-primary btn-block" value="PAY">
								<input type="submit" class="btn btn-default btn-block" value="CONTINUE BUYING">
								<input type="submit" class="btn btn-danger btn-block" value="CANCEL">
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<style>
						.image_picker_image {
							max-width: 100px;
							max-height: 100px;
							min-width: 100px;
							min-height: 100px;
							width: 100px;
							height: 100px;
							background-color: #FF0000;
						}
					</style>
					<h3>Select Payment Method</h3>
					<select id="selectImage" class="image-picker">
						<option data-img-src="https://placeholdit.imgix.net/~text?txtsize=9&txt=100%C3%97100&w=100&h=100" value="0"></option>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html> 