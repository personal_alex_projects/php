<!DOCTYPE html>
<html lang="en">
 <!--
-
- ESTA PAGINA REQUIERE DE: 
- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
- <link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/global.css">
-
-->
<body>
	<div class="navbar-position">
		<!-- include navbar -->
	</div>
	<div class="container">
		<div class="wrapper">
			<div class="featured-items">
				<div class="list-items-first-3">
					<div class="row">
						<div class="col-md-4">
							<div class="item0">
								<div class="thumbnail" style="border-radius: 12px;">
									<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=340%C3%97200&w=340&h=200" alt="...">
									<div class="caption">
										<h3>#item_0</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores earum dolorum amet nobis. Commodi corrupti consectetur aliquid alias hic, expedita explicabo sed quaerat sunt deserunt, totam obcaecati saepe perspiciatis voluptates.</p>
										<div class="text" style="display: ruby-text-container;">
											<p>
												<a href="#" class="btn btn-primary" role="button">BUY</a>
											</p>
											<p class="pull-right">
												$0000.0000
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="item1">
								<div class="thumbnail" style="border-radius: 12px;">
									<img src="https://placeholdit.imgix.net/~text?txtsize=23&amp;txt=310%C3%97260&amp;w=310&amp;h=260" alt="..." style="box-shadow: 9px 9px 5px gray;border-radius: 9px;border: 1px solid coral;" width="94%">
									<div class="caption">
										<h3>#item_1</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, eveniet quasi atque. Exercitationem similique eum molestias doloremque provident aut sunt sit tempora unde error, quibusdam, beatae repellendus, dolore quod animi.</p>
										<div class="text" style="display: ruby-text-container;">
											<p>
												<a href="#" class="btn btn-primary" role="button">BUY</a>
											</p>
											<p class="pull-right">
												$0000.0000
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="item2">
								<div class="thumbnail" style="border-radius: 12px;">
									<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=340%C3%97200&w=340&h=200" alt="...">
									<div class="caption">
										<h3>#item_2</h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis nesciunt molestiae assumenda distinctio sunt praesentium, consequuntur veritatis corrupti amet ut adipisci consequatur magnam voluptate, excepturi at eos inventore dolore soluta.</p>
										<div class="text" style="display: ruby-text-container;">
											<p>
												<a href="#" class="btn btn-primary" role="button">BUY</a>
											</p>
											<p class="pull-right">
												$0000.0000
											</p>
										</div>
									</div>
								</div> 							
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="progress-bar-display">
				<hr>
				<span>60% Complete Goal</span>
				<div class="progress">
					<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
					</div>
				</div>
				<hr>
			</div>
			<div class="store-items-tab">
				<?php include '/var/www/MineGrech/v2/pages_content/page_structure/php_includes/store-tabs.extra.php'; ?>
			</div>
			<hr>
			<div class="recently-purshases" style="margin-top: 2em; margin-bottom: 3em;">
				<h2>Purchases from recent players.</h2>
				<hr>
				<div class="players-recently">
					<ul class="media-list">
						<li class="media"> 
							<div class="media-left"> 
								<a href="#"> 
									<img alt="64x64" class="media-object" data-src="holder.js/64x64" style="width: 64px; height: 64px;" src="https://placeholdit.imgix.net/~text?txtsize=8&txt=64%C3%9764&w=64&h=64" data-holder-rendered="true">
								</a>
							</div> 
							<div class="media-body">
								<h4 class="media-heading">Media heading</h4> 
								<p>
									Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
								</p>
							</div> 
						</li>
					</ul>
				</div>
				<hr>
			</div>
		</div>
	</body>
	</html> 


