<!DOCTYPE html>
<html lang="en">
  <!--
-
- ESTA PAGINA REQUIERE DE: 
- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
- <link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/global.css">
- <script src="https://minegrech.com/v2/pages_content/javascript/search.js"></script>
- <link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/page_search.css">
-
-->
<body>
	<div class="navbar-position" style="display: ">
		<!-- include navbar -->
	</div>
<div class="wrap">
      <a href="https://minegrech.com/" target="_blank"><h1>Minegrech Search</h1></a>
      <div class="searchbar">
        <input class="search-field" type="search" placeholder="Search on Minegrech Profiles..." id="query">
        <div class="buttons">
          <button class="search" id="search">Search</button>
          <button class="random" id="random">Random</button>
        </div>
      </div>
      <div class="result">
      </div>
      <div class="copyright">Designed and built by
        <a href="https://twitter.com/GlaucusNetwork" target="_blank">@GlaucusNetwork</a> • 2016
      </div>
    </div>
</body>
</html> 