<hr>
 					<div id="well" class="well2">
 						<h4>Stats</h4>
 						<ul class="nav nav-tabs">
 							<li class="active"><a data-toggle="tab" href="#glob">Global</a></li>
 							<li><a data-toggle="tab" href="#fullpvp">FullPvP</a></li>
 							<li><a data-toggle="tab" href="#skywars">SkyWars (Casual)</a></li>
 							<li><a data-toggle="tab" href="#swranked">SkyWars (Ranked)</a></li>
 						</ul>

 						<div class="tab-content">
 							<div id="glob" class="tab-pane fade in active">
 								<div class="row">
 									<div class="col-md-6">
 										<div class="title-row">
 											<p style="margin-top:1em;"><b>General Stats</b></p>
 										</div>
 										<div class="stats firstdays">
 											<h4>28 <small>days since first join</small></h4>
 										</div>
 										<div class="stats hourplayed">
 											<h4>28 <small>hours played</small></h4>
 										</div>
 										<div class="stats serverchange">
 											<h4>1547 <small>change server</small></h4>
 										</div>
 										<div class="stats serverchange">
 											<h4>1547 <small>change server</small></h4>
 										</div>
 										<div class="stats serverchange">
 											<h4>1547 <small>change server</small></h4>
 										</div>
 									</div>
 									<div class="col-md-6">
 										<div class="title-row">
 											<p style="margin-top:1em;"><b>Server Stats</b></p>
 										</div>
 										<div class="stats #">
 											<h4>547 <small>Global Kills</small></h4>
 										</div>
 										<div class="stats #">
 											<h4>157 <small>Global Deaths</small></h4>
 										</div>
 										<div class="stats #">
 											<h4>0.41 <small>Global KD</small></h4>
 										</div>
 										<div class="stats #">
 											<h4>0.41 <small>Global KD</small></h4>
 										</div>
 										<div class="stats #">
 											<h4>0.41 <small>Global KD</small></h4>
 										</div>
 									</div>
 								</div>
 							</div>
 							<div id="fullpvp" class="tab-pane fade">
 								<div class="row">
 									<div class="col-md-6">
 										<div class="stats kills">
 											<h4>10<small> Kills</small></h4>
 										</div>

 										<div class="stats kills">
 											<h4>Kills <small>No data provided</small></h4>
 										</div>

 										<div class="stats deaths">
 											<h4>10<small> Deaths</small></h4>
 										</div>

 										<div class="stats deaths">
 											<h4>Deaths <small>No data provided</small></h4>
 										</div>


 										<div class="stats damage">
 											<h4>Damage PS <small>No data available</small></h4>
 										</div>
 										<div class="stats damager">
 											<h4>Damager PS <small>No data available</small></h4>
 										</div>
 									</div>
 									<div class="col-md-6">
 										<div class="stats elo">
 											<h4>Elo<small> ELO Rating</small></h4>
 										</div>
 										
 										<div class="stats elo">
 											<h4>ELO Rating <small>No data provided</small></h4>
 										</div>

 										<div class="stats elorank">
 											<h4><small>ELO Rank</small></h4>
 										</div>
 										<div class="stats elorankposition">
 											<h4>ELO Rank Position <small>No data available</small></h4>
 										</div>
 									</div>
 								</div>
 							</div>
 							<div id="skywars" class="tab-pane fade">
 								<div class="row">
 									<div class="col-md-6">
 										<div class="stats firstdays">
 											<h4>28 <small>days since first join</small></h4>
 										</div>
 										<div class="stats hourplayed">
 											<h4>28 <small>hours asdplayed</small></h4>
 										</div>
 										<div class="stats serverchange">
 											<h4>1547 <small>change server</small></h4>
 										</div>
 									</div>
 									<div class="col-md-6">
 										<div class="stats #">
 											<h4>547 <small>lorem 1</small></h4>
 										</div>
 										<div class="stats #">
 											<h4>157 <small>lorem 2</small></h4>
 										</div>
 										<div class="stats #">
 											<h4>147 <small>lorem 3</small></h4>
 										</div>
 									</div>
 								</div>
 							</div>
 							<div id="swranked" class="tab-pane fade">
 								<div class="row">
 									<div class="col-md-6">
 										<div class="stats firstdays">
 											<h4>28 <small>days since firsadt join</small></h4>
 										</div>
 										<div class="stats hourplayed">
 											<h4>28 <small>hours played</small></h4>
 										</div>
 										<div class="stats serverchange">
 											<h4>1547 <small>change server</small></h4>
 										</div>
 									</div>
 									<div class="col-md-6">
 										<div class="stats #">
 											<h4>547 <small>lorem 1</small></h4>
 										</div>
 										<div class="stats #">
 											<h4>157 <small>lorem 2</small></h4>
 										</div>
 										<div class="stats #">
 											<h4>147 <small>lorem 3</small></h4>
 										</div>
 									</div>
 								</div>
 							</div>
 						</div>
 					</div>