 <audio src="https://minegrech.com/v2/pages_site/page_userprofile/profile_themes/wd2/sound/intro_sound_wd2.mp3" autoplay></audio>
 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 	<link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/global.css">
 	<link rel="stylesheet" href="https://minegrech.com/v2/pages_site/page_userprofile/profile_themes/wd2/wd2.theme.css">
 	<link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/animation.css">
 	<script src="https://minegrech.com/v2/pages_site/page_userprofile/profile_themes/wd2/js/main.js"></script>
 </head>
 <body>
 <div class="background"></div>
 <div id="loader-wrapper">
 			<div id="loader"></div>
 			<div class="loader-section section-left"></div>
 			<div class="loader-section section-right"></div>
 		</div>
 	<div id="content" style="display: none;">
 		
 		<div class="navbar-position">
 			<!-- include navbar -->
 		</div>
 		<div class="container">
 			<div class="profile-body">
 				<div class="row">
 					<div class="col-md-2 animated zoomInDown">
 						<?php //NOMBRE DE USUARIO, IMAGEN, RANGOS. ?>
 						<?php include '/var/www/MineGrech/v2/pages_site/page_userprofile/profile_themes/wd2/part/section1.php'; ?>
 					</div>
 					<div class="col-md-10 animated flipInY">
 						<div class="separation">
 							<hr>
 						</div>
 						<?php include '/var/www/MineGrech/v2/pages_site/page_userprofile/profile_themes/wd2/part/section2.php'; ?>
 						<div class="col-md-2 animated zoomInDown">
 							<?php include '/var/www/MineGrech/v2/pages_site/page_userprofile/profile_themes/wd2/part/right-menu.php'; ?>
 						</div>
 					</div>
 					<div class="col-md-10 animated zoomInDown" style="margin-bottom: 5em;">
 						<?php include '/var/www/MineGrech/v2/pages_site/page_userprofile/profile_themes/wd2/part/stats.php'; ?>
 					</div>
 				</div>
 			</div>
 		</div>
 	</div>
 </body>
 </html> 