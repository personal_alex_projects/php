 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 	<link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/global.css">
 	<link rel="stylesheet" href="https://minegrech.com/v2/pages_site/page_userprofile/profile_themes/dark/dark.theme.css">
 	<link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/animation.css">
 </head>
 <body>
 	<div class="navbar-position" style="display: ">
 		<!-- include navbar -->
 	</div>
 	<div class="container">
 		<div class="profile-body">
 			<div class="row">
 				<div class="col-md-2 animated bounceInDown">
 					<?php //NOMBRE DE USUARIO, IMAGEN, RANGOS. ?>
 					<?php include '/var/www/MineGrech/v2/pages_site/page_userprofile/profile_themes/dark/part/section1.php'; ?>
 				</div>
 				<div class="col-md-10">
 					<div class="separation">
 						<hr>
 					</div>
 					<?php include '/var/www/MineGrech/v2/pages_site/page_userprofile/profile_themes/dark/part/section2.php'; ?>
 					<div class="col-md-2 animated bounceInDown">
 						<?php include '/var/www/MineGrech/v2/pages_site/page_userprofile/profile_themes/dark/part/right-menu.php'; ?>
 					</div>
 				</div>
 				<div class="col-md-10 animated bounceInDown" style="margin-bottom: 5em;">
 					<?php include '/var/www/MineGrech/v2/pages_site/page_userprofile/profile_themes/dark/part/stats.php'; ?>
 				</div>
 			</div>
 			<!-- DISABLED FOR NOW
 			<div class="col-md-10">
 				<hr>
 				<div id="well" class="well2">
 					<h4>Inventory & Rewards</h4>
 					<ul class="nav nav-tabs">
 						<li class="active"><a data-toggle="tab" href="#inventory">Inventory</a></li>
 						<li><a data-toggle="tab" href="#rewards">Rewards</a></li>
 					</ul>

 					<div class="tab-content">
 						<div id="inventory" class="tab-pane fade in active">
 							<h3>Inventory System</h3>
 							<p>We are working on it ...</p>
 						</div>
 						<div id="rewards" class="tab-pane fade">
 							<h3>Rewards System</h3>
 							<p>We are working on it ...</p>
 						</div>
 					</div>
 				</div>
 			</div>-->
 		</div>
 	</div>
 </body>
 </html>  