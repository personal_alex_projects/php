<div class="personal_detail">
	<div class="img-container animated pulse">
		<img class="img-thumbnail" src="https://www.minotar.net/avatar/AlexBanPer/160" alt="_avatar">
	</div>
	<div class="align-center">
		<div class="mg-username">
			<h4 class="theme">AlexBanPer</h4>
		</div>
		<div class="mg-user-role">
			<span class="label label-danger">CEO</span>
		</div>
		<hr>
		<!-- SI ESTA ONLINE Y ES SU CUENTA -->
		<div class="mg-actions-btn">
			<div class="mg-actions-title">
				<h5 class="theme">Actions</h5>
			</div>
			<div class="mg-actions-buttons">
				<button class="btn btn-default btn-block disabled">View My Friends</button>
				<button id="themeLink" class="btn btn-default btn-block">View Stats</button>
				<button id="themeLink" class="btn btn-default btn-block">Report Issue</button>
			</div>
		</div>
		<!-- SI ESTA ONLINE EN OTRA CUENTA -->
		<div class="mg-actions-btn">
			<div class="mg-actions-title">
				<h5 class="theme">Actions</h5>
			</div>
			<div class="mg-actions-buttons">
				<button class="btn btn-default btn-block disabled">Add Friend</button>
				<button id="themeLink" class="btn btn-default btn-block">Report Player</button>
				<button id="themeLink" class="btn btn-default btn-block">Report Issue</button>
			</div>
		</div>
		<!-- SI ESTA OFFLINE -->
		<div class="mg-actions-btn">
			<div class="mg-actions-title">
				<h5 class="theme">Actions</h5>
			</div>
			<div class="mg-actions-buttons">
				<button class="btn btn-default btn-block disabled">Add Friend</button>
				<button id="themeLink" class="btn btn-default btn-block disabled">Report Player</button>
				<button id="themeLink" class="btn btn-default btn-block disabled">Report Issue</button>
			</div>
		</div>
	</div>
</div>