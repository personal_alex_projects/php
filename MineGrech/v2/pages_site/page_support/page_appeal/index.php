  <!DOCTYPE html>
  <html lang="en">
  <!--
-
- ESTA PAGINA REQUIERE DE: 
- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
- <link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/global.css">
- <script src="https://minegrech.com/v2/pages_content/javascript/max-500.js"></script>
- <script src="https://minegrech.com/v2/pages_content/javascript/multipleinputs.tickets.js"></script>
-
-->
<body>
	<div class="navbar-position" style="display: ">
		<!-- include navbar -->
	</div>
	<div class="container" style="margin-top: 5em;">
		<div class="ticket_support">
			<div class="row">
				<form id="form1" action="https://www.minegrech.com/support/ticket/update/" method="POST">
					<h3><b>APPEAL MY ACTUAL BAN</b></h3>
					<br>
					<div class="col-md-6">
						<div class="form-group">
							<label for="sel1">My Actual Ban is:</label>
							<select class="form-control" name="problem" id="sel1">
								<option value="1">I have a problem with my website account.</option>
								<option value="2">I have a problem with my account on the server.</option>
								<option value="3">I want to report a bug / glitch.</option>
								<option value="4">I want information about the premium purchase.</option>
								<option value="5">I want to deliberate my last punishment.</option>
								<option value="6">Other Reason</option>
							</select>
						</div>
						<div class="form-group" id="not-punish">
							<label for="note"><b>Note</b></label>
							<p id="note"><span class="label label-info">This option is to appeal for your recent ban.</span><br><span class="label label-warning">We reserve the right to respond.</span></p>
						</div>
						<div class="form-group">
							<label for="usr">Why should we listen to you?</label>
							<input required type="text" class="form-control" name="subject" id="subject">
						</div>
						<div class="form-group">
							<label for="comment">It details what happened according to: <i>.(As protagonist of the facts).</i></label>
							<textarea required style="resize: none;" class="form-control" rows="5" name="comment" id="comment">Enter ALL the information possible. In the case of a reduced text which fails to explain your vision, this appeal will be denied. We will be able to help you according to the criteria of our moderators.</textarea>
								<span class="help-block"><p id="characterLeft" class="help-block ">You have reached the limit</p></span>
							</div>
						</div>	
						<div class="col-md-6">
							<div class="checkbox">
								<h4>Appeal Options</h4>
								<div class="checkbox">
									<label><input type="checkbox" name="priority" value="1" checked> This appeal is priority</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" name="superior" value="1" checked> My appeal will be sent to a MODERATOR</label>
								</div>
								<div class="checkbox" style="display: none">
									<label><input type="checkbox" name="personinfo" value="1"> My ticket contain personal information</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" name="downloadble" value="1"> My appeal contain downloadble material</label>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-10">
											<p><b>Note:</b> One link per line.</p>
											<label for="data[test][0]"><b>Test File</b></label>
											<input type="text" name="data[test][0]" placeholder="Links, videos." id="data[test][0]" class="form-control" />
											<div class="on-block">
												<button id="add-test" class="btn btn-default">Add File</button>
											</div>
										</div>
									</div>
								</div>
								<input type="submit" name="submit-btn" id="btnSubmit" disabled value="CREATE TICKET" class="btn btn-block btn-primary">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
	</html>