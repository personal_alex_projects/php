<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/global.css">
	<script src="https://use.fontawesome.com/eab7cd6eb3.js"></script>
</head>
 <!--
-
- ESTA PAGINA REQUIERE DE: 
- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
- <link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/global.css">
- FontAwesome (Importad a CDN Glaucus) <script src="https://use.fontawesome.com/eab7cd6eb3.js"></script>
-
-->
<body>
	<div class="navbar-position">
		<!-- include navbar -->
	</div>
	<div class="container">
		<div class="panel-support">
			<div class="row">
				<!-- SI ESTA ONLINE -->
				<div class="col-md-4">
					<div class="support_options">
						<div class="appeal-ban">
							<div class="appeal-ban-title">
								<h2>APPEAL BAN</h2>
							</div>
							<div class="appeal-ban-body">
								<p>Appeals exist in order to catch mistakes, not to argue about the rules, or your personal virtues. <br> If you feel that your punishment was wrong and contains evidence, open an appeal case.</p>
								<a href="#" class="btn btn-primary btn-block">APPEAL</a>
								<a href="#" class="btn btn-default btn-block">MY APPEALS</a>
							</div>
						</div>
						<hr>
						<div class="open-ticket">
							<div class="open-ticket-title">
								<h2>TICKET</h2>
							</div>
							<div class="open-ticket-body">
								<p>If you have questions, problems, you want to report an error, etc. You can do this through a support ticket. You should not appeal your punishment by this method.</p>
								<a href="#" class="btn btn-primary btn-block">OPEN TICKET</a>
								<a href="#" class="btn btn-default btn-block">MY TICKETS</a>
							</div>
						</div>
						<div class="bussines">
							<h2>BUSSINESS</h2>
							<p>You can contact with Glaucus Network via E-mail</p>
							<p><b>Email:</b> business@glaucus.net</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-md-push-2">
					<div class="mylast-ticket">
					<h2>Support Request</h2>
						<table class="table table-stripped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Type</th>
									<th>Status</th>
									<th>Last Update</th>
									<th>Taken By</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>ID001</td>
									<td>Ticket</td>
									<td><span class="label label-warning">Pending</span></td>
									<td>2 hours ago</td>
									<td>AlexBanPer</td>
									<td><a href="" class="btn btn-primary">VIEW</a></td>
								</tr>
								<tr>
									<td>ID001</td>
									<td>Appeal</td>
									<td><span class="label label-danger">Closed</span></td>
									<td>6 hours ago</td>
									<td>AlexBanPer</td>
									<td><a href="" class="btn btn-primary">VIEW</a></td>
								</tr>
								<tr>
									<td>No data registered.</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- SI ESTA OFFLINE -->
			<div class="row">
				<!-- SI ESTA ONLINE -->
				<div class="col-md-4">
					<div class="support_options">
						<div class="appeal-ban">
							<div class="appeal-ban-title">
								<h2>APPEAL BAN</h2>
							</div>
							<div class="appeal-ban-body">
								<p>Appeals exist in order to catch mistakes, not to argue about the rules, or your personal virtues. <br> If you feel that your punishment was wrong and contains evidence, open an appeal case.</p>
								<a href="#" class="btn btn-primary btn-block disabled">APPEAL</a>
							</div>
						</div>
						<hr>
						<div class="open-ticket">
							<div class="open-ticket-title">
								<h2>TICKET</h2>
							</div>
							<div class="open-ticket-body">
								<p>If you have questions, problems, you want to report an error, etc. You can do this through a support ticket. You should not appeal your punishment by this method.</p>
								<a href="#" class="btn btn-primary btn-block disabled">OPEN TICKET</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-md-push-2">
					<div class="mylast-ticket">
					<h2>PLEASE, LOGIN TO CONTINUE</h2>
					<hr>
						<a href="#" class="btn btn-primary btn-block">LOGIN</a>
						<a href="#" class="btn btn-warning btn-block">REGISTER</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html> 