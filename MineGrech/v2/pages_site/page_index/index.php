<!DOCTYPE html>
<html lang="en">
<!--
-
- ESTA PAGINA REQUIERE DE: 
- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
- <link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/global.css">
-
-->
<body>
	<div class="navbar-position">
		<!-- include navbar -->
	</div>
	<div class="site-body">
		<div class="image-picker">
			<div id="carousel-minegrech" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carousel-minegrech" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-minegrech" data-slide-to="1"></li>
					<li data-target="#carousel-minegrech" data-slide-to="2"></li>
				</ol>

				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<a href="#"><img src="http://placekitten.com/1600/600" /></a>
						<div class="carousel-caption">
							<h3>Minegrech Account</h3>
							<div class="row">
								<div class="col-md-6">
									<p><button class="btn btn-default btn-block">Login</button></p>
								</div>
								<div class="col-md-6">
									<p><button class="btn btn-primary btn-block">Register</button></p>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<a href="#"><img src="http://placekitten.com/1600/600" /></a>
						<div class="carousel-caption">
							<h3>Meow</h3>
							<p>Just Kitten Around</p>
						</div>
					</div>
					<div class="item">
						<a href="#"><img src="http://placekitten.com/1600/600" /></a>
						<div class="carousel-caption">
							<h3>Meow</h3>
							<p>Just Kitten Around</p>
						</div>
					</div>
				</div>

				<a class="left carousel-control" href="#carousel-minegrech" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#carousel-minegrech" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="minegrech-info">
					<center>
						<div class="col-md-12 margin border" style="margin-top: 12.5em;">
							<div class="col-md-4">
								<img src="https://cdn.glaucus.net/glaucus_minegrech/images/entrete_pose.png" alt="entretencion_pose" width="50%"> <br>
								Contamos con un sistema de recompensas increíble que contiene "cosas" variables y créeme que verdaderamente locas..
								jugarás horas y horas sin parar ni aburrirte, ¡prepárate para vivir una nueva experiencia con este espectacular y adictivo servidor!
							</div>
							<div class="col-md-4">
								<img src="https://cdn.glaucus.net/glaucus_minegrech/images/mod_pose.png" alt="Mod_pose" width="50%"> <br>
								Variedad: Tenemos muchas ideas que estarán en el futuro servidor mientras el mismo vaya progresando.
								Desde un modo supervivencia muy diferente y muchísimo mejor, muy extremo.. hasta juegos rankeds (por puntos) y variedad de minijuegos muy hardcors.
							</div>
							<div class="col-md-4">
								<img src="https://cdn.glaucus.net/glaucus_minegrech/images/variedad_pose.png" alt="Var_Pose" width="50%"> <br>
								¡Tú! sí tú, cuentas con un gran staff de moderación muy seguro.. que siempre estará al tanto de los problemas que tengas, o por si necesitas ayuda, no te preocupes por los tramposos que nuestra sistema actuará rápidamente y los eliminará adecuadamente del servidor.
							</div>
						</div>
					</center>
				</div>
				<div class="minegreh-blog-post" style="margin-top: 45em; margin-bottom: 5em;">
				<hr>
					<div class="blog-title">
						<h2>Last News <small><a href="#">>> All News</a></small></h2>
						<div class="blog-content">
							<div class="well row">
								<div class="col-md-4"  style="text-align: center;">
									<h3>%blog_subject%</h3>
									<div class="author">
										<div class="img-container">
											<img class="img-thumbnail" src="https://www.minotar.net/avatar/AlexBanPer/160" alt="_avatar">
										</div>
										<div class="align-center">
											<div class="mg-username">
												<h4 class="theme">AlexBanPer</h4>
											</div>
											<div class="mg-user-role">
												<span class="label label-danger">Owner [CEO]</span> <span class="label label-warning">Developer</span>
											</div>
											<hr>
										</div>
									</div>
									<p><b>Contributors:</b> xXNurioXx, Notch.</p>
									<br>
									<p><i>Posted 4 days ago</i></p>
								</div>
								<div class="col-md-6">
									<h3>This is a header</h3>
									<hr>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In incidunt placeat odit quos est magnam minus. Ad tenetur totam et veritatis. Expedita, magnam delectus inventore consequuntur! Dolorem voluptatibus vero veniam. <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt ex at illum ipsam laboriosam deserunt, veniam repudiandae asperiores quaerat totam vel voluptas, doloribus explicabo quae assumenda ea cum, dolorum eos. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consectetur, aliquam tempora eaque vel vitae harum labore mollitia eius nostrum consequatur reprehenderit iure, ullam quod dicta repudiandae possimus sint rerum.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In incidunt placeat odit quos est magnam minus. Ad tenetur totam et veritatis. Expedita, magnam delectus inventore consequuntur! Dolorem voluptatibus vero veniam. <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt ex at illum ipsam laboriosam deserunt, veniam repudiandae asperiores quaerat totam vel voluptas, doloribus explicabo quae assumenda ea cum, dolorum eos. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consectetur, aliquam tempora eaque vel vitae harum labore mollitia eius nostrum consequatur reprehenderit iure, ullam quod dicta repudiandae possimus sint rerum.</p>
								</div>	
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="minegreh-logs">
					<div class="logs-title">
						<h2>Last Updates <small><a href="#">>> All Updates</a></small></h2>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="thumbnail">
							<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=245%C3%97200&w=345&h=200" alt="...">
							<div class="caption">
								<h3><span class="label label-success">Added new road</span></h3>
								<p><b>Description: </b>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo molestiae saepe at illum iste quas quae, dolorem ducimus temporibus numquam accusantium, quam tempora magni dolore dolores aliquam dolor sunt nihil.</p>
								<p><b>Author:</b> AlexBanPer</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="thumbnail">
							<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=245%C3%97200&w=345&h=200" alt="...">
							<div class="caption">
								<h3><span class="label label-success">Added new road</span></h3>
								<p><b>Description: </b>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo molestiae saepe at illum iste quas quae, dolorem ducimus temporibus numquam accusantium, quam tempora magni dolore dolores aliquam dolor sunt nihil.</p>
								<p><b>Author:</b> AlexBanPer</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="thumbnail">
							<img src="https://placeholdit.imgix.net/~text?txtsize=23&txt=242%C3%97200&w=345&h=200" alt="...">
							<div class="caption">
								<h3><span class="label label-success">Added new road</span></h3>
								<p><b>Description: </b>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo molestiae saepe at illum iste quas quae, dolorem ducimus temporibus numquam accusantium, quam tempora magni dolore dolores aliquam dolor sunt nihil.</p>
								<p><b>Author:</b> AlexBanPer</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html> 