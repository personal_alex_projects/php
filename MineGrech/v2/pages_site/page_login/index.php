 <!DOCTYPE html>
 <html lang="en">
 <!--
-
- ESTA PAGINA REQUIERE DE: 
- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
- <link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/global.css">
- <link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/modern-form.css">
- <link rel="stylesheet" href="https://minegrech.com/v2/pages_content/css/modern-errors.css">
- FontAwesome (Importad a CDN Glaucus) <script src="https://use.fontawesome.com/eab7cd6eb3.js"></script>
-
-->
 <body>
 	<div class="navbar-position">
 		<!-- include navbar -->
 	</div>
 	<div class="container">
 		<div class="row">
 			<div class="col-md-8">
 				<div class="body-site">
 					<div class="panel-login">
 						<div class="error-message" style="text-align: center;">
 							<div class="error">
 							<i style="margin-left: -3em;" class="fa fa-minus-circle" aria-hidden="true"></i>
 								This is a error message
 							</div>
 						</div>
 						<div class="login-form">
 							<form action="https://www.minegrech.com/login/validate" method="post">
 								<div class="modern-form">
 									<h1 style="margin-left: -12em;margin-right: -12em;">LOGIN TO SYSTEM</h1>
 									<div class="texto-email">
 										<span>ENTER YOUR E-MAIL</span><br>
 									</div>
 									<input type="text" name="email" placeholder="myemail@something.com">
 									<div class="texto-pass">
 										<span>ENTER YOUR PASSWORD</span><br>
 									</div>
 									<input type="password" name="password" placeholder="***************">
 									<div class="re-captcha">
 										<div class="g-recaptcha" data-sitekey="6LfDKgwUAAAAAFGPdoBNz3LF-nhTYGbE7NDK0bDR"></div>
 									</div>
 									<br>
 									<input type="submit" name="submit" value="LOGIN" style="margin-bottom:1em;">
 									<!-- <input type="submit" name="register" value="%LOGIN_LOGIN%"> !-->
 									<?php echo "<p style='font-size: 10px;'>V3-291216</p>"; ?>
 								</div>
 							</form>
 						</div>
 					</div>
 				</div>
 			</div>
 			<div class="col-md-4">
				<?php include '/var/www/MineGrech/v2/pages_content/page_structure/php_includes/right-tabs.extra.php'; ?>
 			</div>
 		</div>
 	</div>
 </body>
 </html>