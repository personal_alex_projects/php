 <!DOCTYPE html>
 <html lang="en">
 <?php 
 error_reporting(E_ALL);
 ini_set('display_errors', 1);
////////////////////////////////////////////////////
//   Glaucus MySQL Connector | Open connection	  //
////////////////////////////////////////////////////
 global $gnmysql;
 global $gnsession;
 global $mgclass;
 global $gntools;

 include('/var/www/html/dynamic/classes/Global/mysql.class.php');
 $gnmysql = new GNMySQL();
////////////////////////////////////////////////////
 $webmetatitle = "Minegrech Home";
 $webmetadesc  = "Minegrech Inicio, descubre el mundo de diversion que ofrecemos para nuestros jugadores de todo el mundo!";
 $webtitle     = "Minegrech - Inicio";
 $extrahead    = "";

 include  '/var/www/MineGrech/dynamic/content/extrahead.php';

 include('/var/www/html/dynamic/classes/Global/session.class.php');
 $gnsession = new GNSession();

 include('/var/www/html/dynamic/classes/MineGrech/mg.session.class.php');
 if ($gnsession->isLoggedIn()) {
 	$mymgsession = new MGSession(null);
 }

 include('/var/www/html/dynamic/classes/MineGrech/minegrech.class.php');
 $mgclass = new MineGrech();

 include('/var/www/html/dynamic/classes/MineGrech/blog.class.php');
 $mgblog = new MGBlog();

 include('/var/www/html/dynamic/classes/Global/tools.class.php');
 $gntools = new GNTools();

 include('/var/www/html/dynamic/language/setupLanguage.php');
 ?>
 <body>
 	<style>
 		.margin{
 			margin-top: 2%;
 			margin-bottom: 2%;
 		}
 		.border{
 			border: 1px solid #F5F5F5;
 		}
 	</style>
 	<div class="navbar-position">
 		<?php include '/var/www/MineGrech/dynamic/content/navbar.php'; ?>
 	</div>
 	<div class="container">
 		<div class="row">
 			<center>
 				<div class="logo margin" style="margin-top: 9.5em;">
 					<img src="https://placeholdit.imgix.net/~text?txtsize=17&txt=250%C3%97250&w=250&h=250" height="250px" width="250px" alt="logo">
 					<h1>MINEGRECH</h1>
 				</div>
 			</center>
 			<center>
 				<div class="news">
 					<div class="col-md-12 margin border" style="margin-top: 12.5em;">
 						<div class="col-md-4">
 							<img src="https://cdn.glaucus.net/glaucus_minegrech/images/entrete_pose.png" alt="entretencion_pose" width="50%"> <br>
 							Contamos con un sistema de recompensas increíble que contiene "cosas" variables y créeme que verdaderamente locas..
 							jugarás horas y horas sin parar ni aburrirte, ¡prepárate para vivir una nueva experiencia con este espectacular y adictivo servidor!
 						</div>
 						<div class="col-md-4">
 							<img src="https://cdn.glaucus.net/glaucus_minegrech/images/mod_pose.png" alt="Mod_pose" width="50%"> <br>
 							Variedad: Tenemos muchas ideas que estarán en el futuro servidor mientras el mismo vaya progresando.
 							Desde un modo supervivencia muy diferente y muchísimo mejor, muy extremo.. hasta juegos rankeds (por puntos) y variedad de minijuegos muy hardcors.
 						</div>
 						<div class="col-md-4">
 							<img src="https://cdn.glaucus.net/glaucus_minegrech/images/variedad_pose.png" alt="Var_Pose" width="50%"> <br>
 							¡Tú! sí tú, cuentas con un gran staff de moderación muy seguro.. que siempre estará al tanto de los problemas que tengas, o por si necesitas ayuda, no te preocupes por los tramposos que nuestra sistema actuará rápidamente y los eliminará adecuadamente del servidor.
 						</div>
 					</div>
 				</div>
 			</center>
 			<div class="body-site" style="margin-top: 20em;">
 				<div class="col-md-8" style="margin-top: 1.6em; margin-bottom: 2em;margin-left: -2em;">
 					<div class="col-md-12 margin border">
 						<h3>Last Updates</h3>
 						<?php

 						$log = $mgclass->getBuildManagerByType(3);
 						if ($mgclass->getBuildManagerByType(0) != 0) {
 							foreach ($log as $data) {
 								$mills 	= $data['AddedMills'];
 								$status	= $data['status'];
 								$reason = $data['reason'];

 								$ago = $gntools->timeToAGO($mills);


 								if ($status == 1) {
 									echo '[<i>At '.$ago.' - <span class="label label-success">'.$reason.'</span></i>] ';
 								}elseif ($status == 2) {
 									echo '[<i>At '.$ago.' - <span class="label label-warning">'.$reason.'</span></i>] ';
 								}elseif ($status == 3){
 									echo '[<i>At '.$ago.' - <span class="label label-danger">'.$reason.'</span></i>] ';
 								}else{
 									echo '[<i>At '.$ago.' - <span class="label label-default">unknown: '.$reason.'</span></i>] ';
 								}
 							}

 						}else{
 							echo "No data!";
 						}
 						?>
 						<br>
 						<button class="btn btn-default btn-block" style="margin-top: 1em;">All Updates</button>
 					</div>
 					<br>
 					<div class="blog_part border" style="display: flex;" style="margin-top: 2.3em;">
 						<div class="well" style="margin-left: -0.1em;">
 							<div class="col-md-4 margin border" >
 								<center><h5><?php echo $mgblog->getBlogSubject(); ?></h5></center>
 								<b><?php echo $mgblog->getBlogAuthorNick(); ?></b>
 								<i>At <?php echo $mgblog->getBlogCreated(); ?></i>
 								<br>
 								<button class="btn btn-default btn-block" style="margin-top: 1em;">Blog</button>
 								<button class="btn btn-default btn-block" style="margin-top: 1em;">Forums</button>
 							</div>
 							<div class="col-md-8 margin border">
 								<h1><?php echo $mgblog->getBlogHeader(); ?></h1>
 								<div class="blog_content">
 									<?php echo $mgblog->getBlogDescription(); ?>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 				<div class="col-md-4 border" style="margin-bottom: 5em; margin-top: 1em;">
 					<?php include '/var/www/MineGrech/dynamic/content/right_nav.php'; ?>
 				</div>
 			</div>
 		</div>
 	</div>
 </body>
 </html>