<div class="profile-social-network">
			<h3><b>Other Accounts</b></h3>
			<p>You only <b>must</b> set the username. Ex: https://twitter.com/{username} </p>
			<hr>
			<div class="body-sn">
				<div class="row">
					<form action="https://www.minegrech.com/edit/update/social" method="POST">
						<div class="col-md-4">
							<div class="form-group">
								<label for="utwitter"><b>Twitter</b></label>
								<input maxlength="15" type="text" name="utwitter" class="form-control" id="utwitter" value="<?php echo $mymgsession->getUserTwitter(); ?>">
							</div>
							<div class="form-group">
								<label for="uface"><b>Facebook</b></label>
								<input maxlength="50" type="text" name="uface" class="form-control" id="uface" value="<?php echo $mymgsession->getUserFacebook(); ?>">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="uskype"><b>Skype</b></label>
								<input maxlength="30" type="text" name="uskype" class="form-control" id="uskype" value="<?php echo $mymgsession->getUserSkype(); ?>">
							</div>
							<div class="form-group">
								<label for="uyoutube"><b>Youtube</b></label>
								<input maxlength="21" type="text" name="uyoutube" class="form-control" id="uyoutube" value="<?php echo $mymgsession->getUserYoutube(); ?>">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label for="usteam"><b>Steam</b></label>
								<input maxlength="32" type="text" name="usteam" class="form-control" id="usteam" value="<?php echo $mymgsession->getUserSteam(); ?>">
							</div>
							<div class="form-group">
								<label for="ureddit"><b>Reddit</b></label>
								<input maxlength="20" type="text" name="ureddit" class="form-control" id="ureddit" value="<?php echo $mymgsession->getUserReddit(); ?>">
							</div>
						</div>
						<br>
						<input type="submit" name="socialsubmit" class="btn btn-primary btn-block" value="Update">
					</form>
				</div>
			</div>
		</div>