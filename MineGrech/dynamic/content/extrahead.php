<head>
	<title><?php echo $webtitle; ?></title>
	<meta name="Title" content="<?php echo $webmetatitle; ?>"/>
	<meta name="Description" content="<?php echo $webmetadesc; ?>"/>
	<meta name="Keywords" content="minegrech, glaucus network, servidores minecraft, 2016, 2017, alexbanper, xxnurioxx, plugins, kohi, servidores, network, minegrech network, minegrech twitter, glaucus.net, minegrech.com, minijuegos, staff, lobby."/>
	<meta name="Language" content="es"/>
	<meta name="Distribution" content="Global"/>
	<meta name="robots" content="index,follow,all"/>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/boot.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/css/main.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_minegrech/navbarstyle.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<?php 
	echo $extrahead;
	include('/var/www/html/sessions/glaucusstatics.php');
	?>
</head>