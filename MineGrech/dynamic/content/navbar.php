<nav class="navbar navbar-inverse navbar-fixed-top navbar-padding">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header" style="margin-left: 1em;">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      </ul>
      <ul class="nav navbar-nav navbar-right" style="margin-right: 1em;">
      <?php if ($gnsession->isLoggedIn()): ?>
         <li><a href="https://minegrech.com/profile/<?php echo $mymgsession->getName(); ?>">MY PROFILE</a></li>
         <?php if ($mymgsession->getRank() > "500"): ?>
          <li><a href="https://minegrech.com/hk/">HK PANEL</a></li>
         <?php endif ?>
      <?php endif ?>
        <li><a href="https://minegrech.com/">HOME</a></li>
        <li><a href="https://minegrech.com/support">SUPPORT</a></li>
        <li><a href="https://minegrech.com/shop">SHOP</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav> 