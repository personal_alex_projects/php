<?php 
include 'site.include.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="https://cdn.glaucus.net/img/favicon.png" type="image/x-icon">
	<title>Glaucus Indie - <?php echo $title_var; ?> </title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdn.glaucus.net/styles/glaucus_indie/maindesing.css">
	<meta name="Title" content="Indie Glaucus">
	<meta name="Description" content="Indie Gamer is a community where all the developers of applications, games, etc. They can publish their content so that all people can interact with it, whether they are paid or free platforms.">
	<meta name="Keywords" content="minegrech, glaucus network, servidores minecraft, 2016, 2017, alexbanper, xxnurioxx, plugins, kohi, servidores, network, minegrech network, minegrech twitter, glaucus.net, minegrech.com, minijuegos, staff, lobby, indie, developer, gamer, indie game, indie development, development, developer indie">
	<meta name="Language" content="en">
	<meta name="Distribution" content="Global">
	<meta name="robots" content="index,follow,all">
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Glaucus Indie</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<?php if ($active == true): ?>
						<li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
					<?php else: ?>
						<li><a href="#">Home</a></li>
					<?php endif; ?>
					<li><a href="#">About Us</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<form class="navbar-form navbar-left" role="search">
						<button type="submit" class="btn btn-default">Submit</button>
						<div class="form-group">
							<input class="form-control" placeholder="Search: " type="text">
						</div>
					</form>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Account <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="javascript:openGlaucusPopUp();">Login</a></li>
							<li><a href="https://glaucus.net/register/">Register</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
