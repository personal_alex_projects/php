<?php 
$title_var = "Not Found!";
$active = false;
include('../../_site_data/_site_content/head.include.php');
?>
<div class="wrapper-body">
	<div class="row ajust-row" style="text-align: center;">
		<div class="col-md-12">
			<h2>Oops!</h2>
			<p>We are doing maintenance and we will be back in an hour.</p>
			<p style="font-size: 12px;">If you want to know what is happening, visit our twitter.</p>
			<h5><a href="https://twitter.com/GlaucusNetwork">Watch Twitter</a></h5>
		</div>
	</div>
</div>
<div class="nav-footer" style="position: fixed;">
	Glaucus, Inc. 2015-2017 | Because united we are stronger
</div>
</body>
</html>