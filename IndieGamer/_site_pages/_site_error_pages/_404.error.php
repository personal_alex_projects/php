<?php 
$title_var = "Not Found!";
$active = false;
include('../../_site_data/_site_content/head.include.php');
?>
<div class="wrapper-body">
	<div class="row ajust-row" style="text-align: center;">
		<div class="col-md-12">
			<h2>Oops!</h2>
			<p>We can´t seem to find the page you´re looking for.</p>
			<p style="font-size: 12px;">Did you enter the correct link?</p>
			<h5><a href="#">Go to home!</a></h5>
		</div>
	</div>
</div>
<div class="nav-footer" style="position: fixed;">
	Glaucus, Inc. 2015-2017 | Because united we are stronger
</div>
</body>
</html>