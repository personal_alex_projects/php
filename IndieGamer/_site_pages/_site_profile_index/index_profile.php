<?php 
$title_var = "Home";
$active = false;
include('../../_site_data/_site_content/head.include.php');
?>
<div class="wrapper-body">
	<div class="row ajust-row">
		<div class="col-md-3" style="position: fixed;">
			<div class="nav-left">
				<div class="list-group">
					<a href="#" class="list-group-item active">
						My Projects <span class="badge">0</span>
					</a>
					<a href="#" class="list-group-item disabled">
						My History <span class="badge">0</span>
					</a>
					<a href="#" class="list-group-item">
						Messages <span class="badge">0</span>
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-8 col-md-push-3">
			<div class="well">
				<div class="alert alert-dismissible alert-info">
				  <button type="button" class="close" data-dismiss="alert">&times;</button>
				  <strong>Heads up!</strong> This <a href="#" class="alert-link">alert needs your attention</a>, but it's not super important.
				</div>
							<div class="my-games-titles">
				<h3>My Games Library</h3>
			</div>
			</div>
			<hr>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading" style="padding-bottom: 2em;">
						<div class="text-left">
							<div>My Game Name</div>
						</div>
						<div class="text-right">
							<div><i>Alpha</i></div>
						</div>
					</div>
				  <div class="panel-body">
				    <div class="progress-game">
				    Game Progress (60%)
				    	 <div class="progress">
						  <div class="progress-bar" style="width: 60%;"></div>
						</div>
						<div class="last-updates">
							<ul class="breadcrumb">
							  <li><a href="#">Update #1</a></li>
							  <li><a href="#">Update #2</a></li>
							  <li class="active">Release</li>
							</ul>
						</div>
				    </div>
				    <div class="text-left">
				    	<a href="#">Download</a> | <a href="#">Edit Game</a>
				    </div>
				    <div class="text-right">
				    	<a href="#">Add Update</a> | <a href="#">View Details</a>
				    </div>
				  </div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading" style="padding-bottom: 2em;">
						<div class="text-left">
							<div>My Game Name</div>
						</div>
						<div class="text-right">
							<div><i>Alpha</i></div>
						</div>
					</div>
				  <div class="panel-body">
				    <div class="progress-game">
				    Game Progress (60%)
				    	 <div class="progress">
						  <div class="progress-bar" style="width: 60%;"></div>
						</div>
						<div class="last-updates">
							<ul class="breadcrumb">
							  <li><a href="#">Update #1</a></li>
							  <li><a href="#">Update #2</a></li>
							  <li class="active">Release</li>
							</ul>
						</div>
				    </div>
				    <div class="text-left">
				    	<a href="#">Download</a> | <a href="#">Edit Game</a>
				    </div>
				    <div class="text-right">
				    	<a href="#">Add Update</a> | <a href="#">View Details</a>
				    </div>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="nav-footer" style="position: fixed;">
	Glaucus, Inc. 2015-2017 | Because united we are stronger
</div>
</body>
</html>