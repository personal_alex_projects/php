<?php 
$title_var = "Add Application";
$active = false;
include('../../_site_data/_site_content/head.include.php');
?>
<div class="wrapper-body">
	<div class="row ajust-row">
		<div class="col-md-3" style="position: fixed;">
			<?php include '../../_site_data/_site_content/leftnav.include.php'; ?>
		</div>
		<div class="col-md-8 col-md-push-3">
			<div class="well">
				<div class="alert alert-dismissible alert-info">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Heads up!</strong> This <a href="#" class="alert-link">alert needs your attention</a>, but it's not super important.
				</div>
				<div class="my-games-titles">
					<h3>Add New Product</h3>
				</div>
			</div>
			<hr>
			<script>
				function appearNumericVersion(){
					if(document.getElementById('select_version').value == "4") {
						document.getElementById("product_version").style.visibility = "visible";
					}else{
						document.getElementById("product_version").style.visibility = "hidden";
					}
				}
				$(document).ready(function() {
					var max_fields      = 8;
					var wrapper         = $(".input_fields_wrap");
					var add_button      = $(".add_field_button");

					var x = 1;
					$(add_button).click(function(e){ 
						e.preventDefault();
						if(x < max_fields){ 
							x++; 
							$(wrapper).append('<div style="margin-bottom: 5px;" class="form-group" id="product_newimg"><label for="inputVersion" class="col-lg-2 control-label"><a href="#" class="remove_field">Remove</a></label><div class="col-lg-10"><div class="input_fields_wrap"><div><input class="form-control" type="text" name="myImages[]"/></div><br></div></div></div>'); 
						}
					});

    				$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
    					e.preventDefault(); document.getElementById('product_newimg').remove(); x--;
    				})
    			});
    		</script>
    		<form action="" class="form-horizontal">
    			<div class="form-group">
    				<label for="inputName" class="col-lg-2 control-label">Product Name:</label>
    				<div class="col-lg-10">
    					<input name="product_name" class="form-control input-lg" id="inputName" placeholder="Name of your product..." type="text">
    				</div>
    			</div>
    			<div class="form-group">
    				<label for="select_product" class="col-lg-2 control-label">My Product is</label>
    				<div class="col-lg-10">
    					<select name="product_type" class="form-control" id="select_product">
    						<option value="0">Desktop Game</option>
    						<option value="1">Web Game</option>
    						<option value="2">Desktop App</option>
    						<option value="3">Web App</option>
    					</select>
    					<br>
    				</div>
    			</div>
    			<div class="form-group">
    				<label class="col-lg-2 control-label">Require buy?</label>
    				<div class="col-lg-10">
    					<div class="radio">
    						<label class="radio-inline">
    							<input name="requirePaid" id="requirePaid1" value="0" checked="" type="radio">
    							My product is <b>Free</b>.
    						</label>
    					</div>
    					<span class="label label-info">Note: that in the future, the option of "payment product" will be available. However you can update this list once this function its ready.</span>
						<!--<div class="radio disabled">
							<label class="radio-inline disabled">
								<input name="requirePaid" id="requirePaid2" value="1" type="radio">
								Yes,
							</label>
						</div>-->
					</div>
				</div>
				<!--<div class="form-group">
					<label class="control-label">Custom URL - <div style="color: #ff4c06; display: -moz-deck;">[* Premium Feature]</div></label>
					<div class="input-group disabled">
						<span class="input-group-addon">https://indie.glaucus.net/projects/{<b>here</b>}</span>
						<input class="form-control disabled" type="text">
						<span class="input-group-btn">
							<button class="btn btn-default disabled" type="button">Check</button>
						</span>
					</div>
				</div>-->
				<div class="form-group">
					<label for="select_status" class="col-lg-2 control-label">Current Status</label>
					<div class="col-lg-10">
						<select name="product_status" class="form-control" id="select_status">
							<option value="0">Release</option>
							<option value="1">Early Access</option>
						</select>
						<br>
					</div>
				</div>
				<div class="form-group">
					<label for="select_version" class="col-lg-2 control-label">Name Version:</label>
					<div class="col-lg-10">
						<select name="product_version" class="form-control" id="select_version">
							<option value="0">Pre-Alpha</option>
							<option value="1">Alpha</option>
							<option value="2">Open Beta</option>
							<option value="3">Closed Beta</option>
							<option value="4">Launched</option>
						</select>
						<br>
					</div>
				</div>
				<div class="form-group" id="product_version">
					<label for="inputVersion" class="col-lg-2 control-label">Numeric Version:</label>
					<div class="col-lg-10">
						<input name="product_name" class="form-control" id="inputVersion" placeholder="Ex: 1.0.10" type="text">
					</div>
				</div>
				<div class="form-group" id="product_progress">
					<label for="inputVersion" class="col-lg-2 control-label">Game Progress: (0-100)</label>
					<div class="col-lg-10">
						<input name="product_progress" class="form-control" id="inputVersion" placeholder="Ex: 60 = 60%" type="text">
					</div>
				</div>
				<div class="image-inputs">
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">Image References</h3>
								</div>
								<div class="panel-body">
									<p style="font-size: 80%;">At the moment we do not work with files uploaded to our servers, this is due to possible illegal copies, but we are not saying that we will not have these functions over time.</p>
									Currently we work with two image managers, which can be <a href="https://gyazo.com/">Gyazo</a> or <a href="http://imgur.com/">Imgur</a>.
									<p>We recommend using Photoshop for the perfect pixel of the image and a better record of it.</p>
									<p style="font-size: 77%;"><b>Header:</b> 1150x210(px), <b>Reference:</b> 180x150(px), <b>Logo:</b> 200x200(px)</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<span class="label label-warning">Use the links with the direct image, ie the link with termination (.jpg) or (.png)</span>
							<hr>
							<div class="form-group" id="product_img_header">
								<label for="inputVersion" class="col-lg-2 control-label">Header:</label>
								<div class="col-lg-10">
									<input name="product_img_header" class="form-control" id="inputVersion" placeholder="Ex: http://i.imgur.com/rhIFt6w.jpg" type="text">
								</div>
							</div>
							<div class="form-group" id="product_version">
								<label for="inputVersion" class="col-lg-2 control-label">Reference: </label>
								<div class="col-lg-10">
									<input name="product_img_reference" class="form-control" id="inputVersion" placeholder="Ex: http://i.imgur.com/rhIFt6w.jpg" type="text">
								</div>
							</div>
							<div class="form-group" id="product_version">
								<label for="inputVersion" class="col-lg-2 control-label">Logo:</label>
								<div class="col-lg-10">
									<input name="product_img_logo" class="form-control" id="inputVersion" placeholder="Ex: http://i.imgur.com/rhIFt6w.jpg" type="text">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="galery-inputs">
					<div class="row">
						<div class="col-md-6">
							<span class="label label-info">These images appear on the main page of your product.</span>
							<hr>
							<div class="form-group" id="product_img_header">
								<label for="inputVersion" class="col-lg-2 control-label">Trailer Video:</label>
								<div class="col-lg-10">
									<input name="product_gameplay" class="form-control" id="inputVersion" placeholder="Ex: Youtube Video" type="text">
								</div>
							</div>
							<div class="form-group" id="product_version">
								<label for="inputVersion" class="col-lg-2 control-label">Review Video: </label>
								<div class="col-lg-10">
									<input name="product_review" class="form-control" id="inputVersion" placeholder="Ex: Youtube Video" type="text">
								</div>
							</div>
							<div class="form-group" id="product_version">
								<label for="inputVersion" class="col-lg-2 control-label">Images:</label>
								<div class="col-lg-10">
									<div class="input_fields_wrap">
										<div><input class="form-control" type="text" placeholder="Ex: http://i.imgur.com/rhIFt6w.jpg" name="myImages[]"></div>
										<br>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">Library Information</h3>
								</div>
								<div class="panel-body">
									<p>Review video is equals to Gameplay.<br>You can click the button to add fields (images) with a maximum of 8 images.</p>
									<p><span class="label label-danger"><b>Remember: </b></span>&nbsp;&nbsp; The use of pornographic images or anything NOT related to the main theme of the website, will be sanctioned.</p>
									<p>We recommend images of desktop size or 1920×1080(px)</p>
									<button class="btn btn-default btn-block add_field_button">Add Image</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="textArea" class="col-lg-2 control-label">Game Description</label>
					<div class="col-lg-10">
						<textarea class="form-control" name="gamedesc" rows="3" id="textArea" style="width: 704px; height: 137px; resize: none;"></textarea>
						<span class="help-block">Write the best you can about your product, keep in mind that this description should attract the attention of your players. You can also detail all those who cooperated in the game.</span>
					</div>
				</div>
				<div class="form-group">
					<label for="textArea" class="col-lg-2 control-label">Game Credits</label>
					<div class="col-lg-10">
						<textarea name="gamecredits" class="form-control" rows="3" id="textArea" style="width: 704px; height: 137px; resize: none;"></textarea>
						<span class="help-block">Add all developer notes, credits for some content, instructions for use of the product, etc.</span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputName" class="col-lg-2 control-label">Support Email:</label>
					<div class="col-lg-10">
						<input name="product_support" class="form-control" id="inputName" placeholder="Some email of support..." type="text">
					</div>
				</div>
				<div class="downloadcontent">
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">Download References</h3>
								</div>
								<div class="panel-body">
									<p style="font-size: 80%;">We do not want your files to be classified as "Malicious", for this we ask you to follow our instructions, although not by obligation. However, we will give support in your project if you follow all our indications.</p>
									<p>
										<ol>
											<li>You must enter a valid download link.</li>
											<li>You must enter a <a href="https://www.virustotal.com/">VirusTotal</a> link</li>
										</ol> 
									</p>
									<p style="font-size: 77%;"><b>Download Managers Accepted:</b> <a href="https://mega.nz/">MEGA</a>, <a href="https://www.mediafire.com/">Mediafire</a> & <a href="https://www.dropbox.com/">Dropbox</a></p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<hr>
							<div class="form-group">
								<label for="inputName" class="col-lg-2 control-label">Download Link:</label>
								<div class="col-lg-10">
									<input name="product_download" class="form-control" id="inputName" placeholder="Link of MEGA, Mediafire or Dropbox" type="text">
								</div>
							</div>
							<div class="form-group">
								<label for="inputName" class="col-lg-2 control-label">VirusTotal Link:</label>
								<div class="col-lg-10">
									<input name="product_virustotal" class="form-control" id="inputName" placeholder="Link of VirusTotal" type="text">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tagcenter">
					<div class="row">
						<div class="col-md-6">
							<div class="col-md-4">
								<div class="checkbox">
									<label><input name="tag0" type="checkbox" value="">Action</label>
								</div>
								<div class="checkbox">
									<label><input name="tag1" type="checkbox" value="">Adventure</label>
								</div>
								<div class="checkbox">
									<label><input name="tag2" type="checkbox" value="">Puzzle</label>
								</div>
								<div class="checkbox">
									<label><input name="tag3" type="checkbox" value="">Multiplayer</label>
								</div>
								<div class="checkbox">
									<label><input name="tag4" type="checkbox" value="">Co-op</label>
								</div>
								<div class="checkbox">
									<label><input name="tag5" type="checkbox" value="">Survival</label>
								</div>
								<div class="checkbox">
									<label><input name="tag6" type="checkbox" value="">RPG</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="checkbox">
									<label><input name="tag7" type="checkbox" value="">PvP</label>
								</div>
								<div class="checkbox">
									<label><input name="tag8" type="checkbox" value="">PvE</label>
								</div>
								<div class="checkbox">
									<label><input name="tag9" type="checkbox" value="">SandBox</label>
								</div>
								<div class="checkbox">
									<label><input name="tag10" type="checkbox" value="">Strategy</label>
								</div>
								<div class="checkbox">
									<label><input name="tag11" type="checkbox" value="">First Person</label>
								</div>
								<div class="checkbox">
									<label><input name="tag12" type="checkbox" value="">Third Person</label>
								</div>
								<div class="checkbox">
									<label><input name="tag13" type="checkbox" value="">Sports</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="checkbox">
									<label><input name="tag14" type="checkbox" value="">Arcade</label>
								</div>
								<div class="checkbox">
									<label><input name="tag15" type="checkbox" value="">Cartoon</label>
								</div>
								<div class="checkbox">
									<label><input name="tag16" type="checkbox" value="">Auto Update</label>
								</div>
								<div class="checkbox">
									<label><input name="tag17" type="checkbox" value="">Purshases</label>
								</div>
								<div class="checkbox">
									<label><input name="tag18" type="checkbox" value="">Free To Play</label>
								</div>
								<div class="checkbox">
									<label><input name="tag19" type="checkbox" value="">DLC</label>
								</div>
								<div class="checkbox">
									<label><input name="tag20" type="checkbox" value="">Horror</label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">Tag Center</h3>
								</div>
								<div class="panel-body">
									<p>In this section, be sure to select all the features of your game. (Anyway you can edit them later)</p>
									<p>Add everything that your game contains, this will help to advertise the same.</p>
									<p><span class="label label-danger">* Caution:</span> If your game does not fit with a tag, it could be sanctioned.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="nav-footer">
		Glaucus, Inc. 2015-2017 | Because united we are stronger
	</div>
</body>
</html>