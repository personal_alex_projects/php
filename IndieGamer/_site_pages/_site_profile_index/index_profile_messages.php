<?php 
$title_var = "Home";
$active = false;
include('../../_site_data/_site_content/head.include.php');
?>
<div class="wrapper-body">
	<div class="row ajust-row">
		<div class="col-md-3" style="position: fixed;">
			<?php include '../../_site_data/_site_content/leftnav.include.php'; ?>
		</div>
		<div class="col-md-8 col-md-push-3">
			<div class="well">
				<div class="alert alert-dismissible alert-info">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Heads up!</strong> This <a href="#" class="alert-link">alert needs your attention</a>, but it's not super important.
				</div>
				<div class="my-games-titles">
					<h3>Messages</h3>
				</div>
			</div>
			<hr>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">Received 4 hours ago...</div>
					<div class="panel-body">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, dolorem dicta laborum autem beatae veniam nobis quam ad. Fugiat atque voluptatibus molestiae aliquid, possimus ipsa ex libero beatae reprehenderit! Natus!
					</div>
					<div class="panel-footer" style="padding-bottom: 2em;">
						<div class="text-left">
							By Admin.
						</div>
						<div class="text-right">
							<a href="#">Read</a> | <a href="#">Delete</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">Received 4 hours ago...</div>
					<div class="panel-body">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, dolorem dicta laborum autem beatae veniam nobis quam ad. Fugiat atque voluptatibus molestiae aliquid, possimus ipsa ex libero beatae reprehenderit! Natus!
					</div>
					<div class="panel-footer" style="padding-bottom: 2em;">
						<div class="text-left">
							By Admin.
						</div>
						<div class="text-right">
							<a href="#">Read</a> | <a href="#">Delete</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">Received 4 hours ago...</div>
					<div class="panel-body">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, dolorem dicta laborum autem beatae veniam nobis quam ad. Fugiat atque voluptatibus molestiae aliquid, possimus ipsa ex libero beatae reprehenderit! Natus!
					</div>
					<div class="panel-footer" style="padding-bottom: 2em;">
						<div class="text-left">
							By Admin.
						</div>
						<div class="text-right">
							<a href="#">Read</a> | <a href="#">Delete</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">Received 4 hours ago...</div>
					<div class="panel-body">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, dolorem dicta laborum autem beatae veniam nobis quam ad. Fugiat atque voluptatibus molestiae aliquid, possimus ipsa ex libero beatae reprehenderit! Natus!
					</div>
					<div class="panel-footer" style="padding-bottom: 2em;">
						<div class="text-left">
							By Admin.
						</div>
						<div class="text-right">
							<a href="#">Read</a> | <a href="#">Delete</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">Received 4 hours ago...</div>
					<div class="panel-body">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, dolorem dicta laborum autem beatae veniam nobis quam ad. Fugiat atque voluptatibus molestiae aliquid, possimus ipsa ex libero beatae reprehenderit! Natus!
					</div>
					<div class="panel-footer" style="padding-bottom: 2em;">
						<div class="text-left">
							By Admin.
						</div>
						<div class="text-right">
							<a href="#">Read</a> | <a href="#">Delete</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">Received 4 hours ago...</div>
					<div class="panel-body">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, dolorem dicta laborum autem beatae veniam nobis quam ad. Fugiat atque voluptatibus molestiae aliquid, possimus ipsa ex libero beatae reprehenderit! Natus!
					</div>
					<div class="panel-footer" style="padding-bottom: 2em;">
						<div class="text-left">
							By Admin.
						</div>
						<div class="text-right">
							<a href="#">Read</a> | <a href="#">Delete</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">Received 4 hours ago...</div>
					<div class="panel-body">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, dolorem dicta laborum autem beatae veniam nobis quam ad. Fugiat atque voluptatibus molestiae aliquid, possimus ipsa ex libero beatae reprehenderit! Natus!
					</div>
					<div class="panel-footer" style="padding-bottom: 2em;">
						<div class="text-left">
							By Admin.
						</div>
						<div class="text-right">
							<a href="#">Read</a> | <a href="#">Delete</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">Received 4 hours ago...</div>
					<div class="panel-body">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, dolorem dicta laborum autem beatae veniam nobis quam ad. Fugiat atque voluptatibus molestiae aliquid, possimus ipsa ex libero beatae reprehenderit! Natus!
					</div>
					<div class="panel-footer" style="padding-bottom: 2em;">
						<div class="text-left">
							By Admin.
						</div>
						<div class="text-right">
							<a href="#">Read</a> | <a href="#">Delete</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="nav-footer">
	Glaucus, Inc. 2015-2017 | Because united we are stronger
</div>
</body>
</html>