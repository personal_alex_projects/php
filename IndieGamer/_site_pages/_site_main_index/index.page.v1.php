<?php 
$title_var = "Home";
$active = true;
include('../../_site_data/_site_content/head.include.php');
?>
<div class="wrapper-body">
	<div class="row ajust-row">
		<div class="col-md-8">
			<div class="alert alert-dismissible alert-info">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Heads up!</strong> This <a href="#" class="alert-link">alert needs your attention</a>, but it's not super important.
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Panel primary</h3>
				</div>
				<div class="panel-body">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus tempore deleniti nisi aut odit, eveniet, quam. Similique perferendis soluta obcaecati, voluptas ex saepe impedit, laudantium incidunt, a nesciunt ratione iure!
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis consequuntur cumque assumenda dolore eaque. Nostrum cum doloremque hic inventore, nemo voluptatem deserunt minus consequuntur tempore pariatur fuga at itaque suscipit!
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. A nesciunt sint rem, et aliquam. Velit harum reiciendis neque eum soluta recusandae repellendus voluptatem accusantium. Cupiditate tenetur accusantium consectetur saepe alias.
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum dolore hic atque, nesciunt delectus ab veritatis commodi consequuntur omnis, earum ullam velit at quisquam neque, sed voluptatum amet deserunt ipsa.
				</div>
			</div>
			<hr>
			<div class="well">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis quas adipisci possimus at temporibus similique inventore, facilis, dolor, et quod culpa ipsam necessitatibus facere eum soluta dolorum eius debitis rem.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est aperiam sunt eaque et eos dolorem laboriosam accusantium, consectetur fugiat cum iure qui saepe aliquid fugit, aspernatur, hic perspiciatis dolores repellat.
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel-right">
				<div class="panel-right-item-1">
					<h3>Last Registers</h3>
					<div class="well">
						<a href="#">abcdefghfjklñopq</a> <a href="#">abcdefghfjklñopq</a> <a href="#">abcdefghfjklñopq</a> <a href="#">abcdefghfjklñopq</a> <a href="#">abcdefghfjklñopq</a> <a href="#">abcdefghfjklñopq</a> <a href="#">abcdefghfjklñopq</a>
					</div>
				</div>
				<div class="panel-right-division"><hr></div>
				<div class="panel-right-item-2">
					<h3>Last Projects <small>> <a href="#">see all</a></small></h3>
					<div class="ajust-center">
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-body ">
									<img src="https://placeholdit.imgix.net/~text?txtsize=13&txt=140%C3%97120&w=140&h=120" alt="150x150">
								</div>
								<div class="panel-footer">My Last Project</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-body">
									<img src="https://placeholdit.imgix.net/~text?txtsize=13&txt=140%C3%97120&w=140&h=120" alt="150x150">
								</div>
								<div class="panel-footer">My Last Project</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="nav-footer">
	Glaucus, Inc. 2015-2017 | Because united we are stronger
</div>
</body>
</html>