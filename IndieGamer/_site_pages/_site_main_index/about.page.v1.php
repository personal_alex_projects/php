<?php 
$title_var = "About Us";
$active = true;
include('../../_site_data/_site_content/head.include.php');
?>
<div class="wrapper-body">
	<div class="row ajust-row">
		<div class="col-md-8">
			<div class="alert alert-dismissible alert-info">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Heads up!</strong> This <a href="#" class="alert-link">alert needs your attention</a>, but it's not super important.
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Why <b>Glaucus Indie</b>?</h3>
				</div>
				<div class="panel-body">
					<h2>A network of fun</h2>
					<p>Glaucus Indie is inspired for all those independent developers, who want to bring to light their most valued projects, so that they become known to other people, be qualified and can progress thanks to the support of people.</p>
					<p>We want Glaucus Indie to be part of our knowledge to know that there are many people who want to see you succeed. We will make of your game, application or software something that many conoscan, we will help you to report errors, solve problems and many other functions that will be available later.</p>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">About <b>US</b></h3>
				</div>
				<div class="panel-body">
					<h2></h2>
				</div>
			</div>
			<hr>
			<div class="well">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis quas adipisci possimus at temporibus similique inventore, facilis, dolor, et quod culpa ipsam necessitatibus facere eum soluta dolorum eius debitis rem.
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis aliquam a corporis nihil delectus, praesentium facere nam laudantium vel ut? Necessitatibus est, laudantium nulla expedita ea odio. Laudantium, voluptatibus, recusandae.
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel-right">
				<div class="panel-right-item-1">
					<h3>Last News</h3>
					<div class="well">
						<a href="#">test blog name informational</a> <b>|</b> <a href="#">new blog open to the site</a> <b>|</b> <a href="#">now you can write more text!</a>
					</div>
				</div>
				<div class="panel-right-division"><hr></div>
				<div class="panel-right-item-2">
					<h3>Last Projects <small>> <a href="#">see all</a></small></h3>
					<div class="ajust-center">
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-body ">
									<img src="https://placeholdit.imgix.net/~text?txtsize=13&txt=140%C3%97120&w=140&h=120" alt="150x150">
								</div>
								<div class="panel-footer">My Last Project</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-default">
								<div class="panel-body">
									<img src="https://placeholdit.imgix.net/~text?txtsize=13&txt=140%C3%97120&w=140&h=120" alt="150x150">
								</div>
								<div class="panel-footer">My Last Project</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="nav-footer">
	Glaucus, Inc. 2015-2017 | Because united we are stronger
</div>
</body>
</html>