<?php error_reporting(E_ALL);
ini_set("display_errors", 1); ?>
<!DOCTYPE html>
<html>
<head>
	<!-- TITULO -->
	<title>GoodNights Network</title>
	<link rel="shortcut icon" href="http://i.imgur.com/25scyJw.png">


	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">


	<!-- METADATOS -->
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<meta name="GN.Title" content="GoodNights">
	<meta name="GN.Creator" content="MrNaikito - Nicolas Hernandez">
	<meta name="GN.Subject" content="Comunidad de juegos. (Servidores de Juegos)">
	<meta name="GN.Description" content="***">
	<meta name="GN.Publisher" content="AlexBanPer & Bootstrap Desing">
	<meta name="GN.Date" content="8/11/2015 -- ocho de noviembre de dos mil quince.">
	<meta name="GN.Type" scheme="GNMIType" content="Text">
	<meta name="GN.Format.Medium" content="text/html">
	<meta name="GN.Relation.isPartOf" content="http/www.goodnights.cl">
	<meta name="GN.Identifier" content="      ****">
	<meta name="GN.Language" content="es">
	<meta name="keywords" lang="es" content="vacaciones, Grecia, sol">
	<meta name="GN.Rights" content="(c) GoodNights.cl &copy | Derechos Reservados a los respectivos propietarios | Diseño a AlexBanPer & Bootstrap Desing | El uso inadecuado de este sitio puede llevar a causas legales">

	<!-- INSERTAR bootstrap CONTENT -->
	<?php include ('_content/_bootstrap/boot.html'); ?>
	<link rel="stylesheet" type="text/css" href="css/main.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<script type="text/javascript">
	var onwork = "Estamos trabajando en el sitio, por favor informa de cualquier error.";

	alert(onwork);
	</script>
</head>
<body>
	<!-- APARTADO DE DEVELOPER 
	<div class="container">
		<div class="jumbotron">
			<h1>Bajo Desarrollo</h1>
			<p>WebMaster: AlexBanPer => <code>alexbanper@glaucus.net</code></p>
		</div>
	</div>-->
	<!-- DISEÑO GENERAL -->
	<?php include ('_content/_nav/navbar.html'); ?>
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
			<li data-target="#myCarousel" data-slide-to="3"></li>
		</ol>
		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<img src="http://i.imgur.com/l5xAEBf.png" alt="Chania" width="460" height="345">
			</div>

			<div class="item">
				<img src="http://i.imgur.com/Ty7ZQdX.png" alt="Chania" width="460" height="345">
			</div>

			<div class="item">
				<img src="http://i.imgur.com/Ty7ZQdX.png" alt="Flower" width="460" height="345">
			</div>

			<div class="item">
				<img src="http://i.imgur.com/Ty7ZQdX.png" alt="Flower" width="460" height="345">
			</div>
		</div>

		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<img src="https://cdn2.iconfinder.com/data/icons/snipicons/5000/chevron-left-128.png" id="ajusteimg">
			<span class="sr-only">Anterior</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<img src="https://cdn2.iconfinder.com/data/icons/snipicons/5000/chevron-right-512.png" id="ajusteimg">
			<span class="sr-only">Siguiente</span>
		</a>
	</div>
	<div class="banner-bottom" style="text-align:center">
		<div class="pull-left alignleft">
			<!--<a href="http://www.zglobalhost.com/"><img id="achicar" src="http://www.zglobalhost.com/wp-content/uploads/2015/07/logo.png" style="width: 350px;"></a> -->
		</div>
		<div class="pull-right alignright">
			<!-- <a href="http://bandaservers.cl/"><img src="http://bandaservers.cl/foro/public/style_images/BandaServers2014IMG/bannerforos2015.png" style="width: 350px;"></a> -->
		</div>
	</div>
	<!--<div class="jumbotron banner-text">
		<div class="container">
			<audio href="starwars.mp3"></audio>
			<a id="tamano-boton" class="btn btn-primary btn-large" href="/play">Servidores</a>
		</div>
	</div>
	<!--<footer>
		<div class="container">
			<div class="pull-left">
				&copy GoodNights Network
			</div>
			<div class="pull-right">
				Designed by AlexBanPer
			</div>
		</div>
	</footer> -->
	<div class="container" style="margin-bottom:100px;">
		<center>
			<div class="row" style="margin-top:30px; margin-bottom:25px;">
				<div class="col-md-4" id="test">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</div>
				<div class="col-md-4" id="test">
					<img src="">
				</div>
				<div class="col-md-4" id="test">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</div>
			</div>
		</center>
	</div>

	<div class="banner-footer">
		<p style="margin-left:10px; margin-top: 95px;">
			&copy Derechos reservados a GoodNights 2015-2016.
			<div style="float:right; margin-top: -35px;">
				<table style="margin-right: 40px; margin-top: -25px;">
					<tbody>
						<tr style="margin-left: -10px;">
							<td style="padding-right: 15px;"><a href="#">Contact Us</a></td>
							<td><a href="#">Follow Us</a></td>
						</tr>
					</tbody>
				</table>
				<div id="movemenow">
					<img src="http://i.imgur.com/up3so8d.png" id="footer_socials">
					<img src="http://i.imgur.com/NBoqg3G.png" id="footer_socials">
					<img src="http://i.imgur.com/dLz78vp.png" id="footer_socials">
				</div>				
			</div>
		</p>
	</div>
</body>
</html>
<!-- COLOR PARA NAVBAR: #e7a413 -->