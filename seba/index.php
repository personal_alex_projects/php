<?php 
error_reporting(E_ALL & ~E_NOTICE);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Concesionario - Agregar Auto</title>
</head>
<body>
	<center>
		<h3>Agregar Vehiculo a la Base de Datos</h3>
		<div class="error">
			<?php 
			if ($_GET['err'] == "submit") {
				echo '<span style="background-color: red;">Debe rellenar el formulario!</span>';
			}elseif ($_GET['err'] == "modelo") {
				echo '<span style="background-color: red;">Debe ingresar un modelo!</span>';
			}elseif ($_GET['err'] == "marca") {
				echo '<span style="background-color: red;">Debe ingresar una marca!</span>';
			}elseif ($_GET['err'] == "year") {
				echo '<span style="background-color: red;">Debe ingresar un año!</span>';
			}elseif ($_GET['listo'] == "si") {
				echo '<span style="background-color: green;">Datos agregados!</span>';
				header('Refresh: 1; url=/seba/');
			}

			?>
		</div>
		<div class="formulario">
			<form action="validar.php" method="POST">
				<table>
					<tr>
						<td><label for="marca">Marca del Vehiculo</label></td>
						<td><input type="text" name="marca" minlength="2" placeholder="Ingresar marca"></td>
					</tr>
					<tr>
						<td><label for="modelo">Modelo del vehiculo</label></td>
						<td><input type="text" name="modelo" minlength="2" placeholder="Ingresar modelo"></td>
					</tr>
					<tr>
						<td><label for="year">Año del vehiculo</label></td>
						<td>
							<select name="year" id="year">
								<option value="null" disabled selected>Año de Vehiculo</option>
								<?php 
								$i = 1;
								while ($i <= 20) {
									if ($i < 10) {
										echo "<option value='200$i'>200$i</option>";
									}else{
										echo "<option value='20$i'>20$i</option>";
									}

									$i++;
								}

								?>
							</select>
						</td>
					</tr>
					<tr>
						<td><input type="submit" name="submit" value="Agregar Vehiculo"></td>
					</tr>
				</table>
			</form>
		</div>
	</center>
</body>
</html>