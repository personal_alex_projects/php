<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GoodNights Network</title>
	<?php include('/var/www/GoodNights/assets/includes/head.php'); ?>
	<link rel="stylesheet" href="/var/www/GoodNights/assets/desing/css/index_cuentas.css">
</head>
<body>
<?php 
include('/var/www/GoodNights/assets/includes/menu.php');
require('/var/www/GoodNights/assets/functions/cuenta_funcs.php');
$steamidreg = $_SESSION['steamid'];
$lastnickname = $steamprofile['personaname'];
$email = $_REQUEST['email'];

if (isset($_SESSION['steamid'])) {
	if (isset($_POST['submit'])) {
		if ($email) {
			createAccount($steamidreg, $lastnickname, $email);
			?>
			<div class="container">
				<center>
					<div class="well">
						<div class="redireccion">
							<h2>Exito!, te hemos agregado a nuestra base de datos!</h2>
							<br>
							<h3>Serás rederigido en 5 segundos!</h3>
							<p><b>Datos agregados: </b> <?php echo $email; ?> </p>
							<?php header('Refresh: 5; URL=https://goodnights.cl/sesion/micuenta/?exito=confirmado') ?>
						</div>
					</div>
				</center>
				<div class="bottom">
					<?php include('/var/www/GoodNights/assets/includes/footer.php'); ?>
				</div>
			</div>
			<?php
		}else{
			?>
			<div class="container">
				<center>
					<div class="well">
						<div class="redireccion">
							<h2>Alto!, tu correo no fue establecido!</h2>
							<br>
							<h3>Serás rederigido en 5 segundos!</h3>
							<?php header('Refresh: 5; URL=https://goodnights.cl/sesion/micuenta') ?>
						</div>
					</div>
				</center>
				<div class="bottom">
					<?php include('/var/www/GoodNights/assets/includes/footer.php'); ?>
				</div>
			</div>
			<?php
		}
	}else{
		?>
		<div class="container">
			<center>
				<div class="well">
					<div class="redireccion">
						<h2>Alto!, creemos que no deberias estar aqui!</h2>
						<br>
						<h3>Serás rederigido en 5 segundos!</h3>
						<?php header('Refresh: 5; URL=https://goodnights.cl/sesion/micuenta') ?>
					</div>
				</div>
			</center>
			<div class="bottom">
				<?php include('/var/www/GoodNights/assets/includes/footer.php'); ?>
			</div>
		</div>
		<?php
	}
}else{
	header('Location: https://goodnights.cl/sesion/ingreso/');
}
?>
</body>
</html>