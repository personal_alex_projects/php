<?php 
require('/var/www/GoodNights/assets/functions/cuenta_funcs.php');?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>GoodNights Network</title>
<?php include('/var/www/GoodNights/assets/includes/head.php'); ?>
<link rel="stylesheet" href="./var/www/GoodNights/assets/desing/css/index_cuentas.css">
</head>
<body>
<?php 
include('/var/www/GoodNights/assets/includes/menu.php');
if (!isset($_SESSION['steamid'])) {
	header("Location: http://goodnights.cl/sesion/ingreso");
}else{?>
	<div class="container">
		<div class="well">
			<div class="row">
				<div class="col-md-2">
					<div class="personaldetails">
						<div class="imagenperfil">
							<a href="<?php echo $steamprofile['profileurl']; ?>"><img src="<?php echo $steamprofile['avatarfull']; ?>" class="img-responsive" alt="<?php echo $steamprofile['personaname']; ?>_image"></a>
						</div>
						<div class="tag">
							<center><span class="label label-warning">Usuario</span></center>
						</div>
					</div>
				</div>
				<div class="col-md-5">
				<?php
			   		$detalles = verCuenta($_SESSION['steamid']);
			     	foreach ($detalles as $row) {
			     		$steamid = $row['steamid'];
			     		$email = $row['user_email'];
			     		$activada = $row['isActiveAccount'];
			     		$alreadyreg = $row['isUser'];
			     	}
			     ?>
			     <div class="errores">
			     	<span class="label label-danger boldtxt"><b>Tu cuenta no esta activada! </b></span><a style="margin-left: 2.5%;" href='#'>Activar</a>
			     	<br>
			     	<?php 
			     	if ($_GET['exito'] == "confirmado") {
			     	 	echo '<span class="label label-success boldtxt"><b>Tu cuenta fue creada con exito!</b></span>';
			     	}
			     	?>
			     </div>
			     
			     <div class="actualizar">
			     </div>
			     <div class="userinfo">
			     	<div class="miembrodesde">
			     		<span><b>Eres miembro desde: </b> <i><?php echo date("Y-m-d\TH:i:s\Z", $steamprofile['timecreated']); ?></i></span>
			     	</div>
			     	<div class="steamid">
			     		<span><b>SteamID:</b> <i><?php echo $steamprofile['steamid']; ?></i></span>
			     		<?php ?>
			     	</div>
			     	<div class="personalname">
			     		<span><b>Nickname Steam:</b> <i><?php echo $steamprofile['personaname']; ?></i></span>
			     	</div>
			     	<div class="realname">
			     		<span><b>Nombre Real Steam: </b> <i><?php echo $steamprofile['realname']; ?></i></span>
			     	</div>
			     	<div class="profileurl">
			     		<span><b>URL:</b> <i><a href="<?php echo $steamprofile['profileurl']; ?>"><?php echo $steamprofile['profileurl']; ?></a></i></span>
			     	</div>
			     	<?php if ($email == "") {
			     		echo "<div class='correo'>";
			     		echo "<span><b>Email</b> <i>¡ ACTUALIZA TUS DATOS !</i></span>";
			     		echo "</div>";
			     	}else{
			     		echo "<div class='correo'>";
			     		echo "<span><b>Email</b> <i>$email</i></span><a href='#'> +Editar</a>";
			     		echo "</div>";
			     	} ?>
			     	<br>
			     	<?php if ($alreadyreg == ""){ ?>
			     	<div class="actualizardatos">
			     		<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#registro">Actualizar mis datos</button>
			     	</div>
			     	<?php } ?>
			     </div>
			 </div>
			 <div class="col-md-3">
			 	<div class="viptime">
			 		<div class="tiemporestante">
			 			<div class="txtrestante">
			 				Tiempo restante de VIP
			 			</div>
			 			<div class="NOtxtrestante">
			 				Tu no tienes ningun VIP alquilado!
			 			</div>
			 			<span>00:00:00:00</span>
			 			<br>
			 			<div class="NOtxtrestante">
			 				<p style="color:red">Estimado, recuerda que nuestro sitio web esta siendo desarrollado constantemente y esta version no es la oficial, por lo que tendra muchos cambios a lo largo del tiempo. La version actual es compatible para un registro exitoso.</p>
			 			</div>
			 		</div>
			 	</div>
			 </div>
			</div>
		</div>
		<?php } ?>
		<div class="bottom">
			<?php include('../../assets/includes/footer.php'); ?>
		</div>
		<div id="registro" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Crear Registro</h4>
					</div>
					<div class="modal-body">
						<p>Actualmente no estas oficialmente registrado en nuesta base de datos, por lo que no podras realizar compras o utilizar el sistema de soporte.</p>
						<p>Crea de forma oficial tu registro completando los datos faltantes y comprobando los que ya existen.</p>
						<form class="form-horizontal" action="actualizando_cuenta/" method="POST">
							<fieldset>
								<legend>Datos de registro</legend>
								<div class="form-group">
									<label for="steamidreg" class="col-lg-2 control-label">SteamID</label>
									<div class="col-lg-10">
										<input class="form-control" name="steamidreg" type="text" disabled id="steamidreg" value="<?php echo $steamprofile['steamid']; ?>">
									</div>
									<label for="steamnick" class="col-lg-2 control-label">Steam Nickname</label>
									<div class="col-lg-10">
										<input class="form-control" disabled id="steamnick" name="steamnick" value="<?php echo $steamprofile['personaname']; ?>" type="text">
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="col-lg-2 control-label">Email</label>
									<div class="col-lg-10">
										<input class="form-control" id="email" name="email" placeholder="email@example.com" type="text">
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-10 col-lg-offset-2">
										<button type="submit" name="submit" class="btn btn-primary">¡Hacer oficial mi registro!</button>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">No deseo registrarme por ahora</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>