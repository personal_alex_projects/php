<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GoodNights Network</title>
	<?php include('../../assets/includes/head.php'); ?>
	<link rel="stylesheet" href="">
</head>
<body>
	<?php 
	$active = 5;
	include('../../assets/includes/menu.php');
	if(!isset($_SESSION['steamid'])) {
	?>
	<div class="container" style="margin-top:5em;">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="well">
					<form action="" method="">
						<div class="form-group">
							<label for="exampleInputEmail1">Email address</label>
							<input type="email" disabled class="form-control" id="exampleInputEmail1" placeholder="Email">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input type="password" disabled class="form-control" id="exampleInputPassword1" placeholder="Password">
						</div>
						<center><?php loginbutton('large'); ?></center>
						<!--<button type="submit" class="btn btn-primary">Registro</button>-->
						<div class="lostpass">
							<!--<a href="#">Olvide la contraseña</a>-->
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="bottom">
			<?php include('../../assets/includes/footer.php'); ?>
		</div>
	</div>
	<?php
	}else{
		header('https://goodnights.cl/sesion/micuenta');
	}
	?>
</body>
</html>