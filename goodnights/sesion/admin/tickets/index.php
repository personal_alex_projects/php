<?php 
session_start();
if (isset($_SESSION['admin'])) {
	include('/var/www/GoodNights/assets/includes/head.php');
	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>GoodNights Network - Admin Tickets</title>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Asunto</th>
									<th>Enviado Por</th>
									<th>Actualizado</th>
									<th>Ult. Respuesta</th>
									<th>Estado</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>#001</td>
									<td>Problema con pago</td>
									<td>AlexBanPer</td>
									<td>00/00/00 00:00:00</td>
									<td>Naikito</td>
									<td><span class="label label-warning">Pendiente</span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>
	<?php
}else{
	echo "No estas ingresado al sistema!";
}?>