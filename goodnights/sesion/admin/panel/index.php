<?php 
session_start();
if (isset($_SESSION['admin'])) {
	include('/var/www/GoodNights/assets/includes/head.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GoodNights Network - Administracion</title>
</head>
<body>
	<div class="container">
		<?php echo "Bienvenido al panel de administracion: ".$steamprofile['personaname']; ?>
		<div class="menuV1">
			<li>
				<ul><a href="https://goodnights.cl/sesion/admin/tickets/">Ver tickets</a></ul>
				<ul><a href="#">Ver compras</a></ul>
				<ul><a href="https://goodnights.cl/sesion/admin/usuarios">Ver usuarios</a></ul>
				<ul><a href="logout.php">Salir</a></ul>
			</li>
		</div>
	</div>
</body>
</html>
<?php
}else{
	echo "No estas ingresado al sistema!";
}?>