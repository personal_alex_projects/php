<?php 
session_start();
if (!isset($_SESSION['admin'])) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Goodnights Network - Admin</title>
	<link rel="stylesheet" href="https://goodnights.cl/assets/desing/css/index_admin.css">
	<?php include('/var/www/GoodNights/assets/includes/head.php'); ?>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 .col-md-offset-3">
				<div class="well">
					<form action="checkadmin.php" method="POST">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email" name="email" minlength="6" class="form-control" placeholder="Email">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" name="password" class="form-control" placeholder="Password">
					</div>
					<input type="submit" class="btn btn-default" name="submit" value="Ingresar">
				</form>
				</div>
			</div>
		</div>
		<div class="bottom">
			<?php include('/var/www/GoodNights/assets/includes/footer.php'); ?>
		</div>
	</div>
</body>
</html>
<?php 	
}else{

}

?>
