<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GoodNights Network</title>
	<?php include('/var/www/GoodNights/assets/includes/head.php'); ?>
	<link rel="stylesheet" href="https://goodnights.cl/assets/desing/css/index_main.css">
</head>
<body>
	<?php 
	$active = 1;
	include('/var/www/GoodNights/assets/includes/menu.php');
	?>
	<div class="bodyBack">
		<div class="container">
			<div class="parte1">
				<div>
					<div class="logo">
						<center>
							<img class="img-responsive" width="45%" src="https://goodnights.cl/assets/desing/img/logo_central.png" alt="logo_goodnights">
						</center>
					</div>
				</div>
			</div>
			<div class="parte2">
				<div class="servidores">
					<div class="row">
						<div class="col-md-4">
							<div class="servidorcs16">
								<div class="well">
									<div class="imgStatus">
										<img class="offline" src="https://cdn.glaucus.net/goodnights_main/img/offline_red.png" alt="offline">
										<div class="offlineTxt">
											<span>Offline</span>
										</div>
									</div>
									<div class="info">
										<div class="hostname">
											<span class="infotext">HOSTNAME</span>
											<div class="well opacity bordered">
												<code>SERVER NAME</code>
											</div>
										</div>
										<div class="direccionip">
											<span class="infotext">SERVER IP</span>
											<div class="well opacity bordered">
												<code>127.0.0.1</code>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="servidorts">
								<div class="well">
									<div class="imgStatus">
										<img class="offline" src="https://cdn.glaucus.net/goodnights_main/img/offline_red.png" alt="offline">
										<div class="offlineTxt">
											<span>Offline</span>
										</div>
									</div>
									<div class="info">
										<div class="hostname">
											<span class="infotext">HOSTNAME</span>
											<div class="well opacity">
												<code>SERVER NAME</code>
											</div>
										</div>
										<div class="direccionip">
											<span class="infotext">SERVER IP</span>
											<div class="well opacity bordered">
												<code>127.0.0.1</code>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="servidorcsgo">
								<div class="well">
									<div class="imgStatus">
										<img class="offline" src="https://cdn.glaucus.net/goodnights_main/img/offline_red.png" alt="offline">
										<div class="offlineTxt">
											<span>Offline</span>
										</div>
									</div>
									<div class="info">
										<div class="hostname">
											<span class="infotext">HOSTNAME</span>
											<div class="well opacity">
												<code>SERVER NAME</code>
											</div>
										</div>
										<div class="direccionip">
											<span class="infotext">SERVER IP</span>
											<div class="well opacity bordered">
												<code>127.0.0.1</code>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="bottom">
				<?php include('/var/www/GoodNights/assets/includes/footer.php'); ?>
			</div>
		</div>
	</div>
</body>
</html>