<?php  include ('/var/www/GoodNights/assets/steamauth/userInfo.php'); ?>
<div class="ownnavbar">
   <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
			
			<li class="navbar-brand" style="list-style-type: none;">
				<a href="#" class="navbar-brand" data-toggle="dropdown" role="button" aria-expanded="false"><img src="https://placehold.it/230x80" alt="GoodNights Logo"></a>
			</li>

         </div>
         <div id="navbar3" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
               <li <?php if($active == 1) echo 'class="active"';?>><a href="/">Inicio</a></li>
               <li <?php if($active == 2) echo 'class="active"';?>><a href="/tienda">Tienda</a></li>
               <li <?php if($active == 3) echo 'class="active"';?>><a href="/servidores">Servidores</a></li>
               <li <?php if($active == 4) echo 'class="active"';?>><a href="/soporte">Soporte</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
                  <a id="accounts" href="#" class="<?php if($active == 5) echo 'active';?>dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php if(!isset($_SESSION['steamid'])){ echo "Cuenta";}else{
                      include ('/var/www/GoodNights/assets/steamauth/userInfo.php');
                     echo $steamprofile['personaname']; }?> <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                     <li class="divider"></li>
                     <?php
                      if(!isset($_SESSION['steamid'])){
                        echo "<li><a href='https://goodnights.cl/sesion/ingreso'>Ingresar</a></li>";
                        echo "<li><a href='https://goodnights.cl/sesion/ingreso'>Registrarme</a></li>";
                     }else{
                        echo "<li><a href='https://goodnights.cl/sesion/micuenta'>Mi Cuenta</a></li>";
                        echo "<li>"+logoutbutton()+"</li>";
                     }
                     ?>
                     <li class="divider"></li>
                  </ul>
               </li>
            </ul>
         </div>
      </div>
   </nav>
</div>