<!-- Meta Objets -->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<meta name="viewport" content="initial-scale=1.0">

<!-- Global CSS -->
<link rel="stylesheet" href="https://cdn.glaucus.net/goodnights_main/boot.css">
<link rel="stylesheet" href="https://cdn.glaucus.net/goodnights_main/navbarstyle.css">

<!-- APIs & JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<?php
 require '/var/www/GoodNights/assets/steamauth/steamauth.php';
 include ('/var/www/GoodNights/assets/steamauth/userInfo.php');?>
