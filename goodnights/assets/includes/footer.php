<div class="footer">
	<hr class="style1">
	<div class="row">
		<div class="well">
			<div class="col-md-5 .col-xs-5">
				<div class="copyrights">
					Copyrights &copy 2016-2017 <a href="#">GoodNights Network</a> <br>
					<p>Sitio desarrollado por <a href="#">AlexBanPer</a></p><br>
					<p>Fase de desarrollo ALPHA [Dev 07082016.2313 V1.0.01]</p>
				</div>
			</div>
			<div class="col-md-4 .col-sm-4">
				<div class="social_icons">
					<div class="fb_icon .visible-*-inline">
						<a href="https://www.facebook.com/GoodNightsOficial/"><img class="" src="https://goodnights.cl/assets/desing/img/FB_ico.jpg" alt="fb_ico"></a>
					</div>
					<div class="yt_icon .visible-*-inline">
						<a href="https://www.youtube.com/channel/UCXYZ4EptYjITEvHJeWglwfA"><img class="" src="https://goodnights.cl/assets/desing/img/youtube_ico.png" alt="fb_ico"></a>
					</div>
					<div class="tw_icon .visible-*-inline">
						<a href="https://twitter.com/GoodNightsCL"><img class="" src="https://goodnights.cl/assets/desing/img/twitter_ico.png" alt="fb_ico"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>