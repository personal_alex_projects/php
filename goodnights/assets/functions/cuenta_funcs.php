<?php 
function isActiveAccount($steamid){
	require 'dbconn.php';
	$sql = $db->prepare("SELECT 'isActiveAccount' FROM 'usuarios_registrados' WHERE steamid=:steamid");
	$sql->bindParam(':steamid', $steamid, PDO::PARAM_STR, 32);
	$sql->execute();
	$row = $sql->fetchAll();

	return $row;
	$db = null;
}
function createAccount($steamid, $steamnick, $user_email){
	require 'dbconn.php';
	$sql = $db->prepare("INSERT INTO usuarios_registrados (steamid, steamnick, user_email) VALUES (:steamid, :steamnick, :user_email)");
	$sql->bindParam(':steamid',$steamid , PDO::PARAM_STR, 32);
	$sql->bindParam(':steamnick', $steamnick);
	$sql->bindParam(':user_email', $user_email, PDO::PARAM_STR, 35);

	$sql->execute();
	$sql = null;
}
function verCuenta($steamid){
	require 'dbconn.php';
	$sql = $db->prepare("SELECT * FROM usuarios_registrados WHERE steamid=:steamid");
	$sql->bindParam(':steamid',$steamid , PDO::PARAM_STR, 32);

	$sql->execute();
	$row = $sql->fetchAll();

	return $row;
	
	$sql = null;
}



?>