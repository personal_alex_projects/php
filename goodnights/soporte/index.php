<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GoodNights Network</title>
	<?php include('../assets/includes/head.php'); ?>
	<link rel="stylesheet" href="../assets/desing/css/soporte_main.css">
</head>
<body>
	<?php 
	$active = 4;
	include('../assets/includes/menu.php');
	?>
	<div class="container">
		<div class="well wellcolor">
			<form action="">
				<div class="row">
					<div class="col-md-4">
						<div class="opciones">
							<div class="selectserver form-group">
								<label for="SelectTarget">Selecciona el servidor</label>
								<select class="form-control" name="sserver" id="sserver">
									<option value=""></option>
								</select>
							</div>
							<div class="selectoption" class="form-group">
								<label for="SelectTheme">Selecciona un posible tema de conflicto</label>
								<select class="form-control" name="ssolucion" id="ssolucion">
									<option value="">lorem</option>
									<option value="">lorem1</option>
									<option value="">lorem2</option>
									<option value=""></option>
									<option value=""></option>
									<option value=""></option>
								</select>
							</div>
						</div>
						<div class="sujeto">
							<label class="form-group txtIndicaProblema" for="IndicaProblema"><b>Indica en forma resumida el problema.</b></label>
							<input type="text" class="form-group inputTheme col-sm-12" name="sujeto" value="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum sint officia laborum debitis in necessitatibus, iusto facilis nemo maiores! Impedit dignissimos eligendi, sequi quasi! Perspiciatis dicta cupiditate, tenetur ratione incidunt.">
						</div>
						<div class="feedback">
							<p>Tambien si lo deseas, puedes dejar un <a href="/feedback">comentario al desarrollador</a>.</p>
						</div>
					</div>
					<div class="col-md-8">
						<div class="texto">
							<div class="form-group">
								<label for="SelectTheme"><b>Detalla el problema, colocando pruebas, nombres, imagenes, etc.</b></label>
								<textarea class="form-control" name="mensaje" id="mensaje" cols="40" rows="10" class="form-control">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, saepe eveniet ducimus inventore nihil et, dolore quas consectetur labore vel, blanditiis, totam cupiditate architecto! At perferendis sed, quas iusto corporis!</textarea>
							</div>
						</div>
						<hr class="style1">
						<div class="botones">
							<div class="enviar">
								<input class="btn btn-primary form-group" type="submit" name="submit" value="Enviar">
							</div>
							<div class="cancelar">
								<a href="../"><button class="btn btn-danger form-group">Cancelar</button></a>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="bottom">
			<?php include('../assets/includes/footer.php'); ?>
		</div>
	</div>
</body>
</html>