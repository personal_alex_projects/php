<?php 
	require('/var/www/GoodNights/assets/functions/feedback_funcs.php');
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>GoodNights Network</title>
	<?php include('/var/www/GoodNights/assets/includes/head.php'); ?>
	<link rel="stylesheet" href="https://goodnights.cl/assets/desing/css/index_feedback.css">
</head>
<body> 
	<?php 
	include('/var/www/GoodNights/assets/includes/menu.php');
	?>
	<div class="container">
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<div class="table-responsive">
						<table class="table table-striped">
							<tr>
								<th>#</th>
								<th>Dev Log</th>
								<th>Autor</th>
								<th>Fecha</th>
								<th>Estado</th>
								<?php
								if(isset($_SESSION['steamid'])) { 
									if ($steamprofile['steamid'] == "76561198098160950") {
										echo "<th>Más</th>";
										echo "<th><a href='#'>Add Log</a></th>";
									}	
								}?>
							</tr>
							
							<?php
								$detail = getDetails();
								foreach ($detail as $row) {
									$id = $row['id'];
									$log = $row['titulo'];
									$author = $row['author'];
									$date = $row['fecha'];
									$status = $row['status'];
									echo "<tr>";
									echo "<td>".$id."</td>";
									echo "<td>".$log."</td>";
									echo "<td>".$author."</td>";
									echo "<td>".$date."</td>";
									switch ($status) {
										case '1':
											echo "<td><span class='label label-success'>Agregado</span></td>";
											break;
										case '2':
											echo "<td><span class='label label-warning'>Reparado</span></td>";
											break;
										case '3':
											echo "<td><span class='label label-danger'>Removido</span></td>";
											break;
										case '4':
											echo "<td><span class='label label-primary'>En Progreso</span></td>";
											break;
										case '5':
											echo "<td><span class='label label-danger'>En Problemas</span></td>";
											break;
										default:
											echo "<td><span class='label label-info'>Log Privado</span></td>";
											break;
									}
									if (isset($_SESSION['steamid'])) {
										echo "<td><a href='#'>Eliminar</a></td>";
									}
									echo "</tr>";
									
								} 
							?>
						</table>
					</div>
				</div>
				<div class="col-md-4">
					<?php if(!isset($_SESSION['steamid'])) { ?>
						<p>Estoy agradecido en que visites este apartado de GoodNights.<br>Como desarrollador debo mostrar todos los avances del sitio, de esta manera conoceras todos los cambios que tendra a lo largo de los dias.</p>
					<?php }else{?>
						<p> <?php echo $steamprofile['personaname'] ?>, gracias por visitar este apartado del sitio.<br>Como desarrollador debo mostrar todos los avances del sitio, de esta manera conoceras todos los cambios que tendra a lo largo de los dias.</p> <br>
						<p>Te invito a dar opinion respecto al sitio. Haz <a href="#">click aqui</a> para enviar un comentario al desarrollador.</p>
					<?php }?>
				</div>
			</div>
		</div>
		<div class="bottom">
			<?php include('/var/www/GoodNights/assets/includes/footer.php'); ?>
		</div>
	</div>
</body>
</html>