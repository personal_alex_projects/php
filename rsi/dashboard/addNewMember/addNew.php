<?php session_start(); ?>
<?php error_reporting(E_ALL ^ E_NOTICE); ?>
<?php 
echo "<div class='container'>";
echo '<div class="nav-position">';
include('../_parts/navbar.php');
echo "</div>";
echo "</div>";
?>
<?php 
if (!isLogged()) {
	header("Refresh:1; url=/rsi?error=6");
	exit;
}else{
	$realname = $_POST['realname'];
	$email = $_POST['email'];
	$handlen = $_POST['handlename'];
	$rango = $_POST['rango'];
	$enlistado = $_POST['enlistado'];
	$pais = $_POST['pais'];
	$lenguaje = $_POST['lenguaje'];
	$forumlink = $_POST['forum'];
	$seccion = $_POST['seccion'];
	$secMundial = $_POST['seccionmundial'];
	$uue = $_POST['uue'];
	$comentario = $_POST['comentario'];
	$authorcomentario = $_SESSION['username'];
	$isAdmin = $_POST['seraadmin'];
	$notAdmin = "0";
	$nota = $_POST['nota'];
	$password = $_POST['password'];
	$cpassword = $_POST['cpassword'];
	$isDev = "0";
	$isBanned = "0";

	/*
	EMAIL
	HANDLE NAME
	PAIS
	LENGUAJE
	FORUM LINK
	SECCION
	SECCION MUNDIAL
	PASSWORD
	*/
	if ($email && $handlen && $pais && $lenguaje && $forumlink && $seccion && $secMundial && $password && $cpassword) {
		if (strlen($password)>6) {
			if ($password == $cpassword) {
				if (checkUser($handlen) > 0 ) {
					header("Refresh: 1; url=/rsi/dashboard/addNewMember/?error=4");
				}else{
					$passwordmd5 = md5($password);

					try {
						require($_SERVER['DOCUMENT_ROOT']."/rsi/_assets/_functions/dbConn.php");
						$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						/*$sql = "INSERT INTO es42_members('RealName', 'Email', 'Password', 'HandleName', 'RANK', 'ENLISTED', 'UserCountry', 'UserLang', 'FORUMLINK', 'UserSection', 'UserSectionWorld', 'AdminComment', 'AdminAuthorComment', 'IsAdmin', 'isDev', 'isBanned', 'Things', 'UUERecord') VALUES('$realname', '$email', '$passwordmd5', '$handlen', '$rango', '$enlistado', '$pais', '$lenguaje', '$forumlink', '$seccion', '$secMundial', '$comentario', '$authorcomentario', '$isAdmin', '0', '0', '$nota', '$uue')";*/
						$sql = $db->prepare("INSERT INTO es42_members (RealName, Email, Password, HandleName, RANK, ENLISTED, UserCountry, UserLang, FORUMLINK, UserSection, UserSectionWorld, AdminComment, AdminAuthorComment, IsAdmin, isDev, isBanned, Things, UUERecord) VALUES(:realname, :email, :pass, :handlen, :rango, :enlistado, :pais, :lenguaje, :forumlink, :seccion, :secmun, :comentario, :authorcomentario, :isadmin, :isdev, :isbanned, :notas, :uue)");

						$sql->bindParam(':realname', $realname, PDO::PARAM_STR, 30);
						$sql->bindParam(':email', $email, PDO::PARAM_STR, 30);
						$sql->bindParam(':pass', $passwordmd5, PDO::PARAM_STR, 18);
						$sql->bindParam(':handlen', $handlen, PDO::PARAM_STR, 25);
						$sql->bindParam(':rango', $rango, PDO::PARAM_STR, 20);
						$sql->bindParam(':enlistado', $enlistado, PDO::PARAM_STR, 20);
						$sql->bindParam(':pais', $pais, PDO::PARAM_STR, 20);
						$sql->bindParam(':lenguaje', $lenguaje, PDO::PARAM_STR, 20);
						$sql->bindParam(':forumlink', $forumlink, PDO::PARAM_STR, 20);
						$sql->bindParam(':seccion', $seccion, PDO::PARAM_STR, 30);
						$sql->bindParam(':secmun', $secMundial, PDO::PARAM_STR, 20);
						$sql->bindParam(':comentario', $comentario, PDO::PARAM_STR, 30);
						$sql->bindParam(':authorcomentario', $authorcomentario, PDO::PARAM_STR, 20);
						if ($isAdmin == 1) {
							$sql->bindParam(':isadmin', $isAdmin, PDO::PARAM_INT);
						}else{
							$sql->bindParam(':isadmin', $notAdmin, PDO::PARAM_INT);
						}
						$sql->bindParam(':isdev', $isDev, PDO::PARAM_INT);
						$sql->bindParam(':isbanned', $isBanned, PDO::PARAM_INT);
						$sql->bindParam(':notas', $nota, PDO::PARAM_STR, 40);
						$sql->bindParam(':uue', $uue, PDO::PARAM_STR, 20);

						$sql->execute();

						header("Refresh: 1; url=/rsi/dashboard/addNewMember/?exito=1&user=$handlen");
					} catch (PDOException $e) {
						echo "Un error a ocurrido al ingresar los datos: ".$e."<br>"."Estas siendo redireccionado...";
						header("Refresh: 10; url=/rsi/dashboard/addNewMember/?error=5");
					}
				}
			}else{
				header("Refresh: 1; url=/rsi/dashboard/addNewMember/?error=3");
			}
		}else{
			header("Refresh: 1; url=/rsi/dashboard/addNewMember/?error=2");
		}
	}else{
		header("Refresh: 1; url=/rsi/dashboard/addNewMember/?error=1");
	} 
}
?>