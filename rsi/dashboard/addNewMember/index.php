<?php session_start(); ?>
<?php error_reporting(E_ALL ^ E_NOTICE); ?>
<?php 
echo "<div class='container'>";
echo '<div class="nav-position">';
include('../_parts/navbar.php');
echo "</div>";
echo "</div>";
?>
<?php if (!isLogged()) {
	header("Refresh:1; url=/rsi?error=6");
	exit;
}else{
	?>

	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>España 2942: Añadir miembro</title>
		<link rel="stylesheet" href="../../_assets/styles/boots.css">
		<link rel="stylesheet" href="../../_assets/styles/addMember.css">
		<link rel="stylesheet" href="../../_assets/styles/cs-select.css" />
		<link rel="stylesheet" href="../../_assets/styles/demo.css" />
		<link rel="stylesheet" href="../../_assets/styles/cs-skin-boxes.css" />
		<link rel="stylesheet" href="../../_assets/styles/normalize.css" />
		<link rel="stylesheet" href="../../_assets/styles/cs-skin-elastic.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<div class="well">	
				<center>
					<?php 
					if ($_REQUEST['error'] == 1) {
						echo '<div><span style="font-size: 154%;" class="label label-danger">Algun dato requerido no fue establecido...</span></div>;';
					}
					if ($_REQUEST['error'] == 2) {
						echo '<div><span style="font-size: 154%;" class="label label-danger">La contraseña es menor a 6 digitos!</span></div>;';
					}
					if ($_REQUEST['error'] == 3) {
						echo '<div><span style="font-size: 154%;" class="label label-danger">La contraseña no coincide!</span></div>;';
					}
					if ($_REQUEST['error'] == 4) {
						echo '<div><span style="font-size: 154%;" class="label label-danger">Nombre ya registrado!</span></div>;';
					}
					if ($_REQUEST['error'] == 5) {
						echo '<div><span style="font-size: 154%;" class="label label-danger">Ha ocurrido un error en la base de datos!</span></div>;';
					}
					if ($_REQUEST['exito'] == 1) {
						echo '<div><span style="font-size: 154%;" class="label label-success">El nuevo miembro: '.$_REQUEST['user'].' fue registrado con exito!</span></div>;';
					}
					?>
				</center>
				<div style="width: 100%;" class="container">
					<h3 style="text-align: center;">Ingresar a nuevo Miembro <small style="color:red; text-align: center;">Campos con (*) son requeridos.</small></h3>
					<form method="POST" action="addNew.php">
						<div class="col-md-6">
							<div class="text-pro">
								NOMBRE REAL
							</div>
							<div class="text-pro">
								* EMAIL
							</div>
							<div class="text-pro">
								* * HANDLE NAME (RSI)
							</div>
							<div class="text-pro">
								RANGO / CARGO
							</div>
							<div class="text-pro">
								ENLISTADO (RSI)
							</div>
							<div class="text-pro">
								* PAIS
							</div>
							<div class="text-pro">
								* LENGUAJE
							</div>
							<div class="text-pro">
								* FORUM LINK
							</div>
							<div class="text-pro">
								* SECCIÓN
							</div>
							<div class="text-pro">
								* SECCION MUNDIAL
							</div>
							<div class="text-pro">
								UUE (RSI)
							</div>
							<div class="text-pro">
								COMENTARIO
							</div>
							<div class="text-pro">
								¿Será Admin?
							</div>
							<div class="text-pro">
								¿Alguna nota?
							</div>
							<div class="text-pro">
								* Contraseña
							</div>
							<div class="text-pro">
								* Confirmar Contraseña
							</div>
						</div>
						<div class="col-md-6">
							<input type="text" maxlength="25" name="realname" class="textbox">
							<input type="text" name="email" class="textbox">
							<input type="text" name="handlename" class="textbox">
							<section>
								<select name="rango" class="cs-select cs-skin-boxes">
									<option value="" disabled selected>Escoje el rango / cargo</option>
									<option value="miembro" data-class="img-miembro">MIEMBRO</option>
									<option value="soldado" data-class="img-soldado">SOLDADO</option>
									<option value="alferez" data-class="img-alferez">ALFEREZ</option>
									<option value="capitan" data-class="img-capitan">CAPITAN</option>
									<option value="comandante" data-class="img-capitan">COMANDANTE</option>
									<option value="general" data-class="img-general">GENERAL</option>
									<option value="7" disabled>#c1d099</option>
									<option value="8" disabled>#f5eaaa</option>
									<option value="9" disabled>#f5be8f</option>
									<option value="10" disabled>#e1837b</option>
									<option value="11" disabled>#9bbaab</option>
								</select>
							</section>
							<input type="text" name="enlistado" class="textbox">
							<section>
								<select name="pais" class="cs-select cs-skin-elastic">
									<option value="" disabled selected>Seleccion el país</option>
									<option value="argentina" data-class="flag-arg">Argentina</option>
									<option value="bolivia" data-class="flag-boli">Bolivia</option>
									<option value="chile" data-class="flag-chile">Chile</option>
									<option value="colombia" data-class="flag-colo">Colombia</option>
									<option value="mexico" data-class="flag-mex">Mexico</option>
									<option value="ecuador" data-class="flag-ecu">Ecuador</option>
									<option value="uruguay" data-class="flag-uru">Uruguay</option>
								</select>
							</section>
							<section>
								<select name="lenguaje" class="cs-select cs-skin-elastic">
									<option value="" disabled selected>Seleccion el lenguaje</option>
									<option value="es" data-class="lang-es">Español</option>
									<option value="semi-es" data-class="lang-both">Semi-Español</option>
								</select>
							</section>
							<input type="text" name="forum" class="textbox">
							<section>
								<select name="seccion" class="cs-select cs-skin-boxes">
									<option value="" disabled selected>Escoje la sección</option>
									<option value="escuderia" data-class="sec-escuderia">Escuderia</option>
									<option value="fuerzachoque" data-class="sec-choque">FUERZA DE CHOQUE</option>
									<option value="mineria" data-class="sec-mineria">MINERÍA</option>
									<option value="embajada" data-class="sec-embajada">EMBAJADA</option>
									<option value="cazareco" data-class="sec-cazareco">CAZA RECOMPENSAS</option>
									<option value="ciencia" data-class="sec-ciencia">CIENCIAS / EXPLORACION</option>
									<option value="transporte" data-class="sec-transporte">TRANSPORTE</option>
									<option value="hospital" data-class="sec-hospital">HOSPITAL</option>
									<option value="seguridad" data-class="sec-seguridad">SEGURIDAD</option>
									<option value="mecanica" data-class="sec-mecanica">MECANICA</option>
									<option value="ser" data-class="sec-ser">S.E.R</option>
									<option value="ejercito" data-class="sec-ejercito">EJERCITO</option>
									<option value="cascoazul" data-class="sec-cascoazul">CASCOS AZULES</option>
								</select>
							</section>
							<section>
								<select name="seccionmundial" class="cs-select cs-skin-elastic">
									<option value="" disabled selected>Escoje la sección Mundial</option>
									<option value="latam" data-class="flag-latam">LATAM</option>
									<option value="eu" data-class="flag-eu">EU</option>
								</select>
							</section>
							<input type="text" name="uue" class="textbox">
							<input type="text" name="comentario" class="textbox">
							<div class="roundedOne">
								<input type="checkbox" value="1" id="roundedOne" name="seraadmin" />
								<label for="roundedOne"></label>
							</div>
							<input type="text" name="nota" class="textbox">
							<input type="text" name="password" class="textbox">
							<input type="text" name="cpassword" class="textbox">
						</div>
						<input type="submit" class="holotab" name="submit" value="Agregar Miembro">
					</form>	
				</div>
			</div>
		</div>
		<script src="../../_assets/js/classie.js"></script>
		<script src="../../_assets/js/selectFx.js"></script>
		<script>
			(function() {
				[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
					new SelectFx(el, {
						stickyPlaceholder: false,
						onChange: function(){
							document.querySelector('span.cs-placeholder').style.backgroundColor = "rgba(25, 33, 197, 0.25)";
						}
					});
				} );
			})();
		</script>
	</body>
	</html>

	<?php } ?>