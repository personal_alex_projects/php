<?php
session_start();	
error_reporting(E_ALL ^ E_NOTICE);
echo "<div class='container'>";
echo '<div class="nav-position">';
include('../_parts/navbar.php');
echo "</div>";
echo "</div>";

if (!isLogged()){
	header("Refresh:1; url=/rsi?error=6");
	exit;
}else{

$details = getDetails($_SESSION['username']);

foreach ($details as $row) {
	$realn = $row['RealName'];
	$email = $row['Email'];
	$handle = $row['HandleName'];
	$rank = $row['RANK'];
	$enlisted = $row['ENLISTED'];
	$regOnWeb = $row['RegOnWeb'];
	$country = $row['UserCountry'];
	$lang = $row['UserLang'];
	$forumLink = $row['FORUMLINK'];
	$MySection = $row['UserSection'];
	$MySectionWorld = $row['UserSectionWorld'];
	$AdminComment = $row['AdminComment'];
	$AdminAuthorComment = $row['AdminAuthorComment'];
	$isAdmin = $row['IsAdmin'];
	$isDev = $row['IsDev'];
	$someThings = $row['Things'];
	$UUE = $row['UUERecord'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ESPAÑA 2942: <?php echo $_SESSION['username']; ?> </title>
	<link rel="stylesheet" href="../../_assets/styles/profile.css">
	<link rel="stylesheet" href="../../_assets/styles/boots.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">	
		<div class="row">
			<div class="col-md-4">
				<div class="well" style="padding-bottom: 2%;">
					<center>
						<h3><?php echo $_SESSION['username']; ?></h3>
						<span class="label label-primary">
							<?php if (isDev($_SESSION['username'])) { echo "Desarrollador"; } ?>
						</span>
					</center>
				</div>
			</div>
			<div class="col-md-8">
				<div class="well" style="padding-bottom: 2%;">
					<div style="width: 100%;" class="container">
						<h3>Información de la cuenta</h3>
						<div class="col-md-4">
							<div class="text-pro">
								Cuenta Nombre Real
							</div>
							<div class="text-pro">
								Cuenta Email
							</div>
							<div class="text-pro">
								Cuenta Handle Name
							</div>
							<div class="text-pro">
								Cargo / Rango
							</div>
							<div class="text-pro">
								Enlistado
							</div>
							<div class="text-pro">
								Fecha de ingreso
							</div>
							<div class="text-pro">
								Pais
							</div>
							<div class="text-pro">
								Lenguaje
							</div>
							<div class="text-pro">
								Forum Link
							</div>
							<div class="text-pro">
								Mi Sección
							</div>
							<div class="text-pro">
								Mi Sección Mundial
							</div>
							<div class="text-pro">
								Comentario del Admin
							</div>
							<div class="text-pro">
								Author del comentario
							</div>
							<div class="text-pro">
								Soy Admin?
							</div>
							<div class="text-pro">
								Soy Desarrollador?
							</div>
							<div class="text-pro">
								Mis datos curiosos
							</div>
							<div class="text-pro">
								UEE Citizen Record
							</div>
						</div>
						<div class="col-md-6">
							<input type="text" class="textbox" disabled value="<?php echo $realn; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $email; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $handle; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $rank; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $enlisted; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $regOnWeb; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $country; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $lang; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $forumLink; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $MySection; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $MySectionWorld; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $AdminComment; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $AdminAuthorComment; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $isAdmin; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $isDev; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $someThings; ?>">
							<input type="text" class="textbox" disabled value="<?php echo $UEE; ?>">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<?php
}
$now = time();
$mbd = null;
if($now > $_SESSION['expire']){
	session_destroy();
	header("Refresh:1; url=/rsi?error=2");
	exit;
}
?>