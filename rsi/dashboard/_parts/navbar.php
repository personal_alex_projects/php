<?php session_start(); ?>
<?php require($_SERVER['DOCUMENT_ROOT']."/rsi/_assets/_functions/functions.php"); ?>
<?php /* */ ?>
<?php /* */ ?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Dashboard</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administración <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="../addNewMember/">Añadir Miembro</a></li>
            <li><a href="#">Añadir Documento</a></li>
            <li><a href="#">Añadir Escudo/Puesto</a></li>
            <li><a href="#">Añadir Oficio/Sección</a></li>
            <li><a href="#">Añadir Empresa</a></li>
            <li><a href="#">Editar MOTD</a></li>
            <?php 
            $getIsDev = isDev($_SESSION['username']);
            foreach ($getIsDev as $test) {
              $isDev = $test['IsDev'];
            }
            if ($isDev == 1) {
              echo '<li role="separator" class="divider"></li>';
              echo '<li><a href="#">Añadir Dev Status</a></li>';
            }
            ?>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Edición/Purga <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Lista de Miembros</a></li>
            <li><a href="#">Lista de Documentoa</a></li>
            <li><a href="#">Lista de Escudos/Puestos</a></li>
            <li><a href="#">Lista de Oficios/Secciónes</a></li>
            <li><a href="#">Lista de Empresas</a></li>
            <li><a href="#">Lista de Dev Status</a></li>
            <?php 
              $getIsDev = isDev($_SESSION['username']);
              foreach ($getIsDev as $test) {
                $isDev = $test['IsDev'];
              }
              if ($isDev == 1) {
                echo '<li role="separator" class="divider"></li>';
                echo '<li><a href="#">Seccuencia SQL</a></li>';
              }
             ?>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search Member...">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><p class="navbar-text">Hola: <?php echo $_SESSION['username']; ?></p> </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sesión <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="<?php echo $_SERVER['REMOTE_ADDRESS']."/rsi/dashboard/profile/"; ?>">Perfil</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo $_SERVER['REMOTE_ADDRESS']."/rsi/dashboard/logout/"; ?>">Salir</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>