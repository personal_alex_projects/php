<?php
session_start();	
/*<?php require($_SERVER['DOCUMENT_ROOT']."/rsi/_assets/_functions/functions.php"); ?>*/
error_reporting(E_ALL ^ E_NOTICE);
echo "<div class='container'>";
echo '<div class="nav-position">';
include('/_parts/navbar.php');
echo "</div>";
echo "</div>";
if (!isLogged()){
	header("Refresh:1; url=/rsi?error=6");
	exit;
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title>ESPAÑA 2942: DASHBOARD</title>
	<link rel="stylesheet" href="../_assets/styles/dashboard.css">
	<link rel="stylesheet" href="../_assets/styles/boots.css">
	<link rel="stylesheet" href="../_assets/styles/all-animation.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="well well-sm">
			<marquee behavior="scroll" direction="left">
				<img width="30%" class="info_ico" style="vertical-align:middle;  style="display: inline-block;"" src="https://i.gyazo.com/a01438db1336a42ac8240f2aea323eaa.png" alt="info_ico">
				<?php
					$txt = "Bienvenido: ".$_SESSION['username']
					." Tu nivel de acceso es:";

					$getIsDev = isDev($_SESSION['username']);
					foreach ($getIsDev as $test) {
						$isDev = $test['IsDev'];
					}
					if ($isDev == 1) {
						echo $txt." Desarrollador";
					}elseif($isDev == 0){
						echo $txt." Soldado";
					}
				 ?>
			</marquee>
		</div>
		<div class="row">
			<div class="col-md-8">
				<div class="well">
					<div class="row">
						<div style="padding: 4%;" class="col-md-6">
							<h3 id="h3">Registros</h3>
							<div id="listing" class="total_members">
								<p>
									<h3 style="vertical-align:middle">
										<?php 
										$url = 'https://robertsspaceindustries.com/orgs/OEC2/members';
										$content = file_get_contents($url);
										$first_step = explode( '<span class="totalrows js-totalrows">' , $content );
										$second_step = explode("</span>" , $first_step[1] );

										echo $second_step[0];
										?>
									</h3>
									<small>Total Members on RSI Organization</small>
								</p>
							</div>
							<div id="listing" class="total_members_general">
								<p>
									<h3 style="vertical-align:middle">$TotalMembers</h3>
									<small>Total Members</small>
								</p>
							</div>
							<div id="listing" class="total_members_general">
								<p>
									<h3 style="vertical-align:middle">$TotalMembers</h3>
									<small>Total Members</small>
								</p>
							</div>
							<div id="listing" class="total_members_general">
								<p>
									<h3 style="vertical-align:middle">$TotalMembers</h3>
									<small>Total Members</small>
								</p>
							</div>
							<div id="listing" class="total_members_general">
								<p>
									<h3 style="vertical-align:middle">$TotalMembers</h3>
									<small>Total Members</small>
								</p>
							</div>
						</div>
						<div style="padding: 4%;" class="col-md-6">
							<h3 id="h3">Miembro Destacado</h3> <br>
							<label class="label label-info">Razon: <small>$razon</small></label>
							<label class="label label-info">Por: <small>$quienLeDestaco</small></label>
							<h3 style="padding-left: 22%;">$username</h3>
							<br>
							<img width="80%" src="https://i.gyazo.com/04158184baf9b3d08002ec939a2c7f2e.png" alt="icoProfile">
							<br>
							<center>
								<p class="tdP">Nro de Estrella: <input class="textbox" type="text" disabled value="0"></p>
								<p class="tdP">Cargo <input class="textbox" type="text" disabled value="Soldado/Civil"></p>
							</center>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="well">
					<center>
					<h3 style="margin-bottom: 0px; margin-top: 17px;">Ultimos miembros registrados</h3>
						<br>
						<table width="100%">
							<tr>
								<h3>$username</h3>
								<br>
								<img width="40%" src="https://i.gyazo.com/04158184baf9b3d08002ec939a2c7f2e.png" alt="icoProfile">
								<br>
								<h3>$username</h3>
								<br>
								<img width="40%" src="https://i.gyazo.com/04158184baf9b3d08002ec939a2c7f2e.png" alt="icoProfile">
								<br>
								<h3>$username</h3>
								<br>
								<img width="40%" src="https://i.gyazo.com/04158184baf9b3d08002ec939a2c7f2e.png" alt="icoProfile">
								<br>
							</tr>
						</table>
					</center>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<?php
}
$now = time();

if($now > $_SESSION['expire']){
	session_destroy();
	header("Refresh:1; url=/rsi?error=2");
	exit;
}
?>