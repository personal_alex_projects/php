<?php session_start(); ?>
<?php error_reporting(E_ALL ^ E_NOTICE); ?>
<?php 
echo "<div class='container'>";
echo '<div class="nav-position">';
include('../_parts/navbar.php');
echo "</div>";
echo "</div>";
?>
<?php 
	/*$details = getAllDetails();
	foreach ($details as $row) {
		$handlen = $row['HandleName'];
	}*/


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ESPAÑA 2942: Cargando datos!</title>
	<link rel="stylesheet" href="../../_assets/styles/boots.css">
	<link rel="stylesheet" href="../../_assets/styles/addMember.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<center>
			<h2>Información ingresada al sistema con exito!</h2>
			<br>
			<h2><small style="color:aqua">Algunos datos se han omitido en la tabla.</small></h2>
			<br>
			<br>
		</center>
		<table width="101%" border="1">
			<tr class="head">
				<th>Handle Name</th>
				<th>Email</th>
				<th>Rango</th>
				<th>Pais</th>
				<th>Seccion</th>
				<th>Seccion Mundial</th>
				<th>Informacion Adicional</th>
				<th>Tu comentario</th>
				<th>¿Es admin?</th>
			</tr>

			<tr>
				<td><?php echo $email; ?></td>
				<td>
					<?php 

						/* 
						- LISTO escuderia 
						- LISTO choque
						- LISTO mineria
						- LISTO embajada
						- LISTO cazareco
						- ciencia
						- transporte
						- hospital
						- seguridad
						- mecanica
						- ser
						- LISTO ejercito
						- casxoazul
						*/


						if ($seccion == "ejercito") {
							switch ($rango) {
								case 'miembro':
								echo '<img width="30%" src="https://embed.gyazo.com/9d2f3983490d06ac6769c1207c7e01f8.png" alt="miembro-flag">';
								break;
								case 'soldado':
								echo '<img width="30%" src="https://embed.gyazo.com/4d8f84003f146a7910e99e99f4125747.png" alt="soldao-flag">';
								break;
								case 'alferez':
								echo '<img width="30%" src="https://embed.gyazo.com/993e66d2fddac660a21181825373fbee.png" alt="alferez-flag">';
								break;
								case 'capitan':
								echo '<img width="30%" src="https://embed.gyazo.com/34e390be716ab59d8b299243bebba2aa.png" alt="capitan-flag">';
								break;
								case 'comandante':
								echo '<img width="30%" src="https://embed.gyazo.com/31a427e40c447b5800c83027350af739.png" alt="comandante-flag">';
								break;
								case 'general':
								echo '<img width="30%" src="https://embed.gyazo.com/37ad3f72bcb303c66b1dd6fe4f062696.png" alt="comandante-flag">';
								break;
								default:
								echo '<img width="15%" src="https://cdn1.iconfinder.com/data/icons/rounded-flat-country-flag-collection-1/2000/_Unknown.png" alt="unknown-flag"> Desconocido...';
								break;
							}
						}elseif($seccion == "escuderia"){
							switch ($rango) {
								case 'miembro':
								echo '<img width="30%" src="https://i.gyazo.com/bb4de71c4cc0786ab529d1a5a91285bf.png" alt="miembro-flag">';
								break;
								default:
								echo '<img width="30%" src="https://i.gyazo.com/bb4de71c4cc0786ab529d1a5a91285bf.png" alt="miembro-flag">';
								break;
							}
						}elseif($seccion == "fuerzachoque"){
							switch ($rango) {
								case 'miembro':
								echo '<img width="30%" src="https://i.gyazo.com/0a6001933f975532a5e741cc5f88e9c4.png" alt="miembro-flag">';
								break;
								case 'soldado':
								echo '<img width="30%" src="https://i.gyazo.com/133fafc2d3dc5a5b73cafa95551216d6.png" alt="soldao-flag">';
								break;
								case 'alferez':
								echo '<img width="30%" src="https://i.gyazo.com/1e58557d18c5b2b543cc7eed1e063f07.png" alt="alferez-flag">';
								break;
								case 'capitan':
								echo '<img width="30%" src="https://i.gyazo.com/a704d1b26a9f9d5b03033035fd569669.png" alt="capitan-flag">';
								break;
								case 'comandante':
								echo '<img width="30%" src="https://i.gyazo.com/b1506db9ffab519abbabcc3a492af891.png" alt="comandante-flag">';
								break;
								case 'general':
								echo '<img width="30%" src="https://i.gyazo.com/0a6001933f975532a5e741cc5f88e9c4.png" alt="comandante-flag">';
								break;
								default:
								echo '<img width="30%" src="https://i.gyazo.com/0a6001933f975532a5e741cc5f88e9c4.png" alt="miembro-flag">';
								break;
							}
						}elseif($seccion == "mineria"){
							switch ($rango) {
								case 'miembro':
								echo '<img width="30%" src="https://i.gyazo.com/68a0c2381a9498d720b945cdbf6699e5.png" alt="miembro-flag">';
								break;
								case 'soldado':
								echo '<img width="30%" src="https://i.gyazo.com/33b628811ed3843a3274e31e99699352.png" alt="soldao-flag">';
								break;
								case 'alferez':
								echo '<img width="30%" src="https://i.gyazo.com/5de53cf270eb401d76f28303c28f45c1.png" alt="alferez-flag">';
								break;
								case 'capitan':
								echo '<img width="30%" src="https://i.gyazo.com/d81f69a856846406ea538766c12cfdea.png" alt="capitan-flag">';
								break;
								case 'comandante':
								echo '<img width="30%" src="https://i.gyazo.com/8a5a53ff4b6c2b4a275837f9f02f297b.png" alt="comandante-flag">';
								break;
								case 'general':
								echo '<img width="30%" src="https://i.gyazo.com/68a0c2381a9498d720b945cdbf6699e5.png" alt="comandante-flag">';
								break;
								default:
								echo '<img width="30%" src="https://i.gyazo.com/68a0c2381a9498d720b945cdbf6699e5.png" alt="miembro-flag">';
								break;
							}
						}elseif($seccion == "embajada"){
							switch ($rango) {
								case 'miembro':
								echo '<img width="30%" src="https://i.gyazo.com/4c1d504533d803017732081c93faf900.png" alt="miembro-flag">';
								break;
								case 'soldado':
								echo '<img width="30%" src="https://i.gyazo.com/cd8f9bc67bb836fe9ae3681fc152e31f.png" alt="soldao-flag">';
								break;
								default:
								echo '<img width="30%" src="https://i.gyazo.com/4c1d504533d803017732081c93faf900.png" alt="miembro-flag">';
								break;
							}
						}elseif($seccion == "cazareco"){
							switch ($rango) {
								case 'miembro':
								echo '<img width="30%" src="https://i.gyazo.com/3ef3a5324bd2fc8a5609197ad2cc9675.png" alt="miembro-flag">';
								break;
								case 'soldado':
								echo '<img width="30%" src="https://i.gyazo.com/1e144a0b9945d39e4b77209d53081fc0.png" alt="soldao-flag">';
								break;
								case 'alferez':
								echo '<img width="30%" src="https://i.gyazo.com/5e961afebd569f5cfac82e75758f176f.png" alt="alferez-flag">';
								break;
								case 'capitan':
								echo '<img width="30%" src="https://i.gyazo.com/4148b8b368af514dd7a24c2edee670ba.png" alt="capitan-flag">';
								break;
								case 'comandante':
								echo '<img width="30%" src="https://i.gyazo.com/84701a382ee6aa8d3ae29c4d9c178731.png" alt="comandante-flag">';
								break;
								default:
								echo '<img width="30%" src="https://i.gyazo.com/3ef3a5324bd2fc8a5609197ad2cc9675.png" alt="miembro-flag">';
								break;
							}
						}else{
							echo '<img width="30%" src="https://cdn1.iconfinder.com/data/icons/rounded-flat-country-flag-collection-1/2000/_Unknown.png" alt="unknown-flag"> Desconocido...';
						}

						?>
					</td>
					<td>
						<?php 
						switch ($pais) {
							case 'argentina':
							echo '<img width="25%" src="https://i.gyazo.com/8aef5bd3d44e089a3ac2b63f90727fa1.png" alt="arg-flag"> Argentina';
							break;
							case 'chile':
							echo '<img width="25%" src="https://i.gyazo.com/47c8c5707f14b0a56b28c1403ac611e2.png" alt="chile-flag"> Chile';
							break;
							case 'bolivia':
							echo '<img width="25%" src="https://i.gyazo.com/8bfe7cd467d471e179def5502838d1af.png" alt="bolivia-flag"> Bolivia';
							break;
							case 'colombia':
							echo '<img width="25%" src="https://i.gyazo.com/488369c6fedaa3c7e2ef9f5bc595e816.png" alt="colom-flag"> Colombia';
							break;
							case 'mexico':
							echo '<img width="25%" src="https://i.gyazo.com/7ce34af7c547dbbccc8bf29dc8ff36d4.png" alt="mex-flag"> Mexico';
							break;
							case 'ecuador':
							echo '<img width="25%" src="https://i.gyazo.com/64b108dc1a17ad910a844e6562042ac0.png" alt="ecuador-flag"> Ecuador';
							break;
							case 'uruguay':
							echo '<img width="25%" src="https://i.gyazo.com/adef66fcf3d266bf1ca16af6e4b1caaf.png" alt="uruguay-flag"> Uruguay';
							break;
							default:
							echo '<img width="15%" src="https://cdn1.iconfinder.com/data/icons/rounded-flat-country-flag-collection-1/2000/_Unknown.png" alt="unknown-flag"> Desconocido...';
							break;
						}
						?>
					</td>
					<td>
						<?php

						switch ($seccion) {
							case 'escuderia':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Solo-Simbolo-Carreras.png" alt="escuderia-flag">';
							break;
							case 'fuerzachoque':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Solo-simbolo-Choque.png" alt="choque-flag">';
							break;
							case 'mineria':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Solo-simbolo-Mineria.png" alt="mineria-flag">';
							break;
							case 'embajada':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Solo-Simbolo-Embajada.png" alt="embajada-flag">';
							break;
							case 'cazareco':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Solo-Simbolo-Cazarrecompensas.png" alt="cazareco-flag">';
							break;
							case 'ciencia':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Solo-simbolo-Ciencias.png" alt="ciencia-flag">';
							break;
							case 'transporte':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Solo-Simbolo-Transporte.png" alt="transporte-flag">';
							break;
							case 'hospital':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Solo-Simbolo-Hospital.png" alt="hospital-flag">';
							break;
							case 'seguridad':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Solo-simbolo-Seguridad.png" alt="seguridad-flag">';
							break;
							case 'mecanica':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Solo-Simbolo-Mecanicos.png" alt="mecanica-flag">';
							break;
							case 'ser':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/01/solo-simbolo-SER-1.png" alt="ser-flag">';
							break;
							case 'ejercito':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Simbolo-Ejercito-0E.png" alt="ejercito-flag">';
							break;
							case 'cascoazul':
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Cascos-Azules-0E.png" alt="ejercito-flag">';
							break;
							default:
							echo '<img width="60%" src="http://españa2942.es/wp-content/uploads/2016/03/Simbolo-Ejercito-0E.png" alt="default-flag">(error)';
							break;
						}
						?>
					</td>
					<td>
						<?php 

						if ($secMundial == "latam") {
							echo '<img width="25%" src="https://i.gyazo.com/dce3e16f3880586a23a20c870f2c13bd.png" alt="unknown-flag"> Latino America';
						}elseif ($secMundial == "eu") {
							echo '<img width="15%" src="https://i.gyazo.com/13f52c18ec680bca8a2fd8529e8d2d6b.png" alt="unknown-flag"> Europa';
						}else{
							echo '<img width="25%" src="https://i.gyazo.com/dce3e16f3880586a23a20c870f2c13bd.png" alt="unknown-flag"> Latino America';
						}

						?>
					</td>
					<td><?php echo $nota; ?></td>
					<td><?php echo $comentario; ?></td>
					<td>
						<?php 
						if ($isAdmin == "1") {
							echo "SI";
						}else{
							echo "NO";
						}
						?>
					</td>
				</tr>
			</table>
		</div>
	</body>
	</html>