<?php session_start(); ?>
<?php require($_SERVER['DOCUMENT_ROOT']."/rsi/_assets/_functions/functions.php"); ?>
<?php error_reporting(E_ALL ^ E_NOTICE); ?>
<?php 
	if (isLogged()) {
		header("Location: dashboard/");
	}else{
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta charset="UTF-8">

	<title>ESPAÑA 2942: CONTROL PANEL</title>

	<link rel="stylesheet" href="_assets/styles/main.css">
	<link rel="stylesheet" href="_assets/styles/boots.css">
	<link rel="stylesheet" href="_assets/styles/all-animation.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div style="margin-top: 0.7%; padding: 7%;" class="well">
			<div class="row">
				<center>
					<img class="enter-down-bounce" width="20%" src="http://españa2942.es/wp-content/uploads/2016/03/logo-e1458130452937.png" ="Logo">
					<h2 class="title">ESPAÑA 2942: LOGIN TO CONTROL PANEL</h2><br>
					<?php 
					if ($_REQUEST['error'] == "1") {
						echo '<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						Ha ocurrido un error insertando su nombre de usuario (HandleName RSI) y / o la contraseña.</div>';
					}
					if ($_REQUEST['error'] == "2") {
						echo '<div class="alert alert-info alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<strong>Hey!</strong> Tu sesion se ha cerrado por inactividad o tiempo agotado.</div>';
					}
					if ($_REQUEST['error'] == "3") {
					echo '<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Por favor. El ingreso debe hacerse con datos correctos.</div>';
					}
					if ($_REQUEST['error'] == "4") {
					echo '<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Nombre de usuario incorrecto!</div>';
					}
					if ($_REQUEST['error'] == "5") {
					echo '<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Contraseña incorrecta!</div>';
					}
					if ($_REQUEST['error'] == "6") {
					echo '<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Inicia sesión!</div>';
					}
					if ($_REQUEST['error'] == "7") {
					echo '<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Nombre no registrado!</div>';
					}
					if ($_REQUEST['error'] == "8") {
					echo '<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					Sesion cerrada con exito!</div>';
					}
					?>
				<form method="POST" action="checkLogin/">
					<div class="col-md-4">
						<p class="textAjust">Username:</p>
						<p class="textAjust">Password:</p>
					</div>
					<div style="margin-bottom: 3%;" class="col-md-8">
						<p><input class="trans-02s trans-color trans-box-shadow" type="text" name="username"></p>
						<p><input type="password" name="password"></p>
					</div>
					<input type="submit" class="holotab" name="submit" value="Log In">
				</form>
			</center>
		</div>
	</div>
</div>
</body>
</html>
<?php } ?>