<?php 
function isDev($username){
	require("dbConn.php");

	$sql = $db->prepare("SELECT * FROM `es42_members` WHERE HandleName = :hname");
	$sql->bindParam(':hname', $username, PDO::PARAM_STR, 20);
	$sql->execute();
	$row = $sql->fetchAll();

	return $row;

	$db = null;
}

function isLogged(){
	return (isSet($_SESSION['username']) && $_SESSION['username']);
}

function isBanned($username){
	require("dbConn.php");
	$result = "SELECT * FROM `es42_members` WHERE `HandleName`='$username' AND `IsBanned`='1'";
	return $result;
	setcookie("ImBanned", "1");
}

function getDetails($username) {
	require("dbConn.php");
	$sql = $db->prepare("SELECT * FROM `es42_members` WHERE HandleName=:hname");
	$sql->bindParam(':hname', $username, PDO::PARAM_STR, 20);
	$sql->execute();
	$row = $sql->fetchAll();

	return $row;
	$db = null;
}

function getAllDetails() {
	require("dbConn.php");
	$sql = $db->prepare("SELECT * FROM es42_members");
	$sql->execute();
	$row = $sql->fetchAll();
	
	return $row;
	$db = null;
}

function checkUser($username){
	require("dbConn.php");
    $stmt = $db->prepare("SELECT HandleName FROM es42_members WHERE HandleName = :name");
    $stmt->bindParam(':name', $username);
    $stmt->execute();
	$number = $stmt->rowCount();
    return $number;
    $db = null;
}
?>
