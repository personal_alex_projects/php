<?php 
echo "<div class='container'>";
echo '<div class="nav-position">';
include('dashboard/_parts/navbar.php');
echo "</div>";
echo "</div>";

if (!isLogged()) {
	header("Refresh:1; url=/rsi?error=6");
	exit;
}else{
	/* $data = $query->fetchAll(PDO::FETCH_BOTH) */
	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>TEST</title>
		<link rel="stylesheet" href="_assets/styles/boots.css">
		<link rel="stylesheet" href="_assets/styles/addMember.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="_assets/js/tablas.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('#un').keyup(function(){
					var username = $(this).val();
					var Result = $('#result');
   				 if(username.length > 2 || username.length > 11) { // if greater than 2 (minimum 3)
   				 	Result.html('./img/loadgreen.gif');
   				 	var dataPass = 'action=availability&username='+username;
        			$.ajax({ // Send the username val to available.php
        				type : 'POST',
        				data : dataPass,
        				url  : 'available.php',
       				success: function(responseText){ // Get the result
       					if(responseText == 0){
       						Result.html('<span class="success">Available</span>');
       						document.getElementById("un").className -= " red";
       						document.getElementById("un").className += " green";
       					}else if(responseText > 0){
       						Result.html('<span class="error">Unavailable</span>');
       						document.getElementById("un").className -= " green";
       						document.getElementById("un").className += " red";
       					}else{
       						alert('Problem with sql query');
       					}
       				}
       			});
        		}else{
        			Result.html('Enter 3 to 11 characters');
        		}
        		if(username.length == 0) {
        			Result.html('');
        			document.getElementById("un").className -= " red";
        			document.getElementById("un").className -= " green";
        			document.getElementById("un").className += " not";
        		}
        	});
			});
		</script>
		<style>
			.green{
				border: 2px solid green;
			}
			.red{
				border: 2px solid red;
			}
			.not{
				border: 1px solid white;
			}

		</style>
	</head>
	<body>
		<div class="container well">
			<table>
				<tr>
					<td>
						<input type="text" name="username" id="un"  placeholder="Username" class="username" />
					</td>
					<td class="result" id="result"></td>
				</tr>
			</table>
		</div>
	</body>
	</html>
	<?php } ?>