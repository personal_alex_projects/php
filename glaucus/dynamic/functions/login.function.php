<?php 
/*
* TO DO - Generacion de Tokens Procedural.
* TOKEN= 0123456789QAZWSXEDCRFVTGBYHNUJMIKOLP
*
*/

function TryLogin($email, $password){
	if(!filter_var($email, FILTER_VALIDATE_EMAIL))return 1;
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("SELECT GUID,Email,Password,Salt FROM gn_accounts WHERE Email=:email");
	$sql->bindParam(":email", $email, PDO::PARAM_STR, 49);

	$sql->execute();
	$row = $sql->fetchAll();
	$count = $sql->rowCount();
	$db = null;
	if ($count == 0) {
		return 1;
	}


	foreach ($row as $key) {
		$Realpass = $key['Password'];
		$RealSalt = $key['Salt'];
		$guid = $key['GUID'];
	}

	$encPass = hash('sha256', hash('sha256', $password).$RealSalt);
	if ($Realpass === $encPass) {
		$_SESSION['LoginToken'] = generateSession($guid);
		return 5;
	}

	return 4;
}

///////////////////////////////////////////////////////////////////////
//                      Token generation system                      //
///////////////////////////////////////////////////////////////////////

function generateSession($guid){
	while (true) {
		$token = randomTokenSession();
		if (checkTokenAvalaible($token)) {
			setupToken($token, $guid);
			return $token;
		}
	}
}

function setupToken($token, $guid){
	require('/var/www/html/dynamic/db.php');
	$currenttime = round(microtime(true));
	$expiremax = $currenttime+(60*60);
	$sql = $db->prepare("INSERT INTO `gn_sessions`(`token`, `expire`, `guid`, `generatoraddress`) VALUES (:token,:expire,:guid,:generatoraddress)");
	$sql->bindParam(":token", $token, PDO::PARAM_STR, 64);
	$sql->bindParam(":expire", $expiremax, PDO::PARAM_STR, 64);
	$sql->bindParam(":guid", $guid, PDO::PARAM_STR, 64);
	$sql->bindParam(":generatoraddress", $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR, 64);
	$sql->execute();
	$db = null;
}

function checkTokenAvalaible($token){
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("SELECT * FROM gn_sessions WHERE token=:token");
	$sql->bindParam(":token", $token, PDO::PARAM_STR, 64);
	$sql->execute();
	$count = $sql->rowCount();
	$db = null;
	return ($count == 0);
}

function randomTokenSession(){
  $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $String = '';
  for ($i = 0; $i < 64; ++$i) {
    $String .= $characters[rand(0, $charactersLength - 1)];
  }
  return $String;
}

///////////////////////////////////////////////////////////////////////

 ?>
