<?php 
function obtenerGUID($token){
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("SELECT guid FROM gn_sessions WHERE token=:token");
	$sql->bindParam(":token", $token, PDO::PARAM_STR, 64);
	$sql->execute();
	$row = $sql->fetchAll();
	foreach ($row as $key) {
		$DBguid = $key['guid'];
	}
	$db = null;
	return $DBguid;
	
}

function obtenerDatos($guid){
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("SELECT * FROM gn_accounts WHERE GUID=:guid");
	$sql->bindParam(":guid", $guid, PDO::PARAM_STR, 64);
	$sql->execute();
	$row = $sql->fetchAll();

	$db = null;
	return $row;
	
}

function registerLang($guid, $lang){
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("UPDATE gn_accounts SET Language=:lang WHERE GUID=:guid");
	$sql->bindParam(":lang", $lang, PDO::PARAM_STR, 2);
	$sql->bindParam(":guid", $guid, PDO::PARAM_STR, 64);
	$sql->execute();
	$db = null;
}

function registerNews($guid, $val){
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("UPDATE gn_accounts SET news=:val WHERE GUID=:guid");
	$sql->bindParam(":val", $val, PDO::PARAM_STR, 2);
	$sql->bindParam(":guid", $guid, PDO::PARAM_STR, 64);
	$sql->execute();
	$db = null;
}

?>