<?php

function registerNewsletter($subject, $message, $guid){
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("INSERT INTO gn_newsletter (subject, message, created_byguid) VALUES (:subj, :mess, :guid)");
	$sql->bindParam(":subj", $subject, PDO::PARAM_STR, 64);
	$sql->bindParam(":mess", $message, PDO::PARAM_STR, 30000);
	$sql->bindParam(":guid", $guid, PDO::PARAM_STR, 20);
	$sql->execute();

	$db = null;
	return 1;	
}

function updateNewsletter($id, $subject, $message, $date, $status){
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("UPDATE `gn_newsletter` SET `subject`=:subj,`message`=:msg,`update_date`=:upd, status=:sts WHERE id=:id");
	$sql->bindParam(":subj", $subject, PDO::PARAM_STR, 64);
	$sql->bindParam(":sts", $status, PDO::PARAM_INT);
	$sql->bindParam(":msg", $message, PDO::PARAM_STR);
	$sql->bindParam(":id", $id, PDO::PARAM_INT);
	$sql->bindParam(":upd", $date, PDO::PARAM_STR);
	$sql->execute();

	$db = null;
}

function deleteNewsletterByID($id){
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("DELETE FROM gn_newsletter WHERE id=:id");
	$sql->bindParam(":id", $id, PDO::PARAM_INT);
	$sql->execute();

	$db = null;
}

function getNewsletters(){
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("SELECT * FROM gn_newsletter");
	$sql->execute();
	$row = $sql->fetchAll();
	$db = null;
	return $row;
}

function getNewslettersByID($id){
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("SELECT * FROM gn_newsletter WHERE id=:id");
	$sql->bindParam(":id", $id, PDO::PARAM_INT);
	$sql->execute();
	$row = $sql->fetchAll();
	$db = null;
	return $row;
}




?>