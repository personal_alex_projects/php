<?php 

function registerAccount($email, $password, $cpassword){
	if(!filter_var($email, FILTER_VALIDATE_EMAIL))return 1;
	if((strlen($password) < 6) && (strlen($password) > 32)) return 2; //Password short/long
	if(!($password === $cpassword)) return 3; //Password don't match
	if((strlen($email) >= 42)) return 4;
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("SELECT Email FROM gn_accounts WHERE Email=:email");
	$sql->bindParam(":email", $email, PDO::PARAM_STR, 42);
	$sql->execute();
	$count = $sql->rowCount();
	if($count != 0){
		$db = null;
		return 4;
	}
	$salt = generateSALT();
	$currenttime = round(microtime(true));
	$guid = generateGUID();
	$encpass = hash('sha256', hash('sha256', $password).$salt);
	$sql = $db->prepare("INSERT INTO gn_accounts (Email, Password, Salt, GUID,creationtime) VALUES(:email, :password, :salt, :guid, :currenttime)");
	$sql->bindParam(":email", $email, PDO::PARAM_STR, 42);
	$sql->bindParam(":password", $encpass, PDO::PARAM_STR, 256);
	$sql->bindParam(":salt", $salt, PDO::PARAM_STR, 16);
	$sql->bindParam(":guid", $guid, PDO::PARAM_STR, 21);
	$sql->bindParam(":currenttime", $currenttime, PDO::PARAM_STR, 64);
	$sql->execute();
	TryLogin($email,$password);
	return 5;
	
}

///////////////////////////////////////////////////////////////////////
//                       Salt generation system                      //
///////////////////////////////////////////////////////////////////////

function generateSALT(){
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $String = '';
  for ($i = 0; $i < 16; ++$i) {
    $String .= $characters[rand(0, $charactersLength - 1)];
  }
  return $String;
}

///////////////////////////////////////////////////////////////////////
//                       GUID generation system                      //
///////////////////////////////////////////////////////////////////////

function generateGUID(){
	while (true) {
		$guid = randomGUID();
		if (checkGUID($guid)) {
			return $guid;
		}
	}
}

function randomGUID() {
  $characters = '0123456789QAZWSXEDCRFVTGBYHNUJMIKOLP';
  $charactersLength = strlen($characters);
  $String = '';
  for ($i = 0; $i < 16; ++$i) {
    $String .= $characters[rand(0, $charactersLength - 1)];
  }
  return "GUID-".$String;
}


function checkGUID($guid){
	require('/var/www/html/dynamic/db.php');
	$sql = $db->prepare("SELECT GUID FROM gn_accounts WHERE GUID=:guid");
	$sql->bindParam(":guid", $guid, PDO::PARAM_STR, 256);
	$sql->execute();

	$count = $sql->fetchColumn();
	$db = null;

	return ($count == 0);
}

///////////////////////////////////////////////////////////////////////




/*function regLOG($email){
	$reg = "[SISTEMA] Se agrego un nuevo registro ($email)";
	$sql = $db->prepare("INSERT INTO table_name VALUES(:reg)");
	$sql->bindParam(":reg", $reg, PDO::PARAM_STR, 180);
	$sql = null;
}*/

 ?>