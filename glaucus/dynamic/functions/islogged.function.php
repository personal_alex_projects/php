<?php 

function LogOut(){
	if(isset($_SESSION['LoginToken'])){
		$_SESSION['LoginToken'] = "NULL-Token";
	}
}

function isLogged(){
	if(isset($_SESSION['LoginToken'])){
		$token = $_SESSION['LoginToken'];
		if(strlen($token) != 64){
			return false;
		}
		$guid = getTokenGUID($token);
		if($guid == null){
			unset($_SESSION['LoginToken']);
			return false;
		}else{
			return $guid;
		}
	}else{
		return false;
	}
}

function getTokenGUID($token){
	require('/var/www/html/dynamic/db.php');
	$currenttime = round(microtime(true));

	$sql = $db->prepare("SELECT * FROM gn_sessions WHERE token=:token AND `expire` >".$currenttime." LIMIT 1");
	$sql->bindParam(":token", $token, PDO::PARAM_STR, 64);
	$sql->execute();
	$count = $sql->rowCount();
	if ($count == 0) {
		$db = null;
		return null;
	}
	$row = $sql->fetchAll();
	foreach ($row as $key) {
		$guid = $key['guid'];
	}
	$db = null;
	return $guid;
}

$db = null;
?>
