<?php
/* 
-----------------
Language: Spanish - Latin
-----------------
*/

$lang = array();
 
/**
-- Global String
**/
 
$lang['PAGE_LOGINTITLE']    = "Ingresar";
$lang['PAGE_REGTITLE']      = "Regístrate";
$lang['PAGE_PROFILE']       = "Perfil";
$lang['PAGE_DEVINFO']       = "Versión en Desarrollo";
 
/**
-- Login String
-- https://glaucus.net/login/
**/
$lang['LOGIN_TITLE']            = "Ingresa al sistema";
$lang['LOGIN_EMAIL']            = "Introduce el correo electrónico";
$lang['LOGIN_EMAILPLACEHOLDER'] = "Introduzca su correo electrónico registrado";
$lang['LOGIN_PASSWORD']         = "Escribe tu contraseña";
$lang['LOGIN_PASSPLACEHOLDER']  = "*********";
$lang['LOGIN_STARTSESSION']     = "Ingresar";
$lang['LOGIN_REGISTER']         = "Registrar una cuenta nueva";
$lang['LOGIN_INFO']             = "Estamos verificanco los datos de sesión...";
 
/**
-- Login String
-- https://glaucus.net/profile/
**/
$lang['PROFILE_ACCOUNTTITLE']       = "Cuenta";
$lang['PROFILE_EMAIL']              = "Correo:";
$lang['PROFILE_GUID']               = "Guid:";
$lang['PROFILE_LANGUAGE']           = "Idioma:";
$lang['PROFILE_SWITCHLANG']			= "Switch Language";
$lang['PROFILE_SWITCHEMAIL']		= "Switch Email";
$lang['PROFILE_SWITCHPASS']			= "Switch Password";
$lang['PROFILE_EDIT']               = "+Editar";
$lang['PROFILE_LOGOUT']             = "Salir";
$lang['PROFILE_CLICKTOGO']          = "Haga clic aquí para ir al sitio";
$lang['PROFILE_LINKTOMG']           = "https://www.minegrech.com/";
$lang['PROFILE_SOON']               = "Próximamente";
$lang['PROFILE_VERSION']            = "Profile v1.221016";
$lang['PROFILE_MAINTENANCE']        = "En mantenimiendo, por favor regrese más tarde.";
$lang['PROFILE_MANAGETITLE']		= "Configuration";
$lang['PROFILE_MODALHELPLANG']		= "Please, select you language and click on OK! button.";
	$lang['PROFILE_MODAL_SELECT']		= "Select a option!";
	$lang['PROFILE_MODAL_SELECT_ES']	= "Español";
	$lang['PROFILE_MODAL_SELECT_EN']	= "English";
$lang['PROFILE_MODALCHANGELANG']	= "Change my Language";
$lang['PROFILE_MODALCLOSE']			= "Close";
 
 
/**
-- Register String
-- https://glaucus.net/register/
**/
$lang['REG_TITLE']                  = "Registrarse";
$lang['REG_EMAIL']                  = "Introducir correo electrónico";
$lang['REG_EMAILPLACEHOLDER']       = "Introduce tu correo electrónico";
$lang['REG_PASSWORD']               = "Escribe tu contraseña";
$lang['REG_PASSPLACEHOLDER']        = "*********";
$lang['REG_REPEATPASS']             = "Repite la contraseña";
$lang['REG_REPEATPASSPLACEHOLDER']  = "*********";
$lang['REG_STARTSESSION']           = "Registrarse";
$lang['REG_LOGIN']                  = "Iniciar sesión";
$lang['REG_INFO']                   = "Estamos registrando y creando su nuevo perfil...";
$lang['REG_LANG']                   = "Cambiar a versión en español";
$lang['REG_LANGLINK']               = "https://glaucus.net/register/?lang=es";
$lang['REG_LOGIN']					= "Login with existing account";
$lang['REG_LOGINLINK']				= "https://glaucus.net/login";

// Register / Login Errors \\
$lang['validation_fields']          = "Algunos campos están incompletos";
$lang['validation_email']           = "El correo electrónico no se encuentra";
$lang['validation_password']        = "Debes repetir la contraseña";
$lang['validation_lpassword']       = "La contraseña no se encuentra";
$lang['validation_minpassword']     = "La contraseña debe tener 6 carácteres como mínimo";
$lang['validation_cpassword']       = "La contraseña no coincide";
$lang['validation_noemail']         = "El correo electrónico no es valido!";
$lang['validation_alreadyemail']    = "Este correo electrónico ya está registrado";
$lang['validation_captchaCLICK']    = "Debe aceptar el Re-Captcha";
$lang['validation_captchaFailed']   = "El Re-Captcha falló";
$lang['validation_account']         = "Esta cuenta no existe";
$lang['validation_default']         = "Ha ocurrido un error, por favor contactanos enviando un mensaje a support@glaucus.net";
$lang['validation_loginerroremail']     = "El correo electrónico es incorrecto";
$lang['validation_loginerrorpassword']  = "La contraseña es incorrecta";
$lang['validation_successlogout']       = "Se ha logueado correctamente";
 
 
 
?>