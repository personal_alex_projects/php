<?php
/* 
-----------------
Language: English - USA
-----------------
*/

$lang = array();

/**
-- Global String
**/
$lang['PAGE_FAVICO']			= "https://cdn.glaucus.net/img/favicon.png";
$lang['PAGE_LOGIN_TITLE'] 		= "Login...";
$lang['PAGE_REGISTER_TITLE'] 	= "Register";
$lang['PAGE_PROFILE_TITLE']		= "Profile";
$lang['PAGE_DEVINFO_TITLE']		= "Development Version";
$lang['PAGE_DEVVER_TITLE']		= "Version....V2-101116";

/**
-- Login String
-- https://glaucus.net/login/
**/
$lang['LOGIN_TITLE'] 			= "LOGIN TO SYSTEM";
$lang['LOGIN_EMAIL']	 		= "ENTER YOUR E-MAIL";
$lang['LOGIN_EMAILPLACEHOLDER'] = "ENTER YOUR REGISTERED E-MAIL";
$lang['LOGIN_PASSWORD'] 		= "ENTER YOUR PASSWORD";
$lang['LOGIN_PASSPLACEHOLDER']  = "*********";
$lang['LOGIN_STARTSESSION'] 	= "LOGIN";
$lang['LOGIN_REGISTER'] 		= "REGISTER A NEW ACCOUNT";
$lang['LOGIN_INFO'] 			= "We are checking your session data...";
$lang['LOGIN_REG']					= "Create Glaucus account now";
$lang['LOGIN_REGLINK']				= "https://glaucus.net/register";

/**
-- Profiles String (11 / 11 / 2016)
-- https://glaucus.net/profile/
**/

// Seccion 1
$lang['PROFILE_SEC1']				= "Account Details";
$lang['PROFILE_EMAIL']				= "Email:";
$lang['PROFILE_LANGUAGE']			= "Language:";
	$lang['PROFILE_LANGUAGE_EN']	= "English";
$lang['PROFILE_GUID']				= "GUID:";
$lang['PROFILE_uCOINS']				= "uCoins Balance";
$lang['PROFILE_VERIFYAC']			= "Verified Account";
	$lang['PROFILE_VERIFYAC_YES']	= "Account Verified!";
	$lang['PROFILE_VERIFYAC_NO']	= "Not Verified...";
$lang['PROFILE_TESTER']				= "Alpha-Tester";
$lang['PROFILE_GAC']				= "GAC´s Infractions";
$lang['PROFILE_PURCHASES']			= "Global Purchases";
$lang['PROFILE_SEC1_INFO']			= "Information";
	$lang['PROFILE_SEC1_P1']		= "The details of the account can only be viewed and edited. Our system has been created with maximum safety for users, protecting them from theft.";
	$lang['PROFILE_SEC1_P2']		= "All your data are updated every second, here you have all the general information of your account Glaucus Network Services, with any questions see the F.A.Q";
$lang['PROFILE_HELPBTNTITLE']		= "Help Buttons";
$lang['PROFILE_HELPBTNNAME']		= "F.A.Q";
$lang['PROFILE_SUPPBTNNAME']		= "Support";
$lang['PROFILE_LOGOUT']				= "Sign Off";
$lang['PROFILE_ADMPANEL']			= "Admin Panel";

// Seccion 2
$lang['PROFILE_SEC2']				= "Account Preferencess";
$lang['PROFILE_PERSONDETAILS']		= "Personal Details";
$lang['PROFILE_SWITCHLANG']			= "Switch Language";
$lang['PROFILE_REQUESTVALIDATION']	= "Request Validation";
$lang['PROFILE_ACCOUNTDETAIL']		= "Account Detail";
$lang['PROFILE_SWITCHEMAIL']		= "Switch Email";
$lang['PROFILE_SWITCHPASS']			= "Switch Password";
$lang['PROFILE_REQFAM']				= "Request Famous";
$lang['PROFILE_NEWSCONF']			= "Newsletter Mail";
$lang['PROFILE_uCOINSPRE']			= "uCoins";
$lang['PROFILE_AddUCOINS']			= "Add uCoins";
$lang['PROFILE_TransferUCOINS']		= "Transfer uCoins";
$lang['PROFILE_BETUCOINS']			= "Bet uCoins";
$lang['PROFILE_SEC2_INFO']			= "Information";
	$lang['PROFILE_SEC2_P1']		= "You can edit your information according to your preferences, choose what you want to edit and press the button.";
	$lang['PROFILE_SEC2_P2']		= "<b>Note:</b> Not all the details can be edited.";
/*
// Seccion 4
$lang['PROFILE_SEC4']				= "Account Vinculation´s";
$lang['PROFILE_ISLINKED']			= "Account Linked!";
$lang['PROFILE_NOTLINKED']			= "Not Linked!";
$lang['PROFILE_SEC4_ACTIONS']		= "Actions";
$lang['PROFILE_SEC4_ACTIONS_LINK']	= "Link Account!";
$lang['PROFILE_SEC4_ACTIONS_uLINK']	= "Un-Link Account!";
$lang['PROFILE_SEC4_ACTIONS_FAQ']	= "Go to F.A.Q!";
$lang['PROFILE_SEC4_QL']			= "Quick Link´s";

// Glaucus Sv 1
$lang['PROFILE_SV1']				= "Minegrech";
	$lang['PROFILE_SV1_INFO']		= "Minegrech is a server for all players who want to enjoy unique adrenaline. We ofrese many opportunities to play, lots of fun, crazy bets and more.";
	// En orden: Arriba hacia abajo.
	$lang['PROFILE_SV1_QL_1']		= "Go to my account!";
	$lang['PROFILE_SV1_QL_1L']		= "https://www.minegrech.com/profile/(...)";
	$lang['PROFILE_SV1_QL_2']		= "Go to forums!";
	$lang['PROFILE_SV1_QL_2L']		= "https://www.minegrech.com/forums/";
	$lang['PROFILE_SV1_QL_3']		= "Go to blog!";
	$lang['PROFILE_SV1_QL_3L']		= "https://www.minegrech.com/news/";

// Glaucus Sv 2
$lang['PROFILE_SV2']				= "";
	$lang['PROFILE_SV2_INFO']		= "";
	// En orden: Arriba hacia abajo.
	$lang['PROFILE_SV2_QL_1']		= "";
	$lang['PROFILE_SV2_QL_1L']		= "";
	$lang['PROFILE_SV2_QL_2']		= "";
	$lang['PROFILE_SV2_QL_2L']		= "";
	$lang['PROFILE_SV2_QL_3']		= "";
	$lang['PROFILE_SV2_QL_3L']		= "";

// MODALS -> Lenguaje
$lang['PROFILE_MODALHELPLANG']		= "Please, select you language and click on OK! button.";
	$lang['PROFILE_MODAL_SELECT']		= "Select a option!";
	$lang['PROFILE_MODAL_SELECT_ES']	= "Spanish";
	$lang['PROFILE_MODAL_SELECT_EN']	= "English";
$lang['PROFILE_MODALCHANGELANG']	= "OK!";

// MODALS -> Pedir Solicitud
$lang['PROFILE_MODALREQUESTT']		= "REQUEST VALIDATION";
$lang['PROFILE_MODALREQ_P']			= "Send us a request to verify that you own your account. (This is necessary for celebrities on different platforms)";
$lang['PROFILE_MODALCHANGELANG']	= "Change my Language";

// MODALS -> Newsletter
$lang['PROFILE_MAIL_TITLE']				= "CHANGE NEWSLETTER NOTIFICATION";
$lang['PROFILE_CURRENT']				= "Current: ";
$lang['PROFILE_CURRENT_YES']			= "I <b>would</b> like to receive news.";
$lang['PROFILE_CURRENT_NO']				= "I don't <b>like</b> to receive news.";
$lang['PROFILE_MAIL_INFO']				= "Please, select you language and click on OK! button.";
	$lang['PROFILE_MODAL_SELECT']		= "Select a option!";
	$lang['PROFILE_YES_SELECT']	= "I <b>would</b> like to receive news.";
	$lang['PROFILE_NP_SELECT']	= "I don't <b>like</b> to receive news.";
$lang['PROFILE_MODALCHANGEMAIL']		= "OK!";



$lang['PROFILE_MODALCLOSE']			= "Close";

*/

$lang['PROFILE_MAINTENANCE']		= "Under maintenance, please come back later.";
$lang['PROFILE_MANAGETITLE']		= "Configuration";
$lang['PROFILE_MODALHELPLANG']		= "Please, select you language and click on OK! button.";
	$lang['PROFILE_MODAL_SELECT']		= "Select a option!";
	$lang['PROFILE_MODAL_SELECT_ES']	= "Español";
	$lang['PROFILE_MODAL_SELECT_EN']	= "English";
$lang['PROFILE_MODALCHANGELANG']	= "OK!";
$lang['PROFILE_MODALCLOSE']			= "Close";

/**
-- Register String
-- https://glaucus.net/register/
**/
$lang['REG_TITLE'] 					= "REGISTER";
$lang['REG_EMAIL']	 				= "ENTER EMAIL";
$lang['REG_EMAILPLACEHOLDER'] 		= "ENTER YOUR E-MAIL";
$lang['REG_PASSWORD'] 				= "ENTER YOUR PASSWORD";
$lang['REG_PASSPLACEHOLDER'] 		= "*********";
$lang['REG_REPEATPASS']				= "REPEAT YOUR PASSWORD";
$lang['REG_REPEATPASSPLACEHOLDER']	= "*********";
$lang['REG_STARTSESSION'] 			= "REGISTER";
$lang['REG_LOGIN'] 					= "LOGIN";
$lang['REG_INFO'] 					= "We are registering and creating your new profile...";
$lang['REG_LANG']					= "Switch to Spanish version";
$lang['REG_LANGLINK']				= "https://glaucus.net/register/?lang=es";

$lang['REG_LOGIN']					= "Login with existing account";
$lang['REG_LOGINLINK']				= "https://glaucus.net/login";



// Register / Login Errors \\
$lang['validation_fields']			= "The fields were not completed";
$lang['validation_email']  			= "The e-mail is missing!";
$lang['validation_password']		= "The Password or Repeat-Password is missing!";
$lang['validation_lpassword']		= "The Password is missing!";
$lang['validation_minpassword'] 	= "You must have 6 lenght of password!";
$lang['validation_cpassword']   	= "Password do not match!";
$lang['validation_noemail']     	= "The e-mail not valid!";
$lang['validation_alreadyemail']	= "The e-mail is already registered!";
$lang['validation_captchaCLICK']	= "You must accept the Re-Captcha!";
$lang['validation_captchaFailed']	= "The Re-Captcha failed!";
$lang['validation_account']			= "Account not exits!";
$lang['validation_default']			= "An unknown error, please contact support@glaucus.net";
$lang['validation_loginerroremail']		= "Your email is incorrect!";
$lang['validation_loginerrorpassword']  = "Your password is incorrect!";
$lang['validation_successlogout']		= "Successfully logout!";



?>