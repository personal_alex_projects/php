<?php 
/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/

switch ($language) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/
?>
<!-- MODALS -> Switch Language -->
<div id="langchange" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><?php echo $lang['PROFILE_SWITCHLANG']; ?></h4>
			</div>
			<div class="modal-body">
				<p><b>Current:</b> <?php if($language == "en"){echo "English";}elseif($language == "es"){echo "Español";} ?></p>
				<p><?php echo $lang['PROFILE_MODALHELPLANG']; ?></p>
				<br>
				<form action="https://www.glaucus.net/profile/update/language.php" method="GET">
					<select class="form-control" name="slang" id="slang">
						<option disabled value="null" selected><?php echo $lang['PROFILE_MODAL_SELECT']; ?></option>
						<option value="es"><?php echo $lang['PROFILE_MODAL_SELECT_ES']; ?></option>
						<option value="en"><?php echo $lang['PROFILE_MODAL_SELECT_EN']; ?></option>
					</select>
					<br>
					<button type="submit" class="btn btn-primary"><?php echo $lang['PROFILE_MODALCHANGELANG']; ?></button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $lang['PROFILE_MODALCLOSE']; ?></button>
			</div>
		</div>
	</div>
</div>