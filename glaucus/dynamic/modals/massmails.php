<?php 
/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/

$_SESSION['guidS'] = $guid;

switch ($language) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/
?>
<head>
<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>	
</head>
<!-- MODALS -> Request Validation -->
<div id="sendmassmail" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Send massive newsletter</h4>
			</div>
			<div class="modal-body">
				<p>Remember that this function is important, not the bad use.</p>
				<br>
				<form action="https://www.glaucus.net/profile/update/createnewsletter.php" method="POST">
					<b>Send to...</b>
					<p><b><i>ALL THAT WHO WANT RECEIVE NEWSLETTER</i></b></p>
					<br>
					<div class="form-group">
						<label>GUID</label>
						<br>
						<input type="text" class="form-control" name="at_guid" disabled value="<?php echo $guid; ?>">
					</div>
					<div class="form-group">
						<label>Subject</label>
						<br>
						<input type="text" class="form-control" name="subject" placeholder="Subject of this newsletter">
					</div>
					<div class="form-group">
						<label>Message</label>
						<br>
						<textarea name="message" id="message" cols="65" rows="2" style="resize: none;"></textarea>
						 <script>
				            CKEDITOR.replace( 'message' );
				        </script>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4">
							<input type="submit" name="submit" class="btn btn-primary" value="SAVE NEWSLETTER">
						</div>
						<div class="col-md-8">
							<p>The account validation is very important for you to make purchases, changes or use our services as 'User'. Verification helps validate your account and that this is an account 'Official' and can start using our services smoothly.</p>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $lang['PROFILE_MODALCLOSE']; ?></button>
			</div>
		</div>
	</div>
</div>