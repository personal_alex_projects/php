<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/

switch ($language) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/
?>
<!-- MODALS -> Switch News -->
<div id="newschange" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">CHANGE EMAIL NEWS</h4>
			</div>
			<div class="modal-body">
				<p><b>Current:</b> <?php if($notices == "0"){echo "I don't like to receive news.";}elseif($notices == "1"){echo "I would like to receive news.";} ?></p>
				<p><?php echo $lang['PROFILE_MODALHELPLANG']; ?></p>
				<br>
				<form action="https://www.glaucus.net/profile/update/notices.php" method="POST">
					<select class="form-control" name="newsmail" id="newsmail">
						<option disabled value="null" selected><?php echo $lang['PROFILE_MODAL_SELECT']; ?></option>
						<option value="1">I <b>would</b> like to receive news.</option>
						<option value="0">I <b>don't</b> like to receive news.</option>
					</select>
					<br>
					<button type="submit" class="btn btn-primary">OK!</button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $lang['PROFILE_MODALCLOSE']; ?></button>
			</div>
		</div>
	</div>
</div>