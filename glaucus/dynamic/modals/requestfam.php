<?php 
/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/

switch ($language) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/
?>
<!-- MODALS -> Request Validation -->
<div id="requestfam" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">REQUEST FAMOUS ACCOUNT</h4>
			</div>
			<div class="modal-body">
				<p>Send us a request to verify that you own your account. (This is necessary for celebrities on different platforms)</p>
				<br>
				<form action="https://www.glaucus.net/profile/update/famous.php">
					<b>Business Email:</b>
					<input name="email"	value="email@here.com" class="form-control" type="text">
					<br>
					<p>Select your social links to validate your profile</p>

					<div class="checkbox">
						<label><input class="radio" type="checkbox" id="youtube" onclick="showMe('youtube-div')" name="socialCheck" value="YouTube">YouTube</label>
					</div>
					<div id="youtube-div" style="display: none;">
						<input type="text" name="youtube-url" placeholder="YouTube URL">
					</div>

					<div class="checkbox">
						<label><input class="radio" type="checkbox" id="facebook" onclick="showMe('facebook-div')" name="socialCheck" value="Facebook">Facebook</label>
					</div>
					<div id="facebook-div" style="display: none;">
						<input type="text" name="facebook-url" placeholder="Facebook URL">
					</div>

					<div class="checkbox">
						<label><input class="radio" type="checkbox" id="twitter" name="socialCheck" value="Twitter" onclick="showMe('twitter-div')">Twitter</label>
					</div>
					<div id="twitter-div" style="display: none;">
						<input type="text" name="twitter-url" placeholder="Twitter URL">
					</div>
					<br>
					<script>
						function showMe (box) {

							var chboxs = document.getElementsByName("socialCheck");
							var vis = "none";
							for(var i=0;i<chboxs.length;i++) { 
								if(chboxs[i].checked){
									vis = "block";
									break;
								}
							}
							document.getElementById(box).style.display = vis;


						}
					</script>
					<button type="submit" class="btn btn-primary">SEND REQUEST</button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $lang['PROFILE_MODALCLOSE']; ?></button>
			</div>
		</div>
	</div>
</div>