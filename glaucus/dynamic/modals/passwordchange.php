<?php 
/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/

switch ($language) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/
?>
<!-- MODALS -> Switch Email -->
<div id="passchange" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">CHANGE YOUR ACTUAL PASSWORD</h4>
			</div>
			<div class="modal-body">
				<p>Insert your new email</p>
				<br>
				<form action="https://www.glaucus.net/profile/update.php">
					<input name="actualpassword" placeholder="Insert your actual password" class="form-control" type="text">
					<br>
					<input name="newpassword" placeholder="Insert your new password here" class="form-control" type="text">	
					<input name="cnewpassword" placeholder="Repeat your new password here" class="form-control" type="text">						<br>
					<button type="submit" class="btn btn-primary">CHANGE PASSWORD</button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $lang['PROFILE_MODALCLOSE']; ?></button>
			</div>
		</div>
	</div>
</div>