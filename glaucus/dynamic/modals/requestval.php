<?php 
/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/

switch ($language) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/
?>
<!-- MODALS -> Request Validation -->
<div id="requestval" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">REQUEST VALIDATION</h4>
			</div>
			<div class="modal-body">
				<p>Confirm your email, if this right continues with the verification. Otherwise go back and change your email.</p>
				<br>
				<form action="https://www.glaucus.net/profile/update/verification.php" method="POST">
					<b>Account Email:</b>
					<input name="emailactual" value="<?php echo $_SESSION['email']; ?>" class="form-control" disabled type="text">
					<br>
					<p>Answer a few <b>optionals</b> questions to complete verification.</p>
					
					<div class="form-group">
						<label>Are you an old user?</label>
						<br>
						<textarea name="question1" id="question1" cols="65" rows="2" style="resize: none;"></textarea>
					</div>
					<div class="form-group">
						<label>How long do you spend on video games?</label>
						<br>
						<textarea name="question2" id="question2" cols="65" rows="2" style="resize: none;"></textarea>
					</div>
					<div class="form-group">
						<label>How did you get Glaucus Network?</label>
						<br>
						<textarea name="question3" id="question3" cols="65" rows="2" style="resize: none;"></textarea>
					</div>
					<div class="form-group">
						<label>Do you often play with friends?</label>
						<br>
						<textarea name="question4" id="question4" cols="65" rows="2" style="resize: none;"></textarea>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4">
							<input type="submit" name="submit" class="btn btn-primary" value="SEND REQUEST">
						</div>
						<div class="col-md-8">
							<p>The account validation is very important for you to make purchases, changes or use our services as 'User'. Verification helps validate your account and that this is an account 'Official' and can start using our services smoothly.</p>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $lang['PROFILE_MODALCLOSE']; ?></button>
			</div>
		</div>
	</div>
</div><?php ?>