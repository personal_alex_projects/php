<!DOCTYPE html>
<html>
<head>
	<title>Glaucus Network - Terms and Conditions</title>
	<link rel="icon" href="https://cdn.glaucus.net/img/favicon.png" type="image/x-icon"/>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="container">
	<div class="introduction">
		<h1><b>Terms of Service</b> <small>Last update November 30, 2016</small></h1>
	</div>
	<hr>
	<p style="color:black;">Glaucus Network reserves the right to update and change the Terms of Service from time to time without notice. Any new features that augment or enhance the current Service, including the release of new tools and resources, shall be subject to the Terms of Service. Continued use of the Service after any such changes shall constitute your consent to such changes. You can review the most current version of the Terms of Service at any time at: <a href="https://www.glaucus.net/terms">https://glaucus.net/terms</a></p>
	<div class="wrapper fontColor">
		<div class="pointA">
			<h2>A. Account Terms</h2>
			<hr>
			<ol>
				<li id="liStyle">You must be a human. Accounts registered by "bots" or other automated methods are not permitted.</li>
				<li id="liStyle">You must provide a valid email address, and any other information requested in order to complete the form.</li>
				<li id="liStyle">Your login may only be used by one person - a single login shared by multiple people is not permitted.</li>
				<li id="liStyle">You are responsible for maintaining the security of your account and password. Glaucus Network cannot and will not be liable for any loss or damage from your failure to comply with this security obligation.</li>
				<li id="liStyle">You are responsible for all Content posted and activity that occurs under your account (even when Content is posted by others who have accounts under your account).</li>
				<li id="liStyle">You may not use the Service for any illegal or unauthorized purpose. You must not, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright or trademark laws).</li>
				<li id="liStyle">Any suspicious account usage, the user membership (VIP Groups, uCoins, etc) be removed permanently or temporal and will be removed without notice. [Have option to recover only if administrators wish]</li>
			</ol>
		</div>
		<div class="pointB">
			<h2>B. Payment, Refunds, Upgrading and Downgrading Terms</h2>
			<hr>
			<ol>
				<li>All paid plans must provide a valid account through one of the approved payment providers provided. Free accounts are not required to provide an account through an approved payment provider.</li>
				<li>The Service is billed instantly and is non-refundable. There will be no refunds or credits for partial months of service, upgrade/downgrade refunds, or refunds for months unused with an open account. In order to treat everyone equally, no exceptions will be made.</li>
				<li>All fees are exclusive of all taxes, levies, or duties imposed by taxing authorities, and you shall be responsible for payment of all such taxes, levies, or duties, excluding only Spanish (federal or state) taxes.</li>
				<li>For any upgrade or downgrade in plan level, your payment account that you provided will automatically be charged the new rate.</li>
				<li>All "unlimited" and "lifetime" plans are only valid for the lifetime of the Service.</li>
				<li>All donations are taken as such and are under their responsibility.</li>
			</ol>
		</div>
		<div class="pointC">
			<h2>C. Cancellation and Termination</h2>
			<hr>
			<ol>
				<li>
					You are solely responsible for properly canceling your account. In order to cancel your account you must email
					<a href="mailto:support@glaucus.net">support@glaucus.net</a>
					with your cancellation request.
				</li>
				<li>All of your Content will be immediately deleted from the Service upon cancellation. This information can not be recovered once your account is cancelled.</li>
				<li>If you cancel the Service before the end of your current paid up month, your cancellation will take effect immediately and you will not be charged again.</li>
				<li>Glaucus Network (Glaucus Primal), in its sole discretion, has the right to suspend or terminate your account and refuse any and all current or future use of the Service, or any other Glaucus Network (Glaucus Primal) service, for any reason at any time. Such termination of the Service will result in the deactivation or deletion of your Account or your access to your Account, and the forfeiture and relinquishment of all Content in your Account.</li>
				<li>Glaucus Network (Glaucus Primal) reserves the right to refuse service to anyone for any reason at any time.</li>
			</ol>
		</div>
		<div class="pointD">
			<h2>D. Modifications to the Service and Prices</h2>
			<hr>
			<ol>
				<li>Glaucus Network reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Service (or any part thereof) with or without notice.</li>
				<li>Prices of all Services, including but not limited to monthly subscription plan fees to the Service, are subject to change upon 15 days notice from us. Such notice may be provided at any time by posting the changes to the Glaucus Network site or the Service itself.</li>
				<li>Glaucus Network shall not be liable to you or to any third party for any modification, price change, suspension or discontinuance of the Service.</li>
			</ol>
		</div>
		<div class="pointE">
			<h2>E. Confusion or erroneous payments</h2>
			<hr>
			<ol>
				<li>In the first place, services donations are properly created and distributed by Glaucus Network, this means that: If you think that some package contains something unsaid or not shown on the website you can not claim and your money will not be refunded as it stated in section C of this list.</li>
				<li>If you purchase any service that you "believe" in something unsaid, it is your responsibility to ask before donating. It means that you have all rights to ask for the service that you will acquire if you buy without consent read point C.</li>
				<li>Any donation you consider as an error, this will not be taken into account (Refer to point C). Because you had a lot of opportunity to cancel the payment and read where that service was or depositing their money.</li>
				<li><i>(In some services)</i>By donating an amount not described on the website, automatically your donation will be examined with the nearest decimal place and rounded. This means that if you donate $ 24 dollars, will be considered a donation of $ 20 or in this case the donation is considered as above (ie donation of $ 10 dollars). If you donate $ 26 from a total of $ 25, spend the same case.</li>
				<li>The remaining money will be considered as free donation. They do not accumulate for next donations.</li>
			</ol>
		</div>
		<div class="pointF">
			<h2>F. General Conditions</h2>
			<hr>
			<ol>
				<li>Your use of the Service is at your sole risk. The service is provided on an "as is" and "as available" basis.</li>
				<li><b>Technical</b> support is only available via email.</li>
				<li>You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service without the express written permission by Glaucus Network.</li>
				<li>We may, but have no obligation to, remove Content and Accounts containing Content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party's intellectual property or these Terms of Service.</li>
				<li>Verbal, physical, written or other abuse (including threats of abuse or retribution) of any Glaucus Network customer, employee, member, or officer will result in immediate account termination.</li>
			</ol>
		</div>
	</div>
	<div class="division-bottom">
		<hr>
		<p class="copyrights">Copyright &copy 2016 Glaucus Network <i>(All services)</i>, all rights reserved. <small>Powered by <a href="http://www.glaucus.net/">Glaucus Network</a></small></p>
	</div>
	</div>
</body>
</html>
<style>
	.fontColor{
		color: black;
		font-size: 15px;
	}
	.pointA{
		text-align: left;
	}
	.pointA ol li:before{
		color: blue;
	}
	.pointB{
		text-align: left;
	}
	.pointB ol li:before{
		color: blue;
	}
	.pointC{
		text-align: left;
	}
	.pointC a:link{
		color: blue;
	}
	.pointD{
		text-align: left;
	}
	.pointE{
		text-align: left;
	}
	.wrapper{
		margin: auto;
		margin-bottom: 5%;
		width: 90%;
		border: 0px solid #73AD21;
		padding: auto;
	}
</style>