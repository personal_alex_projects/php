<?php session_start(); ?>
<?php 
require ('/var/www/html/dynamic/functions/islogged.function.php');
require ('/var/www/html/dynamic/functions/profile.function.php');
if (!isLogged()) {
	header("Location: https://www.glaucus.net/login/");
}


$detail = obtenerDatos(obtenerGUID($_SESSION['LoginToken']));
foreach ($detail as $key) {
	$guid 		= $key['GUID'];
	$email 		= $key['Email'];
	$language 	= $key['Language'];
	$gcoins 	= $key['GCoins'];
	$verified 	= $key['verified'];
	$notices 	= $key['news'];
	$timemills 	= $key['creationtime'];
	$isAdmin	= $key['isAdmin'];
}

$_SESSION['email'] = $email;

/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/

switch ($language) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - <?php echo $lang['PAGE_PROFILE_TITLE']; ?></title>
	<link rel="stylesheet" href="https://www.glaucus.net/assets/styles/index.profile.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style>
		.backwell{
			background-color: #0064F91A;
		}
		.backwellfamous{
			background-color: #F9B0001A;
		}
		hr{
			border-color: #33dce3b3;
		}
	</style>
</head>
<body style="background-color: rgba(121, 130, 37, 0.08);">
<?php 
if (!isset($_COOKIE['page_information'])) {
	echo '
	<script type="text/javascript">
		$(window).load(function(){
			$("#page_info").modal("show");
		});
	</script>
	';
	$value = "ready";
	sleep(1);
	setcookie("page_information", $value, time()+1800, "/");
}?>
<div class="container">
	<div class="margin-border">
		<div class="section-0">
			<center>
				<img class="img-responsive" src='https://cdn.glaucus.net/img/logo2.png' style='margin-top: 1em; width: 400px; margin-left: 0.1%;'><br>
			</center>
		</div>
		<div class="section-1">
			<!-- <span class="label label-info">La traducción aun NO esta disponible.</span> -->
			<h1><?php echo $lang['PROFILE_SEC1']; ?></h1>
			<div class="well backwell">
				<div class="row">
					<div class="col-md-8">
						<div class="col-md-4">
							<p><b><?php echo $lang['PROFILE_EMAIL']; ?></b></p>
							<p><i><?php echo $email; ?></i></p>
							<p><b><?php echo $lang['PROFILE_LANGUAGE']; ?></b></p>
							<p><i><?php if($language == "en"){echo $lang['PROFILE_LANGUAGE_EN'];}elseif($language == "es"){echo "Español";} ?></i></p>
							<p><b><?php echo $lang['PROFILE_GUID']; ?></b></p>
							<code><?php echo $guid; ?></code>
						</div>
						<div class="col-md-4">
							<p><b><?php echo $lang['PROFILE_uCOINS']; ?></b></p>
							<p><i>$<?php echo $gcoins; ?></i></p>
							<p><b><?php echo $lang['PROFILE_VERIFYAC']; ?></b></p>
							<p><i><?php if($verified=="0"){echo $lang['PROFILE_VERIFYAC_NO'];}elseif($verified=="1"){echo $lang['PROFILE_VERIFYAC_YES'];} ?></i></p>
						</div>
						<div class="col-md-4">
							<p><b><?php echo $lang['PROFILE_GAC']; ?></b></p>
							<p><i>none</i></p>
							<p><b><?php echo $lang['PROFILE_PURCHASES']; ?></b></p>
							<p><i>none</i></p>
						</div>
					</div>
					<div class="col-md-4">
						<h3><?php echo $lang['PROFILE_SEC1_INFO']; ?></h3>
						<p><?php echo $lang['PROFILE_SEC1_P1']; ?></p>
						<p>All your data are updated every second, here you have all the general information of your account Glaucus Network Services, with any questions see the F.A.Q</p>
					</div>
				</div>
			</div>
			<h5>Help buttons</h5>
			<div class="btn-line">
				<button class="btn btn-default disabled">F.A.Q</button>
				<button class="btn btn-default disabled">SUPPORT</button>
			</div>
		</div>
		<hr>
		<a href="https://www.glaucus.net/profile/leave.php" class="btn btn-lg btn-block btn-danger">Sign off</a>
		<?php if ($isAdmin == "1") {
			echo '<a href="https://www.glaucus.net/profile/admin/" class="btn btn-lg btn-block btn-warning">Admin Panel</a>';
		} ?>
		
		<hr>
		<div class="section-2">
			<h1>Account Preferencess</h1>
			<div class="well backwell">
				<div class="row">
					<div class="col-md-8">
						<div class="col-md-4">
						<h4>Personal Details</h4>
							<p><button data-toggle="modal" data-target="#langchange" class="btn btn-primary">Switch Language</button></p>
							<!-- data-target: #requestfam -->
							<p><button data-toggle="modal" data-target="" class="btn btn-primary disabled" >Request Famous</button></p>
							<p><button data-toggle="modal" data-target="#newschange" class="btn btn-primary">Email News</button></p>
						</div>
						<div class="col-md-4">
						<h4>Account Details</h4>
						<!-- data-target: #emailchange -->
							<p><button data-toggle="modal" data-target="" class="btn btn-primary disabled">Switch Email</button></p>
							<p><button class="btn btn-primary disabled">Switch Password</button></p>
							<!-- data-target: #requestval -->
							<p><button data-toggle="modal" data-target="" class="btn btn-primary disabled">Request Validation</button></p>
						</div>
						<div class="col-md-4">
							<h4>uCoins</h4>
							<p><button class="btn btn-primary disabled">Add uCoins</button></p>
							<p><button class="btn btn-primary disabled">Transfer uCoins</button></p>
							<p><button class="btn btn-primary disabled">Bet uCoins</button></p>
						</div>
					</div>
					<div class="col-md-4">
						<h3>Information</h3>
						<p>You can edit your information according to your preferences, choose what you want to edit and press the button.</p>
						<p><b>Note:</b> Not all the details can be edited.</p>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<!--<div class="section-3">
			<h1>Famous Account</h1>
			<div class="well backwellfamous">
				<div class="row">
					<div class="col-md-8">
						<div class="col-md-4">
						<h4>Social Link</h4>
							<p><button data-toggle="modal" data-target="" class="btn btn-primary disabled">Link Youtube</button></p>
							<p><button data-toggle="modal" data-target="" class="btn btn-primary disabled">Link Facebook</button></p>
							<p><button data-toggle="modal" data-target="" class="btn btn-primary disabled">Link Twitter</button></p>
						</div>
						<div class="col-md-4">
						<h4>Actions</h4>
							<p><button data-toggle="modal" data-target="" class="btn btn-danger disabled">Leave Program</button></p>
						</div>
						<div class="col-md-4">
						<h4>Details</h4>
							<p><b>Quality</b></p>
							<p><i>60,12%</i></p>
							<p><b>Estimated money</b></p>
							<p><i>$5 - $10</i></p>
							<p><b>End Program</b></p>
							<p><i>4 days left</i></p>
						</div>
					</div>
					<div class="col-md-4">
						<h3>Information</h3>
						<p>Your account has been established as famous, you can edit your preferences famous in the next buttons. Remember to specify your data well, these will be necessary for the Sponsorship Program.</p>
						<p><b>Important:</b> Sponsorship Program is linked with <b>each</b> of which accounts you have.</p>
					</div>
				</div>
			</div>
		</div> -->
		<hr>
		<!--
		<div class="section-4">
			<h1>Account Vinculation`s</h1>
			<div class="well backwell">
				<div class="row">
					<div class="col-md-8">
						<div class="col-md-4">
							<img class="img-responsive" src="https://cdn.glaucus.net/img/green-tick.png" width="80%" alt="verified">
							<p style="margin-left: 12%;"><b>Account Linked!</b></p>
						</div>
						<div class="col-md-4">
							<h4>Actions</h4>
							<p><button class="btn btn-danger">Un-Link Account!</button></p>
						</div>
						<div class="col-md-4">
							<h4>Quick Link</h4>
							<p><button class="btn btn-primary">Go to my account!</button></p>
							<p><button class="btn btn-primary">Go to forums!</button></p>
							<p><button class="btn btn-primary">Go to blog!</button></p>
							<p><button class="btn btn-primary">Go to shop!</button></p>
						</div>
					</div>
					<div class="col-md-4">
						<h3>Minegrech</h3>
						<p>Minegrech is a server for all players who want to enjoy unique adrenaline. We ofrese many opportunities to play, lots of fun, crazy bets and more.</p>
					</div>
				</div>
			</div>
			<div class="well backwell">
				<div class="row">
					<div class="col-md-8">
						<div class="col-md-4">
							<img class="img-responsive" src="https://cdn.glaucus.net/img/check-red.png" width="80%" alt="verified">
							<p style="margin-left: 20%;"><b>Not Linked!</b></p>
						</div>
						<div class="col-md-4">
							<h4>Actions</h4>
							<p><button class="btn btn-danger">Link account!</button></p>
							<p><button class="btn btn-warning">Go to F.A.Q!</button></p>
						</div>
					</div>
					<div class="col-md-4">
						<h3>Server</h3>
						<p>Server is Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quam dolor blanditiis fuga, placeat maxime, sit voluptatibus illum commodi itaque! Provident similique fugit nihil, odio accusamus officia ratione architecto sit.</p>
					</div>
				</div>
			</div>
		</div> 
		-->
		<p>Version V2-101116</p>
	</div>
</div>
</body>
</html>

<!-- MODALS -> Page Information -->
<div id="page_info" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">MANTENIMIENTO EN CURSO</h4>
			</div>
			<div class="modal-body">
				<p>Estaremos realizando mantenimientos diarios en nuestra base de datos, tanto como en el sitio web.</p>
				<p><b>IMPORTANTE:</b> La verificacion de cuenta estara disponible la fecha 13/11/16 </p>
				<p><b>Nota:</b> El lenguaje "Español" aun contiene errores de traducción.</p>
				<p><b>Nota:</b> Algunos datos aun no son requeridos.</p>
				<p>Te rogamos el informar ante un administrador o el uso del correo <code>alexbanper@glaucus.net</code> o <code>development@glaucus.net</code> cualquier error que puedas notar.</p>
				<p>Gracias de antemano, AlexBanPer.</p>
			</div>
			<div class="modal-footer">
				<button type="button" name="mancookie" class="btn btn-default" data-dismiss="modal">CERRAR</button>
			</div>
		</div>

	</div>
</div>
<?php  
foreach (glob("/var/www/html/dynamic/modals/*.php") as $filename) {
	include $filename;
}
?>