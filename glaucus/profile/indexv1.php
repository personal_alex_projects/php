<?php session_start(); ?>
<?php 
require ('/var/www/html/dynamic/functions/islogged.function.php');
require ('/var/www/html/dynamic/functions/profile.function.php');
if (!isLogged()) {
	header("Location: https://www.glaucus.net/login/");
}

$detail = obtenerDatos(obtenerGUID($_SESSION['LoginToken']));
foreach ($detail as $key) {
	$guid 	= $key['GUID'];
	$email 	= $key['Email'];
	$language 	= $key['Language']; 
}


/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/
date_default_timezone_set("UTC");

switch ($language) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - <?php echo $lang['PAGE_PROFILE']; ?></title>
	<link rel="stylesheet" href="https://www.glaucus.net/assets/styles/index.profile.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body style="background-color: #F5F5F5;">
	<?php 

	?>
	<br>
	<center>
		<div class="centro-tabla">
			<div class="sec-0">
				<img class="img-responsive" src='https://cdn.glaucus.net/img/logo2.png' style='width: 400px; margin-left: 0.1%;'>
			</div>
			<div class="sec-1">
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-5">
						<h2 style="margin-right: 30%;"><?php echo $lang['PROFILE_ACCOUNTTITLE']; ?></h2>
						<div class="col-md-6">
							<p class="text-input"><?php echo $lang['PROFILE_EMAIL']; ?></p>
							<p><i><?php echo $email; ?></i></p>
							<p class="text-input"><?php echo $lang['PROFILE_LANGUAGE']; ?></p>
							<p><i><?php if($language == "en"){echo "English";}elseif($language == "es"){echo "Español";} ?></i><br></p>
							<p class="text-input"><?php echo $lang['PROFILE_GUID']; ?></p>
							<code><?php echo $guid; ?></code>
						</div>
						<div class="col-md-6">
							<p class="text-input">uCoins</p>
							<p><i>$000.00</i></p>
							<p class="text-input">Verification Account</p>
							<p><i>VERIFIED</i><br></p>
							<p class="text-input">Alpha Tester</p>
							<p><i>ACTIVED</i></p>
						</div>
					</div>
					<div class="col-md-4">
						<h2 style="margin-right: 30%;"><?php echo $lang['PROFILE_MANAGETITLE']; ?></h2>
						<div class="col-md-7">
							<p><a class="btn btn-md btn-primary" href="" class="change-lang">Add uCoin`s</a></p>
							<p><button type="button" class="btn btn-md btn-primary" data-toggle="modal" data-target="#langchange" class="change-lang"><?php echo $lang['PROFILE_SWITCHLANG']; ?></button></p>
							<p><a class="btn btn-md btn-primary" href="" class="change-lang"><?php echo $lang['PROFILE_SWITCHEMAIL']; ?></a></p>
							<p><a class="btn btn-md btn-primary" href="" class="change-lang"><?php echo $lang['PROFILE_SWITCHPASS']; ?></a></p>
						</div>
						<div class="col-md-4">
							<p><a class="btn btn-md btn-danger" href="https://www.glaucus.net/profile/leave.php"><?php echo $lang['PROFILE_LOGOUT']; ?></a></p>
						</div>	
					</div>
				</div>
			</div>
			<div class="sec-2">
				<div class="row">
					<div class="col-md-4">
						<img class="img-responsive border" src="https://www.glaucus.net/avatar.jpg" width="45%" alt="user_avatar">
					</div>
					<div class="col-md-8">
						<div class="col-md-8 sv-name">
							<h2 class="sv-name-title">Minegrech Website</h2>
						</div>
						<div class="col-xs-4 link-to-site">
							<a href="<?php echo $lang['PROFILE_LINKTOMG']; ?>"><h3 class="link-to"><?php echo $lang['PROFILE_CLICKTOGO']; ?></h3></a>
						</div>
					</div>
				</div>
			</div>
			<div class="sec-3">
				<div class="row">
					<div class="col-md-4">
						<img class="img-responsive border" src="https://placeholdit.imgix.net/~text?txtsize=15&txt=161%C3%97161&w=161&h=161" width="45%" alt="user_avatar">
					</div>
					<div class="col-md-8">
						<div class="col-md-8 sv-name">
							<h2 class="sv-name-title">Glaucus Network</h2>
						</div>
						<div class="col-xs-4 link-to-site">
							<a href="#"><h3 class="link-to"><?php echo $lang['PROFILE_SOON']; ?></h3></a>
						</div>
					</div>
				</div>
			</div>
			<div class="sec-4">
				<div class="row">
					<div class="col-md-4">
						<img class="img-responsive border" src="https://placeholdit.imgix.net/~text?txtsize=15&txt=161%C3%97161&w=161&h=161" width="45%" alt="user_avatar">
					</div>
					<div class="col-md-8">
						<div class="col-md-8 sv-name">
							<h2 class="sv-name-title">Glaucus Network</h2>
						</div>
						<div class="col-xs-4 link-to-site">
							<a href="#"><h3 class="link-to"><?php echo $lang['PROFILE_SOON']; ?></h3></a>
						</div>
					</div>
				</div>
			</div>
			<p><?php echo $lang['PROFILE_VERSION']; ?></p>
		</div>
	</center>
</body>
</html>

<!-- MODALS -> Switch Language -->
<div id="langchange" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><?php echo $lang['PROFILE_SWITCHLANG']; ?></h4>
			</div>
			<div class="modal-body">
				<p><?php echo $lang['PROFILE_MODALHELPLANG']; ?></p>
				<form action="update.php">
					<select class="form-control" name="slang" id="slang">
						<option disabled value="null" selected><?php echo $lang['PROFILE_MODAL_SELECT']; ?></option>
						<option value="es"><?php echo $lang['PROFILE_MODAL_SELECT_ES']; ?></option>
						<option value="en"><?php echo $lang['PROFILE_MODAL_SELECT_EN']; ?></option>
					</select>
					<button type="submit" class="btn btn-default"><?php echo $lang['PROFILE_MODALCHANGELANG']; ?></button>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $lang['PROFILE_MODALCLOSE']; ?></button>
			</div>
		</div>

	</div>
</div>


<!-- MODALS -> Page Information -->
<div id="page_info" class="modal" role="" data-backdrop="static" 
   data-keyboard="false" >
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Mantenimiento en Curso</h4>
			</div>
			<div class="modal-body">
				<p><b>MANTENIMIENTO</b></p>
				<p>Este mantenimiento se llevara a cabo en un par de horas.</p>
				<p><b>Hora Actual:</b> <?php $time = time(); echo date("H:i:s A", $time)." UTC"; ?></p>
				<p><b>Hora Fin:</b> 18:00:00 PM UTC</p>
			</div>
			<div class="modal-footer">
				<p><b>Estamos actualizando los perfiles de Glaucus Network...</b></p>
			</div>
		</div>

	</div>
</div>
<?php  
	echo '
	<script type="text/javascript">
		$(window).load(function(){
			$("#page_info").modal("show");
		});
	</script>
	';


?>