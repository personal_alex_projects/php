<?php session_start(); ?>
<?php 
ini_set('date.timezone', 'UTC');
require ('/var/www/html/dynamic/functions/islogged.function.php');
require ('/var/www/html/dynamic/functions/newsletter.function.php');

if (!isLogged()) {
	header("Location: https://www.glaucus.net/login/");
}
$subject_mail = $_POST['subject'];
$message_mail = $_POST['message'];
$status_mail  = $_POST['status_sec'];
$newsID  = $_SESSION['newsID'];

if (isset($_POST['submit'])) {
	if ($_POST['subject']) {
		$date = date('Y-m-d H:i:s');
		updateNewsletter($newsID, $subject_mail, $message_mail, $date, $status_mail);
		unset($_SESSION['newsID']);
	}else{
		setcookie("update_complete", "notnewssend", time()+5);
	}
	
}


header("Refresh: 1, url=https://www.glaucus.net/profile/admin/newsletters/");
setcookie("update_complete", "update_new", time()+5);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - Update</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<link rel="stylesheet" href="../../assets/styles/index.reg.css">
</head>
<body>
	<center>
		<div class="mensaje-error">
			<img src="https://cdn.glaucus.net/img/logo2.png" style="width: 400px; margin-right:15px;">	
		</div>
		<img src="https://cdn.glaucus.net/img/Loading1.gif" style="height: 200px;">	
	</center>
	<style>
		center {
			top: calc(50% - 250px);
			position: absolute;
			left: calc(50% - 207px);
		}
		body{
			background-color: #F5F5F5;
		}
		.ajust{
			margin-top:10em;
		}
	</style>
</body>
</html>