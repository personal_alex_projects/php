<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>%username% - Glaucus Network Profile.</title>
	<link rel="icon" href="https://cdn.glaucus.net/img/favicon.png" type="image/x-icon">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://www.glaucus.net/assets/styles/index.profile.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/eab7cd6eb3.js"></script>
</head>
<body>
	<link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.6.3/font-awesome.min.css">
	<section>
		<div class="container" style="margin-top: 30px;">
			<div class="profile-head">
				<div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: 3%;">
					<img src="https://placeholdit.imgix.net/~text?txtsize=28&txt=300%C3%97300&w=300&h=300" class="img-responsive" />
					<h6>alexbanper@glaucus.net</h6>
					<br>
					<div class="logout">
						<a href="#" class="btn btn-danger"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<h5>alexbanper@glaucus.net</h5>
					<p>Web Designer / Developer </p>
					<ul>
						<li><i class="fa fa-id-badge" aria-hidden="true"></i> Verified <i style="color:lightgreen;" class="fa fa-check" aria-hidden="true"></i></span><i style="color:red;" class="fa fa-times" aria-hidden="true"></i></li>
						<li><i class="fa fa-cc" aria-hidden="true"></i> <b>Language:</b> </li>
						<li><i class="fa fa-user-secret" aria-hidden="true"></i> <a href="#" title="GUID">GUID-00000000000</a></li>
					</ul>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="well" style="padding-bottom: 20%;background-color: #c6e3ff;">
						<center>
							<h3 style="color: blue; font-weight: bold;">gCOINS BALANCE</h3>
							<h1 style="color:black;"><b>500</b><small><b>.00</b></small> <b>G$</b></h1>
							<div class="form-style-6" style="margin-bottom: -3em;">
								<input value="Add gCoins" type="button">
							</div>
							<br>
							<div class="col-xs-5 col-md-6">
								<div class="form-style-6">
									<input style="padding: 5%;" value="History" type="button">
								</div>
							</div>
							<div class="col-xs-5 col-md-6">
								<div class="form-style-6">
									<input style="padding: 5%;" value="Store" type="button">
								</div>
							</div>
						</center>
					</div>
				</div>
			</div>
		</div>
		<div id="sticky" class="container">
			<ul class="nav nav-tabs nav-menu" role="tablist">
				<li class="active">
					<a style="background-color: #4768F6 !important;" href="#profile" role="tab" data-toggle="tab">
						<i class="fa fa-male"></i> Glaucus Profile
					</a>
				</li>
				<li>
					<a style="background-color: orangered !important;" href="#mgprofile" role="tab" data-toggle="tab">
						<i class="fa fa-male"></i> Minegrech Profile
					</a>
				</li>
				<li>
					<a href="#change" role="tab" data-toggle="tab">
						<i class="fa fa-cogs"></i> Configuration
					</a>
				</li>
				<li>
					<a href="#links" role="tab" data-toggle="tab">
						<i class="fa fa-plus-square"></i> Accounts
					</a>
				</li>
				<li>
					<a href="#" role="tab" data-toggle="tab">
						<i class="fa fa-tachometer"></i> Administration
					</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane fade active in" id="profile">
					<div class="container">
						<div class="col-sm-11" style="float:left;">
							<div class="hve-pro">
								<h6>Hello I'm AlexBanPer, designer and CEO of Glaucus Network, I am currently working on improving the website, so that we can all enjoy something beautiful and pleasant. I invite you to give your ideas to my mail <code>alexbanper@glaucus.net</code>.</h6>
							</div>
						</div>
						<br clear="all" />
						<div class="row">
							<div class="col-md-12">
								<h4 class="pro-title">My Information</h4>
							</div>


							<div class="col-md-6">

								<div class="table-responsive responsiv-table">
									<table class="table bio-table">
										<tbody>
											<tr>      
												<th>Firstname</th>
												<th>: jenifer</th> 
											</tr>
											<tr>    
												<th>Birthday</th>
												<th>: 10 january 1980</th>       
											</tr>
											<tr>    
												<th>Contury</th>
												<th>: U.S.A.</th>       
											</tr>
											<tr>
												<th>Occupation</th>
												<th>: Web Designer</th> 
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-6">
								<div class="table-responsive responsiv-table">
									<table class="table bio-table">
										<tbody>
											<tr>  
												<th>Email</th>
												<th>: jenifer123@gmail.com</th> 
											</tr>
											<tr>
												<th>Twiter</th>
												<th>: @jenifer123</th> 
											</tr>
											<tr>    
												<th>Mobile</th>
												<th>: (+6283) 456 789</th>       
											</tr>
											<tr>    
												<th>Punishments</th>
												<th>: 0</th>       
											</tr>
											<tr>    
												<th>Purshases</th>
												<th>: 5 </th>       
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="mgprofile">
					<div class="container fom-main">
						<div class="row">
							<div class="col-sm-12">
								<h2 class="register">Minegrech Account</h2>
								<div class="col-md-push-3 col-sm-6">
									<div class="alert alert-info" role="alert">This application does not allow change some adjust.</div>
								</div>
							</div>
						</div>
						<br />
						<div class="row">
							<form class="form-horizontal main_form text-left" action=" " method="post"  id="contact_form">
								<!--<fieldset>
									<div class="form-group col-md-12">
										<label class="col-md-10 control-label">Username</label>  
										<div class="col-md-12 inputGroupContainer">
											<div class="input-group">
												<input disabled name="username" placeholder="Username" class="form-control"  type="text" value="MyUsername"
											</div>
										</div>
									</div>
								</fieldset>-->
							</form>
						</div>
					</div>         
				</div><!--tab-pane close-->
				<div class="tab-pane fade" id="change">
					<div class="container fom-main">
						<div class="row">
							<div class="col-sm-12">
								<h2 class="register">CONFIGURATION</h2>
							</div>
						</div>
						<br />
						<div class="row" style="margin-right: -57px;padding-right: 0px;padding-left: 31px;margin-left: -37px;">
							<form class="form-horizontal main_form text-left" action=" " method="post"  id="contact_form">
								<div class="form-group col-md-11 col-sm-12">
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Personal Details</div>
											<div class="panel-body">
												<div class="form-style-6" style="display: initial;margin-left: -3.2%;">
													<input data-toggle="modal" data-target="#langchange" value="Switch Language" type="button">								
													<br>
													<input data-toggle="modal" data-target="#mailchange" value="Switch Email" type="button">
													<br>
													<input data-toggle="modal" data-target="#passchange" value="Switch Password" type="button">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Account Details</div>
											<div class="panel-body">
												<div class="form-style-6" style="display: initial;margin-left: -3.2%;">
													<input data-toggle="modal" data-target="#reqvalid" value="Request Validation" type="button">							
													<br>
													<input data-toggle="modal" data-target="#reqfamo" value="Request Famous" type="button">	
													<br>
													<input data-toggle="modal" data-target="#newsletter" value="Newsletter" type="button">
												</div>
											</div>
										</div>
									</div>
									<br>
									<br>
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Security Details</div>
											<div class="panel-body">
												<div class="form-style-6" style="display: initial;margin-left: -3.2%;">
													<input data-toggle="modal" data-target="#" value="Purshases" type="button">								
													<br>
													<input data-toggle="modal" data-target="#" value="Account" type="button">
													<br>
													<input data-toggle="modal" data-target="#" value="Services" type="button">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">Database Details</div>
											<div class="panel-body">
												
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>         
				</div><!--tab-pane close-->
				<div class="tab-pane fade" id="links">
					<div class="container fom-main">
						<div class="row">
							<div class="col-sm-12">
								<h2 class="register">Vinculation Accounts</h2>
							</div>
						</div>
						<br />
						<div class="row" style="margin-right: -57px;padding-right: 0px;padding-left: 31px;margin-left: -37px;">
							<form class="form-horizontal main_form text-left" action="#" method="post"  id="linkMG">
								<div class="col-md-11 col-sm-12">
									<div class="panel panel-success">
										<div class="panel-heading">Some Account Linked</div>
										<div class="panel-body">
											<div class="col-md-4">
												<img class="img-responsive" src="https://cdn.glaucus.net/glaucus_minegrech/images/yes.png" alt="verified" width="55%">
												<p style="margin-left: 12%;"><b>Account Linked!</b></p>
											</div>
											<div class="col-md-4">
												<h4>One Click!</h4>
												<div class="form-style-6">
													<p><a href="https://www.minegrech.com/profile/AlexBanPer"><input value="My Profiles" type="button"></a></p>
													<p><a href="https://www.minegrech.com/forum/"><input value="Support Forums" type="button"></a></p>
												</div>
											</div>
											<div class="col-md-4">
												<h3>Some Account</h3>
												<p>Some account description Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam placeat, incidunt. At autem soluta recusandae fugit sed, iusto placeat eaque. Quod dolorem cupiditate eveniet, architecto amet ea excepturi!.</p>
											</div>
										</div>
									</div>
									<div class="panel panel-danger">
										<div class="panel-heading">
											<h3 class="panel-title">Some unlinked account</h3>
										</div>
										<div class="panel-body">
											<div class="col-md-4">
												<img class="img-responsive" src="https://cdn.glaucus.net/glaucus_minegrech/images/no.png" alt="verified" width="55%">
												<p style="margin-left: 12%;"><b>Account Linked!</b></p>
											</div>
											<div class="col-md-4">
												<h4>One Click!</h4>
												<div class="form-style-6">
													<p><button style="background-image: linear-gradient(#ef3f3f, #d7171e 60%, #c84137); color: white;" data-toggle="modal" data-target="#linkgp" class="btn btn-danger btn-block">Link Account!</button></a></p>
												</div>
											</div>
											<div class="col-md-4">
												<h3>Some Account</h3>
												<p>Some account description Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam placeat, incidunt. At autem soluta recusandae fugit sed, iusto placeat eaque. Quod dolorem cupiditate eveniet, architecto amet ea excepturi!.</p>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div><!--row close-->
					</div><!--container close -->          
				</div><!--tab-pane close-->
			</div>
		</div>
	</section>
</body>
</html>

<style>
	@import url('https://fonts.googleapis.com/css?family=Noto+Sans');
	body { background:#ffffff; font-family: 'Noto Sans', sans-serif;}
	.page-header {background:#ccc;margin:0;}
	.profile-head { width:100%;background-color: rgb(71, 104, 246);float: left;padding: 15px 5px;}
	.profile-head img { height:150px; width:150px; margin:0 auto; border:5px solid #fff; border-radius:5px;}
	.profile-head h5 {width: 100%;padding:5px 5px 0px 5px;text-align:justify;font-weight: bold;color: #fff;font-size: 25px;text-transform:capitalize; margin-bottom: 0;}
	.profile-head p {width: 100%;text-align: justify;padding:0px 5px 5px 5px;color: #fff;font-size:17px;text-transform:capitalize;margin:0;}
	.profile-head a {width: 100%;text-align: center;padding: 10px 5px;color: #fff;font-size: 15px;text-transform: capitalize;}
	.profile-head ul { list-style:none;padding: 30px;margin-left: -7%;}
	.profile-head ul li { display:block; color:#ffffff;padding:5px;font-weight: 400;font-size: 15px;}
	.profile-head ul li:hover span { color:rgb(0, 4, 51);}
	.profile-head ul li span { color:#ffffff;padding-right: 10px;}
	.profile-head ul li a { color:#ffffff;}
	.profile-head h6 {width: 100%;text-align: center;font-weight: 100;color: #fff;font-size: 15px;text-transform: uppercase;margin-bottom: 0;}
	.nav-tabs {margin: 0;padding: 0;border: 0;}
	.nav-tabs > li > a {background: #DADADA;border-radius: 0; box-shadow: inset 0 -8px 7px -9px rgba(0,0,0,.4),-2px -2px 5px -2px rgba(0,0,0,.4);}
	.nav-tabs > li.active > a,
	.nav-tabs > li.active > a:hover {background: #F5F5F5; box-shadow: inset 0 0 0 0 rgba(0,0,0,.4),-2px -3px 5px -2px rgba(0,0,0,.4);}
	.tab-pane {background: #ffffff;box-shadow: 0 0 4px rgba(0,0,0,.4);border-radius: 0;text-align: center;padding: 10px;}
.tab-content>.active {margin-top:50px;/*width:100% !important;*/} 

/* edit profile css*/

.hve-pro {    background-color:rgb(0, 138, 255);padding: 5px; width:100%; height:auto;float:left;}
.hve-pro h6 {width: 100%;text-align: center;font-weight: 100;color: #fff;font-size: 15px;text-transform: uppercase;margin-bottom: 0;}
.hve-pro p {float: left;color:#fff;font-size: 15px;text-transform: capitalize;padding: 5px 20px;font-family: 'Noto Sans', sans-serif;}
h2.register { padding:10px 25px; text-transform:capitalize;font-size: 25px;color: rgb(255, 102, 0);}
.fom-main { overflow:hidden;}

legend {font-family: 'Bitter', serif;color:#ff3200;border-bottom:0px solid;}
.main_form {background-color: #;}
label.control-label {font-family: 'Noto Sans', sans-serif;font-weight: 100; margin-bottom:5px !important; text-align:left !important; text-transform:uppercase; color:#798288;}
.submit-button {color: #fff;background-color:rgb(255, 102, 0);width:190px;border: 0px solid;border-radius: 0px; transition:all ease 0.3s;margin: 5px; float:left;}
.submit-button:hover, .submit-button:focus {color: #fff;background-color:rgb(0, 4, 51);}
.hint_icon {color:#ff3200;}
.form-control:focus {border-color: #ff3200;}
select.selectpicker { color:#99999c;}
select.selectpicker option { color:#000 !important;}
select.selectpicker option:first-child { color:#99999c;}
.input-group { width:100%;}
.uplod-picture {width: 100%; background-color:#;color: #fff; padding:20px 10px;margin-bottom:10px;}
.uplod-file {position: relative;overflow: hidden;color: #fff;background-color: rgb(0, 4, 51);border: 0px solid #a02e09;border-radius: 0px;transition:all ease 0.3s;margin: 5px;}
.uplod-file input[type=file] {position: absolute;top: 0;right: 0;min-width: 100%;min-height: 100%;font-size: 100px;text-align: right; filter: alpha(opacity=0);opacity: 0;outline: none;background: white;cursor: inherit;display: block;}
.uplod-file:hover, .uplod-file:focus {color: #fff;background-color:rgb(255, 102, 0);}
h4.pro-title { font-size:24px; color:rgba(0, 4, 51, 0.96); text-transform:capitalize; text-align:justify;padding: 10px 15px;font-family: 'Bitter', serif;}
.bio-table { width:75%;border:0px solid;}
.bio-table th {text-transform: capitalize;text-align: left;font-size: 15px;}
.bio-table>tbody>tr>th { border:0px solid;text-transform: capitalize;color: rgb(0, 4, 51); font-size:15px;}
.responsiv-table { border:0px solid;}
.nav-menu li a {margin: 5px 5px 5px 5px;position: relative;display: block;padding: 10px 50px;border: 0px solid !important;box-shadow: none !important;background-color: rgb(0, 4, 51) !important;color: #fff !important;    white-space: nowrap;}
.nav-menu li.active a {background-color: rgba(0, 4, 51, 0.81) !important;}
.stick{position:fixed !important;top:0;z-index:999 !important;width:100%;background:#ffffff !important;height:auto; transition:all ease 0.8s;-webkit-box-shadow: 0px 2px 7px 0px rgba(0,0,0,0.75);-moz-box-shadow: 0px 2px 7px 0px rgba(0,0,0,0.75);box-shadow: 0px 2px 7px 0px rgba(0,0,0,0.75);}
.stick a { line-height:20px !important;}
.stick img { margin:0 !important;}



@media all and (max-width:768px){
	.navbar-inverse .drop_menu {display: block;visibility: visible;width: 110px;height:1000px;padding:0px 20px;position: absolute;right:-100px;transition:all ease 0.5s;border-top: 0px solid;cursor: pointer;}
	.navbar-brand {padding: 0;margin-left: 10px !important;}
	a.menu { display:block !important;margin: 9px 2px;float: right;color: rgba(255, 102, 0, 0.98); border:0px solid; background:none; font-size:30px;width:27px;position: relative;cursor:pointer;}
	a.menu:hover, a.menu:focus { color:rgb(0, 4, 51);}

	.drop_menu1 { display: block;visibility: visible;width:250px;height:1000px;padding:5px 30px;position: absolute;right:0 !important;background-color:#ffffff !important; transition:all ease 0.5s;border-top: 0px solid;cursor: pointer;}
}

@media all and (max-width:430px){
	.profile-head ul li {font-size: 12px !important;}
	.nav-menu li { width:50%;}
	.bio-table>tbody>tr>th {font-size: 13px;}
}


</style>
<script>
	$(document).ready(function(e) {
		$('.form').find('input, textarea').on('keyup blur focus', function (e) {
			var $this = $(this),
			label = $this.prev('label');
			if (e.type === 'keyup') {
				if ($this.val() === '') {
					label.removeClass('active highlight');
				} else {
					label.addClass('active highlight');
				}
			} else if (e.type === 'blur') {
				if( $this.val() === '' ) {
					label.removeClass('active highlight'); 
				} else {
					label.removeClass('highlight');   
				}   
			} else if (e.type === 'focus') {

				if( $this.val() === '' ) {
					label.removeClass('highlight'); 
				} 
				else if( $this.val() !== '' ) {
					label.addClass('highlight');
				}
			}
		});
		$('.tab a').on('click', function (e) {
			e.preventhefault();
			$(this).parent().addClass('active');
			$(this).parent().siblings().removeClass('active');
			target = $(this).attr('href');

			$('.tab-content > div').not(target).hide();

			$(target).fadeIn(600);
		});
//canvas off js//
$('#menu_icon').click(function(){
	if($("#content_details").hasClass('drop_menu'))
	{
		$("#content_details").addClass('drop_menu1').removeClass('drop_menu');
	}
	else{
		$("#content_details").addClass('drop_menu').removeClass('drop_menu1');
	}
});

$("#flip").click(function(){
	$("#panel").slideToggle("5000");
});

$(window).scroll(function(){
	if ($(window).scrollTop() >= 500) {
		$('nav').addClass('stick');
	}
	else {
		$('nav').removeClass('stick');
	}
});
});
</script>