<?php session_start(); ?>
<?php 
require ('/var/www/html/dynamic/functions/islogged.function.php');
require ('/var/www/html/dynamic/functions/profile.function.php');
require ('/var/www/html/dynamic/functions/newsletter.function.php');

if (!isLogged()) {
	header("Location: https://www.glaucus.net/login/");
}
$detail = obtenerDatos(obtenerGUID($_SESSION['LoginToken']));
foreach ($detail as $key) {
	$guid 		= $key['GUID'];
}

$guid  = $_SESSION['guidS'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - Create new newsletter</title>
	<link rel="icon" href="https://cdn.glaucus.net/img/favicon.png" type="image/x-icon">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://www.glaucus.net/assets/styles/index.profile.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<hr>
				<div style="display: inline-block;">
					<h3>Actions</h3>
					<a class="btn btn-default" href="https://www.glaucus.net/profile/admin/newsletters/">Back</a>
					<a class="btn btn-danger" href="https://www.glaucus.net/profile/admin/newsletters/remove/?&id=$id&action=remove">Delete</a>
					<button class="btn btn-primary">Save & Publish</button>
				</div>
				<br>
				<hr>
				<form action="https://www.glaucus.net/profile/update/createnewsletter.php" method="POST">
					<div class="col-md-4">
						<b>Created at:</b>
						<p><i><?php $date = date('Y-m-d H:i:s'); echo $date; ?></i></p>
					</div>
					<br>
					<br>
					<br>
					<div class="form-group">
						<label>Select Status</label>
						<br>
						<select class="form-control" name="status_sec" id="status">
							<option value="null" disabled>Select a option</option>
							<option value="0" selected>- Saved</option>
						</select>
					</div>
					<div class="form-group">
						<label>Subject</label>
						<br>
						<input type="text" class="form-control" name="subject" placeholder="Subject of this newsletter">
					</div>
					<div class="form-group">
						<label>Message</label>
						<br>
						<textarea name="message" id="message" cols="102" rows="50" style="resize: none;"></textarea>
						<script>
							CKEDITOR.replace( 'message' );
						</script>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4">
							<input type="submit" name="submit" class="btn btn-primary" value="SAVE NEWSLETTER">
						</div>
						<div class="col-md-8">
							<p>Save it for future use</p>
						</div>
						<br>
						<hr>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>