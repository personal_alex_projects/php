<?php session_start(); ?>
<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require ('/var/www/html/dynamic/functions/islogged.function.php');
require ('/var/www/html/dynamic/functions/profile.function.php');
require ('/var/www/html/dynamic/functions/newsletter.function.php');
if (!isLogged()) {
	header("Location: https://www.glaucus.net/login/");
}

$detail = obtenerDatos(obtenerGUID($_SESSION['LoginToken']));
foreach ($detail as $key) {
	$guid 		= $key['GUID'];
	$email 		= $key['Email'];
	$language 	= $key['Language'];
	$isAdmin 	= $key['isAdmin'];
}

if ($isAdmin == "0") {
	header("Location: https://www.glaucus.net/profile/");
}

/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/

switch ($language) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - Administration</title>
	<link rel="icon" href="https://cdn.glaucus.net/img/favicon.png" type="image/x-icon">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://www.glaucus.net/assets/styles/index.profile.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
	<style>
		.backwell{
			background-color: #0064F91A;
		}
		.backwellfamous{
			background-color: #F9B0001A;
		}
		hr{
			border-color: #33dce3b3;
		}
	</style>
</head>
<body style="background-color: rgba(121, 130, 37, 0.08);">
	<div class="container">
		<div class="margin-border">
			<div class="section-0">
				<center>
					<img class="img-responsive" src='https://cdn.glaucus.net/img/logo2.png' style='margin-top: 1em; width: 400px; margin-left: 0.1%;'><br>
				</center>
			</div>
			<div class="section-1">
				<h1>Newsletter Actions <small><a href="https://www.glaucus.net/profile/admin/newsletters/create">Create new newsletter</a></small></h1>
				<div class="well backwell">
					<div class="row">
						<div class="col-md-12">
							<h3>Notices List</h3>
							<table class="table table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Subject</th>
										<th>Creation Date</th>
										<th>Status</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									function statusRead($statusF){
										if ($statusF == 0) {
											return "<span class='label label-primary'>Saved...</span>";
										}elseif ($statusF == 1) {
											return "<span class='label label-warning'>Pending...</span>";
										}elseif ($statusF == 2) {
											return "<span class='label label-info'>Sended...</span>";
										}
									}
									$detail = getNewsletters();
									foreach ($detail as $key) {
										$id 		= $key['id'];
										$subject 	= $key['subject'];
										$date 		= $key['created_date'];
										$status     = $key['status'];

										echo "<tr>";
										echo "<td>".$id."</td>";
										echo "<td>".$subject."</td>";
										echo "<td>".$date."</td>";
										echo "<td>".statusRead($status)."</td>";
										echo "<td>"."<a class='btn btn-default' href='https://www.glaucus.net/profile/admin/newsletters/edit/?&id=$id&withStatus=$status&action=edit'>Edit</a> "."<a class='btn btn-danger' href='https://www.glaucus.net/profile/admin/newsletters/remove/?&id=$id&action=remove'>Remove</a>"."</td>";
										echo "</tr>";
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<h5>Help buttons</h5>
				<div class="btn-line">
					<button class="btn btn-default disabled">F.A.Q</button>
				</div>
			</div>
			<hr>
			<a href="https://www.glaucus.net/profile/leave.php" class="btn btn-lg btn-block btn-danger">Sign off</a>
			<a href="https://www.glaucus.net/profile/admin/" class="btn btn-lg btn-block btn-default">Back</a>
			<hr>
			<p>Version V2-271016</p>
		</div>
	</div>
</body>
</html>
<?php 
foreach (glob("/var/www/html/dynamic/modals/*.php") as $filename) {
	include $filename;
}
?>