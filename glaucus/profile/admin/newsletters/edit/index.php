<?php session_start(); ?>
<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require ('/var/www/html/dynamic/functions/islogged.function.php');
require ('/var/www/html/dynamic/functions/profile.function.php');
require ('/var/www/html/dynamic/functions/newsletter.function.php');
if (!isLogged()) {
	header("Location: https://www.glaucus.net/login/");
}

if (!$_REQUEST['id']) {
	header("Location: https://www.glaucus.net/profile/admin/newsletters/");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - Newsletter</title>
	<link rel="icon" href="https://cdn.glaucus.net/img/favicon.png" type="image/x-icon">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://www.glaucus.net/assets/styles/index.profile.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="//cdn.ckeditor.com/4.5.11/full/ckeditor.js"></script>
</head>
<body>
	<?php 
	$detail = getNewslettersByID($_REQUEST['id']);
	foreach ($detail as $key) {
		$id 		= $key['id'];
		$subject 	= $key['subject'];
		$date 		= $key['created_date'];
		$message    = $key['message'];
		$update 	= $key['update_date'];
		$status 	= $key['status'];

	}
	$_SESSION['newsID'] = $id;


	?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<hr>
				<div style="display: inline-block;">
					<h3>Actions</h3>
					<a class="btn btn-default" href="https://www.glaucus.net/profile/admin/newsletters/">Back</a>
					<a class="btn btn-danger" href="https://www.glaucus.net/profile/admin/newsletters/remove/?&id=$id&action=remove">Delete</a>
					<button class="btn btn-primary">Save & Publish</button>
				</div>
				<br>
				<hr>
				<form action="https://www.glaucus.net/profile/update/updatenewsletter.php" method="POST">
					<div class="col-md-6">
						<b>Created at:</b>
						<p><i><?php echo $date; ?></i></p>
					</div>
					<div class="col-md-6">
						<b>Last update:</b>
						<p><i><?php echo $update; ?></i></p>
					</div>
					<br>
					<br>
					<div class="form-group">
						<label>Newsletter ID</label>
						<br>
						<input type="text" class="form-control" name="id" disabled value="<?php echo $id; ?>">
					</div>
					<div class="form-group">
						<label>Select Status</label>
						<br>
						<select class="form-control" name="status_sec" id="status">
							<option value="null" disabled>Select a option</option>
							<option value="0" <?php if ($status==0) {echo "selected";} ?>>- Saved</option>
							<option value="1" <?php if ($status==1) {echo "selected";} ?>>- Pending of edition</option>
							<option value="2" <?php if ($status==2) {echo "selected";} ?>>- Sended</option>
						</select>
					</div>
					<div class="form-group">
						<label>Subject</label>
						<br>
						<input type="text" class="form-control" name="subject" placeholder="Subject of this newsletter" value="<?php echo $subject; ?>">
					</div>
					<div class="form-group">
						<label>Message</label>
						<br>
						<textarea name="message" id="message" cols="102" rows="50" style="resize: none;"><?php echo $message; ?></textarea>
						<script>
							CKEDITOR.replace( 'message' );
						</script>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4">
							<input type="submit" name="submit" class="btn btn-primary" value="SAVE NEWSLETTER">
						</div>
						<div class="col-md-8">
							<p>Save it for future use</p>
						</div>
						<br>
						<hr>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>