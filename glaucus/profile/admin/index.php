<?php session_start(); ?>
<?php 
require ('/var/www/html/dynamic/functions/islogged.function.php');
require ('/var/www/html/dynamic/functions/profile.function.php');
if (!isLogged()) {
	header("Location: https://www.glaucus.net/login/");
}

$detail = obtenerDatos(obtenerGUID($_SESSION['LoginToken']));
foreach ($detail as $key) {
	$guid 		= $key['GUID'];
	$email 		= $key['Email'];
	$language 	= $key['Language'];
	$isAdmin 	= $key['isAdmin'];
}

if ($isAdmin == "0") {
	header("Location: https://www.glaucus.net/profile/");
}

/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/

switch ($language) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - Administration</title>
	<link rel="stylesheet" href="https://www.glaucus.net/assets/styles/index.profile.css">
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style>
		.backwell{
			background-color: #0064F91A;
		}
		.backwellfamous{
			background-color: #F9B0001A;
		}
		hr{
			border-color: #33dce3b3;
		}
	</style>
</head>
<body style="background-color: rgba(121, 130, 37, 0.08);">
<div class="container">
	<div class="margin-border">
		<div class="section-0">
			<center>
				<img class="img-responsive" src='https://cdn.glaucus.net/img/logo2.png' style='margin-top: 1em; width: 400px; margin-left: 0.1%;'><br>
			</center>
		</div>
		<div class="section-1">
			<h1>Admin Actions</h1>
			<div class="well backwell">
				<div class="row">
					<div class="col-md-8">
						<div class="col-md-4">
							<h3>Mail Actions</h3>
							<p><button data-target="#sendmassmail" data-toggle="modal" class="btn btn-info">Create Massive Newsletter</button></p>
							<p><button data-toggle="modal" data-target="" class="btn btn-info">Send Global Newsletter</button></p>
							<p><a href="https://www.glaucus.net/profile/admin/newsletters/" class="btn btn-info">Newsletter List</a></p>
						</div>
						<div class="col-md-4">
						</div>
						<div class="col-md-4">
						</div>
					</div>
					<div class="col-md-4">
						<h3>Information</h3>
						<p>The <b>mass</b> mailing of newsletter only affects those who want to receive the news.</p>
						<p>The <b>global</b> newsletter sent to all users regardless of their condition of yes or not to receive the news.</p>
					</div>
				</div>
			</div>
			<h5>Help buttons</h5>
			<div class="btn-line">
				<button class="btn btn-default disabled">F.A.Q</button>
			</div>
		</div>
		<hr>
		<a href="https://www.glaucus.net/profile/leave.php" class="btn btn-lg btn-block btn-danger">Sign off</a>
		<hr>
		<p>Version V2-271016</p>
	</div>
</div>
</body>
</html>
<?php 
foreach (glob("/var/www/html/dynamic/modals/*.php") as $filename) {
	include $filename;
}
 ?>