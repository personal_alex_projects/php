<?php session_start(); ?>
<?php 
require ('/var/www/html/dynamic/functions/islogged.function.php');
require ('/var/www/html/dynamic/functions/profile.function.php');
if (!isLogged()) {
	header("Location: https://www.glaucus.net/login/");
}

$detail = obtenerDatos(obtenerGUID($_SESSION['LoginToken']));
foreach ($detail as $key) {
	$guid 		= $key['GUID'];
	$email 		= $key['Email'];
	$language 	= $key['Language'];
	$isAdmin 	= $key['isAdmin'];
}

if ($isAdmin == "0") {
	header("Location: https://www.glaucus.net/profile/");
}
?>
<head>
  <meta charset="UTF-8">
  <title>Glaucus Network - Perfil</title>
  <link rel="stylesheet" href="https://www.glaucus.net/assets/styles/index.profile.css">
  <link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
  <script src="https://code.jquery.com/jquery-latest.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="piechart.css" rel="stylesheet">
	<script src="piechart.js"></script>
  <style>
    .backwell{
    background-color: #0064F91A;
    }
    .backwellfamous{
    background-color: #F9B0001A;
    }
    hr{
    border-color: #33dce3b3;
    }
  </style>
  <style>.cke{visibility:hidden;}</style>
  <script type="text/javascript" src="https://cdn.ckeditor.com/4.5.11/full/config.js?t=G87E"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.ckeditor.com/4.5.11/full/skins/moono/editor.css?t=G87E">
  <script type="text/javascript" src="https://cdn.ckeditor.com/4.5.11/full/lang/es.js?t=G87E"></script><script type="text/javascript" src="https://cdn.ckeditor.com/4.5.11/full/styles.js?t=G87E"></script>
</head>
<body style="background-color: rgba(121, 130, 37, 0.0784314);" class="">
  <div class="container">
    <div class="margin-border">
      <div class="section-0">
        <center>
          <img class="img-responsive" src="https://cdn.glaucus.net/img/logo2.png" style="margin-top: 1em; width: 400px; margin-left: 0.1%;"><br>
        </center>
      </div>
      <div class="section-1">
        <h1>Welcome, <?php echo $email;?> to voting web!</h1>
        <div class="well backwell">
          <div class="row">
            <div class="col-md-3">
				<div id="content">
				<div class="framework-container">
				<div class="example"></div>
				</div>
				</div>
			</div>
			<div class="col-md-7">
			Here put description jeje
			</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

<script>
$(".example").piechart([
["Test", "Votes"],
["Close server", 1, 'red'],
["Open server", 10, 'green'],
["Wait a year", 1, 'yellow'],
["Yagami!", 1, 'pink'],
["Im retard!", 1, 'pink']
]);
</script>