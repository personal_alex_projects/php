<?php session_start(); ?>

<?php
require ('/var/www/html/dynamic/functions/islogged.function.php');

LogOut();

$cookie_error = "validation_successlogout";
setcookie("ErrorLogin", $cookie_error, time()+5, '/');
header("Refresh: 1, url=https://www.glaucus.net/login");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - Update</title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<link rel="stylesheet" href="../../assets/styles/index.reg.css">
</head>
<body>
<center>
	<div class="mensaje-error">
		<img src="https://cdn.glaucus.net/img/logo2.png" style="width: 400px; margin-right:15px;">	
	</div>
	<img src="https://cdn.glaucus.net/img/Loading1.gif" style="height: 200px;">	
</center>
<style>
	center {
		top: calc(50% - 250px);
		position: absolute;
		left: calc(50% - 207px);
	}
	body{
		background-color: #F5F5F5;
	}
	.ajust{
		margin-top:10em;
	}
</style>
</body>
</html>
