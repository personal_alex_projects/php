<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
require ('../../dynamic/functions/login.function.php');
require ('../../dynamic/functions/register.function.php');
require ('/var/www/html/dynamic/functions/islogged.function.php');
require_once ('/var/www/html/dynamic/recaptchalib.php');

/**
* Index.php -> VALIDATE REGISTER
*	Created by AlexBanPer
 Warning: PDOStatement::execute(): SQLSTATE[HY093]: Invalid parameter number: parameter was not defined in /var/www/html/dynamic/functions/register.function.php on line 9
**/

/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/
if (isset($_GET['lang'])) {
	$lang = $_GET['lang'];
}else{
	$lang = "en";
}

switch ($lang) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/

if (isLogged()) {
	header("Refresh: 0, url=https://www.glaucus.net/profile/");
}

/* COOKIE NAME */
$cookie_name = "LoginError";

// your secret key
$secret = "6Ld3KwgTAAAAANOJKGhCNcc7CiOeePW5f-ImMCow";
 
// empty response
$response = null;
 
// check secret key
$reCaptcha = new ReCaptcha($secret);
if ($_POST["g-recaptcha-response"]) {
    $response = $reCaptcha->verifyResponse(
        $_SERVER["REMOTE_ADDR"],
        $_POST["g-recaptcha-response"]
    );
}


	
if ($_POST['email'] && $_POST['password'] && $_POST['cpassword']) {
	$email = $_POST['email'];
	$password = $_POST['password'];
	$cpassword = $_POST['cpassword'];
	if ($response != null && $response->success) {
		$llamado = registerAccount($email, $password, $cpassword);
		if ($llamado == 1) {
			$cookie_error = "validation_fields";
			setcookie($cookie_name, $cookie_error, time()+5, '/');
			header("Refresh: 1, url=../");
		}elseif ($llamado == 2) {
			$cookie_error = "validation_minpassword";
			setcookie($cookie_name, $cookie_error, time()+5, '/');
			header("Refresh: 1, url=../");
		}elseif ($llamado == 3) {
			$cookie_error = "validation_cpassword";
			setcookie($cookie_name, $cookie_error, time()+5, '/');
			header("Refresh: 1, url=../");
		}elseif ($llamado == 4) {
			$cookie_error = "validation_alreadyemail";
			setcookie($cookie_name, $cookie_error, time()+5, '/');
			header("Refresh: 1, url=../");
		}elseif ($llamado == 5) {
			header("Refresh: 1, url=https://www.glaucus.net/profile/");
		}
	}else{
		$cookie_error = "validation_captchaFailed";
		setcookie($cookie_name, $cookie_error, time()+5, '/');
		header("Refresh: 1, url=../");
	}
}else{
	$cookie_error = "validation_fields";
	setcookie($cookie_name, $cookie_error, time()+5, '/');
	header("Refresh: 1, url=../");
}


//MENSAJE CORREO
/*
if ($_POST && $_POST['submit']) {
	if ($_POST['email']) {
		if ($_POST['password'] || $_POST['cpassowrd']) {
			if ($_POST['password'] == $_POST['cpassword']) {
				if (strlen($_POST['cpassword']) >= 6  ) {
					if (preg_match("/^[_a-z0-9-]+(.[_a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)(.[a-z]{2,3})$/", $_POST['email'])) {
						if (!($email == $dbEmail)) {
							if ($response != null && $response->success) {
    							register($email, $encpass, $salt, $guid);
								$_SESSION['GUID'] = $guid;
								$_SESSION['email'] = $email;
								$_SESSION['lang'] = "en";
								header("Refresh: 1, url=https://www.glaucus.net/profile/");
							}else{
								$cookie_error = "validation_captchaFailed";
								setcookie($cookie_name, $cookie_error, time()+5, '/');
								header("Refresh: 1, url=../");
							}
						}else{
							$cookie_error = "validation_alreadyemail";
							setcookie($cookie_name, $cookie_error, time()+5, '/');
							header("Refresh: 1, url=/../");
						}
					}else{
						$cookie_error = "validation_noemail";
						setcookie($cookie_name, $cookie_error, time()+5, '/');
						header("Refresh: 1, url=../");
					}
				}else{
					$cookie_error = "validation_minpassword";
					setcookie($cookie_name, $cookie_error, time()+5, '/');
					header("Refresh: 1, url=../");
				}
			}else{
				$cookie_error = "validation_cpassword";
				setcookie($cookie_name, $cookie_error, time()+5, '/');
				header("Refresh: 1, url=../");
			}
		}else{
			$cookie_error = "validation_password";
			setcookie($cookie_name, $cookie_error, time()+5, '/');
			header("Refresh: 1, url=../");
		}
	}else{
		$cookie_error = "validation_email";
		setcookie($cookie_name, $cookie_error, time()+5, '/');
		header("Refresh: 1, url=../");
	}
}else{
	$cookie_error = "validation_fields";
	setcookie($cookie_name, $cookie_error, time()+5, '/');
	header("Refresh: 1, url=../");
}
*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - <?php echo $lang['PAGE_REGTITLE']; ?></title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<link rel="stylesheet" href="../../assets/styles/index.reg.css">
</head>
<body>
<center>
	<div class="mensaje-error">
		<img src="https://cdn.glaucus.net/img/logo2.png" style="width: 400px; margin-right:15px;">	
	</div>
	<img src="https://cdn.glaucus.net/img/Loading1.gif" style="height: 200px;">	
</center>
<style>
	center {
		top: calc(50% - 250px);
		position: absolute;
		left: calc(50% - 207px);
	}
	body{
		background-color: #F5F5F5;
	}
	.ajust{
		margin-top:10em;
	}
</style>
</body>
</html>