<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
session_start();
require ('../dynamic/functions/islogged.function.php');

/**
* Index.php -> REGISTER GCN
*	Created by AlexBanPer
**/

if (isLogged() != false) {
	header("Refresh: 0, url=https://www.glaucus.net/profile/");
}

/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/
if (isset($_GET['lang'])) {
	$lang = $_GET['lang'];
}else{
	$lang = "en";
}

switch ($lang) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - <?php echo $lang['PAGE_REGISTER_TITLE']; ?></title>
	<link rel="stylesheet" href="https://www.glaucus.net/assets/styles/index.reg.css">
	<link rel="icon" href="<?php echo $lang['PAGE_FAVICO']; ?>" type="image/x-icon">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdn.glaucus.net/cookies/jq.c0okie.js"></script> 
	<link rel="stylesheet" type="text/css" href="https://cdn.glaucus.net/cookies/jq.c0okie.css"/>
	<script type="text/javascript">
		$(document).ready(function(){
			$.c0okieBar({
			});
		});
	</script>
</head>
<body>
	<center>
		<div class="panel-principal">
			<div class="panel-login">
				<div class="mensaje-error">
					<?php
					echo "<img src='https://cdn.glaucus.net/img/logo2.png' style='width: 400px; margin-right:15px;'>";
					if (isset($_COOKIE['LoginError'])) {
						switch ($_COOKIE['LoginError']) {
							case 'validation_fields':
							echo '<div class="error">'.$lang['validation_fields'].'</div>';
							break;
							case 'validation_email':
							echo '<div class="error">'.$lang['validation_email'].'</div>';
							break;
							case 'validation_password':
							echo '<div class="error">'.$lang['validation_password'].'</div>';
							break;
							case 'validation_minpassword':
							echo '<div class="error">'.$lang['validation_minpassword'].'</div>';
							break;
							case 'validation_cpassword':
							echo '<div class="error">'.$lang['validation_cpassword'].'</div>';
							break;
							case 'validation_noemail':
							echo '<div class="error">'.$lang['validation_noemail'].'</div>';
							break;
							case 'validation_alreadyemail':
							echo '<div class="error">'.$lang['validation_alreadyemail'].'</div>';
							break;
							case 'validation_captchaFailed':
							echo '<div class="error">'.$lang['validation_captchaFailed'].'</div>';
							break;
							default:
							echo '<div class="error">'.$lang['validation_default'].'</div>';
							break;
						}
					}
					?>
				</div>
				<form action="validate/" method="post">
					<div class="form-style-6">
						<h1><?php echo $lang['REG_TITLE']; ?><br><?php echo " <a class='aLang' href='".$lang['REG_LOGINLINK']."'>".$lang['REG_LOGIN']."</a>"; ?></h1>
						<div class="texto-email">
							<span><?php echo $lang['REG_EMAIL']; ?></span><br>
						</div>
						<input type="text" name="email" placeholder="<?php echo $lang['REG_EMAILPLACEHOLDER']; ?>">
						<div class="texto-pass">
							<span><?php echo $lang['REG_PASSWORD']; ?></span><br>
						</div>
						<input minlength="8" type="password" name="password" placeholder="<?php echo $lang['REG_PASSPLACEHOLDER']; ?>">
						<div class="texto-pass">
							<span><?php echo $lang['REG_REPEATPASS']; ?></span><br>
						</div>
						<input minlength="8" type="password" name="cpassword" placeholder="<?php echo $lang['REG_REPEATPASSPLACEHOLDER']; ?>">
						<div class="re-captcha">
							<div class="g-recaptcha" data-sitekey="6Ld3KwgTAAAAALgrPERqbkRODiV0Xf4IVYWQ_F1D"></div>
						</div>
						<br>
						<input type="submit" name="submit" value="<?php echo $lang['REG_STARTSESSION']; ?>">
						<!-- <input type="submit" name="register" value="%REG_LOGIN%"> !-->
						<?php echo "<p style='font-size: 10px;'>".$lang['PAGE_DEVVER_TITLE']."</p>"; ?>
					</div>
				</form>
				<script src='https://www.google.com/recaptcha/api.js'></script>
			</div>
		</div>
	</center>	
</body>
</html>