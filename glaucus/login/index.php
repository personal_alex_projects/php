<?php 
session_start();
require ('/var/www/html/dynamic/functions/islogged.function.php');
require_once ('/var/www/html/dynamic/recaptchalib.php');

/**
* Index.php -> REGISTER GCN
*	Created by AlexBanPer
**/

/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/
if (isset($_GET['lang'])) {
	$lang = $_GET['lang'];
}else{
	$lang = "en";
}

switch ($lang) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/

if (isLogged() != false) {
	header("Refresh: 0, url=https://www.glaucus.net/profile/");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - <?php echo $lang['PAGE_LOGIN_TITLE']; ?></title>
	<link rel="stylesheet" href="../assets/styles/index.reg.css">
	<link rel="icon" href="<?php echo $lang['PAGE_FAVICO']; ?>" type="image/x-icon">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdn.glaucus.net/cookies/jq.c0okie.js"></script> 
	<link rel="stylesheet" type="text/css" href="https://cdn.glaucus.net/cookies/jq.c0okie.css"/>
	<script type="text/javascript">
		$(document).ready(function(){
			$.c0okieBar({
			});
		});
	</script>
</head>
<body>
	<center>
		<div class="panel-principal">
			<div class="panel-login">
				<div class="mensaje-error">
					<?php
					echo "<img src='https://cdn.glaucus.net/img/logo2.png' style='width: 400px; margin-right:15px;'>"; 
					if (isset($_COOKIE['ErrorLogin'])) {
						switch ($_COOKIE['ErrorLogin']) {
							case 'validation_fields':
							echo '<div class="error">'.$lang['validation_fields'].'</div>';
							break;
							case 'validation_email':
							echo '<div class="error">'.$lang['validation_email'].'</div>';
							break;
							case 'validation_password':
							echo '<div class="error">'.$lang['validation_lpassword'].'</div>';
							break;
							case 'validation_noemail':
							echo '<div class="error">'.$lang['validation_noemail'].'</div>';
							break;
							case 'validation_loginerroremail':
							echo '<div class="error">'.$lang['validation_loginerroremail'].'</div>';
							break;
							case 'validation_loginerrorpassword':
							echo '<div class="error">'.$lang['validation_loginerrorpassword'].'</div>';
							break;
							case 'validation_captchaFailed':
							echo '<div class="error">'.$lang['validation_captchaFailed'].'</div>';
							break;
							case 'validation_account':
							echo '<div class="error">'.$lang['validation_account'].'</div>';
							break;
							case 'validation_successlogout':
							echo '<div class="info">'.$lang['validation_successlogout'].'</div>';
							break;
							default:
							echo '<div class="error">'.$lang['validation_default'].'</div>';
							break;
						}
					}
					?>
				</div>
				<form action="validate/" method="post">
					<div class="form-style-6">
						<h1><?php echo $lang['LOGIN_TITLE']; ?><br><?php echo " <a class='aLang' href='".$lang['LOGIN_REGLINK']."'>".$lang['LOGIN_REG']."</a>"; ?></h1>
						<div class="texto-email">
							<span><?php echo $lang['LOGIN_EMAIL']; ?></span><br>
						</div>
						<input type="text" name="email" placeholder="<?php echo $lang['LOGIN_EMAILPLACEHOLDER']; ?>">
						<div class="texto-pass">
							<span><?php echo $lang['LOGIN_PASSWORD']; ?></span><br>
						</div>
						<input type="password" name="password" placeholder="<?php echo $lang['LOGIN_PASSPLACEHOLDER']; ?>">
						<div class="re-captcha">
							<div class="g-recaptcha" data-sitekey="6Ld3KwgTAAAAALgrPERqbkRODiV0Xf4IVYWQ_F1D"></div>
						</div>
						<br>
						<input type="submit" name="submit" value="<?php echo $lang['LOGIN_STARTSESSION']; ?>">
						<!-- <input type="submit" name="register" value="%LOGIN_LOGIN%"> !-->
						<?php echo "<p style='font-size: 10px;'>".$lang['PAGE_DEVINFO']."</p>"; ?>
					</div>
				</form>
				<script src='https://www.google.com/recaptcha/api.js'></script>
			</div>
		</div>
	</center>	
</body>
</html>