<?php 
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require ('/var/www/html/dynamic/functions/islogged.function.php');
require ('/var/www/html/dynamic/functions/login.function.php');
require_once ('/var/www/html/dynamic/recaptchalib.php');


/* Cookie Name */
$cookie_name = "ErrorLogin";

/*
* Lenguajes, configuracion by AlexBanPer.
*  Traducciones por Glaucus Network. (EN, ES)
*/
if (isset($_GET['lang'])) {
	$lang = $_GET['lang'];
}else{
	$lang = "en";
}

switch ($lang) {
	case 'es':
	$lang_file = "/var/www/html/dynamic/language/lang.ES.php";
	break;
	case 'en':
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
	default:
	$lang_file = "/var/www/html/dynamic/language/lang.EN.php";
	break;
}

include_once $lang_file;
/*
* ========== FIN LENGUAJES ==========
*/


if (isLogged() != false) {
	header("Refresh: 0, url=https://www.glaucus.net/profile/");
}

/* CAPTCHA */
// your secret key
$secret = "6Ld3KwgTAAAAANOJKGhCNcc7CiOeePW5f-ImMCow";

// empty response
$response = null;

// check secret key
$reCaptcha = new ReCaptcha($secret);
if ($_POST["g-recaptcha-response"]) {
	$response = $reCaptcha->verifyResponse(
		$_SERVER["REMOTE_ADDR"],
		$_POST["g-recaptcha-response"]
		);
}
/*****/
if (!isset($_POST['password']) && !isset($_POST['email'])) {
	$cookie_error = "validation_fields";
	setcookie($cookie_name, $cookie_error, time()+5, '/');
	header("Refresh: 1, url=../");
}else{
	if ($response != null && $response->success) {
		$llamado = TryLogin($_POST['email'], $_POST['password']);
		if ($llamado == 1) {
			$cookie_error = "validation_account";
			setcookie($cookie_name, $cookie_error, time()+5, '/');
			header("Refresh: 1, url=../");
		}elseif ($llamado == 4) {
			$cookie_error = "validation_password";
			setcookie($cookie_name, $cookie_error, time()+5, '/');
			header("Refresh: 1, url=../");
		}elseif ($llamado == 5) {
			header("Refresh: 1, url=https://www.glaucus.net/profile/");
		}
	}else{
		$cookie_error = "validation_captchaFailed";
		setcookie($cookie_name, $cookie_error, time()+5, '/');
		header("Refresh: 1, url=../");
	}
}




/*if ($_POST or isset($_POST['submit'])) {
	if ($_POST['email']) {
		if ($_POST['password']) {
			if ($llamado == 1) {
				if ($llamado == 4) {
					echo "Los datos coinciden!";
				}else{
					$cookie_error = "validation_loginerrorpassword";
					setcookie($cookie_name, $cookie_error, time()+5, '/');
					header("Refresh: 1, url=../");
				}
			}else{
				$cookie_error = "validation_loginerroremail";
				setcookie($cookie_name, $cookie_error, time()+5, '/');
				header("Refresh: 1, url=../");
			}
		}else{
			$cookie_error = "validation_password";
			setcookie($cookie_name, $cookie_error, time()+5, '/');
			header("Refresh: 1, url=../");
		}
	}else{
		$cookie_error = "validation_email";
		setcookie($cookie_name, $cookie_error, time()+5, '/');
		header("Refresh: 1, url=../");
	}
}else{
	$cookie_error = "validation_fields";
	setcookie($cookie_name, $cookie_error, time()+5, '/');
	header("Refresh: 1, url=../");
}
*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Glaucus Network - <?php echo $lang['PAGE_LOGINTITLE']; ?></title>
	<link rel="stylesheet" href="https://cdn.glaucus.net/glaucus_main/css/boot.css">
	<link rel="stylesheet" href="../../assets/styles/index.reg.css">
</head>
<body>
<center>
	<div class="mensaje-error">
		<img src="https://cdn.glaucus.net/img/logo2.png" style="width: 400px; margin-right:15px;">	
	</div>
	<img src="https://cdn.glaucus.net/img/Loading1.gif" style="height: 200px;">
</center>
<style>
	center {
		top: calc(50% - 250px);
		position: absolute;
		left: calc(50% - 207px);
	}
	body{
		background-color: #F5F5F5;
	}
	.ajust{
		margin-top:10em;
	}
</style>
</body>
</html>